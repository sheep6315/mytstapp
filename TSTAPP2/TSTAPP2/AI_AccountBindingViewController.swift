//
//  AI_AccountBindingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/20.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class AI_AccountBindingViewController: TSUIViewController {
    
    @IBOutlet weak var logoImageTopGap: NSLayoutConstraint!
    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    var hideBackButton: Bool = false
    
    var loginDataDic = Dictionary<String, Any>()
    var pwschange = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        if hideBackButton {
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
        
        //美工
        accountTextField.attributedPlaceholder = NSAttributedString(string: "請輸入門號", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "請輸入門號密碼", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        logoImageTopGap.constant = 80
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
//        let backItem = UIBarButtonItem(title: "Title",
//                                       style: UIBarButtonItemStyle.plain,
//                                       target: nil,
//                                       action: nil)
//        backItem.setBackgroundImage(UIImage(named: "bg"), for: .normal, barMetrics: .default)
//
//        let leftItem = UIBarButtonItem(title: "Title",
//                                       style: UIBarButtonItemStyle.plain,
//                                       target: nil,
//                                       action: nil)
//        self.navigationItem.leftBarButtonItem = leftItem
//        self.navigationItem.leftBarButtonItems = [leftItem,backItem]
        self.navigationItem.setHidesBackButton(false, animated: true)
        
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_BIND", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "連結門號登入")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if LoginData.isLogin() {
            let _ = self.navigationController?.popViewController(animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.tabBarController?.tabBar.isTranslucent = false
        self.tabBarController?.tabBar.isHidden = false
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_BIND", action: "")
        
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
        if hideBackButton {
//            let avc = AutomaticViewChange.sharedInstance
//            avc.changeToTab(0)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toAIChangePassword" {
            let aiChangePasswordVC = segue.destination as! AI_ChangePasswordViewController
            aiChangePasswordVC.loginDataDic = loginDataDic;
            aiChangePasswordVC.pwschangeStatus = pwschange
            aiChangePasswordVC.password = passwordTextField.text!
        }
        
    }
    
    
//    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: false, notification: notification)
    }
    
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        self.scrollView.endEditing(true)
        
    }
    
//    MARK: - IBAction
    @IBAction func didTapBindingButton(_ sender: Any) {
        
        let extInfoDic = UserDefaults.standard.dictionary(forKey: FACEBOOK_LOGIN_DATA)
        
        if extInfoDic != nil {
            
            let id = extInfoDic!.stringValue(ofKey: "id")
            let token = extInfoDic!.stringValue(ofKey: "token")
            let email = extInfoDic!.stringValue(ofKey: "email")
            let name = extInfoDic!.stringValue(ofKey: "name")
            let birthday = extInfoDic!.stringValue(ofKey: "birthday")
            let gender = extInfoDic!.stringValue(ofKey: "gender")
            
            let msisdn = accountTextField.text!
            let password = passwordTextField.text!
            
            if accountTextField.text != "" && passwordTextField.text != "" {
                
                TSTAPP2_API.sharedInstance.bindExt(withMSISDN: msisdn, extId: id, extType: "facebook", extToken: token, password: password, email: email, name: name, birthday: birthday, gender: gender, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        
                        UbaRecord.sharedInstance.sendUba(withPage: "AP_BIND", action: "AA_BIND")
                        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "使用FB登入", action: "連結門號", label: "連結門號登入Button", value: nil)
                        
                        UserDefaults.standard.set(self.passwordTextField.text, forKey: ACCOUNT_PASSWORD)
                        UserDefaults.standard.synchronize()
                        
                        let apiData = dic["data"] as! Dictionary<String, Any>
                        let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
                        self.pwschange = custProfile.stringValue(ofKey: "pwschange")

                        
                        let loginData = LoginData.sharedLoginData()
                        
                        if self.pwschange != "1" {
                            self.loginDataDic = dic;
                            self.performSegue(withIdentifier: "toAIChangePassword", sender: nil)
                        }
                        else {
                            loginData.setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                            self.hideBackButton = false
                            let _ = self.navigationController?.popViewController(animated: true)
                        }
                        
//                        let deviceID = UIDevice.current.identifierForVendor!.uuidString
//                        TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: "", showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//                            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//                            UIApplication.shared.registerUserNotificationSettings(settings)
//                            UIApplication.shared.registerForRemoteNotifications()
//                        }, failure: { (error: Error) in
//
//                        })
                        
                        TSTAPP2_Utilities.registerDevice()
                    }
                    else {
                        TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                    }
                }, failure: { (error: Error) in
                    
                })
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: "帳號或密碼不能為空白", viewController: self)
            }
            
        }
    }
    
    @IBAction func didTapPrePaidButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_BIND", action: "AA_PREPAID_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "使用FB登入", action: "連結門號", label: "預付卡門號請由此登入", value: nil)
        
        let urlString: String = "https://www.tstartel.com/mCWS/prepaidCardLogin.php?attest_to=prepaidCard&service_code=PAYMENT_PREPAID"
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
        
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @IBAction func didTapApplyPassword(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "AP_BIND", action: "A010201")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "使用FB登入", action: "連結門號", label: "申請/忘記密碼", value: nil)
    }
    
    @IBAction func didTapAQButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "AP_BIND", action: "AA_EC_APPLY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "使用FB登入", action: "連結門號", label: "門號申辦", value: nil)
        
        #if SIT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #elseif UAT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #else
            let urlString: String = "https://www.tstartel.com/mCWS/shop.php"
        #endif
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
     class func didTapBackButton() {
//        self.tabBarController?.selectedIndex = 0
        let avc = AutomaticViewChange.sharedInstance
        avc.changeToTab(0)
    }

}
