//
//  AI_AccountLoginViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/8/30.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class AI_AccountLoginViewController: TSUIViewController {
    
    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var myNavigationBar: UIView!
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backArrowIcon: UIButton!
    @IBOutlet weak var myNavigationTopGap: NSLayoutConstraint!
    
    var hideBackButton: Bool = false
    
    var loginDataDic = Dictionary<String, Any>()
    var extInfoDic: Dictionary<String, Any>?
    var pwschange = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        navigationTitle.text = "門號登入"
        accountTextField.attributedPlaceholder = NSAttributedString(string: "請輸入門號", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "請輸入門號密碼", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        if hideBackButton {
            self.navigationItem.setHidesBackButton(true, animated: false)
            myNavigationBar.isHidden = true
        }
        facebookButton.layer.borderColor = UIColor.white.cgColor
        facebookButton.layer.borderWidth = 1
        changeMynavigationBarTopGap()
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeMynavigationBarTopGap), name: .UIApplicationWillChangeStatusBarFrame, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isTranslucent = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_LOGIN", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入系統", action: "門號登入", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "門號登入")
        
        if LoginData.isLogin() {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            accountTextField.text = ""
            passwordTextField.text = ""
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if LoginData.isLogin() {
            let _ = self.navigationController?.popViewController(animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isTranslucent = false
        self.tabBarController?.tabBar.isHidden = false
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_LOGIN", action: "")
        self.navigationController?.navigationBar.isHidden = false
        
        //下一個頁面需要設定的navigation樣式，必須在上一頁就完成設定
        navigationItem.setHidesBackButton(false, animated: true)
        let backItem = UIBarButtonItem()
        backItem.title = "門號登入"
        navigationItem.backBarButtonItem = backItem
        
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toChangePassword" {
            let aiChangePasswordVC = segue.destination as! AI_ChangePasswordViewController
            aiChangePasswordVC.loginDataDic = loginDataDic;
            aiChangePasswordVC.pwschangeStatus = pwschange
            aiChangePasswordVC.password = passwordTextField.text!
        }
        else if segue.identifier == "toUpdatePassword" {
            let updatePasswordVC = segue.destination as! UpdatePasswordViewController
            let apiData = loginDataDic["data"] as! Dictionary<String, Any>
            let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
            let msisdn = custProfile.stringValue(ofKey: "msisdn")
            updatePasswordVC.msisdn = msisdn
            updatePasswordVC.hideBackButton = true
        }
        
    }
    
    
    //    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: false, notification: notification)
    }
    
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        self.scrollView.endEditing(true)
        
    }
    
    //    MARK: - IBAction
    @IBAction func didTapBackArrow(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        
        if accountTextField.text != "" && passwordTextField.text != "" {
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "門號登入_button_登入", label: "", value: nil)
            
            TSTAPP2_API.sharedInstance.login(withMSISDN: accountTextField.text!, password: passwordTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    
                    UbaRecord.sharedInstance.sendUba(withPage: "AP_LOGIN", action: "AA_POSTPAID_LOGIN")
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "執行登入", action: "成功", label: nil, value: nil)
                    
                    self.loginDataDic = dic
                    
                    UserDefaults.standard.removeObject(forKey: API_CACHED_DATA)
                    
                    UserDefaults.standard.set(self.passwordTextField.text, forKey: ACCOUNT_PASSWORD)
                    UserDefaults.standard.synchronize()
                    
                    let apiData = dic["data"] as! Dictionary<String, Any>
                    let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
                    self.pwschange = custProfile.stringValue(ofKey: "pwschange")
                    if self.pwschange == "0" {
                        
                        self.performSegue(withIdentifier: "toChangePassword", sender: nil)
                    }
                    else if self.pwschange == "1" {
                        LoginData.sharedLoginData().setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                        
                        TSTAPP2_Utilities.registerDevice()
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    else if self.pwschange == "2" {
                        self.performSegue(withIdentifier: "toChangePassword", sender: nil)

                    }
                    else {
                        TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
                    }
                    
                }
                else {
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "執行登入", action: "失敗", label: nil, value: nil)
                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
                
            }, failure: { (error: Error) in
                let err = error as NSError
                if err.code == -1009 {
                    TSTAPP2_Utilities.showAlert(withMessage: "請確認網路狀態", viewController: self)
                }
                else if err.code == -1001 {
                    TSTAPP2_Utilities.showAlert(withMessage: "請再次登入", viewController: self)
                }
                else {
                    TSTAPP2_Utilities.showAlert(withMessage: "請再次登入", viewController: self)
                }
            })
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "帳號或密碼不能為空白", viewController: self)
        }
    }
    
    @IBAction func didTapPrePaidButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_LOGIN", action: "AA_PREPAID_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "預付卡門號請由此登入", label: "", value: nil)
        
//        let urlString: String = "https://www.tstartel.com/mCWS/prepaidCardLogin.php?attest_to=prepaidCard&service_code=PAYMENT_PREPAID"
        #if UAT
            let urlString: String = "http://uatsso.tstartel.com/mc-ws/MC/MCLogin.action?sid=tstapp&ru=http://uatsso.tstartel.com/MyTstar/MyData.action&cid=001,009,025,026"
        #elseif SIT
            let urlString: String = "http://uatsso.tstartel.com/mc-ws/MC/MCLogin.action?sid=tstapp&ru=http://uatsso.tstartel.com/MyTstar/MyData.action&cid=001,009,025,026"
        #elseif DEV
            let urlString: String = "http://uatsso.tstartel.com/mc-ws/MC/MCLogin.action?sid=tstapp&ru=http://uatsso.tstartel.com/MyTstar/MyData.action&cid=001,009,025,026"
        #else
            let urlString: String = "http://sso.tstartel.com/mc-ws/MC/MCLogin.action?sid=tstapp&ru=http://sso.tstartel.com/MyTstar/MyData.action&cid=001,009,025,026"
        #endif

        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
        
        //        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @IBAction func didTapApplyPassword(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_LOGIN", action: "A010201")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "申請/忘記密碼", label: "", value: nil)
    }
    
    @IBAction func didTapFbLoginButton(_ sender: AnyObject) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_LOGIN", action: "AA_FB_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "門號登入_button_使用FB登入", label: "", value: nil)
        
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result: FBSDKLoginManagerLoginResult?, error: Error?) in
            if error != nil {
                print("Process error")
                let description = (error! as NSError).userInfo["FBSDKErrorLocalizedDescriptionKey"]
                print("Process error \(String(describing: description))")
            }
            else if result!.isCancelled {
                print("Cancelled")
            }
            else {
                print("Logged in")
                
                self.requestGraph()
                
            }
        }
    }
    
    @IBAction func didTapAQButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "AP_LOGIN", action: "AA_EC_APPLY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "門號申辦", label: "", value: nil)
        
        #if SIT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #elseif UAT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #else
            let urlString: String = "https://www.tstartel.com/mCWS/shop.php"
        #endif
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    class func didTapBackButton() {
        //        self.tabBarController?.selectedIndex = 0
        let avc = AutomaticViewChange.sharedInstance
        avc.changeToTab(0)
    }
    
    //MARK: - custom func
    @objc func changeMynavigationBarTopGap() {
        if UIApplication.shared.statusBarFrame.height > 20 {
            self.myNavigationTopGap.constant = UIApplication.shared.statusBarFrame.height - 20
        }
    }
    
//    MARK: - FB Login
    func requestGraph() {
        TSTAPP2_Utilities.showPKHUD()
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, birthday, gender", "locale": "zh_TW"]).start(completionHandler: { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if result != nil {
                var resultDic = result as! Dictionary<String, String>
                print("FBSDKGraphRequest\n\(String(describing: resultDic["id"])) \(String(describing: resultDic["name"])) \(String(describing: resultDic["email"]))")
                
                resultDic["token"] = FBSDKAccessToken.current().tokenString
                self.extInfoDic = resultDic
                UserDefaults.standard.set(resultDic, forKey: FACEBOOK_LOGIN_DATA)
                UserDefaults.standard.synchronize()
                
                let id = resultDic.stringValue(ofKey: "id")
                let token = resultDic.stringValue(ofKey: "token")
                let email = resultDic.stringValue(ofKey: "email")
                let name = resultDic.stringValue(ofKey: "name")
                let birthday = resultDic.stringValue(ofKey: "birthday")
                let gender = resultDic.stringValue(ofKey: "gender")
                
                TSTAPP2_API.sharedInstance.loginExt(withExtId: id, extType: "facebook", extToken: token, email: email, name: name, birthday: birthday, gender: gender, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        
                        self.loginDataDic = dic
                        
                        if dic["data"] == nil {
                            
                            let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
                            let accountBingindVC: AI_AccountBindingViewController = storyBoard.instantiateViewController(withIdentifier: "AI_AccountBindingViewController") as! AI_AccountBindingViewController
                            accountBingindVC.hideBackButton = false
                            self.navigationController?.pushViewController(accountBingindVC, animated: true)
                        }
                        else {
                            let apiData = dic["data"] as! Dictionary<String, Any>
                            let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
                            self.pwschange = custProfile.stringValue(ofKey: "pwschange")
                            if self.pwschange == "0" {
                                
                                self.performSegue(withIdentifier: "toChangePassword", sender: nil)
                            }
                            else if self.pwschange == "1" {
                                
                                LoginData.sharedLoginData().setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                                TSTAPP2_Utilities.registerDevice()
                                self.navigationController?.popViewController(animated: true)
                            }
                            else if self.pwschange == "2" {
                                self.performSegue(withIdentifier: "toChangePassword", sender: nil)
                            }
                            else {
                                TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
                            }
                            
                        }
                    }
                    else {
                        FBSDKLoginManager().logOut()
                        if dic.stringValue(ofKey: "message") == "" {
                            TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
                        }
                        else {
                            TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                        }
                    }
                }, failure: { (error: Error) in
                    FBSDKLoginManager().logOut()
                    TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
                })
            }
            else {
                TSTAPP2_Utilities.hidePKHUD()
            }
        })
    }
    
}
