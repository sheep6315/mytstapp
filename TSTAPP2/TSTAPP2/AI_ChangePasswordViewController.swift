//
//  AI_ChangePasswordViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/20.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class AI_ChangePasswordViewController: TSUIViewController {
    
    var loginDataDic = Dictionary<String, Any>()
    
    
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var oldPasswordViewHeight: NSLayoutConstraint!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rePasswordTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var captchaField: UITextField!
    @IBOutlet weak var captchaView: UIView!
    @IBOutlet weak var captchaImageView: UIImageView!
    @IBOutlet weak var captchaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var submitButton: UIButton!
    
    var pwschangeStatus = ""
    var password = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)

        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let captchaImg = Captcha.sharedInstance
        captchaImg.captchaSize = CGSize(width: 120, height: 44)
        self.captchaImageView.image = captchaImg.captchaImage.image
        let tapCaptchaGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.didTapRefreshCaptcha))
        self.captchaImageView.addGestureRecognizer(tapCaptchaGestureRecognizer)
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        if pwschangeStatus == "2" {
            self.title = "變更會員密碼"
            captchaView.isHidden = false;
            captchaViewHeight.constant = 44
        }
        else if pwschangeStatus == "0" {
            captchaView.isHidden = true;
            captchaViewHeight.constant = 0
        }
        
        if password != "" {
            oldPasswordView.isHidden = true
            oldPasswordViewHeight.constant = 0
        }
        else {
            
        }
        
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkVIPStatusAndChangeSkin()
        
        if pwschangeStatus == "2" {
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_FORCE_CH_PW", action: "")
        }
        else if pwschangeStatus == "0" {
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP010202", action: "")
//            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入系統", action: "首次登入", label: nil, value: nil)
            TSTAPP2_Utilities.sendGAScreen(withScreenName: "首次登入")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
        
        if pwschangeStatus == "2" {
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_FORCE_CH_PW", action: "")
        }
        else if pwschangeStatus == "0" {
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP010202", action: "")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        let apiData = loginDataDic["data"] as! Dictionary<String, Any>
        let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
        let isVIP = custProfile.stringValue(ofKey: "isVIP")
        if isVIP == "Y" {
            self.navigationController?.navigationBar.barTintColor = goldColor
            submitButton.setBackgroundImage(UIImage(named: "button_normal_gold"), for: UIControlState.normal)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: false, notification: notification)
    }
    
    //MARK: - textFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    //    MARK: - IBAction
    @objc func didTapRefreshCaptcha() {
        let captchaImg = Captcha.sharedInstance
        self.captchaImageView.image = captchaImg.refresh().image
    }
    
    @IBAction func didTapSubmitButton(_ sender: AnyObject) {
        
        if pwschangeStatus == "2" {
            if self.captchaField.text != Captcha.sharedInstance.captchaString {
                TSTAPP2_Utilities.showAlert(withMessage: "驗證碼輸入錯誤", viewController: self)
                return
            }
        }
        
        let regex = try! NSRegularExpression(pattern: ".*[^0-9a-zA-Z].*", options: .caseInsensitive)
        
        if passwordTextField.text != "" && rePasswordTextField.text != "" {
            
            let pwText = passwordTextField.text!
            var validPW = true
            
            let range: NSRange = NSMakeRange(0, pwText.utf16.count)
            let matchArray: Array = regex.matches(in: pwText, options: NSRegularExpression.MatchingOptions.anchored, range: range)
            if matchArray.count > 0 {
                validPW = false
            }
            
            if password == "" {
                password = oldPasswordTextField.text!
            }
            
            if password == "" {
                TSTAPP2_Utilities.showAlert(withMessage: "請輸入舊密碼", viewController: self)
            }
            else if passwordTextField.text!.count < 6 || passwordTextField.text!.count > 12 || !validPW {
                TSTAPP2_Utilities.showAlert(withMessage: "密碼格式錯誤，必須為6-12碼英數字", viewController: self)
            }
            else if passwordTextField.text != rePasswordTextField.text {
                TSTAPP2_Utilities.showAlert(withMessage: "新密碼輸入不一致", viewController: self)
            }
            else {
//                let loginData = LoginData.sharedLoginData()
//                let password = UserDefaults.standard.string(forKey: ACCOUNT_PASSWORD)!
                
                let apiData = loginDataDic["data"] as! Dictionary<String, Any>
                let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
                let msisdn = custProfile.stringValue(ofKey: "msisdn")
                
                TSTAPP2_API.sharedInstance.setPassword(withMSISDN: msisdn, oldPassword: password, newPassword: passwordTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        
                        if self.pwschangeStatus == "2" {
                            UbaRecord.sharedInstance.sendUba(withPage: "AP_FORCE_CH_PW", action: "AA_FORCE_CH_PW")
                            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "忘記密碼_變更密碼", label: "修改密碼Button", value: nil)
                        }
                        else if self.pwschangeStatus == "0" {
                            UbaRecord.sharedInstance.sendUba(withPage: "APP010202", action: "A010202")
                            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入", action: "首次登入", label: "修改密碼Button", value: nil)
                        }
                        
                        LoginData.sharedLoginData().setDataValue(withDic: self.loginDataDic, onlyUpdateCustProfile: false)
                        
//                        TSTAPP2_Utilities.registerDevice()
                        
                        self.tabBarController?.tabBar.isHidden = false
                        
//                        let VCs = self.navigationController?.viewControllers
//                        let _ = self.navigationController?.popToViewController(VCs![VCs!.count - 3], animated: true)
                        let action = UIAlertAction(title: "重新登入", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
                            
                            TSTAPP2_Utilities.logout(completion: {
//                                self.performSegue(withIdentifier: "toMain", sender: nil)
                                
                                if let viewCount = self.navigationController?.viewControllers.count {
                                    let vc = self.navigationController?.viewControllers[viewCount - 2]
                                    self.navigationController?.popToViewController(vc!, animated: true)
                                }
                            }, failure: {
                                
                            })
                        })
                        TSTAPP2_Utilities.showAlert(withMessage: "密碼已修改完成，下次請使用新密碼登入。", action: action, viewController: self)
                    }
                    else {
                        TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                    }
                }, failure: { (error: Error) in
                    
                })
            }
            
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "密碼格式錯誤，必須為6-12碼英數字", viewController: self)
        }
        
        
    }
    
//    MARK: -
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        
    }

}
