//
//  AccountInformationTableViewController.swift
//  TSTAPP2
//
//  Created by rd22 on 2016/10/19.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Crashlytics

let KEEP_LOGIN = "KEEP_LOGIN"

var AccountInformation_CallGetBillTypeInfo = false

class AccountInformationTableViewController: UITableViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var isFacebookLoggedIn: Bool = false
    var isAccountLoggedIn: Bool = false
    var displayMode: Int = 0
    
    var isGetCustProfile = false
    var isGetBillTypeInfo = false
    var ebillType: EnumEbillType = EnumEbillType.NoData
    
    var loginData = LoginData.sharedLoginData()
    
    var shouldReloadAllAPI = true
    
    @IBOutlet var facebookLoginButton: UIButton!
    
    @IBOutlet var loginInfoLabel: UILabel!
    @IBOutlet var accountLoginButton: UIButton!
    @IBOutlet var loginCell: UITableViewCell!
    
    @IBOutlet weak var accountPhotoImageView: UIImageView!
    @IBOutlet weak var changePhotoButton: UIButton!
    @IBOutlet weak var displayNickNameLabel: UILabel!
    @IBOutlet weak var memberTypeLabel: UILabel!
    @IBOutlet weak var msisdnLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var extAccountLabel: UILabel!
    @IBOutlet weak var extAccountBindingButton: UIButton!
    
    @IBOutlet weak var vipStartDateLabel: UILabel!
    @IBOutlet weak var vipEndDateLabel: UILabel!
    @IBOutlet weak var billStatusLabel: UILabel!
    
    @IBOutlet weak var billTypeButton: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var changeEmailButton: UIButton!
    
    @IBOutlet weak var changeAddressButton: UIButton!
    
    @IBOutlet weak var applyEmailButton: UIButton!
    
    @IBOutlet weak var extAccountButton: UIButton!
    @IBOutlet weak var keepLoginSwitch: UISwitch!
    
    @IBOutlet weak var appVersionButton: UIButton!
    
    @IBOutlet weak var logoutButton: UIButton!
    
    @IBOutlet weak var shareButton: UIButton!
    
    let ACCOUNT_LOGIN = 1
    let FACEBOOK_LOGIN = 2
    let ALL_LOGIN = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        let frame = CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude)
        self.tableView.tableHeaderView = UIView(frame: frame)
        
        #if APPSTORE
            if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                appVersionButton.setTitle("APP版號：\(text)", for: UIControlState.normal)
            }
        #else
            let shortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
            let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
            appVersionButton.setTitle("APP版號：\(shortVersion)(\(bundleVersion))", for: UIControlState.normal)
        #endif
        
//        self.tableView.estimatedRowHeight = 44
//        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        TSTAPP2_Utilities.roundedImage(accountPhotoImageView, radius: 40)
        
        
        if UserDefaults.standard.bool(forKey: KEEP_LOGIN) == true {
            keepLoginSwitch.isOn = true
        }
        else {
            keepLoginSwitch.isOn = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0201", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "會員中心")
        
        loginData = LoginData.sharedLoginData()
        
        determineFlag()
        
        if displayMode == ACCOUNT_LOGIN || displayMode == ALL_LOGIN {
            
            callAPI {
                
            }
        }
        else if displayMode == FACEBOOK_LOGIN {
            
            changePhotoButton.isHidden = true
            
            if let fbLoginDic = UserDefaults.standard.dictionary(forKey: FACEBOOK_LOGIN_DATA) {
                let id = fbLoginDic.stringValue(ofKey: "id")
                let facebookPictureUrl = "https://graph.facebook.com/\(id)/picture?type=large"
                self.accountPhotoImageView.setImageWith(URL(string: facebookPictureUrl)!)
                
                displayNickNameLabel.text = "Hi, \(TSTAPP2_Utilities.encodeForSecurity(sourceString: fbLoginDic.stringValue(ofKey: "name"), type: encodeForSecurityType.Name))"
                userNameLabel.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: fbLoginDic.stringValue(ofKey: "name"), type: encodeForSecurityType.Name)
                extAccountLabel.text = fbLoginDic.stringValue(ofKey: "email")
                
            }
            
            facebookLoginButton.isHidden = true
            accountLoginButton.isHidden = false
            extAccountBindingButton.isHidden = true
            extAccountButton.isHidden = true
            loginInfoLabel.isHidden = false
            loginInfoLabel.text = "請登入台灣之星門號，可獲得更多完整服務。"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0201", action: "")
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: displayNickNameLabel)
        UISkinChange.sharedInstance.changeUIColor(theObject: accountLoginButton)
        UISkinChange.sharedInstance.changeUIColor(theObject: keepLoginSwitch)
        UISkinChange.sharedInstance.changeUIColor(theObject: shareButton, mode: ButtonColorMode.button_share.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: logoutButton)
        UISkinChange.sharedInstance.changeUIColor(theObject: changePhotoButton, mode: ButtonColorMode.button_photo.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: accountPhotoImageView, mode: ImageColorMode.icon_head.rawValue)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    MARK: -
    func determineFlag() {
        isAccountLoggedIn = LoginData.isLogin()
        
        if LoginData.isFacebookLogin() && loginData.custProfile_extId != "" {
            isFacebookLoggedIn = true
        }
        else if LoginData.isFacebookLogin() && loginData.custProfile_extId == "" {
            if LoginData.sharedLoginData().hasData == "Y" {
                isFacebookLoggedIn = false
                TSTAPP2_Utilities.logoutFacebook()
            }
            else if LoginData.sharedLoginData().hasData == "N" {
                isFacebookLoggedIn = true
            }
        }
        else if !LoginData.isFacebookLogin() && loginData.custProfile_extId != "" {
            isFacebookLoggedIn = true
        }
        else {
            isFacebookLoggedIn = false
        }
        
        if isAccountLoggedIn && !isFacebookLoggedIn {
            displayMode = ACCOUNT_LOGIN
        }
        else if !isAccountLoggedIn && isFacebookLoggedIn {
            displayMode = FACEBOOK_LOGIN
        }
        else if isAccountLoggedIn && isFacebookLoggedIn {
            displayMode = ALL_LOGIN
        }
    }
    
//    MARK: - API
    func callAPI(completion: @escaping () -> Void) {
        
        if shouldReloadAllAPI {
            TSTAPP2_Utilities.showPKHUD()
            
            TSTAPP2_API.sharedInstance.getCustProfile(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, uid: loginData.custProfile_uid, token: loginData.token, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                self.isGetCustProfile = true
                
                if dic.stringValue(ofKey: "code") == "00000" {
                    self.loginData.setDataValue(withDic: dic, onlyUpdateCustProfile: true)
                    
//                    if AccountInformation_CallGetBillTypeInfo == true {
//                        AccountInformation_CallGetBillTypeInfo = false
                        TSTAPP2_API.sharedInstance.getBillTypeInfo(withMsisdn: self.loginData.custProfile_msisdn, isMainMsisdn: self.loginData.custProfile_isMainMsisdn, showProgress: true, completion: { (dic, response) in
                            
                            self.isGetBillTypeInfo = true
                            
                            if dic.stringValue(ofKey: "code") == "00000" {
                                if let _ = dic["data"] as? Dictionary<String, Any> {
                                    self.loginData.setBillTypeDataValue(withDic: dic)
                                    self.determineFlag()
                                    self.apiResponded(completion: completion)
                                }
                            }
                            
                        }, failure: { (error) in
                            self.isGetBillTypeInfo = true
                            self.determineFlag()
                            self.apiResponded(completion: completion)
                        })
//                    }
//                    else {
//                        self.determineFlag()
//                        self.apiResponded(completion: completion)
//                    }
                    
                    
                }
                else {
                    self.isGetBillTypeInfo = true
                    self.determineFlag()
                    self.apiResponded(completion: completion)
                }
            }, failure: { (error: Error) in
                self.isGetCustProfile = true
                self.isGetBillTypeInfo = true
                self.apiResponded(completion: completion)
                
            })
            
        }
        
        tableView.reloadData()
    }
    
    func apiResponded(completion: @escaping () -> Void) {
        print("apiResponded123")
        if isGetCustProfile && isGetBillTypeInfo {
            
            isGetCustProfile = false
            isGetBillTypeInfo = false
            
            updateUI()
            TSTAPP2_Utilities.hidePKHUD()
            completion()
            
            checkVIPStatusAndChangeSkin()
        }
    }
    
//    MARK: - Update UI for Account and All login
    func updateUI() {
        print("updateUI123")
        if URL(string: loginData.custProfile_picUrl) != nil {
            if UISkinChange.sharedInstance.isVIP() {
                accountPhotoImageView.setImageWith(URL(string: loginData.custProfile_picUrl)!, placeholderImage: UIImage(named: "icon_head_gold"))
            }
            else {
                accountPhotoImageView.setImageWith(URL(string: loginData.custProfile_picUrl)!, placeholderImage: UIImage(named: "icon_head"))
            }
        }
        else {
            UISkinChange.sharedInstance.changeUIColor(theObject: accountPhotoImageView, mode: ImageColorMode.icon_head.rawValue)
        }
        
        if loginData.custProfile_nickname != "" {
            displayNickNameLabel.text = "Hi, \(loginData.custProfile_nickname)"
        }
        else if loginData.custProfile_extName != "" {
            displayNickNameLabel.text = "Hi, \(loginData.custProfile_extName)"
        }
        else {
            displayNickNameLabel.text = "Hi, \(loginData.custProfile_displayNickname)"
        }
        memberTypeLabel.text = loginData.custProfile_memberType
        msisdnLabel.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: loginData.custProfile_msisdn, type: encodeForSecurityType.Phone)
        userNameLabel.text = loginData.custProfile_displayNickname
        nickNameLabel.text = loginData.custProfile_nickname
        passwordLabel.text = "*****"
        vipStartDateLabel.text = loginData.custProfile_vipStartDate
        vipEndDateLabel.text = loginData.custProfile_vipEndDate
        addressLabel.text = loginData.billType_address
        
        if displayMode == ACCOUNT_LOGIN {
            facebookLoginButton.isHidden = false
            accountLoginButton.isHidden = true
            extAccountLabel.text = ""
            extAccountLabel.isHidden = true
            extAccountBindingButton.isHidden = false
            extAccountButton.isHidden = true
            loginInfoLabel.isHidden = false
            loginInfoLabel.text = "將您的Facebook與台灣之星門號連結，下次登入更迅速。"
        }
        else if displayMode == FACEBOOK_LOGIN {
            facebookLoginButton.isHidden = true
            accountLoginButton.isHidden = false
            extAccountBindingButton.isHidden = true
            extAccountButton.isHidden = true
            loginInfoLabel.isHidden = false
            loginInfoLabel.text = "請登入台灣之星門號，可獲得更多完整服務。"
        }
        else if displayMode == ALL_LOGIN {
            facebookLoginButton.isHidden = true
            accountLoginButton.isHidden = true
            extAccountLabel.text = loginData.custProfile_extEmail
            extAccountLabel.isHidden = false
            extAccountBindingButton.isHidden = true
            extAccountButton.isHidden = false
            loginInfoLabel.isHidden = true
        }
        
        
//        let loginData = LoginData.sharedLoginData()
        
        if loginData.billType_billTypeEdit == "Y" {
            self.billTypeButton.isHidden = false
        }
        else {
            self.billTypeButton.isHidden = true
        }
        
        if loginData.billType_emailEdit == "Y" {
            self.changeEmailButton.isHidden = false
        }
        else {
            self.changeEmailButton.isHidden = true
        }
        
        if loginData.billType_addressEdit == "Y" {
            self.changeAddressButton.isHidden = false
        }
        else {
            self.changeAddressButton.isHidden = true
        }
        
        self.billStatusLabel.text = loginData.billType_billType
        self.ebillType = EnumEbillType(rawValue: loginData.billType_memberType) ?? EnumEbillType.NoData
        
        if loginData.billType_ebillApply == "Y" {
            
            applyEmailButton.isUserInteractionEnabled = true
            
            let billTypeEmail = LoginData.sharedLoginData().billType_maskEmail
            let attributedString = try! NSMutableAttributedString(data: billTypeEmail.data(using: String.Encoding.unicode)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            attributedString.addAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], range: NSMakeRange(0, attributedString.string.count))
            applyEmailButton.setAttributedTitle(attributedString, for: UIControlState.normal)
        }
        else {
            applyEmailButton.isUserInteractionEnabled = false
            
            let billTypeEmail = LoginData.sharedLoginData().billType_maskEmail
            let attributedString = try! NSMutableAttributedString(data: billTypeEmail.data(using: String.Encoding.unicode)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            attributedString.addAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1)                                                   ], range: NSMakeRange(0, attributedString.string.count))
            applyEmailButton.setAttributedTitle(attributedString, for: UIControlState.normal)
        }
        tableView.reloadData()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (section == 1 || section == 3 || section == 4) && displayMode == FACEBOOK_LOGIN {
            return 0.1
            
        }
        else if section == 3 && loginData.custProfile_isVIP == "N" {
            return 0.1
        }
        else if section == 3 && (loginData.custProfile_vipStartDate == "" && loginData.custProfile_vipEndDate == "") {
            return 0.1
        }
        else {
            return super.tableView(tableView, heightForHeaderInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if (section == 1 || section == 3 || section == 4) && displayMode == FACEBOOK_LOGIN {
            return 0.1
            
        }
        else if section == 3 && loginData.custProfile_isVIP == "N" {
            return 0.1
        }
        else if section == 3 && (loginData.custProfile_vipStartDate == "" && loginData.custProfile_vipEndDate == "") {
            return 0.1
        }
        else {
            return super.tableView(tableView, heightForFooterInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 && displayMode == ALL_LOGIN {
            
//            if indexPath.section == 0 {
                return 124
//            }
        }
        else if indexPath.section == 2 && displayMode == FACEBOOK_LOGIN {
            if indexPath.row == 1 || indexPath.row == 2 {
                return 0
            }
        }
        else if indexPath.section == 3 {
            if indexPath.row == 0 && loginData.custProfile_vipStartDate == "" {
                return 0
            }
            else if indexPath.row == 1 && loginData.custProfile_vipEndDate == "" {
                return 0
            }
        }
        else if indexPath.section == 4 {
            if indexPath.row == 1 && loginData.billType_emailShow != "Y" {
                return 0
            }
            else if indexPath.row == 2 && loginData.billType_addressShow != "Y" {
                return 0
            }
        }
        
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (section == 1 || section == 3  || section == 4) && displayMode == FACEBOOK_LOGIN {
            return 0
        }
        else if section == 3 && loginData.custProfile_isVIP == "N" {
            return 0
        }
        else if section == 3 && (loginData.custProfile_vipStartDate == "" && loginData.custProfile_vipEndDate == "") {
            return 0
        }
        else{
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
        
    }
    
    //MARK: - IBAction
    @IBAction func didTapLoginButton(_ sender: Any) {
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "門號登入(會員中心)", action: nil, label: nil, value: nil)
        UbaRecord.sharedInstance.sendUba(withPage: "APP0201", action: "A0202")
    }
    
    @IBAction func didTapShare(_ sender: Any) {

        UbaRecord.sharedInstance.sendUba(withPage: "APP02", action: "A0204")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "分享台灣之星APP(會員中心)", action: nil, label: nil, value: nil)
        
        let str = "Hi~我正在使用台灣之星官方APP，看帳單、繳帳款，網路覆蓋率、比較各家資費輕鬆查！"
        let url = URL(string: "http://www.tstartel.com/CWS/app/precheck.htm")!
        let activity = UIActivityViewController(activityItems: [str,url], applicationActivities: nil)
        
        self.present(activity, animated: true, completion: nil)
    }
    
    @IBAction func didTapFacebookLoginButton(_ sender: AnyObject) {
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "FB登入(會員中心)", action: nil, label: nil, value: nil)
        UbaRecord.sharedInstance.sendUba(withPage: "APP0201", action: "A0201")
        
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result: FBSDKLoginManagerLoginResult?, error: Error?) in
            if error != nil {
                print("Process error")
                let description = (error! as NSError).userInfo["FBSDKErrorLocalizedDescriptionKey"]
                print("Process error \(String(describing: description))")
            }
            else if result!.isCancelled {
                print("Cancelled")
            }
            else {
                print("Logged in")
                
                self.requestGraph()
            }
        }
        
    }
    
    @IBAction func didTapChangePhotoButton(_ sender: AnyObject) {
        
        let actionSheet = UIAlertController(title: "更換大頭貼", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancelAction = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel) { (action) in
            
        }
        let takeNewAction = UIAlertAction(title: "拍攝新的大頭貼", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        let chooseAction = UIAlertAction(title: "選擇大頭貼照", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        
        actionSheet.addActions(array: [cancelAction, takeNewAction, chooseAction])
        
        
        actionSheet.show(viewController: self)
    }
    
    @IBAction func didTapLogoutButton(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "確認是否登出", preferredStyle: UIAlertControllerStyle.alert)
        let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel, handler: nil)
        let logout = UIAlertAction(title: "登出", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登出(會員中心)", action: "1", label: nil, value: nil)
            UbaRecord.sharedInstance.sendUba(withPage: "APP0201", action: "A0203")
            
            TSTAPP2_Utilities.logout(completion: { 
                self.performSegue(withIdentifier: "toMain", sender: nil)
            }, failure: { 
                
            })
        
        })
        alert.addActions(array: [cancel, logout])
        alert.show(viewController: self)
        
    }
    
    @IBAction func didTapExtAccountBindingButton(_ sender: Any) {
        didTapFacebookLoginButton(self)
    }
    
    @IBAction func didTapBillTypeButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP0201", action: "AA_BILL_TYPE")
        
        self.performSegue(withIdentifier: "toBillType", sender: nil)
        
    }
    
    @IBAction func didChangeKeepLoginSwitch(_ sender: Any) {
        
        if (sender as! UISwitch).isOn {
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "保持登入(會員中心)", action: "ON", label: nil, value: nil)
        }
        else {
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "保持登入(會員中心)", action: "OFF", label: nil, value: nil)
        }
        UserDefaults.standard.set((sender as! UISwitch).isOn, forKey: KEEP_LOGIN)
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func didTapChangeEmail(_ sender: Any) {
        
        if self.ebillType == .Apply_A || self.ebillType == .Confirming_W {
            let message = loginData.billType_showMessage
            let action = UIAlertAction(title: "修改", style: UIAlertActionStyle.default, handler: { (action) in
                let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "ApplyEbillViewController") as! ApplyEbillViewController
                vc.email = self.loginData.billType_email
                
                self.navigationController?.show(vc, sender: self)
            })
            
            let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel, handler: { (action) in
                
            })
            
            TSTAPP2_Utilities.showAlert(withMessage: message, action: action, cancel: cancel, viewController: self)
        }
        else {
            let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ApplyEbillViewController") as! ApplyEbillViewController
            vc.email = self.loginData.billType_email
            
            self.navigationController?.show(vc, sender: self)
        }
    }
    
    @IBAction func didTapApplyEmail(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ChangeEmailViewController") as! ChangeEmailViewController

        self.navigationController?.show(vc, sender: self)
    }
    
    @IBAction func didTapVIPButton(_ sender: Any) {

        if LoginData.isLogin() {
        
            #if SIT
                let urlString: String = "http://uattsp.tstartel.com/static/temp/vip/"
            #elseif UAT
                let urlString: String = "http://uattsp.tstartel.com/static/temp/vip/"
            #else
                let urlString: String = "http://doc.tstartel.com/tstar_vip/"
            #endif
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "TCLUB會員專區", action: nil, label: nil, value: nil)
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "T-Club 會員專區", urlString: urlString, viewController: self)
            
            Answers.logContentView(withName: "TCLUB會員專區",
                                   contentType: "Function",
                                   contentId: urlString,
                                   customAttributes: [:])
        }
    }
    
    //MARK: -
    func requestGraph() {
        TSTAPP2_Utilities.showPKHUD()
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, birthday, gender", "locale": "zh_TW"]).start(completionHandler: { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if result != nil {
                var resultDic = result as! Dictionary<String, String>
                print("FBSDKGraphRequest\n\(String(describing: resultDic["id"])) \(String(describing: resultDic["name"])) \(String(describing: resultDic["email"]))")
                
                resultDic["token"] = FBSDKAccessToken.current().tokenString
                UserDefaults.standard.set(result, forKey: FACEBOOK_LOGIN_DATA)
                UserDefaults.standard.synchronize()
                
                let id = resultDic.stringValue(ofKey: "id")
                let token = resultDic.stringValue(ofKey: "token")
                let email = resultDic.stringValue(ofKey: "email")
                let name = resultDic.stringValue(ofKey: "name")
                let birthday = resultDic.stringValue(ofKey: "birthday")
                let gender = resultDic.stringValue(ofKey: "gender")
                
                
                let password = UserDefaults.standard.string(forKey: ACCOUNT_PASSWORD)!
                TSTAPP2_API.sharedInstance.bindExt(withMSISDN: self.loginData.custProfile_msisdn, extId: id, extType: "facebook", extToken: token, password: password, email: email, name: name, birthday: birthday, gender: gender, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        self.callAPI {
                            TSTAPP2_Utilities.showAlert(withMessage: "Facebook帳號連結完成", viewController: self)
                            
                            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改FB帳號(會員中心)", action: "連結", label: nil, value: nil)
                        }
                    }
                    else {
                        TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                        TSTAPP2_Utilities.hidePKHUD()
                    }
                }, failure: { (error: Error) in
                    TSTAPP2_Utilities.hidePKHUD()
                })
                
            }
            else {
                TSTAPP2_Utilities.hidePKHUD()
            }
        })
    }
    
//    MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        dismiss(animated: true) { 
            
        }
        
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let resizedImage = TSTAPP2_Utilities.resize(image: image, maxWidth: 500, maxHeight: 500)
        
        TSTAPP2_Utilities.showPKHUD()
        shouldReloadAllAPI = false
        
        TSTAPP2_API.sharedInstance.setProfilePic(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, image: resizedImage, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5, execute: {
                TSTAPP2_API.sharedInstance.getCustProfile(withMSISDN: self.loginData.custProfile_msisdn, contractId: self.loginData.custProfile_contractId, uid: self.loginData.custProfile_uid, token: self.loginData.token, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    
                    TSTAPP2_Utilities.hidePKHUD()
                    self.shouldReloadAllAPI = true
                    if dic.stringValue(ofKey: "code") == "00000" {
                        self.loginData.setDataValue(withDic: dic, onlyUpdateCustProfile: true)
                        self.updateUI()
                    }
                    else {
                        
                    }
                }, failure: { (error: Error) in
                    TSTAPP2_Utilities.hidePKHUD()
                    self.shouldReloadAllAPI = true
                })
            })
            
            
        }) { (error: Error) in
            TSTAPP2_Utilities.hidePKHUD()
            self.shouldReloadAllAPI = true
        }
    }

}
