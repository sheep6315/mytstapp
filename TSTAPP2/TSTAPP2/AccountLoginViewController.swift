//
//  AccountLoginViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/13.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let ACCOUNT_PASSWORD = "ACCOUNT_PASSWORD"

class AccountLoginViewController: TSUIViewController, UINavigationControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate {

    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP010101", action: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP010101", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.

        if segue.identifier == "toChangePassword" {
            (segue.destination as! ChangePasswordViewController).pwschangeStatus = LoginData.sharedLoginData().custProfile_pwschange
            (segue.destination as! ChangePasswordViewController).password = passwordTextField.text!
            (segue.destination as! ChangePasswordViewController).msisdn = accountTextField.text!
        }
    }
 
    
//    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: false, notification: notification)
    }
    
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        self.scrollView.endEditing(true)
        
    }
    

//    MARK: - IBAction
    @IBAction func didTapLoginButton(_ sender: AnyObject) {
        
        if accountTextField.text != "" && passwordTextField.text != "" {
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP010101", action: "A0101")
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "執行登入", action: "其他登入方式", label: "門號登入", value: nil)
            
            TSTAPP2_API.sharedInstance.login(withMSISDN: accountTextField.text!, password: passwordTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    
                    UserDefaults.standard.set(self.passwordTextField.text, forKey: ACCOUNT_PASSWORD)
                    UserDefaults.standard.synchronize()
                    
                    let loginData = LoginData.sharedLoginData()
                    loginData.setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                    
                    if loginData.custProfile_pwschange != "1" {
                        self.performSegue(withIdentifier: "toChangePassword", sender: nil)
                    }
                    else {
                        
                        self.performSegue(withIdentifier: "toTabBar", sender: nil)
                    }
                    
//                    let deviceID = UIDevice.current.identifierForVendor!.uuidString
//                    TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: "", showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//                        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//                        UIApplication.shared.registerUserNotificationSettings(settings)
//                        UIApplication.shared.registerForRemoteNotifications()
//                    }, failure: { (error: Error) in
//
//                    })
                    
                    TSTAPP2_Utilities.registerDevice()
                    
                }
                else {
                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
                
            }, failure: { (error: Error) in
                let err = error as NSError
                if err.code == -1009 {
                    TSTAPP2_Utilities.showAlert(withMessage: "請確認網路狀態", viewController: self)
                }
                else if err.code == -1001 {
                    TSTAPP2_Utilities.showAlert(withMessage: "請再次登入", viewController: self)
                }
                else {
                    TSTAPP2_Utilities.showAlert(withMessage: "請再次登入", viewController: self)
                }
            })
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "帳號或密碼不能為空白", viewController: self)
        }
        
    }
    
    @IBAction func didTapPrePaidButton(_ sender: Any) {
        let urlString: String = "https://www.tstartel.com/mCWS/prepaidCardLogin.php?attest_to=prepaidCard&service_code=PAYMENT_PREPAID"
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
        
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @IBAction func didTapApplyPassword(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP010101", action: "A010201")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "申請密碼", action: nil, label: nil, value: nil)
    }


    //MARK: - textFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }

}
