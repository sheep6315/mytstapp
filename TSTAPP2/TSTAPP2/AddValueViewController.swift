//
//  AddValueViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/7/19.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let AddValueContentViewHeight: CGFloat = 44

class AddValueSvcMessageView: UIView {
    @IBOutlet weak var svcMessageLabel: UILabel!
    
    @IBOutlet weak var svcWebView: UIWebView!
    
}

class AddValueTryView: UIView {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var tryButton: CustomButton!
}

class AddValueContentView: UIView {
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
}

class AddValueCell: UITableViewCell {
    
    @IBOutlet weak var svcNameLabel: UILabel!
    
}

class AddValueViewController: TSUIViewController, UITableViewDataSource, UITableViewDelegate,UIWebViewDelegate {
    
    @IBOutlet weak var addValueTableView: UITableView!
    
    let appliedAddValue = AppliedAddValue.sharedInstance
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addValueTableView.rowHeight = UITableViewAutomaticDimension
        addValueTableView.estimatedRowHeight = 91
//        addValueTableView.reloadData()
        
        addValueTableView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_APLD_AVSVC", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "已申裝之服務", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "已申裝之服務")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_APLD_AVSVC", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appliedAddValue.appliedAddValueSvcList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddValueCell", for: indexPath) as! AddValueCell
        
        cell.svcNameLabel.text = appliedAddValue.appliedAddValueSvcList[indexPath.row].svcName
        
        for subView in cell.contentView.subviews {
            if subView.isKind(of: AddValueContentView.self) {
                subView.removeFromSuperview()
            }
            else if subView.isKind(of: AddValueSvcMessageView.self) {
                subView.removeFromSuperview()
            }
        }
        
        var count: CGFloat = 0
        for content in appliedAddValue.appliedAddValueSvcList[indexPath.row].contentList {
            
            let addValueContentView = UINib(nibName: "AddValueContent", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! AddValueContentView
            
            addValueContentView.textLabel.text = content.text
            addValueContentView.descLabel.text = content.desc
            addValueContentView.autoresizingMask = .flexibleWidth
            addValueContentView.frame = CGRect(x: 0, y: 55 + (count * (AddValueContentViewHeight + 2)), width: CGFloat(self.view.frame.width), height: AddValueContentViewHeight)
            cell.contentView.addSubview(addValueContentView)
            count += 1
        }
        
        if appliedAddValue.appliedAddValueSvcList[indexPath.row].svcMessage != "" {
            let svcMessage = appliedAddValue.appliedAddValueSvcList[indexPath.row].svcMessage
            let addValueSvcMessageView = UINib(nibName: "AddValueContent", bundle: nil).instantiate(withOwner: nil, options: nil)[2] as! AddValueSvcMessageView
            addValueSvcMessageView.autoresizingMask = .flexibleWidth
            addValueSvcMessageView.svcWebView.delegate = self
            
            let width = CGFloat(self.view.frame.width)
            
            let svcAttributedString = try! NSAttributedString(data: svcMessage.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
            addValueSvcMessageView.svcWebView.loadHTMLString(svcMessage, baseURL: nil)
            
//            addValueSvcMessageView.svcMessageLabel.attributedText = svcAttributedString

            
            let constraintRect = CGSize(width: width - 32, height: .greatestFiniteMagnitude)
            let boundingBox = addValueSvcMessageView.svcMessageLabel.attributedText!.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
            
            addValueSvcMessageView.frame = CGRect(x: 0, y: 55 + (count * (AddValueContentViewHeight + 2)), width: width, height: boundingBox.height + 11)
            cell.contentView.addSubview(addValueSvcMessageView)
        }

        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat = 0
        
        let contentCount = appliedAddValue.appliedAddValueSvcList[indexPath.row].contentList.count
        
        height = 55 + (CGFloat(contentCount) * (AddValueContentViewHeight + 2))
        
        if appliedAddValue.appliedAddValueSvcList[indexPath.row].svcMessage != "" {
            
            let addValueSvcMessageView = UINib(nibName: "AddValueContent", bundle: nil).instantiate(withOwner: nil, options: nil)[2] as! AddValueSvcMessageView
            addValueSvcMessageView.autoresizingMask = .flexibleWidth
            let width = CGFloat(self.view.frame.width)
            
            let constraintRect = CGSize(width: width - 32, height: .greatestFiniteMagnitude)
            let boundingBox = addValueSvcMessageView.svcMessageLabel.attributedText!.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
            height += (boundingBox.height + 11)
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK: - UIWebView Delegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.isOpaque = false
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        webView.backgroundColor = UIColor.clear
        webView.frame.size.height = webView.scrollView.contentSize.height
        
        print("webViewDidFinishLoad")
    }
}
