//
//  Advertising2CollectionViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/4.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Advertising2Cell"

protocol Advertising2Delegate: class {
    func didTapAdvertisement2(sender: Any?) -> ()
}

class Advertising2Cell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var stampType3View: UIView!
    @IBOutlet weak var stampType3Label: UILabel!
    
    @IBOutlet weak var stampType1View: UIView!
    @IBOutlet weak var stampType1Label: UILabel!
    
    
}

class Advertising2CollectionViewController: UICollectionViewController {
    
    weak var advertising2Delegate: Advertising2Delegate?
    
    var productArray: [Product] = []
    var newTargetOffset: Float = 0
    var cellWidth: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let modifiedLayout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        cellWidth = (collectionView!.frame.width/2) - 0.5
        modifiedLayout.itemSize = CGSize(width: cellWidth, height: collectionView!.frame.height)
        collectionView?.setCollectionViewLayout(modifiedLayout, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return productArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! Advertising2Cell
    
        // Configure the cell
//        cell.frame = CGRect(x: cell.frame.origin.x, y: 0, width: collectionView.frame.width/2 - 0.5, height: collectionView.frame.height)
        let imageURL = URL(string: productArray[indexPath.row].path)
        if imageURL != nil {
//            cell.imageView.backgroundColor = UIColor.lightGray
            cell.imageView.setImageWith(imageURL!, placeholderImage: UIImage(named: "img_placeHolder"))
        }
        else {
            cell.imageView.image = UIImage(named: "img_placeHolder")
        }
        
        cell.titleLabel.text = productArray[indexPath.row].productName
        cell.contentLabel.text = "單購價：\(productArray[indexPath.row].price)"
        
        if productArray[indexPath.row].stampType == "1" && productArray[indexPath.row].stampText != "" {
            cell.stampType1View.isHidden = false
            cell.stampType1Label.text = productArray[indexPath.row].stampText
        }
        else if productArray[indexPath.row].stampType == "3" && productArray[indexPath.row].stampText != "" {
            cell.stampType3View.isHidden = false
            cell.stampType3Label.text = productArray[indexPath.row].stampText
        }
        else {
            cell.stampType1View.isHidden = true
            cell.stampType3View.isHidden = true
        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = productArray[indexPath.row]
        advertising2Delegate?.didTapAdvertisement2(sender: product)
    }

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
//    MARK: - UIScrollViewDelegate
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//        cellWidth = (collectionView!.frame.width/2) - 0.5
//        modifiedLayout.itemSize = CGSize(width: cellWidth, height: collectionView!.frame.height)
        return CGSize(width: cellWidth, height: collectionView.frame.height)
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageWidth: Float = Float(self.collectionView!.frame.width / 2 - 0.5 + 1) //480 + 50
        // width + space
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
//        var newTargetOffset: Float = 0
        if targetOffset == currentOffset {
            let x = Int(targetOffset) % Int(self.collectionView!.frame.width)
            if x > Int(pageWidth/2) {
                newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
            }
            else {
                newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
            }
        }
        else if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
        setUserInteraction()
    }
    
//    MARK: - Scroll
    func resetScrollPosition() {
        newTargetOffset = 0
        collectionView?.setContentOffset(CGPoint(x: 0, y: collectionView!.contentOffset.y), animated: false)
    }
    
    func scrollBackward() {
//        print("backward newTargetOffset \(newTargetOffset)")
        if newTargetOffset > 0 && collectionView!.isUserInteractionEnabled {
            collectionView?.setContentOffset(CGPoint(x: CGFloat(newTargetOffset) - cellWidth + 1, y: collectionView!.contentOffset.y), animated: true)
            setUserInteraction()
            newTargetOffset = newTargetOffset - Float(cellWidth + 1)
        }
    }
    
    func scrollForward() {
//        print("forward newTargetOffset \(newTargetOffset)")
        if newTargetOffset < Float(cellWidth * CGFloat(productArray.count - 2)) && collectionView!.isUserInteractionEnabled {
            collectionView?.setContentOffset(CGPoint(x: CGFloat(newTargetOffset) + cellWidth + 1, y: collectionView!.contentOffset.y), animated: true)
            setUserInteraction()
            newTargetOffset = newTargetOffset + Float(cellWidth + 1)
        }
    }
    
    func setUserInteraction() {
        
        collectionView?.isUserInteractionEnabled = false
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { (timer: Timer) in
                self.collectionView?.isUserInteractionEnabled = true
            }
        } else {
            // Fallback on earlier versions
            Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(setUserInteractionInverse), userInfo: nil, repeats: false)
        }
    }
    
    @objc func setUserInteractionInverse() {
        collectionView?.isUserInteractionEnabled = !collectionView!.isUserInteractionEnabled
    }

}
