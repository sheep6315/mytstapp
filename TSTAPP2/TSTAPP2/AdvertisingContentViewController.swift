//
//  AdvertisingContentViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/3.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class AdvertisingContentViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var pageIndex: Int = 0
    var urlString: String = ""
    var imageName: String = ""
    var tapGesture: UITapGestureRecognizer!
    var imgName: String = ""
    var protocolName: String = ""
    var id: String = ""
    
    var smartBanner: SmartBanner = SmartBanner()
    
    weak var advertisingDelegate: AdvertisingDelegate?
    
    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(AdvertisingContentViewController.tapImageView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapGesture)
        
//        Utilities.setImageView(imageView, imageName: imageName, category: "banner")
        
        let imageURL = URL(string: smartBanner.path)
        if imageURL != nil {
            imageView.setImageWith(imageURL!, placeholderImage: UIImage(named: "img_placeHolder"))
        }
        else {
            imageView.image = UIImage(named: "img_placeHolder")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func tapImageView(_ recongnizer: UIGestureRecognizer) {
        print("did tap index \(pageIndex) \(protocolName) \(id) \(urlString)")
//        Utilities.goToFunctionPage(protocolName, url: urlString)
        advertisingDelegate?.didTapAdvertisement(sender: smartBanner)
    }

}
