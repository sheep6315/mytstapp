//
//  AdvertisingPageViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/3.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let DidChangeAdvertisingNotification = "DidChangeAdvertisingNotification"

protocol AdvertisingDelegate: class {
    func didTapAdvertisement(sender: Any?) -> ()
}

class AdvertisingPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var currentIndex: Int = 0
    var pageCount = 0
    var autoScrollTimer: Timer = Timer()
    var bannerDataArray: [String] = []
    var smartBannerArray: [SmartBanner] = []
    var transitioning: Bool = false
    
    var needToValidateTimer = false
    
    weak var advertisingDelegate: AdvertisingDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.delegate = self
        self.dataSource = self
        
        self.view.isUserInteractionEnabled = false
        
        let pageControl: UIPageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor(red: 183/255, green: 78/255, blue: 115/255, alpha: 0.4)
        pageControl.currentPageIndicatorTintColor = purpleColor
        
//        bannerDataArray = ["https://www.tstartel.com/upload/APP1/23/4/app_1_20161006101649.jpg", "https://www.tstartel.com/upload/APP1/5/1/app_1_20160722181819.jpg", "https://www.tstartel.com/upload/APP1/27/1/app_1_20161101001145.jpg", "https://www.tstartel.com/upload/APP1/1/5/app_1_20161101110715.jpg", "https://www.tstartel.com/upload/APP1/13/6/app_1_20161101000445.jpg", "https://www.tstartel.com/upload/APP1/7/7/app_1_20161003193624.jpg", "https://www.tstartel.com/upload/APP1/29/1/app_1_20161101001454.jpg", "https://www.tstartel.com/upload/APP1/9/2/app_1_20160831192701.jpg"]
        
//        pageCount = bannerDataArray.count
//        setPageViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if needToValidateTimer {
            needToValidateTimer = false
            self.autoScrollTimer.invalidate()
            self.autoScrollTimer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(AdvertisingPageViewController.autoScroll), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        autoScrollTimer.invalidate()
        currentIndex = 0
//        needToValidateTimer = true
    }
    
    override func viewDidLayoutSubviews() {
        for subView in self.view.subviews {
            if subView is UIScrollView {
                subView.frame = self.view.bounds
            } else if subView is UIPageControl {
                self.view.bringSubview(toFront: subView)
            }
        }
        super.viewDidLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
//    MARK: -
    func contentViewAtIndex(_ pageIndex: Int) -> AdvertisingContentViewController {
        let pageContentVC : AdvertisingContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "AdvertisingContentViewController") as! AdvertisingContentViewController
        pageContentVC.advertisingDelegate = advertisingDelegate
        pageContentVC.pageIndex = pageIndex
        
        if pageIndex >= 0 && pageIndex < smartBannerArray.count {
            pageContentVC.smartBanner = smartBannerArray[pageIndex]
        }
        
        return pageContentVC
    }
    
    func setPageViewController() {
        if !transitioning {
            transitioning = true
            self.setViewControllers([contentViewAtIndex(0)], direction: UIPageViewControllerNavigationDirection.forward, animated: false) { (finished: Bool) -> Void in
                self.autoScrollTimer.invalidate()
                self.autoScrollTimer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(AdvertisingPageViewController.autoScroll), userInfo: nil, repeats: true)
                self.view.isUserInteractionEnabled = true
                self.currentIndex = 0
                self.transitioning = false
            }
        }
    }
    
//    MARK: - Auto scroll
    @objc func autoScroll() {
        
        self.view.isUserInteractionEnabled = false
//        print("isUserInteractionEnabled = false")
        
        if currentIndex == pageCount - 1 {
            currentIndex = 0
        }
        else {
            currentIndex += 1
        }
        
//        print("Auto scroll \(currentIndex)")
        
        if !transitioning && pageCount > 1 {
            self.setViewControllers([contentViewAtIndex(currentIndex)], direction: UIPageViewControllerNavigationDirection.forward, animated: true) { (finished: Bool) -> Void in
                let pageVCScrollView = self.view.subviews.filter { $0 is UIScrollView }.first as! UIScrollView
//                let multiplier = pageVCScrollView.contentOffset.x % pageVCScrollView.frame.width
                if pageVCScrollView.contentOffset.x.truncatingRemainder(dividingBy: pageVCScrollView.frame.width) != 0 {
                    pageVCScrollView.setContentOffset(CGPoint(x: pageVCScrollView.frame.width * CGFloat(self.currentIndex), y: 0), animated: false)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.35) {
                    self.view.isUserInteractionEnabled = true
                }
//                print("isUserInteractionEnabled = true")
            }
        }
    }
    
//    MARK: - UIPageViewControllerDataSource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if pageCount == 1 {
            return nil
        }
        
        var pageIndex = (viewController as! AdvertisingContentViewController).pageIndex
        currentIndex = pageIndex
        
        if pageIndex == 0 {
            pageIndex = pageCount
        }
        
        pageIndex -= 1
        
        return contentViewAtIndex(pageIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if pageCount == 1 {
            return nil
        }
        
        var pageIndex = (viewController as! AdvertisingContentViewController).pageIndex
        currentIndex = pageIndex
        
        pageIndex += 1
        
        if pageIndex == pageCount {
            pageIndex = 0
        }
        
        return contentViewAtIndex(pageIndex)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return smartBannerArray.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex
    }

//    MARK: - UIPageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if self.view.isUserInteractionEnabled == true {
            self.view.isUserInteractionEnabled = false
            autoScrollTimer.invalidate()
            transitioning = true
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if finished == true {
            currentIndex = (pageViewController.viewControllers![0] as! AdvertisingContentViewController).pageIndex
            print("\(currentIndex)")
            
            print("completed \(completed)")
            self.view.isUserInteractionEnabled = true
            
//        if completed {
            transitioning = false
//        }
            autoScrollTimer.invalidate()
            autoScrollTimer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(AdvertisingPageViewController.autoScroll), userInfo: nil, repeats: true)
        }
    }
    
}
