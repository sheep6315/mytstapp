//
//  AppDelegate.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/4.
//  Copyright © 2016年 Tony Hsu. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import GoogleMaps
import Fabric
import Crashlytics
import UserNotifications

let PAUSE_TIME = "PAUSE_TIME"
let VERSION_STATUS = "VERSION_STATUS"

enum VersionStatus: Int {
    case forceUpdate = 0
    case remindUpdate
    case notReleased
    case noUpdateStatus
}
let FUNCTION_STATUS = "FUNCTION_STATUS"
enum FunctionStatus: Int {
    case shutdown = 0
    case maintain
}

enum URL_SCHEME: String {
    case tstarcs = "tstarcs"
    case tstapp = "tstapp"
    case tstappE = "tstappE"
}

let DID_UPDATE_LOGIN_DATA = "DID_UPDATE_LOGIN_DATA"
let MAINTAIN_INFO = "MAINTAIN_INFO"
let SAVED_VERSION_CODE = "SAVED_VERSION_CODE"
let SERVICE_ORDER_SEND_TIME = "SERVICE_ORDER_SEND_TIME"
let APP_VERSION = "APP_VERSION"

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    //var pauseTime: TimeInterval = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        #if SIT
            let filePath = Bundle.main.path(forResource: "GoogleService-Info-SIT", ofType: "plist")!
        #elseif UAT
            let filePath = Bundle.main.path(forResource: "GoogleService-Info-UAT", ofType: "plist")!
        #else
            let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
        #endif
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GMSServices.provideAPIKey("AIzaSyC_9zQ5tgiKijUd77jUyO3BzxvoacP9X7I")
        
        // Configure tracker from GoogleService-Info.plist.
//        var configureError:NSError?
//        GGLContext.sharedInstance().configureWithError(&configureError)
//        assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
        
//        guard let gai = GAI.sharedInstance() else {
//            assert(false, "Google Analytics not configured correctly")
//        }
        if let gai = GAI.sharedInstance() {
            gai.tracker(withTrackingId: "UA-54439660-1")
            gai.trackUncaughtExceptions = false
        }
        
        Fabric.with([Crashlytics.self])
        
        if UserDefaults.standard.object(forKey: KEEP_LOGIN) == nil {
            UserDefaults.standard.set(true, forKey: KEEP_LOGIN)
            UserDefaults.standard.synchronize()
        }
        else if UserDefaults.standard.bool(forKey: KEEP_LOGIN) == false {
            TSTAPP2_Utilities.logout(completion: {
                
            }, failure: {
                
            })
        }
        
        if LoginData.isLogin() {
//            let deviceID = UIDevice.current.identifierForVendor!.uuidString
//            let loginData = LoginData.sharedLoginData()
//            TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: "", showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
////                let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
////                UIApplication.shared.registerUserNotificationSettings(settings)
////                UIApplication.shared.registerForRemoteNotifications()
//
//                if #available(iOS 10.0, *) {
//                    // For iOS 10 display notification (sent via APNS)
//                    UNUserNotificationCenter.current().delegate = self
//
//                    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//                    UNUserNotificationCenter.current().requestAuthorization(
//                        options: authOptions,
//                        completionHandler: {_, _ in })
//                }
//                else {
//                    let settings: UIUserNotificationSettings =
//                        UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//                    application.registerUserNotificationSettings(settings)
//                }
//
//                application.registerForRemoteNotifications()
//
//                let token = Messaging.messaging().fcmToken
//                print("FCM token: \(token ?? "")")
//
//            }, failure: { (error: Error) in
//
//            })
            
            TSTAPP2_Utilities.registerDevice()
        }
        
        let color: UIColor = UIColor.white
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: color], for: .normal)
        
        // then if StateSelected should be different, you should add this code
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 87/255, green: 14/255, blue: 68/255, alpha: 1)], for: .selected)
        
//        NSDictionary *pushNotificationPayload = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//        if (pushNotificationPayload) {
//            [self application:application didReceiveRemoteNotification:pushNotificationPayload];
//        }
        

        
        if let remoteNotificationPayload = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            self.application(application, didReceiveRemoteNotification: remoteNotificationPayload)
        }
        
        let webView = UIWebView()
        let agent = webView.stringByEvaluatingJavaScript(from: "navigator.userAgent")!
        if !agent.contains("_tstar") {
            UserDefaults.standard.register(defaults: ["UserAgent": "\(agent)_tstar"])
        }
        
        if UserDefaults.standard.dictionary(forKey: SERVICE_NEW_ICON) == nil {
            UserDefaults.standard.set(SERVICE_NEW_ICON_DIC, forKey: SERVICE_NEW_ICON)
            UserDefaults.standard.synchronize()
        }
        else {
            if let serviceNewIconDic = UserDefaults.standard.dictionary(forKey: SERVICE_NEW_ICON) as? Dictionary<String, Bool> {
                if serviceNewIconDic != SERVICE_NEW_ICON_DIC {
                    var newServiceNewIconDic = SERVICE_NEW_ICON_DIC
                    for serviceNewIcon in serviceNewIconDic {
                        if serviceNewIcon.key == "13" {
                            newServiceNewIconDic[serviceNewIcon.key] = newServiceNewIconDic[serviceNewIcon.key]
                        }
                        else {
                            newServiceNewIconDic[serviceNewIcon.key] = serviceNewIcon.value
                        }
                    }
                    UserDefaults.standard.set(newServiceNewIconDic, forKey: SERVICE_NEW_ICON)
                    UserDefaults.standard.synchronize()
                }
            }
        }
        
        checkServiceOrder()
        
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 133/255, green: 39/255, blue: 107/255, alpha: 1)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        let pauseTime = NSDate().timeIntervalSince1970
        UserDefaults.standard.set(pauseTime, forKey: PAUSE_TIME)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        checkServiceOrder()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()
        
        if let pauseTime = UserDefaults.standard.object(forKey: PAUSE_TIME) as? TimeInterval {
            if pauseTime > Double(0) && UserDefaults.standard.bool(forKey: KEEP_LOGIN) {
                let pauseTimeDate = NSDate.init(timeIntervalSince1970: pauseTime)
                let interval = NSDate().timeIntervalSince(pauseTimeDate as Date)
                
                let savedVersionCode = UserDefaults.standard.integer(forKey: SAVED_VERSION_CODE) 
                
                if interval > Double(1800) || (VERSION_CODE > savedVersionCode) {
                    if let password = UserDefaults.standard.string(forKey: ACCOUNT_PASSWORD) {
                        TSTAPP2_API.sharedInstance.login(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, password: password, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                            if dic.stringValue(ofKey: "code") == "00000" {
                                
                                let apiData = dic["data"] as? Dictionary<String, Any>
                                let custProfile = apiData?["custProfile"] as? Dictionary<String, Any>
                                let pwschange = custProfile?.stringValue(ofKey: "pwschange")
                                if pwschange != "1" {
                                    self.logoutAndShowFacebookLoginView()
                                }
                                else {
                                    let loginData = LoginData.sharedLoginData()
                                    loginData.setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                                    
                                    UserDefaults.standard.set(VERSION_CODE, forKey: SAVED_VERSION_CODE)
                                }
                            }
                            else {
                                TSTAPP2_Utilities.logout(completion: { 
                                    
                                }, failure: { 
                                    
                                })
//                                let vc = self.window?.rootViewController
//                                let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
//                                    self.logoutAndShowFacebookLoginView()
//                                })
//                                TSTAPP2_Utilities.showAlert(withMessage: "請重新登入帳號", action: action, viewController: vc!)
                            }
                        }, failure: { (error: Error) in
                            UserDefaults.standard.removeObject(forKey: PAUSE_TIME)
                        })
                    }
                    else {
                        let extInfoDic = UserDefaults.standard.dictionary(forKey: FACEBOOK_LOGIN_DATA)
                        
                        if extInfoDic != nil {
                            
                            let id = extInfoDic!.stringValue(ofKey: "id")
                            let token = extInfoDic!.stringValue(ofKey: "token")
                            let email = extInfoDic!.stringValue(ofKey: "email")
                            let name = extInfoDic!.stringValue(ofKey: "name")
                            let birthday = extInfoDic!.stringValue(ofKey: "birthday")
                            let gender = extInfoDic!.stringValue(ofKey: "gender")
                            
                            TSTAPP2_API.sharedInstance.loginExt(withExtId: id, extType: "facebook", extToken: token, email: email, name: name, birthday: birthday, gender: gender, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                                if dic.stringValue(ofKey: "code") == "00000" {
                                    
                                    if let apiData = dic["data"] as? Dictionary<String, Any> {
                                        let custProfile = apiData["custProfile"] as? Dictionary<String, Any>
                                        let pwschange = custProfile?.stringValue(ofKey: "pwschange")
                                        if pwschange != "1" {
                                            self.logoutAndShowFacebookLoginView()
                                        }
                                        else {
                                            let loginData = LoginData.sharedLoginData()
                                            loginData.setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                                            
                                            UserDefaults.standard.set(VERSION_CODE, forKey: SAVED_VERSION_CODE)
                                        }
                                    }
                                    else {
                                        UserDefaults.standard.removeObject(forKey: FACEBOOK_LOGIN_DATA)
                                        UserDefaults.standard.synchronize()
                                    }
                                }
                                else {
                                    TSTAPP2_Utilities.logout(completion: {
                                        
                                    }, failure: {
                                        
                                    })
//                                    let vc = self.window?.rootViewController
//                                    let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
//                                        self.logoutAndShowFacebookLoginView()
//                                    })
//                                    TSTAPP2_Utilities.showAlert(withMessage: "請重新登入帳號", action: action, viewController: vc!)
                                }
                            }, failure: { (error: Error) in
                                
                            })
                        }
                    }
                }
            }
        }
        TSTAPP2_API.sharedInstance.getUrlMap(showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000"{
                let urlMap = UrlMap.sharedInstance
                urlMap.setDataValue(withDic: dic)
            }
        }) { (error: Error) in
            
        }
        
        functionStatus()
        appVersionInfo()
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("Did Register for Remote Notifications With Device Token \(deviceToken)")
        
        Messaging.messaging().apnsToken = deviceToken
        
        let token = Messaging.messaging().fcmToken != nil ? Messaging.messaging().fcmToken! : ""
        
//        let token = deviceToken.toHexString()//deviceToken.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
//        token = token.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        if LoginData.isLogin() {
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: token, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                
            }) { (error: Error) in
                
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Did Register for Remote Notifications Fail")
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        if LoginData.isLogin() {
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: "", showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                
            }) { (error: Error) in
                
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if application.applicationState != UIApplicationState.active {
            if let action = userInfo["action"] as? String {
                var actionParam = ""
                if ((userInfo["actionParam"] as? String) != nil) {
                    actionParam = userInfo["actionParam"] as! String
                }
                AutomaticViewChange.sharedInstance.setTabIndex(withAction: action, actionParam: actionParam)
                print("action \(action) actionParam \(actionParam)")
            }
            
            if let seq = userInfo["seq"] as? String {
                let loginData = LoginData.sharedLoginData()
                TSTAPP2_API.sharedInstance.updateReadStatus(withMSISDN: loginData.custProfile_msisdn, messageSeq: seq, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    
                }, failure: { (error: Error) in
                    
                })
            }
        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if application.applicationState != UIApplicationState.active {
            if let action = notification.userInfo?["action"] as? String {
                
                if let seq = notification.userInfo?["seq"] as? String {
                    TSTAPP2_API.sharedInstance.updateReadStatus(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, messageSeq: seq, showProgress: false, completion: { (dic, response) in
                        
                    }, failure: { (error) in
                        
                    })
                }
                
                AutomaticViewChange.sharedInstance.setTabIndex(withAction: action, actionParam: notification.userInfo?["actionParam"] as? String)
            }
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        TSTAPP2_Utilities.changeFunctionPage(url: url)
        
        if #available(iOS 9.0 , *){
            let sourceApplication: String? = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication, annotation: nil)
        }
        
        return true
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        TSTAPP2_Utilities.changeFunctionPage(url: url)
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        if #available(iOS 10.0, *) {
            self.saveContext()
        } else {
            // Fallback on earlier versions
        }
        
        if UserDefaults.standard.bool(forKey: KEEP_LOGIN) == false {
            TSTAPP2_Utilities.logout(completion: {
                
            }, failure: {
                
            })
        }
        
        let pauseTime = 1
        UserDefaults.standard.set(pauseTime, forKey: PAUSE_TIME)
        
        
        UserDefaults.standard.removeObject(forKey: API_CACHED_DATA)
        UserDefaults.standard.synchronize()
        
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: nil)
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "TSTAPP2")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    @available(iOS 10.0, *)
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
//    MARK: - AppVersionInfo
    func appVersionInfo() {
        UserDefaults.standard.set(VersionStatus.noUpdateStatus.rawValue, forKey: VERSION_STATUS)
        TSTAPP2_API.sharedInstance.getAppVersionInfo(withCompletion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "status") == "00000" {
                let versionArray = dic["data"] as? Array<Dictionary<String, Any>>
                var versionDic: Dictionary<String, Any>?
                
                guard versionArray != nil else {
                    return
                }
                
                guard versionArray!.count > 0 else {
                    return
                }
                
                versionDic = versionArray![0]
                print("versionDic \(String(describing: versionDic))")
                
                let minVerCode = Int(versionDic!.stringValue(ofKey: "minVerCode"))!
                let curVerCode = Int(versionDic!.stringValue(ofKey: "curVerCode"))!
                
                if VERSION_CODE < minVerCode {
                    UserDefaults.standard.set(VersionStatus.forceUpdate.rawValue, forKey: VERSION_STATUS)
                }
                else if VERSION_CODE < curVerCode {
                    UserDefaults.standard.set(VersionStatus.remindUpdate.rawValue, forKey: VERSION_STATUS)
                }
                else if VERSION_CODE > curVerCode {
                    UserDefaults.standard.set(VersionStatus.notReleased.rawValue, forKey: VERSION_STATUS)
                }
                else {
                    UserDefaults.standard.set(VersionStatus.noUpdateStatus.rawValue, forKey: VERSION_STATUS)
                }
                UserDefaults.standard.synchronize()
                
                
                let alertFlag = versionDic!["alert"] as? String
                
                if alertFlag == "1" {
                    let version = versionDic!.stringValue(ofKey: "curVer")
                    
                    var title = versionDic!.stringValue(ofKey: "title")
                    if title == "" {
                        title = "版本更新"
                    }
                    var message = versionDic!.stringValue(ofKey: "message")
                    if message == "" {
                        message = "請至App Store下載最新版本\nV\(version)"
                    }
                    
//                    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
                    let alert = UIAlertController(title: versionDic!.stringValue(ofKey: "title"), message: nil, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let openAppStore = UIAlertAction(title: "確定", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
                        let appStoreURL = URL(string: "https://itunes.apple.com/app/id937699766")!
                        UIApplication.shared.openURL(appStoreURL)
                    })
                    
                    let status = UserDefaults.standard.value(forKey: VERSION_STATUS) as! Int
                    
                    if status == VersionStatus.forceUpdate.rawValue {
                        alert.addAction(openAppStore)
                    }
                    else if status == VersionStatus.remindUpdate.rawValue {
                        let remindMeLater = UIAlertAction(title: "稍後提醒", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
                            
                        })
                        alert.addAction(remindMeLater)
                        alert.addAction(openAppStore)
                    }
                    else if status == VersionStatus.notReleased.rawValue {
                        
                    }
                    else if status == VersionStatus.noUpdateStatus.rawValue {
                        
                    }
                    
                    if status < VersionStatus.notReleased.rawValue {
//                        self.window?.rootViewController?.present(alert, animated: true, completion: {
//                            
//                        })
                        
                        let storyBoard = UIStoryboard(name: "TStarWebView", bundle: nil)
                        let webViewVC = storyBoard.instantiateViewController(withIdentifier: "TstarWebViewController") as! TstarWebViewController
                        webViewVC.preferredContentSize.height = 300
                        alert.setValue(webViewVC, forKey: "contentViewController")
                        self.window?.rootViewController?.present(alert, animated: true, completion: {
                            webViewVC.contentWebView.backgroundColor = UIColor.clear
                            webViewVC.contentWebView.scalesPageToFit = false
                            webViewVC.contentWebView.loadHTMLString(versionDic!.stringValue(ofKey: "message"), baseURL: nil)
                        })

                    }
                }
            }
        }, failure: { (error: Error) in
            
        })
    }
    
    func functionStatus() {
        TSTAPP2_API.sharedInstance.getFuncStatus(withCompletion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                if let data = dic["data"] as? Dictionary<String, Any> {
                    UserDefaults.standard.set(data, forKey: FUNCTION_STATUS)
                    UserDefaults.standard.synchronize()
                    
                    let (shutdownStatus, shutdownTitle, shutdownMessage) = TSTAPP2_Utilities.isFunctionShutdown()
                    if shutdownStatus {
                        let sVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShutdownViewController") as! ShutdownViewController
                        
                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            
                            // topController should now be your topmost view controller
                            print("\(topController)")
                            
                            sVC.shutdownMessage = "\(shutdownTitle)\n\n\(shutdownMessage)"
                            
                            if topController.isKind(of: UIAlertController.self) {
                                (topController as! UIAlertController).dismiss(animated: true, completion: {
                                    self.window?.rootViewController?.present(sVC, animated: true, completion: {
                                        print("SHUTDOWN\n\(shutdownTitle)\n\(shutdownMessage)")
                                    })
                                })
                            }
                            else {
                                self.window?.rootViewController?.present(sVC, animated: true, completion: {
                                    print("SHUTDOWN\n\(shutdownTitle)\n\(shutdownMessage)")
                                })
                            }
                        }
                    }
                    else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: DISMISS_SHUTDOWN), object: nil)
                    }
                    
                    let (maintainStatus, maintainTitle, maintainMessage) = TSTAPP2_Utilities.isFunctionMaintain()
                    UserDefaults.standard.set([maintainStatus, maintainTitle, maintainMessage], forKey: MAINTAIN_INFO)
                    UserDefaults.standard.synchronize()
                }
            }
        }, failure: { (error: Error) in
            
        })
    }
    
//    MARK: - Logout and show FacebookLoginView
    private func logoutAndShowFacebookLoginView() {
        TSTAPP2_Utilities.logout(completion: { 
            let vc = self.window?.rootViewController
            
            let navigationVC = vc as? UINavigationController
            
            guard navigationVC?.viewControllers.count == 2 else {
                return
            }
            
            if !(navigationVC!.viewControllers[1].isKind(of: FacebookLoginViewController.self)) {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as UIViewController
                vc?.show(viewController, sender: nil)
            }
        }) { 
            
        }
    }
    
//    MARK: - Check ServiceOrder
    func checkServiceOrder() {
        
        let time = UserDefaults.standard.double(forKey: SERVICE_ORDER_SEND_TIME)
        
        guard (NSDate().timeIntervalSince1970 - time) > 86400 && isAppUpdated() else {
            return
        }
        
        ServicesViewController().setupCellDataArray()
        
        if let savedData = UserDefaults.standard.value(forKey: SERVICE_ORDER) as? [[[String]]] {
        
            let presetServiceOrder = ServicesViewController().generateServiceDataArray()
            var sectionIndex = 0
            var itemIndex = 0
            var sameOrder = true
            
            for section in savedData {
                if sameOrder {
                    itemIndex = 0
                    for item in section {
                        if item == presetServiceOrder[sectionIndex][itemIndex] {
                            
                        }
                        else {
                            sameOrder = false
                            break
                        }
                        itemIndex += 1
                    }
                    sectionIndex += 1
                }
            }
            
            print("sameOrder \(sameOrder)")
            
            let serviceOrderSendTime = NSDate().timeIntervalSince1970
            UserDefaults.standard.set(serviceOrderSendTime, forKey: SERVICE_ORDER_SEND_TIME)
            
            Answers.logContentView(withName: "常用服務排列狀態",
                                   contentType: "Services",
                                   contentId: "ServiceOrder",
                                   customAttributes: ["ServiceOrder Changed": sameOrder ? "NO" : "YES"])
        }
        else {
            Answers.logContentView(withName: "常用服務排列狀態",
                                   contentType: "Services",
                                   contentId: "ServiceOrder",
                                   customAttributes: ["ServiceOrder Changed": "NO"])
        }
    }
    
//    MARK: - 
    func isAppUpdated() -> Bool {
        
        var updated = false
        let appVersion = UserDefaults.standard.value(forKey: APP_VERSION)
        
        if appVersion == nil {
            updated = true
            UserDefaults.standard.set(VERSION_CODE, forKey: APP_VERSION)
            UserDefaults.standard.synchronize()
        }
        else {
            if VERSION_CODE > (appVersion! as! Int) {
                updated = true
                UserDefaults.standard.set(VERSION_CODE, forKey: APP_VERSION)
                UserDefaults.standard.synchronize()
            }
        }
        print("isUpdated \(updated)")
        return updated
    }

}

