//
//  AppliedAddValue.swift
//  TSTAPP2
//
//  Created by Tony on 2017/7/17.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class AppliedAddValue: NSObject {
    
    static let sharedInstance = AppliedAddValue()
    
    private(set) var appliedAddValueSvcList: [AddValueSvc] = []
    
    private(set) var promoAddValueSvcList: [AddValuePromo] = []
    
    ///小標題
    private(set) var subtitle: String = ""
    
    ///標題
    private(set) var title: String = ""
    
    ///內容
    private(set) var content: String = ""
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "code") == "00000" {
            
            appliedAddValueSvcList = []
            promoAddValueSvcList = []
            
            let apiData = dic["data"] as! Dictionary<String, Any>
            
            subtitle = apiData.stringValue(ofKey: "subtitle")
            title = apiData.stringValue(ofKey: "title")
            content = apiData.stringValue(ofKey: "description")
            
            if let appliedAddValues = apiData["appliedAddValueSvcList"] as? Array<Dictionary<String, Any>> {
                appliedAddValues.forEach({ (appliedAddValue: [String : Any]) in
                    self.appliedAddValueSvcList.append(AddValueSvc(withDic: appliedAddValue))
                })
            }
            
            if let promoAddValues = apiData["promoAddValueSvcList"] as? Array<Dictionary<String, Any>> {
                promoAddValues.forEach({ (promoAddValue: [String: Any]) in
                    self.promoAddValueSvcList.append(AddValuePromo(withDic: promoAddValue))
                })
            }
        }
    }
}

class AddValuePromo: NSObject {
    
    private(set) var svcName: String = ""
    
    private(set) var svcUrl: String = ""
    
    private(set) var buttonText: String = ""
    
    private(set) var svcGACode: String = ""
    
    private(set) var svcUBAActionCode: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        self.svcName = dic["svcName"] as? String ?? ""
        self.svcUrl = dic["svcUrl"] as? String ?? ""
        self.buttonText = dic["buttonText"] as? String ?? ""
        self.svcGACode = dic["svcGACode"] as? String ?? ""
        self.svcUBAActionCode = dic["svcUBAActionCode"] as? String ?? ""
    }
}

class AddValueSvc: NSObject {
    
    private(set) var showCancelButton: Bool = false
    
    private(set) var svcId: String = ""
    
    private(set) var svcName: String = ""
    
    private(set) var systemType: String = ""
    
    private(set) var svcMessage: String = ""
    
    private(set) var contentList: [AddValueContent] = []
    
    init(withDic dic: Dictionary<String, Any>) {
        self.showCancelButton = dic["showCancelButton"] as? String == "Y" ? true : false
        self.svcId = dic["svcId"] as? String ?? ""
        self.svcName = dic["svcName"] as? String ?? ""
        self.systemType = dic["systemType"] as? String ?? ""
        self.svcMessage = dic["svcMessage"] as? String ?? ""
        
        if let content = dic["contentList"] as? Array<Dictionary<String, Any>> {
            var contents: [AddValueContent] = []
            content.forEach({ (data: [String : Any]) in
                contents.append(AddValueContent(withDic: data))
            })
            self.contentList = contents
        }

    }
    
}

class AddValueContent: NSObject {
    
    private(set) var desc: String = ""
    
    private(set) var text: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        self.desc = dic["desc"] as? String ?? ""
        self.text = dic["text"] as? String ?? ""
    }
    
}
