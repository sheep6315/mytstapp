//
//  ApplyEbillViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/5/2.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ApplyEbillViewController: TSUIViewController {
    
    @IBOutlet weak var msisdn: UILabel!
    
    @IBOutlet weak var currentEmail: UILabel!
    
    @IBOutlet weak var changeEmail: UITextField!
    
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var email: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        currentEmail.text = email
        msisdn.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: LoginData.sharedLoginData().custProfile_msisdn, type: encodeForSecurityType.Phone)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkVIPStatusAndChangeSkin()
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP020104", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "修改Email")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP020104", action: "")
    }
    
//    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: false, notification: notification)
    }
    
    func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        self.scrollView.endEditing(true)
        
    }
    
//    MARK: -
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    func callChangeMailAPI() {
        
        let loginData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.changeMailAddress(withMSISDN: loginData.custProfile_msisdn, newEmailAddress: changeEmail.text!, contractId: loginData.custProfile_contractId, accountId: loginData.custProfile_accountId, showProgress: true, completion: { (dic :Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                var resultMessage = ""
                if let data = dic["data"] as? Dictionary<String, Any> {
                    resultMessage = data.stringValue(ofKey: "resultMessage")
                }
                
                AccountInformation_CallGetBillTypeInfo = true
                
                TSTAPP2_Utilities.showAlertAndPop(withTitle:"完成修改", message: resultMessage, viewController: self)
                
//                TSTAPP2_API.sharedInstance.getBillTypeInfo(withMsisdn: loginData.custProfile_msisdn, isMainMsisdn: loginData.custProfile_isMainMsisdn, showProgress: true, completion: { (dic, response) in
//                    if dic.stringValue(ofKey: "code") == "00000" {
//                        if let _ = dic["data"] as? Dictionary<String, Any> {
//                            loginData.setBillTypeDataValue(withDic: dic)
//                        }
//                    }
//                    
//                    TSTAPP2_Utilities.showAlertAndPop(withTitle:"完成修改", message: resultMessage, viewController: self)
//                    
//                }, failure: { (error) in
//                    TSTAPP2_Utilities.showAlertAndPop(withTitle:"完成修改", message: resultMessage, viewController: self)
//                })
                
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }, failure: { (Error) in
            
            
        })
    }
    
    @IBAction func didTapSubmit(_ sender: Any) {
        
        let verifyRules: [Dictionary<String, Any>] = [
            [
                "attribute": "email",
                "label": "Email",
                "value": self.changeEmail.text!,
                "rules": ["required", "email"]
            ]
        ]
        
        let validator = Validators(withRules: verifyRules)
        do {
            let verifyResult = try validator.verify()
            if verifyResult == true {
                
                UbaRecord.sharedInstance.sendUba(withPage: "APP020104", action: "A040101")
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改Email(會員中心)", action: nil, label: nil, value: nil)
                callChangeMailAPI()
            }
        } catch {
            
            TSTAPP2_Utilities.showAlert(withMessage: validator.errors.first!, viewController: self)
        }
    }
}
