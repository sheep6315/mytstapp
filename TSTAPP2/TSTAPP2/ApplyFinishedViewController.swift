//
//  ApplyFinishedViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/24.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ApplyFinishedViewController: TSUIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.setHidesBackButton(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func didTapSubmit(_ sender: Any) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        self.navigationController?.show(storyBoard.instantiateInitialViewController()!, sender: self)
        
//        let _ = self.navigationController?.popToRootViewController(animated: true)

    }

}
