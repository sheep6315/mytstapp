//
//  AutomaticViewChange.swift
//  TSTAPP2
//
//  Created by Tony on 2016/12/11.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let AUTO_BROWSER = "browser"

//首頁
let AUTO_LANDING = "main"
let AUTO_LOGIN = "login"
let AUTO_WEBVIEW = "webView"
let AUTO_MEMBERCENTER = "memberCenter"

//我的帳戶
let AUTO_MYPACKAGE = "account"
let AUTO_IVRQUALIFY = "ivrQualify"
let AUTO_CONTRACTTERM = "contractTerm"
let AUTO_CONTRACT = "contract"

//我的帳單
let AUTO_BILLING = "billing"
let AUTO_PAYMENT_BARCODE = "paymentBarcode"
let AUTO_PAYMENT_BANK = "paymentBank"
let AUTO_PAYMENT_CREDITCARD = "paymentCreditCard"

//優惠專區
let AUTO_MEMBERONLY = "memberOnly"

//常用服務
let AUTO_CUSTOMSERVICE = "customService"
let AUTO_BASICSERVICE = "basicService"
let AUTO_ROAMING = "roaming"
let AUTO_TICKETQUERY = "ticketQuery"
let AUTO_VOLTE = "volte"
let AUTO_CREDITCARDTRANSFER = "creditCardTransfer"
let AUTO_EBILLAPPLY = "eBillApply"
let AUTO_TCLUB = "tclub"
let AUTO_ORDERQUERY = "orderQuery"

let AUTO_PAYMENTRECORD = "paymentRecord"
let AUTO_EINVOICE = "eInvoice"


enum TabIndex: Int {
    case landing = 0
    case myAccount
    case myBill
    case memberOnly
    case services
}

enum FunctionIndex: Int {
    
    case browser = 0
    
    case login
    case webView
    case memberCenter
    
    case account
    case ivrQualify
    case contractTerm
    case contract
    
    case billing
    case paymentBarcode
    case paymentBank
    case paymentCreditCard
    
    case customService
    case basicService
    case roaming
    case ticketQuery
    case volte
    case creditCardTransfer
    case eBillApply
    case tclub
    case orderQuery
    
//    case paymentRecord
//    case eInvoice

}


class IndexOfChange: NSObject {
    var tabIndex = -1
    var functionIndex = -1
    
    func set(tabIndex newTabIndex: Int, newFunctionIndex: Int) -> IndexOfChange {
        tabIndex = newTabIndex
        functionIndex = newFunctionIndex
        
        return self
    }
}

class AutomaticViewChange: NSObject {
    
    static let sharedInstance = AutomaticViewChange()
    var actionParam: String = ""
    
    private override init() {
        //This prevents others from using the default '()' initializer for this class.
    }
    
    var tabBarVC: TabBarViewController? {
        didSet {
            changeTab()
        }
    }
    
    var landingVC: LandingViewController? {
        didSet {
            changeFunction(landingVC!)
        }
    }
    
    var myAccountVC: MyAccountViewController? {
        didSet {
            changeFunction(myAccountVC!)
        }
    }

    var myBillVC: MyBillViewController? {
        didSet {
            changeFunction(myBillVC!)
        }
    }

    var servicesVC: ServicesViewController? {
        didSet {
            changeFunction(servicesVC!)
        }
    }
    
    var index: IndexOfChange = IndexOfChange() {
        didSet {
            changeTab()
        }
    }
    
    func setTabIndex(withAction action: String, actionParam: String?) {
        switch action {
            
        case AUTO_BROWSER:
            if actionParam != nil {
                if let url = URL(string: actionParam!) {
                    UIApplication.shared.openURL(url)
                }
            }
            
        case AUTO_LANDING:
            index = IndexOfChange().set(tabIndex: TabIndex.landing.rawValue, newFunctionIndex: -1)
        case AUTO_LOGIN:
            index = IndexOfChange().set(tabIndex: TabIndex.landing.rawValue, newFunctionIndex: FunctionIndex.login.rawValue)
        case AUTO_WEBVIEW:
            self.actionParam = actionParam ?? ""
            index = IndexOfChange().set(tabIndex: TabIndex.landing.rawValue, newFunctionIndex: FunctionIndex.webView.rawValue)
        case AUTO_MEMBERCENTER:
            index = IndexOfChange().set(tabIndex: TabIndex.landing.rawValue, newFunctionIndex: FunctionIndex.memberCenter.rawValue)
            
        case AUTO_MYPACKAGE:
            index = IndexOfChange().set(tabIndex: TabIndex.myAccount.rawValue, newFunctionIndex: -1)
        case AUTO_IVRQUALIFY:
            index = IndexOfChange().set(tabIndex: TabIndex.myAccount.rawValue, newFunctionIndex: FunctionIndex.ivrQualify.rawValue)
        case AUTO_CONTRACTTERM:
            index = IndexOfChange().set(tabIndex: TabIndex.myAccount.rawValue, newFunctionIndex: FunctionIndex.contractTerm.rawValue)
        case AUTO_CONTRACT:
            index = IndexOfChange().set(tabIndex: TabIndex.myAccount.rawValue, newFunctionIndex: FunctionIndex.contract.rawValue)
            
        case AUTO_BILLING:
            index = IndexOfChange().set(tabIndex: TabIndex.myBill.rawValue, newFunctionIndex: -1)
        case AUTO_PAYMENT_BARCODE:
            index = IndexOfChange().set(tabIndex: TabIndex.myBill.rawValue, newFunctionIndex: FunctionIndex.paymentBarcode.rawValue)
        case AUTO_PAYMENT_BANK:
            index = IndexOfChange().set(tabIndex: TabIndex.myBill.rawValue, newFunctionIndex: FunctionIndex.paymentBank.rawValue)
        case AUTO_PAYMENT_CREDITCARD:
            index = IndexOfChange().set(tabIndex: TabIndex.myBill.rawValue, newFunctionIndex: FunctionIndex.paymentCreditCard.rawValue)
            
        case AUTO_MEMBERONLY:
            index = IndexOfChange().set(tabIndex: TabIndex.memberOnly.rawValue, newFunctionIndex: -1)
            
//        case AUTO_PAYMENTRECORD:
//            index = IndexOfChange().set(tabIndex: TabIndex.landing.rawValue, newFunctionIndex: FunctionIndex.webView.rawValue)
//        case AUTO_EINVOICE:
//            index = IndexOfChange().set(tabIndex: TabIndex.landing.rawValue, newFunctionIndex: FunctionIndex.webView.rawValue)
            
        case AUTO_CUSTOMSERVICE:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: -1)
            
        case AUTO_BASICSERVICE:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.basicService.rawValue)
        
        case AUTO_ROAMING:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.roaming.rawValue)
        case AUTO_TICKETQUERY:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.ticketQuery.rawValue)
        case AUTO_VOLTE:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.volte.rawValue)
        case AUTO_CREDITCARDTRANSFER:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.creditCardTransfer.rawValue)
        case AUTO_EBILLAPPLY:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.eBillApply.rawValue)
        case AUTO_TCLUB:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.tclub.rawValue)
        case AUTO_ORDERQUERY:
            index = IndexOfChange().set(tabIndex: TabIndex.services.rawValue, newFunctionIndex: FunctionIndex.orderQuery.rawValue)
            
        default:
            index = IndexOfChange().set(tabIndex: -1, newFunctionIndex: -1)
        }
    }
    
    func changeTab() {
        if tabBarVC != nil && index.tabIndex != -1 {
            backToRoot(index.tabIndex)
        }
    }
    
    func backToRoot(_ tabIndex: Int) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller
            print("\(topController)")
            
            if topController.isKind(of: UIAlertController.self) {
                (topController as! UIAlertController).dismiss(animated: true, completion: {
                    self.changeToTab(tabIndex)
                })
            }
            else if topController.isKind(of: TstarWebViewController.self) {
                (topController as! TstarWebViewController).dismiss(animated: true, completion: {
                    self.changeToTab(tabIndex)
                })
            }
            else if topController.isKind(of: TStarFullScreenViewController.self) {
                UserDefaults.standard.set(true, forKey: "DID_SHOW_SERVICE_TUTORIAL")
                (topController as! TStarFullScreenViewController).dismiss(animated: true, completion: { 
                    self.changeToTab(tabIndex)
                })
            }
            else {
                if tabBarVC?.selectedIndex == tabIndex {
                    postNotification(tabIndex)
                }
                else {
                    changeToTab(tabIndex)
                    postNotification(tabIndex)
                }
            }
        }
    }
    
    func changeToTab(_ tabIndex: Int) {
        tabBarVC?.selectedIndex = tabIndex
        tabBarVC?.currentTab = (tabBarVC?.tabBar.items?[tabIndex].title)!
        index.tabIndex = -1
    }
    
    private func changeFunction(_ VC: UIViewController) {
        guard index.functionIndex != -1 else {
            return
        }
        
        DispatchQueue.main.async {
            
            if let landingVC = VC as? LandingViewController {
                print("\(landingVC)")
                if self.index.functionIndex == FunctionIndex.webView.rawValue {
                    TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: self.actionParam, viewController: landingVC)
                }
                else if self.index.functionIndex == FunctionIndex.login.rawValue {
                    if !LoginData.isLogin() {
                        TSTAPP2_Utilities.pushToLoginViewController(withViewController: landingVC)
                    }
                }
                else if self.index.functionIndex == FunctionIndex.memberCenter.rawValue {
                    if LoginData.isLogin() {
                        landingVC.didTapAccountInfoButton(sender: nil)
                    }
                }
                self.index.functionIndex = -1
            }
            else if let myAccountVC = VC as? MyAccountViewController {
                print("\(myAccountVC)")
                if self.index.functionIndex == FunctionIndex.ivrQualify.rawValue {
                    
                }
                else if self.index.functionIndex == FunctionIndex.contractTerm.rawValue {
                    
                }
                else if self.index.functionIndex == FunctionIndex.contract.rawValue {
                    myAccountVC.performSegue(withIdentifier: "toMyContract", sender: nil)
                }
                
                self.index.functionIndex = -1
            }
            else if let myBillVC = VC as? MyBillViewController {
                print("\(myBillVC)")
                if self.index.functionIndex == FunctionIndex.paymentBarcode.rawValue {
                    myBillVC.performSegue(withIdentifier: "toPaymentBarcode", sender: nil)
                }
                else if self.index.functionIndex == FunctionIndex.paymentBank.rawValue {
                    myBillVC.performSegue(withIdentifier: "toPaymentBank", sender: nil)
                }
                else if self.index.functionIndex == FunctionIndex.paymentCreditCard.rawValue {
                    myBillVC.performSegue(withIdentifier: "toPaymentCreditCard", sender: nil)
                }
                self.index.functionIndex = -1
            }
            else if let servicesVC = VC as? ServicesViewController {
                if self.index.functionIndex == FunctionIndex.basicService.rawValue {
                    servicesVC.didTapBasicServiceButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.roaming.rawValue {
                    servicesVC.didTapRoamingButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.ticketQuery.rawValue {
                    servicesVC.didTapTicketQueryButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.volte.rawValue {
                    servicesVC.didTapVOLTEButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.creditCardTransfer.rawValue {
                    servicesVC.didTapCreditCardTransferButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.eBillApply.rawValue {
                    servicesVC.didTapEbillButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.tclub.rawValue {
                    servicesVC.didTapTClubButton(UIButton())
                }
                else if self.index.functionIndex == FunctionIndex.orderQuery.rawValue {
                    servicesVC.didTapOrderQueryButton(UIButton())
                }
                self.index.functionIndex = -1
            }
        }
    }
    
    func postNotification(_ tabIndex: Int) {
        switch tabIndex {
        case TabIndex.landing.rawValue:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LANDING), object: nil)
        case TabIndex.myAccount.rawValue:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: MY_ACCOUNT), object: nil)
        case TabIndex.myBill.rawValue:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: MY_BILL), object: nil)
        case TabIndex.services.rawValue:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SETTING), object: nil)
        default:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LANDING), object: nil)
        }
    }
}
