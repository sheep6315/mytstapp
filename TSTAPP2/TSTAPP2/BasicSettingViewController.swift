//
//  BasicSettingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/7.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class BasicCell: UITableViewCell {
    
    @IBOutlet weak var serviceName: UILabel!
    
    @IBOutlet weak var switchButton: UISwitch!
}

class BasicSettingViewController: TSUIViewController ,UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var serviceList :Array<Services> = []
    var changeSerivceList :Array<Services> = []
    var checkBoxOn :Bool = false
    var apiString :String = ""
    var alertString :String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        callGetServiceStatusAPI()
        // Do any additional setup after loading the view.
    }
    
    deinit{
        tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0501", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "基本服務設定", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "基本服務設定")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0501", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    //MARK: - IBAction
    
    @IBAction func didTapCheck(_ sender: Any) {
        
        if checkBoxOn {
            checkButton.setImage(UIImage.init(named: "checkbox_inactive"), for: .normal)
        }
        else {
            checkButton.setImage(UIImage.init(named: "checkbox_active"), for: .normal)
            TSTAPP2_Utilities.showAlert(withMessage: "1.申請本服務，無需收取服務費用。\n2.本公司目前提供002/005/006/007/009/012/015/016/017/019等固網業者國際語音電話冠碼撥接服務。\n3.撥打國際影像電話之「國際冠碼」，可使用「＋」、「002」、「006」或「009」，於依上述操作方式完成號碼輸入後，按「影像發話鍵」。", viewController: self)
        }
        
        checkBoxOn = !checkBoxOn
    }
    
    @IBAction func didTapSubmit(_ sender: Any) {
        
        if !checkBoxOn {
            
            TSTAPP2_Utilities.showAlert(withMessage: "確定要變更服務？\n請勾選「本人已知悉並同意上述相關業務事項」", viewController: self)
        }
        else {
            
            let alert = UIAlertController(title: "", message: "是否變更服務狀態", preferredStyle: .alert)
            let submit = UIAlertAction(title: "確認", style: .default, handler: { (UIAlertAction) in
                
                self.apiString = ""
                self.alertString = ""
                
                if self.changeSerivceList.count == 0 {
                    
                    TSTAPP2_Utilities.showAlert(withMessage: "無更改任何基本服務！", viewController: self)
                }
                else {
                    
                    let dic = ["1-1XMD-1":"A030115"
                        ,"1-A5-13":"A030102"
                        ,"1-A5-14":"A030103"
                        ,"1-A5-15":"A030104"
                        ,"1-A5-16":"A030105"
                        ,"1-A5-18":"A030106"
                        ,"1-A5-27":"A030107"
                        ,"1-A5-32":"A030108"
                        ,"1-A5-35":"A030109"
                        ,"1-3SR-1":"A030110"
                        ,"1-A5-48":"A030111"
                        ,"1-A5-34":"A030112"
                        ,"1-A5-33":"A030113"
                        ,"1-APJ-2":"A030114"
                        ,"1-2V6E-1":"A030116"
                        ,"1-3677-1":"A030117"
                        ,"1-42G-1":"A030118"
                        ,"1-9ZF-1":"A030119"]
                    
                    for service in self.changeSerivceList {
                        
                        UbaRecord.sharedInstance.sendUba(withPage: "APP0501", action: dic.stringValue(ofKey: service.servNo))
                        
                        let status = service.status == "1" ? "ON" : "OFF"
                        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "基本服務設定", action: "\(service.servName)", label: status, value: nil)
                        
                        if self.apiString == "" {
                            
                            self.apiString = "\(service.servName),\(service.servFeeType),\(service.status)"
                            self.alertString = "[\(service.servName)]"
                        }
                        else {
                            
                            self.apiString = "\(self.apiString);\(service.servName),\(service.servFeeType),\(service.status)"
                            self.alertString = "\(self.alertString)\n[\(service.servName)]"
                        }
                    }
                    
//                    self.alertString = "\(self.alertString)\n\n申請/取消服務已送出,系統將於 1 小時內為您處 理完成"
                    self.callServiceStatusChangeAPI()
                }
            })
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            alert.addActions(array: [submit,cancel])
            alert.show(viewController: self)
        }
        
    }
    
    @objc func didTapCellSwitch(_ sender: AnyObject) {
        
        let switchButton = sender as! UISwitch
        let service = serviceList[switchButton.tag]
        let serviceStatus = service.status
        let switchStatus = switchButton.isOn ? "1":"0"
        let index = changeSerivceList.index(of: serviceList[switchButton.tag])
        
        if serviceStatus != switchStatus && index == nil{
            
            service.status = switchStatus
            changeSerivceList.append(service)
        }
        else if index != nil {
            
            changeSerivceList.remove(at: index!)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "contentSize" {
            
            let height = tableView.contentSize.height
            
            if tableViewHeight.constant != height {
//                tableViewHeight.constant = height
            }
        }
    }

    
    //MARK:- API
    func callGetServiceStatusAPI() {
        
        let loginData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.queryServiceStatus(withMSISDN: loginData.custProfile_msisdn, custId: loginData.custProfile_custId, companyId: loginData.custProfile_companyId, contractId: loginData.custProfile_contractId, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let service = ServiceStatus.sharedInstance
                service.setDataValue(withDic: dic)
                
                self.serviceList = ServiceStatus.sharedInstance.servicesList
                
                
                self.tableViewHeight.constant = CGFloat(self.serviceList.count) * 49.0
                self.view.setNeedsUpdateConstraints()
                
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                    self.view.layoutIfNeeded()
                    self.tableView.reloadSections([0], with: UITableViewRowAnimation.top)
                }, completion: { (isComplete: Bool) in
                    
                })
            }
            else {
                let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), action: action, viewController: self)
            }
        }) { (Error) in
            
        }
    }
    
    func callServiceStatusChangeAPI() {
        
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.changeServiceStatus(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, serviceStatus: self.apiString, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let alert = UIAlertController(title: "", message: dic.stringValue(ofKey: "message"), preferredStyle: .alert)
                let action = UIAlertAction(title: "確認", style: .cancel, handler: { (UIAlertAction) in
                    
                    let _ = self.navigationController?.popToRootViewController(animated: true)
                })

                alert.addAction(action)
                alert.show(viewController: self)
            } else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (Error) in
            
        }
    }
    
    //MARK: - tableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 49
    }
    
    //    MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.serviceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = BasicCell()
        
        // Configure the cell...
        if tableView.tag == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "basicCell", for: indexPath) as! BasicCell
            
            cell.serviceName.text = serviceList[indexPath.row].servName
            
            let status = serviceList[indexPath.row].status
            if status == "2" {
                cell.switchButton.isHidden = true
            }
            else {
                cell.switchButton.isOn = status == "1" ? true:false
            }
            cell.switchButton.tag = indexPath.row
            cell.switchButton.addTarget(self, action: #selector(didTapCellSwitch(_:)), for: UIControlEvents.valueChanged)
            UISkinChange.sharedInstance.changeUIColor(theObject: cell.switchButton)
        }
        
        return cell
    }

}
