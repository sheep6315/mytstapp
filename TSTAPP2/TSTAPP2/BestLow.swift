//
//  BestLow.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/12/1.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class BestLowProjects: NSObject {
    
    // 專案試算清單
    private(set) var projects: [BestLowProject] = []
    
    init(withDic dic: Dictionary<String, Any>) {
//        let response:Dictionary<String, Any> = dic["GetBlRes"] as? [String : Any] ?? ["ResultCode": "99999", "ResultText": "API回傳格式異常"]
//        if response.stringValue(ofKey: "ResultCode") == "00000" {
//            if let respData = response["BlLists"] as? Array<Dictionary<String, Any>> {
//                var blList:[BestLow2]  = []
//                respData.forEach({ (rawData: [String : Any]) in
//                    blList.append(BestLow(withDic: rawData))
//                })
//                self.list = blList
//            }
//        }
        
        
        if dic.stringValue(ofKey: "code") == "00000" {
            if let data = dic["data"] as? [Dictionary<String, Any>] {
                projects = []
                for project in data {
                    
                    projects.append(BestLowProject.init(withDic: project))
                }
            }
        }
    }
}

class BestLowProject: NSObject {
    
    private(set) var amount: String = ""
    private(set) var bestChoice: String = ""
    private(set) var projectName: String = ""
    private(set) var detailList: [BestLowDetailList] = []
    
    init(withDic dic: Dictionary<String, Any>) {
        amount = dic.stringValue(ofKey: "amount")
        bestChoice = dic.stringValue(ofKey: "bestChoice")
        projectName = dic.stringValue(ofKey: "projectName")
        detailList = []
        if let dList = dic["detailList"] as? [Dictionary<String, Any>] {
            for detail in dList {
                detailList.append(BestLowDetailList.init(withDic: detail))
            }
        }
    }
    
}

class BestLowDetailList: NSObject {
    
    private(set) var itemList: [BestLowItemList] = []
    private(set) var name: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        itemList = []
        if let iList = dic["itemList"] as? [Dictionary<String, Any>] {
            for item in iList {
                itemList.append(BestLowItemList.init(withDic: item))
            }
        }
        name = dic.stringValue(ofKey: "name")
    }
}

class BestLowItemList: NSObject {
    
    private(set) var desc: String = ""
    private(set) var name: String = ""
    private(set) var nameText: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        desc = dic.stringValue(ofKey: "desc")
        name = dic.stringValue(ofKey: "name")
        nameText = dic.stringValue(ofKey: "nameText")
    }
    
}

class BestLowList: NSObject {
    
    // 專案試算清單
    private(set) var list: [BestLow] = []
    
    init(withDic dic: Dictionary<String, Any>) {
        let response:Dictionary<String, Any> = dic["GetBlRes"] as? [String : Any] ?? ["ResultCode": "99999", "ResultText": "API回傳格式異常"]
        if response.stringValue(ofKey: "ResultCode") == "00000" {
            if let respData = response["BlLists"] as? Array<Dictionary<String, Any>> {
                var blList:[BestLow]  = []
                respData.forEach({ (rawData: [String : Any]) in
                    blList.append(BestLow(withDic: rawData))
                })
                self.list = blList
            }
        }
    }
}

class BestLow: NSObject {
    
    // 帳號
    private(set) var accountNum: String = ""
    
    // 合約編號
    private(set) var contractId: String = ""
    
    // 批價週期
    private(set) var bc: String = ""
    
    // 專案名稱
    private(set) var projectName: String = ""
    
    // 專案月租費
    private(set) var monthly: String = ""
    
    // 專案月租費減免
    private(set) var monthlyCut: String = ""
    
    // 實收月租費
    private(set) var monthlyFinal: String = ""
    
    // 專案內含上網量
    private(set) var internetFree: String = ""
    
    // 上網傳輸量
    private(set) var internetVolumn: String = ""
    
    // 上網計費傳輸量
    private(set) var internetVolumnChg: String = ""
    
    // 4G專案:最小加購組合  / 3G專案:上網費用
    private(set) var internetVolumnMnt: String = ""
    
    // 專案內含網內語音
    private(set) var mobileOnFree: String = ""
    
    // 網內通聯時間
    private(set) var mobileOnNetDur: String = ""
    
    // 網內計費通聯時間
    private(set) var mobileOnNetChargeDur: String = ""
    
    // 網內語音費用
    private(set) var mobileOnNetMny: String = ""
    
    // 專案內含網外語音
    private(set) var mobileOffFree: String = ""
    
    // 網外通聯時間
    private(set) var mobileOffNetDur: String = ""
    
    // 網外計費通聯時間
    private(set) var mobileOffNetChargeDur: String = ""
    
    // 網外語音費用
    private(set) var mobileOffNetMny: String = ""
    
    // 專案內含市話語音
    private(set) var mobilePstnFree: String = ""
    
    // 市話通聯時間
    private(set) var mobilePstnDur: String = ""
    
    // 市話計費通聯時間
    private(set) var mobilePstnChargeDur: String = ""
    
    // 市話語音費用
    private(set) var mobilePstnMny: String = ""
    
    // 專案內含網內簡訊
    private(set) var smsOnFreeAg: String = ""
    
    // 網內簡訊則數
    private(set) var smsOnNetVolumn: String = ""
    
    // 網內計費簡訊則數
    private(set) var smsOnNetChargeVolumn: String = ""
    
    // 網內簡訊費用
    private(set) var smsOnNetMny: String = ""
    
    // 專案內含網外簡訊
    private(set) var smsOffFreeAg: String = ""
    
    // 網外簡訊則數
    private(set) var smsOffNetVolumn: String = ""
    
    // 網外計費簡訊則數
    private(set) var smsOffNetChargeVolumn: String = ""
    
    // 網外簡訊費用
    private(set) var smsOffNetMny: String = ""
    
    // 資費合計
    private(set) var amount: String = ""
    
    // 群組資費名稱
    private(set) var coProjectName: String = ""
    
    // 最適資費(Y)
    private(set) var bestChoice: Bool = false

    init(withDic dic:Dictionary<String, Any>) {
        self.accountNum = dic.stringValue(ofKey: "AccountNum")
        self.contractId = dic.stringValue(ofKey: "ContractId")
        self.bc = dic.stringValue(ofKey: "Bc")
        self.projectName = dic.stringValue(ofKey: "ProjectName")
        self.monthly = dic.stringValue(ofKey: "Monthly")
        self.monthlyCut = dic.stringValue(ofKey: "MonthlyCut")
        self.monthlyFinal = dic.stringValue(ofKey: "MonthlyFinal")
        self.internetFree = dic.stringValue(ofKey: "InternetFree")
        self.internetVolumn = dic.stringValue(ofKey: "InternetVolumn")
        self.internetVolumnChg = dic.stringValue(ofKey: "InternetVolumnChg")
        self.internetVolumnMnt = dic.stringValue(ofKey: "InternetVolumnMnt")
        self.mobileOnFree = dic.stringValue(ofKey: "MobileOnFree")
        self.mobileOnNetDur = dic.stringValue(ofKey: "MobileOnNetDur")
        self.mobileOnNetChargeDur = dic.stringValue(ofKey: "MobileOnNetChargeDur")
        self.mobileOnNetMny = dic.stringValue(ofKey: "MobileOnNetMny")
        self.mobileOffFree = dic.stringValue(ofKey: "MobileOffFree")
        self.mobileOffNetDur = dic.stringValue(ofKey: "MobileOffNetDur")
        self.mobileOffNetChargeDur = dic.stringValue(ofKey: "MobileOffNetChargeDur")
        self.mobileOffNetMny = dic.stringValue(ofKey: "MobileOffNetMny")
        self.mobilePstnFree = dic.stringValue(ofKey: "MobilePstnFree")
        self.mobilePstnDur = dic.stringValue(ofKey: "MobilePstnDur")
        self.mobilePstnChargeDur = dic.stringValue(ofKey: "MobilePstnChargeDur")
        self.mobilePstnMny = dic.stringValue(ofKey: "MobilePstnMny")
        self.smsOnFreeAg = dic.stringValue(ofKey: "SmsOnFreeAg")
        self.smsOnNetVolumn = dic.stringValue(ofKey: "SmsOnNetVolumn")
        self.smsOnNetChargeVolumn = dic.stringValue(ofKey: "SmsOnNetChargeVolumn")
        self.smsOnNetMny = dic.stringValue(ofKey: "SmsOnNetMny")
        self.smsOffFreeAg = dic.stringValue(ofKey: "SmsOffFreeAg")
        self.smsOffNetVolumn = dic.stringValue(ofKey: "SmsOffNetVolumn")
        self.smsOffNetChargeVolumn = dic.stringValue(ofKey: "SmsOffNetChargeVolumn")
        self.smsOffNetMny = dic.stringValue(ofKey: "SmsOffNetMny")
        self.amount = dic.stringValue(ofKey: "Amount")
        self.coProjectName = dic.stringValue(ofKey: "CoProjectName")
        self.bestChoice = (dic.stringValue(ofKey: "BestChoice") == "Y")
    }
}
