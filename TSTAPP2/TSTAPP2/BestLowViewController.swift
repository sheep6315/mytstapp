//
//  BestLowViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/30.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let blCellHeaderHeight: CGFloat = 45
let blCellContentHeight: CGFloat = 1260

class DetailView: UIView {
    
    @IBOutlet weak var detailNameLabel: UILabel!
}

class ItemView: UIView {
    
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescLabel: UILabel!
    
//    init(frame: CGRect, name: String, desc: String) {
//        super.init(frame: frame)
//        
//        itemNameLabel.text = name
//        itemDescLabel.text = desc
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    
}


class BestLowCell2: UITableViewCell {
    
    @IBOutlet weak var projectNameLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    var isExpand = false
}

class BestLowCell: UITableViewCell {
    
    @IBOutlet weak var projectNameLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var monthlyLabel: UILabel!
    
    @IBOutlet weak var monthlyCutLabel: UILabel!
    
    @IBOutlet weak var monthlyFinalLabel: UILabel!
    
    @IBOutlet weak var internetFreeLabel: UILabel!
    
    @IBOutlet weak var internetVolumnChgLabel: UILabel!
    
    @IBOutlet weak var internetVolumnMntTitleLabel: UILabel!
    
    @IBOutlet weak var internetVolumnMntLabel: UILabel!
    
    @IBOutlet weak var mobileOnFreeLabel: UILabel!
    
    @IBOutlet weak var mobileOnNetChargeDurLabel: UILabel!
    
    @IBOutlet weak var mobileOnNetMnyLabel: UILabel!
    
    @IBOutlet weak var mobileOffFreeLabel: UILabel!
    
    @IBOutlet weak var mobileOffNetChargeDurLabel: UILabel!
    
    @IBOutlet weak var mobileOffNetMnyLabel: UILabel!
    
    @IBOutlet weak var mobilePstnFreeLabel: UILabel!
    
    @IBOutlet weak var mobilePstnChargeDurLabel: UILabel!
    
    @IBOutlet weak var mobilePstnMnyLabel: UILabel!
    
    @IBOutlet weak var smsOnFreeAgLabel: UILabel!
    
    @IBOutlet weak var smsOnNetChargeVolumnLabel: UILabel!
    
    @IBOutlet weak var smsOnNetMnyLabel: UILabel!
    
    @IBOutlet weak var smsOffFreeAgLabel: UILabel!
    
    @IBOutlet weak var smsOffNetChargeVolumnLabel: UILabel!
    
    @IBOutlet weak var smsOffNetMnyLabel: UILabel!
    
    var isExpand = false
}

class BestLowViewController: TSUIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var blList: [BestLow] = []
    var blProjects: BestLowProjects = BestLowProjects.init(withDic: [:])
    
    @IBOutlet weak var blTableView: UITableView!
    
    @IBOutlet weak var blTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var informationView: UIView!
    var expandBlCellIndex: IndexPath!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.blTableView.rowHeight = UITableViewAutomaticDimension
        self.blTableView.estimatedRowHeight = blCellHeaderHeight + blCellContentHeight
        self.callGetBlAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_BL", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "保證最低價最近一期出帳明細", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "保證最低價最近一期出帳明細")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_BL", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.blProjects.projects.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BLCell", for: indexPath) as! BestLowCell2
        
        if indexPath.row == 0 {
            cell.projectNameLabel.textColor = UIColor.red
            cell.amountLabel.textColor = UIColor.red
        }
        else {
            cell.projectNameLabel.textColor = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1)
            cell.projectNameLabel.textColor = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1)
        }
        
        cell.projectNameLabel.text = blProjects.projects[indexPath.row].projectName
        cell.amountLabel.text = blProjects.projects[indexPath.row].amount
        
        cell.isExpand = false
        
        
        var listCount: Int = 1
        for detail in blProjects.projects[indexPath.row].detailList {
            
            let detailView = UINib(nibName: "BestLowData", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as! DetailView
            detailView.detailNameLabel.text = detail.name
            detailView.autoresizingMask = .flexibleWidth
            detailView.frame = CGRect(x: 0, y: listCount * Int(blCellHeaderHeight), width: Int(self.view.frame.width), height: Int(blCellHeaderHeight))
            cell.contentView.addSubview(detailView)
            listCount = listCount + 1
            
            for item in detail.itemList {
                
                let itemView = UINib(nibName: "BestLowData", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ItemView
                itemView.itemNameLabel.text = item.nameText
                itemView.itemDescLabel.text = item.desc
                itemView.autoresizingMask = .flexibleWidth
                
                itemView.frame = CGRect(x: 0, y: listCount * Int(blCellHeaderHeight), width: Int(self.view.frame.width), height: Int(blCellHeaderHeight))
                cell.contentView.addSubview(itemView)
                listCount = listCount + 1
            }
            
        }
        
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var cellHeight: CGFloat = blCellHeaderHeight
        
        if let blCell = tableView.cellForRow(at: indexPath) as? BestLowCell2 {
            if blCell.isExpand {
//                cellHeight += blCellContentHeight
                var dataCount: CGFloat = 1// = blProjects.projects[indexPath.row].detailList.count
//                let itemCount = blProjects.projects[indexPath.row].detailList
                for detail in blProjects.projects[indexPath.row].detailList {
                    dataCount = dataCount + 1
                    for _ in detail.itemList {
                        dataCount = dataCount + 1
                    }
                }
                cellHeight = blCellHeaderHeight * dataCount
            }
        }
        
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.expandBlCellIndex != nil && self.expandBlCellIndex != indexPath {
            if let expandCell = self.blTableView.cellForRow(at: self.expandBlCellIndex) as? BestLowCell2 {
                expandCell.isExpand = false
            }
        }
        
        var contentHeight: CGFloat = blCellContentHeight
        let blCell = tableView.cellForRow(at: indexPath) as! BestLowCell2
        if blCell.isExpand {
            contentHeight = 0
            self.expandBlCellIndex = nil
            blCell.arrowImage.image = UIImage(named: "icon_arrow_down")
        } else {
            self.expandBlCellIndex = indexPath
            blCell.arrowImage.image = UIImage(named: "icon_arrow_up")
        }
        blCell.isExpand = !blCell.isExpand
        
        let tableHeight = CGFloat(self.blProjects.projects.count) * blCellHeaderHeight + contentHeight
        
        self.blTableViewHeightConstraint.constant = tableHeight
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
            tableView.beginUpdates()
            tableView.endUpdates()
            if self.expandBlCellIndex != nil {
                let cellInTableView = tableView.rectForRow(at: self.expandBlCellIndex)
                let cellInScrollView = tableView.convert(cellInTableView, to: tableView.superview)
                self.contentScrollView.setContentOffset(cellInScrollView.origin, animated: true)
            }
        }, completion: { (isComplete: Bool) in
        })
    }
    
    // MARK: - API
    func callGetBlAPI() {
        
        let loginData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.getBL(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, companyId: loginData.custProfile_companyId, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                self.blProjects = BestLowProjects.init(withDic: dic)
                
                self.blTableViewHeightConstraint.constant = CGFloat(self.blProjects.projects.count) * blCellHeaderHeight
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()

                if self.blProjects.projects.count > 0 {
                    self.blTableView.reloadData() {
                        
                        for (idx, bl) in self.blProjects.projects.enumerated() {
                            if bl.bestChoice == "Y" {
                                self.blTableView.delegate?.tableView!(self.blTableView, didSelectRowAt: (self.blTableView.indexPathsForVisibleRows?[idx])!)
                                return
                            }
                        }
                    }
                }
                else {
                    self.informationView.isHidden = false
                }

            }
            else {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            
        }) { (error: Error) in
            
        }
        
    }
}
