//
//  BillInfo.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/9.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class BillInfo : NSObject {
    
    static let sharedInstance = BillInfo()
    
    private(set) var billDate : String = "" //出帳日期
    private(set) var dueDate : String = "" //繳款期限
    private(set) var totDebt : String = "" //應繳總額
    private(set) var preBalance : String = "" //前期應繳
    private(set) var prePaid : String = "" //前期已繳
    private(set) var curBalance : String = "" //本期新增
    private(set) var curPayment : String = "" //本期應繳
    private(set) var billingMessage : String = "" //帳單訊息(帳單明細無資料或錯誤時顯示)
    private(set) var paymentText : String = "" //繳款提示文字
    private(set) var showPaymentButton : String = "" //顯示繳款按鈕(Y:顯示/N:不顯示)
    private(set) var billingDetail : Array<BillingDetail> = []  //帳單明細

    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                billDate = apiData.stringValue(ofKey: "billDate")
                dueDate = apiData.stringValue(ofKey: "dueDate")
                totDebt = apiData.stringValue(ofKey: "totDebt")
                preBalance = apiData.stringValue(ofKey: "preBalance")
                prePaid = apiData.stringValue(ofKey: "prePaid")
                curBalance = apiData.stringValue(ofKey: "curBalance")
                curPayment = apiData.stringValue(ofKey: "curPayment")
                billingMessage = apiData.stringValue(ofKey: "billingMessage")
                paymentText = apiData.stringValue(ofKey: "paymentText")
                showPaymentButton = apiData.stringValue(ofKey: "showPaymentButton")
                
                billingDetail = []
                
                if let array = apiData["billingDetail"] as? Array<Dictionary<String,Any>> {
                    
                    for data in array {
                        
                        let detail = BillingDetail()
                        detail.setDataValue(withDic: data)
                        billingDetail.append(detail)
                    }
                }
            }
        }
    }
}

class BillingDetail : NSObject {
    
    private(set) var accountNum : String = "" //帳戶編號
    private(set) var contractId : String = "" //合約編號
    private(set) var billDate : String = "" //主帳日
    private(set) var paymentEndDate : String = "" //繳款截止日
    private(set) var mnyLastpay : String = "" //上期已繳
    private(set) var mnyLast : String = "" //上期應繳
    private(set) var mnyNow : String = "" //本期新增
    private(set) var mnyPay : String = "" //本期應繳
    private(set) var detailType : String = ""
    private(set) var detailText : String = ""
    private(set) var detailUrl : String = ""
    private(set) var chargeItemList : Array<ChargeItemList> = [] //收費明細
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
       
        accountNum = dic.stringValue(ofKey: "accountNum")
        contractId = dic.stringValue(ofKey: "contractId")
        billDate = dic.stringValue(ofKey: "billDate")
        paymentEndDate = dic.stringValue(ofKey: "paymentEndDate")
        mnyLastpay = dic.stringValue(ofKey: "mnyLastpay")
        mnyLast = dic.stringValue(ofKey: "mnyLast")
        mnyNow = dic.stringValue(ofKey: "mnyNow")
        mnyPay = dic.stringValue(ofKey: "mnyPay")
        detailType = dic.stringValue(ofKey: "detailType")
        detailText = dic.stringValue(ofKey: "detailText")
        detailUrl = dic.stringValue(ofKey: "detailUrl")

        if let array = dic["chargeItemList"] as? Array<Dictionary<String,Any>> {
            
            for data in array {
                
                let item = ChargeItemList()
                item.setDataValue(withDic: data)
                chargeItemList.append(item)
            }
        }
    }
}

class ChargeItemList : NSObject {
    
    private(set) var itemGroupName : String = "" //項目群組
    private(set) var itemList : Array<ItemList> = [] //項目清單

    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        itemGroupName = dic.stringValue(ofKey: "itemGroupName")
        
        if let array = dic["itemList"] as? Array<Dictionary<String,Any>> {
            
            for data in array {
                
                let item = ItemList()
                item.setDataValue(withDic: data)
                itemList.append(item)
            }
        }
    }
}

class ItemList : NSObject {
    
    private(set) var chargeType : String = "" //類型
    private(set) var itemDesc : String = "" //收費項目
    private(set) var revenueMny : String = "" //金額

    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        chargeType = dic.stringValue(ofKey: "chargeType")
        itemDesc = dic.stringValue(ofKey: "itemDesc")
        revenueMny = dic.stringValue(ofKey: "revenueMny")

    }
}
