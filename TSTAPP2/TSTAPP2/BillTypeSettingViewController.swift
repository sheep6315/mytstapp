//
//  BillTypeSettingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/11/14.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class BillTypeSettingViewController: TSUIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var billTypeLabel: UILabel!
    
    @IBOutlet weak var billTypeButton: UIButton!
    
    @IBOutlet weak var billTypeTextField: UITextField!
    
    @IBOutlet weak var applyButton: UIButton!
    
    let loginData = LoginData.sharedLoginData()
    var ebillType = EnumEbillType.NoData
    var billTypeArray: [String] = []
    var selectRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(gesture)
        
        ebillType = EnumEbillType(rawValue: loginData.billType_memberType)!
        
        billTypeButton.layer.borderWidth = 1
        billTypeButton.layer.cornerRadius = 5
        billTypeButton.layer.borderColor = UIColor.lightGray.cgColor
        
        if ebillType == .Complete_Y {
            billTypeButton.setTitle("紙本帳單", for: UIControlState.normal)
            billTypeArray = ["紙本帳單", "電子帳單"]
        }
        else if ebillType == .Not_N {
            billTypeButton.setTitle("電子帳單", for: UIControlState.normal)
            billTypeArray = ["電子帳單", "紙本帳單"]
        }
        else {
            billTypeButton.setTitle("電子帳單", for: UIControlState.normal)
            billTypeArray = ["電子帳單", "紙本帳單"]
        }
        
        let billTypeMsg = LoginData.sharedLoginData().billType_billTypeMsg
        let attributedString = try! NSMutableAttributedString(data: billTypeMsg.data(using: String.Encoding.unicode)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        
        attributedString.addAttributes([NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)], range: NSMakeRange(0, attributedString.string.count))
        billTypeLabel.attributedText = attributedString
        
        setPickerData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_BILL_TYPE", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "會員中心", action: "修改帳單類型", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "修改帳單類型")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_BILL_TYPE", action: "")
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: applyButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setPickerData() {
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        billTypeTextField.inputView = pickerView
        billTypeTextField.inputAccessoryView = pickBar
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }
    
    //    MARK: - IBAction
    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    @objc func didTapDone(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapBillTypeButton(_ sender: Any) {
        
        billTypeTextField.becomeFirstResponder()
        
    }
    
    @IBAction func didTapApplyButton(_ sender: Any) {
        self.view.endEditing(true)
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改帳單類別(會員中心)", action: nil, label: nil, value: nil)
        
        if billTypeArray[selectRow] == "電子帳單" {
            let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ChangeEmailViewController") as! ChangeEmailViewController
            vc.fromBillTypeVC = true
            self.navigationController?.show(vc, sender: self)
        }
        else if billTypeArray[selectRow] == "紙本帳單" {
            let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ChangeAddressViewController") as! ChangeAddressViewController
            vc.fromBillTypeVC = true
            self.navigationController?.show(vc, sender: self)
        }
        
    }
    
    //MARK: - PikerView Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
            
        case 0:
            return billTypeArray.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView.tag {
            
        case 0:
            return billTypeArray[row]
            
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        switch pickerView.tag {
            
        case 0:
            selectRow = row
            
            billTypeButton.setTitle(billTypeArray[row], for: .normal)
            
            if loginData.billType_memberType == "Y" || loginData.billType_memberType == "N" {
                if row == 0 {
                    applyButton.isEnabled = true
                }
                else if row == 1 {
                    applyButton.isEnabled = false
                }
            }
            
            break
            
        default: break
        }
    }
    

}
