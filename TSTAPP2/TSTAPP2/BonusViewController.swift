//
//  BonusViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/6/12.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class BonusViewController: TSUIViewController {
    
    @IBOutlet weak var loginView: UIView!
    
    var webView: TstarWebViewController?
    
    var disableBackToRoot = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if LoginData.isLogin() {
            
            loginView.isHidden = true
            
            webView?.webUrl = UrlMap.sharedInstance.PROMO_ZONE
        }
        else {
            
            loginView.isHidden = false
            
            webView?.webUrl = "https://www.tstartel.com/static/tstapp/memberbenefit/sample.html"
        }
        
        checkVIPStatusAndChangeSkin()
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_PROMO_ZONE", action: "AA_PROMO_ZONE")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "優惠專區", action: nil, label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "優惠專區")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_PROMO_ZONE", action: "AA_PROMO_ZONE")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        TSTAPP2_Utilities.checkIfOnlyFacebookLogin()
        
//        let isMaintain = TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self)
        
//        disableBackToRoot = TSTAPP2_Utilities.isForceAccountBindingAndPushToBindingView(hideBackButton: true, withViewController: self)
//        if disableBackToRoot {
//            return
//        }
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "toWebView" {
            webView = (segue.destination as! TstarWebViewController)
        }
    }
    
//    MARK: - IBAction
    @IBAction func didTapLoginButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "AP_PROMO_ZONE", action: "AA_MASK_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "優惠專區", label: "登入", value: nil)
        
        TSTAPP2_Utilities.pushToLoginViewController(withViewController: self)
    }
    
    @IBAction func didTapAQButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "AP_PROMO_ZONE", action: "AA_EC_APPLY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "優惠專區", label: "門號申辦", value: nil)
        
        #if SIT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #elseif UAT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #else
            let urlString: String = "https://www.tstartel.com/mCWS/shop.php"
        #endif
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
}
