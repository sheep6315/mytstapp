//
//  CampaignInfo.swift
//  TSTAPP2
//
//  Created by Tony on 2017/9/18.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class CampaignInfo: NSObject {
    
    static let sharedInstance = CampaignInfo()
    
    private(set) var sort: String = ""
    
    private(set) var type: String = ""
    
    private(set) var text1: String = ""
    
    private(set) var text2: String = ""
    
    private(set) var ubaActionCode: String = ""
    
    private(set) var buttonList: [CampaignButtonList] = []
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "code") == "00000" {
            
            buttonList = []
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
            
                if let bannerDic = apiData["banner"] as? Dictionary<String, Any> {
                    sort = bannerDic.stringValue(ofKey: "sort")
                    type = bannerDic.stringValue(ofKey: "type")
                    text1 = bannerDic.stringValue(ofKey: "text1")
                    text2 = bannerDic.stringValue(ofKey: "text2")
                    ubaActionCode = bannerDic.stringValue(ofKey: "ubaActionCode")
                }
                
                if let bannerListArray = apiData["buttonList"] as? [Dictionary<String, Any>] {
                    for buttonData in bannerListArray {
                        buttonList.append(CampaignButtonList.init(withDic: buttonData))
                    }
                    
                }
            }
        }
    }

}

class CampaignButtonList: NSObject {
    
    private(set) var sort: String = ""
    
    private(set) var type: String = ""
    
    private(set) var text1: String = ""
    
    private(set) var text2: String = ""
    
    private(set) var ubaActionCode: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        sort = dic.stringValue(ofKey: "sort")
        type = dic.stringValue(ofKey: "type")
        text1 = dic.stringValue(ofKey: "text1")
        text2 = dic.stringValue(ofKey: "text2")
        ubaActionCode = dic.stringValue(ofKey: "ubaActionCode")
    }
}
