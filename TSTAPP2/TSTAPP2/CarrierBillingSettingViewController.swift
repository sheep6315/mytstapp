//
//  CarrierBillingSettingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/5/16.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class CarrierBillingSettingViewController: TSUIViewController ,UITextFieldDelegate {
    
    @IBOutlet weak var dcbStatusSwitch: UISwitch!
    @IBOutlet weak var mpgStatusSwitch: UISwitch!
    @IBOutlet weak var setSecurityCodeButton: UIButton!
    @IBOutlet weak var securityCodeBtnConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dcbInfoButton: UIButton!
    @IBOutlet weak var mpgInfoButton: UIButton!
    
    
    let checkLock = CheckLock.sharedInstance
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dcbStatusSwitch.isHidden = true
        mpgStatusSwitch.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0308", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "電信帳單代收服務開關管理", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "電信帳單代收服務開關管理")
        
        checkVIPStatusAndChangeSkin()
        
        callCheckLock()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0308", action: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
        
        UISkinChange.sharedInstance.changeUIColor(theObject: dcbStatusSwitch)
        UISkinChange.sharedInstance.changeUIColor(theObject: mpgStatusSwitch)
        
        UISkinChange.sharedInstance.changeUIColor(theObject: dcbInfoButton, mode: ButtonColorMode.button_purple.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: mpgInfoButton, mode: ButtonColorMode.button_purple.rawValue)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //MARK: - IBAction
    @IBAction func whenStatusToggle(_ sender: UISwitch) {
        if sender.tag == 1 { //dcb
            if checkLock.dcb.changeSwitch == "Y" { //可否異動按鈕
                TSTAPP2_API.sharedInstance.updateSwitchStatus(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, switchType: "\(sender.tag)", switchStatus: "\(sender.isOn ? "Y" : "N")", securityCode: "", showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
                    if let data = dic["data"] as? Dictionary<String, Any> {
                        if let resultMessage = data["resultMessage"] as? String {
                            TSTAPP2_Utilities.showAlert(withMessage: resultMessage, viewController: self)
                            UbaRecord.sharedInstance.sendUba(withPage: "APP0308", action: "AA_CBMP_SWCH_I")
                            
                            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "電信帳單代收服務開關管理", action: "Apple iTunes狀態", label: "\(sender.isOn ? "ON" : "OFF")", value: nil)
                        }
                    }
                }) { (error:Error) in
                    print(error)
                }
            }else {
                let action = UIAlertAction(title: "確認", style: .cancel, handler: { (alertaction) in
                    sender.setOn(false, animated: false)
                })
                TSTAPP2_Utilities.showAlert(withMessage: checkLock.dcb.resultMessage, action: action, viewController: self)
            }
        }else if sender.tag == 2 { //mpg
            if checkLock.mpg.changeSwitch == "Y" { //可否異動按鈕
                if checkLock.mpg.needSecurityCode == "Y" { //可否需要安全碼
                    TSTAPP2_Utilities.showAlertAndSegue(withMessage: checkLock.mpg.resultMessage, segueIdentifier: "SetSecurityCode", sender: nil, viewController: self)
                }else {
                    TSTAPP2_API.sharedInstance.updateSwitchStatus(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, switchType: "\(sender.tag)", switchStatus: "\(sender.isOn ? "Y" : "N")", securityCode: "", showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
                        if dic.stringValue(ofKey: "code") == "00000" {
                            if let data = dic["data"] as? Dictionary<String, Any> {
                                if let resultMessage = data["resultMessage"] as? String {
                                    self.securityCodeBtnConstraint.constant = (sender.isOn ? 30 : 0)
                                    self.setSecurityCodeButton.isHidden = (sender.isOn ? false : true)
                                    TSTAPP2_Utilities.showAlert(withMessage: resultMessage, viewController: self)
                                    UbaRecord.sharedInstance.sendUba(withPage: "APP0308", action: "AA_CBMP_SWCH_MPG")
                                    
                                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "電信帳單代收服務開關管理", action: "小額付款狀態", label: "\(sender.isOn ? "ON" : "OFF")", value: nil)
                                }
                            }
                        }else {
                            TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                        }
                    }) { (error:Error) in
                        print(error)
                    }
                }
            }else {
                let action = UIAlertAction(title: "確認", style: .cancel, handler: { (_) in
                    sender.setOn(false, animated: false)
                })
                TSTAPP2_Utilities.showAlert(withMessage: checkLock.mpg.resultMessage, action: action, viewController: self)
            }
        }
    }
    
    @IBAction func didTapSetSecurityCodeButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SetSecurityCode", sender: nil)
    }
    
    @IBAction func didTapDesriptionButton(_ sender: UIButton) {
        var url = ""
        
        if sender.tag == 1 {
//            #if SIT
//                url = "https://uattsp.tstartel.com/mCWS/carrierBilling.php?tabs=3"
//            #elseif UAT
//                url = "https://uattsp.tstartel.com/mCWS/carrierBilling.php?tabs=3"
//            #else
                url = "https://www.tstartel.com/static/pages/carrierBilling/carrierBilling.html?tabs=3"
//            #endif
        }else if sender.tag == 2 {
//            #if SIT
//                url = "https://uattsp.tstartel.com/mCWS/carrierBilling.php?tabs=4"
//            #elseif UAT
//                url = "https://uattsp.tstartel.com/mCWS/carrierBilling.php?tabs=4"
//            #else
                url = "https://www.tstartel.com/static/pages/carrierBilling/carrierBilling.html?tabs=4"
//            #endif
        }
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: sender.titleLabel!.text!, urlString: url, viewController: self)
    }
    
//    MARK: - API
    func callCheckLock() {
        TSTAPP2_API.sharedInstance.checkLock(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId,showProgress: true,completion: {(dic:Dictionary<String, Any>, response:URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                self.dcbStatusSwitch.isHidden = true
                self.mpgStatusSwitch.isHidden = true
                
                CheckLock.sharedInstance.setDataValue(withDic: dic)
                if self.checkLock.dcb.showSwitch == "Y" {
                    self.dcbStatusSwitch.isHidden = false
                }
                if self.checkLock.mpg.showSwitch == "Y" {
                    self.mpgStatusSwitch.isHidden = false
                }
                let dcbOn = (self.checkLock.dcb.lock == "Y" ? false:true)
                let mpgOn = (self.checkLock.mpg.lock == "Y" ? false:true)
                self.dcbStatusSwitch.setOn(dcbOn, animated: false)
                self.mpgStatusSwitch.setOn(mpgOn, animated: false)
                if self.checkLock.mpg.lock == "N" {
                    self.securityCodeBtnConstraint.constant = 30
                    self.setSecurityCodeButton.isHidden = false
                }
            }else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (error:Error) in
            TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
            print(error)
        }
    }

}
