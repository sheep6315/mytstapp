//
//  CarrierBillingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/2/14.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class CarrierBillingViewController: TSUIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var submitButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var applyCreditCardTransferButton: UIButton!
    
    @IBOutlet weak var quota: UILabel!

    @IBOutlet weak var remaining: UILabel!
    
    @IBOutlet weak var availableQuotaView: UIView!
    
    @IBOutlet weak var availableQuotaViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var availableQuota: UILabel!
    
    @IBOutlet weak var availableQuotaViewTextField: UITextField!
    
    @IBOutlet weak var availableQuotaButton: UIButton!
    
    @IBOutlet weak var applyView: UIView!
    @IBOutlet weak var applyViewHeightConstraint: NSLayoutConstraint!
    
    var availableQuotaList: [Int] = []
    var newQuota: String = ""
    
    var isCheckLock: Bool = false
    var isQueryDirectDebit: Bool = false
    var isUserQuota: Bool = false
    var applyViewHeightHolder: CGFloat = 0
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        submitButton.isHidden = true
        availableQuotaView.isHidden = true
        // Do any additional setup after loading the view.
        setPickerData()
        
        applyView.isHidden = true
        applyViewHeightHolder = applyViewHeightConstraint.constant
        applyViewHeightConstraint.constant = 0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0306", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "電信帳單代收額度管理", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "電信帳單代收額度管理")
        
        checkVIPStatusAndChangeSkin()
        
        loadAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0306", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: applyCreditCardTransferButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    func hideView() {
        
        submitButtonHeight.constant = 0
        availableQuotaViewHeight.constant = 0
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
    }
    
    func setPickerData(){
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        availableQuotaViewTextField.delegate = self
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        availableQuotaViewTextField.inputView = pickerView
        availableQuotaViewTextField.inputAccessoryView = pickBar
        
        availableQuotaButton.layer.borderWidth = 1
        availableQuotaButton.layer.cornerRadius = 5
        availableQuotaButton.layer.borderColor = UIColor.lightGray.cgColor
    }
    //MARK: - IBAction
    @objc func didTapDone(button: UIBarButtonItem) {
    
        self.view.endEditing(true)
    }
    
    @IBAction func didTapavailableQuota(_ sender: Any) {
        
        availableQuotaViewTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapSubmit(_ sender: Any) {
        
        self.view.endEditing(true)
        let text = availableQuota.text!
        
        if newQuota == "" {
            TSTAPP2_Utilities.showAlert(withMessage: "請先選擇額度", viewController: self)
        }
        else{
            
            let alert = UIAlertController(title: "", message: "請確認電信帳單代收調整額度至\(text)\n提醒您，按下確認立即生效。", preferredStyle: .alert)
            let submit = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                
                UbaRecord.sharedInstance.sendUba(withPage: "APP0306", action: "A030601")
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "電信帳單代收額度管理", action: "\(text)", label: nil, value: nil)
                
                self.callUpdateUserQuotaAPI()
            }
            
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            
            alert.addActions(array: [submit, cancel])
            alert.show(viewController: self)
        }
    }
    
    @IBAction func didTapCreditCard(_ sender: Any) {
        
        AutomaticViewChange.sharedInstance.setTabIndex(withAction: AUTO_CREDITCARDTRANSFER, actionParam: nil)
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP0306", action: "AA_CBMP_QTA_DD")
    }
    
    //MARK: - API
    
    func loadAPI() {
        TSTAPP2_Utilities.showPKHUD()
        callCheckLockAPI()
        callQueryDirectDebit()
    }
    
    func apiResponded() {
        
        if isCheckLock && isQueryDirectDebit && isUserQuota {
            TSTAPP2_Utilities.hidePKHUD()
        }
        
    }
    
    func callCheckLockAPI() {
        TSTAPP2_API.sharedInstance.checkLock(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, showProgress: false, completion: { (dic, response) in
            
            self.isCheckLock = true
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let checkLock = CheckLock.sharedInstance
                checkLock.setDataValue(withDic: dic)
                if checkLock.canAdjustment == "Y" {
                    self.callUserQuotaAPI()
                }else {
                    TSTAPP2_Utilities.showAlert(withMessage: checkLock.resultMessage, viewController: self)
                }
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            
            self.apiResponded()
        }) { (error) in
            self.isCheckLock = true
            self.apiResponded()
        }
    }
    
    func callUpdateUserQuotaAPI() {
        
        let quota = newQuota != "" ? newQuota : "0"
        let text = availableQuota.text!
        TSTAPP2_API.sharedInstance.updateQuota(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, newQuota: quota, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "電信帳單代收額度管理", action: "成功", label: nil, value: nil)
                TSTAPP2_Utilities.showAlertAndPop(withMessage: "您的可用額度已經調整為\(text)。", viewController: self)
            }else {
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "電信帳單代收額度管理", action: "失敗", label: nil, value: nil)
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (Error) in
            
        }
    }
    
    func callUserQuotaAPI() {
    
        TSTAPP2_API.sharedInstance.queryUserQuota(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, showProgress: false, completion: { (dic :Dictionary<String, Any>, reponse :URLResponse) in
            
            self.isUserQuota = true
            
            let data = UserQuota.sharedInstance
            data.setDataValue(withDic: dic)

            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let quota = Int(data.quota) ?? 0
            let remaining = Int(data.remaining) ?? 0
            self.quota.text = "$\(numberFormatter.string(from: NSNumber(value: quota))!)元"
            self.remaining.text = "$\(numberFormatter.string(from: NSNumber(value: remaining))!)元"
            self.availableQuotaList = data.availableQuotaList
            if data.lock != "N" { //若功能為鎖定
                self.hideView()
            }
            else {
                self.submitButton.isHidden = false
                self.availableQuotaView.isHidden = false
                
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                self.availableQuota.text = "$\(numberFormatter.string(from: NSNumber(value: self.availableQuotaList[0]))!)元"
                self.newQuota = String(self.availableQuotaList[0])
            }
            self.apiResponded()
        }) { (Error) in
            self.isUserQuota = true
            self.apiResponded()
        }
    }
    
    func callQueryDirectDebit() {
        TSTAPP2_API.sharedInstance.queryDirectDebit(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, accountId: LoginData.sharedLoginData().custProfile_accountId, custId: LoginData.sharedLoginData().custProfile_custId, companyId: LoginData.sharedLoginData().custProfile_companyId, channel: "APP", isMainMsisdn: LoginData.sharedLoginData().custProfile_isMainMsisdn, showProgress: false, completion:{ (dic :Dictionary<String, Any>, response :URLResponse) in
            
            self.isQueryDirectDebit = true
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                let debit = DirectDebit.sharedInstance
                debit.setDataValue(withDic: dic)
                
                if debit.result == "1" || debit.result == "5" {
                    
                    self.applyView.isHidden = true
                    self.applyViewHeightConstraint.constant = 0
                    self.view.setNeedsUpdateConstraints()
                    self.view.layoutIfNeeded()
                }
                else {
                    self.applyView.isHidden = false
                    self.applyViewHeightConstraint.constant = self.applyViewHeightHolder
                    self.view.setNeedsUpdateConstraints()
                    self.view.layoutIfNeeded()
                }
            }
            else {
                
            }
            
            self.apiResponded()
            
        }, failure: { (Error) in
            self.isQueryDirectDebit = true
            self.applyView.isHidden = true
            self.applyViewHeightConstraint.constant = 0
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
            self.apiResponded()
        })

    }

    //MARK: - PickDelegate

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if availableQuotaList.count > 0 {
            return availableQuotaList.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return String(availableQuotaList[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        availableQuota.text = "$\(numberFormatter.string(from: NSNumber(value: availableQuotaList[row]))!)元"
        newQuota = String(availableQuotaList[row])
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let pickerView = textField.inputView as? UIPickerView {
            if pickerView.numberOfRows(inComponent: 0) > 0 {
                pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
            }
        }
    }
}
