//
//  ChangeAddressViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/18.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ChangeAddressViewController: TSUIViewController ,UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    @IBOutlet weak var currentAddress: UILabel!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var cityButton: UIButton!
    
    @IBOutlet weak var countyTextField: UITextField!
    
    @IBOutlet weak var countyButton: UIButton!
    
    @IBOutlet weak var addressTextView: UITextView!
    
    @IBOutlet weak var noticeLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var cityList: [City] = []
    var countyList: [County] = []
    var seletedCity: City? = nil
    var seletedCounty: County? = nil
    var fromBillTypeVC: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cityButton.layer.borderWidth = 1
        cityButton.layer.cornerRadius = 5
        cityButton.layer.borderColor = UIColor.lightGray.cgColor
        countyButton.layer.borderWidth = 1
        countyButton.layer.cornerRadius = 5
        countyButton.layer.borderColor = UIColor.lightGray.cgColor
        addressTextView.layer.borderWidth = 1
        addressTextView.layer.cornerRadius = 5
        addressTextView.layer.borderColor = UIColor.lightGray.cgColor
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(gesture)
        
        noticeLabel.text = "1.提醒您！個人資料修改時，請您務必提供正確之個人聯絡資料，本公司將依此資料郵寄帳單、各項資料及與您聯絡。\n2.未於上述服務所提供之其他個人資料欲修改，請攜帶身份證正本至全省服務中心或來電客服中心申請。\n3.若您已申請合併帳單，變更帳單地址服務需求，請您以主門號登入服務申請，如需與客服人員聯絡請以手機直撥123，或來電0908-000-123洽詢辦理，不便之處，敬請見諒。\n4.帳寄地址請勿為郵政信箱。"
        currentAddress.text = LoginData.sharedLoginData().billType_billingAddr
        addressTextView.text = LoginData.sharedLoginData().billType_billingAddr
        cityButton.setTitle(LoginData.sharedLoginData().billType_billingState, for: .normal)
        countyButton.setTitle(LoginData.sharedLoginData().billType_billingCity, for: .normal)
        
        callCountyAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP020105", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "會員中心", action: "修改帳寄地址", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "修改帳寄地址")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP020105", action: "")
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    //MARK: - 按鈕didTap Events
    @objc func didTapDone(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
    }
    
    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapCityButton(_ sender: Any) {
        
        cityTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapCountyButton(_ sender: Any) {
        
        countyTextField.becomeFirstResponder()
    }

    @IBAction func didTapSubmit(_ sender: Any) {
        
        var isPOBox = false;
        let addressString = (addressTextView.text as NSString)
        addressString.replacingOccurrences(of: " ", with: "")
        
        if addressString.range(of: "郵政路").location != NSNotFound || addressString.range(of: "郵局街").location != NSNotFound {
            
            isPOBox = false;
        }
        else if addressString.range(of: "郵政").location != NSNotFound || addressString.range(of: "郵局").location != NSNotFound || addressString.range(of: "P.O.BOX").location != NSNotFound {
            
            isPOBox = true;
        }
        
        if seletedCounty == nil || seletedCity == nil {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇縣市/地區", viewController: self)
        }
        else if addressTextView.text.lengthOfBytes(using: .utf8) == 0 {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請輸入地址", viewController: self)
        }
        else if isPOBox {
            
            TSTAPP2_Utilities.showAlert(withMessage: "帳寄地址不可輸入郵政信箱，請重新輸入", viewController: self)
        }
        else {
            
//            let alert = UIAlertController(title: "", message: "您確定要變更帳單地址為\n\(seletedCounty!.zipCode)\(seletedCity!.name)\(seletedCounty!.name)\(addressTextView.text!)?", preferredStyle: .alert)
//
//            let submit = UIAlertAction(title: "確定", style: .default, handler: { (UIAlertAction) in
//
//                UbaRecord.sharedInstance.sendUba(withPage: "APP020105", action: "A0402")
//                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改帳寄地址(會員中心)", action: nil, label: nil, value: nil)
//
//                self.callChangeBillAddressAPI()
//            })
//            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
//
//            alert.addActions(array: [submit,cancel])
//            alert.show(viewController: self)
            
            self.callChangeBillAddressAPI()
        }
    }
    
    //MARK: - custom Func
    func setPickerData() {
                
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        for city in self.cityList {
            
            if city.name == LoginData.sharedLoginData().custInfoBrief_billingState {
                
                self.seletedCity = city
                self.countyList = city.county
                
                for county in city.county {
                    
                    if county.name == LoginData.sharedLoginData().custInfoBrief_billingCity {
                        
                        self.seletedCounty = county
                    }
                }
            }
        }
        
        cityTextField.delegate = self
        countyTextField.delegate = self
        
        var pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        cityTextField.inputView = pickerView
        cityTextField.inputAccessoryView = pickBar
        
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 1
        countyTextField.inputView = pickerView
        countyTextField.inputAccessoryView = pickBar
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //textField.inputAccessoryView = self.keyboardToolBar
        if let pickerView = textField.inputView as? UIPickerView {
            
            if pickerView.numberOfRows(inComponent: 0) > 0 {
            
                if pickerView.tag == 0 {
                    pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
                }
                else {
                    pickerView.selectRow(0, inComponent: 0, animated: false)
                    pickerView.delegate?.pickerView!(pickerView, didSelectRow: 0, inComponent: 0)
                }
            }
        }
    }
    
    //MARK: - PikerView Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
            
        case 0:
            return cityList.count
        case 1:
            
            if countyList.count == 0 {
                return 1
            } else {

                return countyList.count
            }
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView.tag {
            
        case 0:
            return cityList[row].name
        case 1:
            
            if countyList.count == 0 {
                return "請先選擇縣市"
            } else {

                return countyList[row].name
            }
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        switch pickerView.tag {
            
            case 0:
                seletedCity = cityList[row]
                seletedCounty = nil
                countyList = cityList[row].county
                cityButton.setTitle(cityList[row].name, for: .normal)
                countyButton.setTitle("請選擇地區", for: .normal)
                break
            case 1:
                
                if seletedCity != nil {

                    seletedCounty = countyList[row]
                    countyButton.setTitle(countyList[row].name, for: .normal)
                }
                break
                
            default: break
        }
    }
    
    //MARK: - API
    func callCountyAPI() {
        
        let county = ZipCode.sharedInstance
        
        if county.towns.count > 0 {
            
            self.cityList = county.towns
            self.setPickerData()
        }
        else {
            TSTAPP2_API.sharedInstance.getZipCode(showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                
                if dic.stringValue(ofKey: "code") == "00000" {
                    
                    let county = ZipCode.sharedInstance
                    county.setDataValue(withDic: dic)
                    self.cityList = county.towns
                    self.setPickerData()
                }
                else {
                    
                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
                
            }) { (Error) in
                print("error: \(Error)")
            }
        }
    }
    
    func callChangeBillAddressAPI() {
        
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.esbillMemberApply(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, accountId: loginData.custProfile_accountId, billType: "P", emailAddress: "", zip: seletedCounty!.zipCode, city: seletedCity!.name, state: seletedCounty!.name, address: addressTextView.text, showProgress: true, completion: { (dic, response) in
            if dic.stringValue(ofKey: "code") == "00000" {
                if let data = dic["data"] as? Dictionary<String, Any> {
                    
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改帳單類別(會員中心)", action: "\(loginData.billType_billType),紙本帳單", label: nil, value: nil)
                    
                    AccountInformation_CallGetBillTypeInfo = true
                    
                    if self.fromBillTypeVC == true {
                        TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
                    }
                    else {
                        TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成修改", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
                    }
                    
                    
//                    TSTAPP2_API.sharedInstance.getBillTypeInfo(withMsisdn: loginData.custProfile_msisdn, isMainMsisdn: loginData.custProfile_isMainMsisdn, showProgress: true, completion: { (dic, response) in
//                        if dic.stringValue(ofKey: "code") == "00000" {
//                            if let _ = dic["data"] as? Dictionary<String, Any> {
//                                loginData.setBillTypeDataValue(withDic: dic)
//                            }
//                        }
//
//                        if self.fromBillTypeVC == true {
//                            TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//                        else {
//                            TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成修改", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//
//                    }, failure: { (error) in
//                        if self.fromBillTypeVC == true {
//                            TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//                        else {
//                            TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成修改", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//                    })
                }
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (error: Error) in
            TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
        }
        
        
//        TSTAPP2_API.sharedInstance.changeBillAddress(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, isMainMsisdn: LoginData.sharedLoginData().custProfile_isMainMsisdn, zip: seletedCounty!.zipCode, city: seletedCity!.name, state: seletedCounty!.name, address: addressTextView.text, showProgress: true, completion: {(dic :Dictionary<String, Any>, response :URLResponse) in
//
//            if dic.stringValue(ofKey: "code") == "00000" {
//
//                TSTAPP2_Utilities.showAlertAndPop(withMessage: "資料修改申請已送出，系統將於1小時內為您處理完成。\n若您有任何問題，請已手機直撥123或撥0908-000-123至客服中心洽詢。", viewController: self)
//            }
//            else {
//
//                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
//            }
//        }, failure: {(Error) in
//
//
//        })
    }
}
