//
//  ChangeContractViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/29.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ChangeContractViewController: TSUIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var finishView: UIView!
    
    @IBOutlet weak var applyView: UIView!
    
    @IBOutlet weak var ratePlanLabel: UILabel!
    
    @IBOutlet weak var billCycleLabel: UILabel!
    
    @IBOutlet weak var ratePlanField: UITextField!
    
    var ratePlanList: [RatePlan] = []
    
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var backButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.callGetRatePlanAPI()
        self.billCycleLabel.text = LoginData.sharedLoginData().custProfile_billingCycle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkVIPStatusAndChangeSkin()
        
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "資費變更申請", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "資費變更申請")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: sendButton, mode: ButtonColorMode.backgroundImage.rawValue)
         UISkinChange.sharedInstance.changeUIColor(theObject: backButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setPickerViewField() {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.didTapDone))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: false)
        
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        self.ratePlanField.inputAccessoryView = toolBar
        self.ratePlanField.inputView = pickerView
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "" {
            if let pickerView = textField.inputView as? UIPickerView {
                if ratePlanList.count > 0 {
                    pickerView.delegate?.pickerView!(pickerView, didSelectRow: 0, inComponent: 0)
                }
            }
        }
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.ratePlanList.count
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(self.ratePlanList[row].ratePlan)型"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.ratePlanField.text = "\(self.ratePlanList[row].ratePlan)型"
    }
    
    // MARK: - IBAction
    
    @IBAction func didTapNoticeButton(_ sender: Any) {
        let noticeAlertController = UIAlertController(title: "注意事項", message: "提醒您！變更資費申請後，將於下一帳單週期開始啟用新資費計費。申請變更資費後，在新資費啟用之前，不提供再次變更資費。\n若您的門號因合約或優惠內容等限制，暫時無法使用網站提供之變更資費申請服務，如需查詢合約內容或有任何不清楚之處，歡迎利用客服信箱與我們聯絡，或來電洽詢客服人員為您說明。", preferredStyle: .alert)
        noticeAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
        self.present(noticeAlertController, animated: true, completion: nil)
    }
    
    @IBAction func didTapPickRatePlan(_ sender: Any) {
        self.ratePlanField.becomeFirstResponder()
    }
    
    @IBAction func didTapBackButton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSendButton(_ sender: Any) {
        self.view.endEditing(true)
        if self.ratePlanField.text == "" {
            TSTAPP2_Utilities.showAlert(withMessage: "請先選擇方案", viewController: self)
            return
        }
        let confirmAlertController = UIAlertController(title: "資費變更申請", message: "確定是否變更資費，此資費將於下個帳單週期生效", preferredStyle: .alert)
        confirmAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (_: UIAlertAction) in
            
            UbaRecord.sharedInstance.sendUba(withPage: "AP_CONT", action: "A0403")
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "資費變更申請", label: "資費變更申請確認", value: nil)
            
            self.callDoRatePlanChangeAPI()
        }))
        confirmAlertController.addAction(UIAlertAction(title: "取消", style: .default, handler: nil))
        self.present(confirmAlertController, animated: true, completion: nil)
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "資費變更申請", label: "", value: nil)
    }
    
    @objc func didTapDone() {
        self.view.endEditing(true)
    }
    
    // MARK: - API
    func callGetRatePlanAPI() {
        let userData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.findRatePlan(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, planName: userData.custProfile_planName, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                let ratePlan = RatePlanList(withDic: dic)
                self.ratePlanLabel.text = ratePlan.currentRatePlan
                self.ratePlanList = ratePlan.list
                if self.ratePlanList.count == 0 {
                    TSTAPP2_Utilities.showAlert(withMessage: "目前無可供選擇的變更方案。", viewController: self)
                }
            }
            else {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            self.setPickerViewField()
        }) { (error: Error) in
            
        }
        
//        TSTAPP2_API.sharedInstance.getRatePlan(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
        
//            let apiResponse = dic["GetRatePlanRes"] as? Dictionary<String, Any> ?? ["StatusMsg":"系統繁忙中，請稍後再嘗試。"]
//            
//            if apiResponse.stringValue(ofKey: "ResultCode") == "00000" {
//                if apiResponse.stringValue(ofKey: "status") == "0" {
//                    let ratePlan = RatePlanList(withDic: dic)
//                    self.ratePlanLabel.text = ratePlan.currentRatePlan
//                    self.ratePlanList = ratePlan.list
//                    if self.ratePlanList.count == 0 {
//                        TSTAPP2_Utilities.showAlert(withMessage: "目前無可供選擇的變更方案。", viewController: self)
//                    }
//                } else {
//                    TSTAPP2_Utilities.showAlertAndPop(withMessage: apiResponse.stringValue(ofKey: "StatusMsg"), viewController: self)
//                }
//            } else {
//                TSTAPP2_Utilities.showAlert(withMessage: apiResponse.stringValue(ofKey: "ResultText"), viewController: self)
//            }
//            self.setPickerViewField()
//        }) { (Error) in
//            print("error: \(Error)")
//        }
    }
    
    func callDoRatePlanChangeAPI() {
        let userData = LoginData.sharedLoginData()
        if let pickerView = self.ratePlanField.inputView as? UIPickerView {
            let ratePlan = self.ratePlanList[pickerView.selectedRow(inComponent: 0)]
            
            
            TSTAPP2_API.sharedInstance.changeRatePlan(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, fromSystem: ratePlan.fromSystem, newRatePlan: ratePlan.ratePlan, oriProjName: ratePlan.oriProjName, chgProjName: ratePlan.chgProjName, handset: ratePlan.handSet, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    
                    if let data = dic["data"] as? Dictionary<String, Any> {
                        TSTAPP2_Utilities.showAlertAndPop(withMessage: data.stringValue(ofKey: "exeMessage"), viewController: self)
                    }
                }
                else {
                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
            }, failure: { (error: Error) in
                
            })
            
            
//            TSTAPP2_API.sharedInstance.doRatePlanChange(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, fromSystem: ratePlan.fromSystem, newRate: ratePlan.ratePlan, oriProjName: ratePlan.oriProjName, chgProjName: ratePlan.chgProjName, handset: ratePlan.handSet, showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
//                
//                let apiResponse = dic["DoRatePlanChangeRes"] as? Dictionary<String, Any> ?? ["StatusMsg":"系統繁忙中，請稍後再嘗試。"]
//                if apiResponse.stringValue(ofKey: "ResultCode") == "00000" {
//                    if apiResponse.stringValue(ofKey: "status") == "0" {
//                        self.finishView.isHidden = false
//                        self.applyView.isHidden = true
//                    } else {
//                        TSTAPP2_Utilities.showAlert(withMessage: apiResponse.stringValue(ofKey: "StatusMsg"), viewController: self)
//                    }
//                } else {
//                    TSTAPP2_Utilities.showAlert(withMessage: apiResponse.stringValue(ofKey: "ResultText"), viewController: self)
//                }
//                
//            }) { (Error) in
//                print("error: \(Error)")
//            }

        }
    }
}
