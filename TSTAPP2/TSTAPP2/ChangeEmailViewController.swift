//
//  ChangeEmailViewController.swift
//  TSTAPP2
//
//  Created by apple on 2016/10/26.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ChangeEmailViewController: TSUIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var checkBox: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var msisdn: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var email: String = ""
    var ebillType: EnumEbillType = EnumEbillType.NoData
    var fromBillTypeVC: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        msisdn.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: LoginData.sharedLoginData().custProfile_msisdn, type: encodeForSecurityType.Phone) //LoginData.sharedLoginData().custProfile_msisdn
        
        // Do any additional setup after loading the view.
        
        
        ebillType = EnumEbillType(rawValue: LoginData.sharedLoginData().billType_memberType)!
        if ebillType == .Apply_A || ebillType == .Confirming_W {
            emailTextField.text = LoginData.sharedLoginData().billType_email
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP020104", action: "")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "會員中心", action: "修改Email", label: nil, value: nil)
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP020104", action: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        self.scrollView.endEditing(true)
        
    }
    
    func callApplyEmailAPI() {
        
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.esbillMemberApply(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, accountId: loginData.custProfile_accountId, billType: "E", emailAddress: emailTextField.text!, zip: "", city: "", state: "", address: "", showProgress: true, completion: { (dic, response) in
            if dic.stringValue(ofKey: "code") == "00000" {
                if let data = dic["data"] as? Dictionary<String, Any> {
                    
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改帳單類別(會員中心)", action: "\(loginData.billType_billType),電子帳單", label: nil, value: nil)
                    
                    AccountInformation_CallGetBillTypeInfo = true
                    
                    if self.fromBillTypeVC == true {
                        TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
                    }
                    else {
                        TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成修改", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
                    }
                    
//                    TSTAPP2_API.sharedInstance.getBillTypeInfo(withMsisdn: loginData.custProfile_msisdn, isMainMsisdn: loginData.custProfile_isMainMsisdn, showProgress: true, completion: { (dic, response) in
//                        if dic.stringValue(ofKey: "code") == "00000" {
//                            if let _ = dic["data"] as? Dictionary<String, Any> {
//                                loginData.setBillTypeDataValue(withDic: dic)
//                            }
//                        }
//
//                        if self.fromBillTypeVC == true {
//                            TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//                        else {
//                            TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//
//                    }, failure: { (error) in
//                        if self.fromBillTypeVC == true {
//                            TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//                        else {
//                            TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                        }
//                    })
                    
//                    if self.fromBillTypeVC == true {
//                        TSTAPP2_Utilities.showAlertAndPop(toLevel: 2, title: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                    }
//                    else {
//                        TSTAPP2_Utilities.showAlertAndPop(withTitle: "完成申請", message: data.stringValue(ofKey: "resultMessage"), viewController: self)
//                    }
                }
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (error: Error) in
            TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
        }
        
//        TSTAPP2_API.sharedInstance.applyEBill(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, emailAddress: emailTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//            if dic.stringValue(ofKey: "code") == "00000" {
//                TSTAPP2_Utilities.showAlertAndPop(withMessage: "電子信箱設定完成，系統將在稍後寄送確認函至 \(self.emailTextField.text!)，請依信件說明 完成驗證，以啟用電子帳單服務。", viewController: self)
//            }
//            else {
//                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
//            }
//        }, failure: { (error: Error) in
//
//        })

    }
    
    //MARK: - button
    
    @IBAction func didTapCheckBox(_ sender: Any) {
        
        let isOn = checkBox.isSelected
        
        if isOn {
            checkBox.isSelected = false
        }
        else {
            checkBox.isSelected = true
            TSTAPP2_Utilities.showHtmlMessage(withTitle: "台灣之星 電子帳單服務協定", message: "<!DOCTYPE HTML> <html> <head><style> *{font-family:Helvetica,Arial,sans-serif; font-size:14px; margin:0; padding:0;} ol.main_ol{padding-right:8px;} ol li{list-style-position:outside; line-height:22px;} ol.main_ol>li{list-style-type:cjk-ideographic; margin:0 0 15px 30px; font-weight:bold;} ol.main_ol>li li{font-weight:normal;} </style></head> <body> <ol class=\"main_ol\"> <li>申請對象<div>台灣之星月租型用戶皆可申請【電子帳單服務】。</div></li><li>服務說明 <ol> <li>台灣之星(以下簡稱「本公司」) 【電子帳單服務】(以下簡稱「本服務」)係提供用戶以電子郵件式收取帳單，本服務申請成功後，本公司將自生效首期傳送【電子帳單】至用戶指定之電子郵件信箱並同時取消郵寄實體帳單予用戶。</li> <li>公司戶用戶申請本服務如需稅務申報，請將電子帳單印出後即可作為會計憑證。</li> <li>本服務提供六個月內之帳單瀏覽、帳單列印、線上繳款、帳款查詢等服務。</li> <li>用戶若欲取消本服務，可至本服務網頁申請，本公司將自取消成功後之次期帳單開始恢復郵寄體帳單。</li> <li>用戶申請本服務或變更電子郵件信箱時，應保證其填寫電子郵件信箱正確無誤，以確保將來電帳單通知函及相關訊息能準確送達；若因用戶輸入資料有誤或未主動即時更新資料所致之損害應由用自行負責。</li> <li>用戶對於【電子帳單」內容不得作任何修改，如有修改對本公司均不生效力。若用戶違反法令違反本協定之任何條款，致本公司或其關係企業受有損害時，用戶應負擔損害賠償責任。</li> <li>用戶經由本服務所下載或取得之任何資料應自負風險任何因資料之下載而導致用戶電腦系統之何損壞或資料流失等，皆由用戶自行負責，本公司對於因此所生損失或損害皆不負任何賠償責任。<li> </ol> </li> <li>繳款方式 <ol> <li>用戶可直接列印【電子帳單】（務必使用「雷射印表機」）後，至本公司各繳款通路進行繳費。如因用戶因素（例如：電子信箱空間已滿、Isp業者網路中斷等）致無法收取【電子帳單】，請用戶行至本公司網站或各直營店補印帳單，並應自行承擔因延遲繳款造成之任何費用及損害。</li> <li>用戶亦可辦理轉帳代繳、或利用網站公告繳費方式、或利用其他繳款途徑進行繳款。</li> <li>已辦理轉帳代繳之用戶仍會以原轉帳方式扣款請勿重複繳款。</li> </ol> </li> <li>服務終止或暫停： <div>如有下列情形，本公司得終止或暫停本服務之一部或全部且對用戶不負任何責任：</div> <ol> <li>因天災、不可抗力或其他非可歸責於本公司之事由所致之暫停或中斷。</li> <li>對本服務相關軟硬體設備進行搬遷、更換、保養或維修時。</li> <li>如用戶有門號退租、一退一租、過戶、更名、切結非本人申請、銷號、合併／個別出帳或其他號契約終止等情形時，本公司得逕行停止用戶使用本服務功能，但在用戶原來行動通信契約有效期內，本公司仍將繼續寄發電子帳單。</li> </ol> </li> <li>服務協定之修改 <div>本公司得隨時修改本服務協定條款，如有修改將公佈於本公司網站。修改事項於公佈時即時效，不另行個別通知用戶。</div> </li> </ol></body> </html>", viewController: self)
        }
    }
    
    @IBAction func didTapSubmitButton(_ sender: AnyObject) {
        
        let isOn = checkBox.isSelected
        
        if !isOn {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請勾選同意台灣之星電子帳單服務協定！", viewController: self)
        }
        else {
            
            let verifyRules: [Dictionary<String, Any>] = [
                [
                    "attribute": "email",
                    "label": "Email",
                    "value": self.emailTextField.text!,
                    "rules": ["required", "email"]
                ]
            ]
            
            let validator = Validators(withRules: verifyRules)
            do {
                let verifyResult = try validator.verify()
                if verifyResult == true {
                   
                    callApplyEmailAPI()
                }
            } catch {
                
                TSTAPP2_Utilities.showAlert(withMessage: validator.errors.first!, viewController: self)
            }
        }
    }
    
    
    //MARK: - textFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }

}
