//
//  ChangeNicknameViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/14.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ChangeNicknameViewController: TSUIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nicknameTextField: UITextField!
    
    @IBOutlet weak var sendButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        nicknameTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP020101", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "會員中心", action: "修改暱稱", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "修改暱稱")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP020101", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: sendButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {

        self.scrollView.endEditing(true)        
    }
    
//    MARK: - IBAction
    @IBAction func didTapSendButton(_ sender: Any) {
        
        if nicknameTextField.text != "" {
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP020101", action: "A020101")
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改暱稱(會員中心)", action: nil, label: nil, value: nil)
            
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.changeProfile(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, nickname: nicknameTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    TSTAPP2_Utilities.showAlertAndPop(withMessage: "變更暱稱成功", viewController: self)
                }
                else {
                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
            }, failure: { (error: Error) in
                
            })
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "請輸入暱稱", viewController: self)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
}
