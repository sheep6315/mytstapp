//
//  ChangePasswordViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/13.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ChangePasswordViewController: TSUIViewController, UITextFieldDelegate {

    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var rePasswordTextField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var captchaField: UITextField!
    
    @IBOutlet weak var captchaView: UIView!
    @IBOutlet weak var captchaImageView: UIImageView!
    @IBOutlet weak var captchaImageViewHeight: NSLayoutConstraint!
    
    var msisdn = ""
    var pwschangeStatus = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        let captchaImg = Captcha.sharedInstance
        captchaImg.captchaSize = CGSize(width: 120, height: 44)
        self.captchaImageView.image = captchaImg.captchaImage.image
        let tapCaptchaGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.didTapRefreshCaptcha))
        self.captchaImageView.addGestureRecognizer(tapCaptchaGestureRecognizer)
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        if pwschangeStatus == "2" {
            self.title = "變更會員密碼"
            captchaView.isHidden = false;
            captchaImageViewHeight.constant = 44
        }
        else if pwschangeStatus == "0" {
            captchaView.isHidden = true;
            captchaImageViewHeight.constant = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if pwschangeStatus == "0" {
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP010202", action: "")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
        
        if pwschangeStatus == "0" {
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP010202", action: "")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapRefreshCaptcha() {
        let captchaImg = Captcha.sharedInstance
        self.captchaImageView.image = captchaImg.refresh().image
    }
    
//    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: false, notification: notification)
    }
    
    //MARK: - textFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    //    MARK: - IBAction
    @IBAction func didTapSubmitButton(_ sender: AnyObject) {
        
        if pwschangeStatus == "2" {
            if self.captchaField.text != Captcha.sharedInstance.captchaString {
                TSTAPP2_Utilities.showAlert(withMessage: "驗證碼輸入錯誤", viewController: self)
                return
            }
        }
        
        let regex = try! NSRegularExpression(pattern: ".*[^0-9a-zA-Z].*", options: .caseInsensitive)
        
        if passwordTextField.text != "" && rePasswordTextField.text != "" {
            
            let pwText = passwordTextField.text!
            var validPW = true
            
            let range: NSRange = NSMakeRange(0, pwText.utf16.count)
            let matchArray: Array = regex.matches(in: pwText, options: NSRegularExpression.MatchingOptions.anchored, range: range)
            if matchArray.count > 0 {
                validPW = false
            }
            
            if passwordTextField.text!.count < 6 || passwordTextField.text!.count > 12 || !validPW {
                TSTAPP2_Utilities.showAlert(withMessage: "密碼格式錯誤，必須為6-12碼英數字", viewController: self)
            }
            else if passwordTextField.text != rePasswordTextField.text {
                TSTAPP2_Utilities.showAlert(withMessage: "新密碼輸入不一致", viewController: self)
            }
            else {
                if pwschangeStatus == "0" {
                    UbaRecord.sharedInstance.sendUba(withPage: "APP010202", action: "A010202")
                
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "首次登入修改密碼", action: nil, label: nil, value: nil)
                }
                
//                let loginData = LoginData.sharedLoginData()
//                let password = UserDefaults.standard.string(forKey: ACCOUNT_PASSWORD)!
                TSTAPP2_API.sharedInstance.setPassword(withMSISDN: msisdn, oldPassword: password, newPassword: passwordTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        
//                        if self.pwschangeStatus == "0" {
//                            UserDefaults.standard.set(self.passwordTextField.text!, forKey: ACCOUNT_PASSWORD)
//                            self.performSegue(withIdentifier: "toTabBar", sender: nil)
//                        }
//                        else if self.pwschangeStatus == "2" {
                        
                        let action = UIAlertAction(title: "重新登入", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
                            
                            TSTAPP2_Utilities.logout(completion: {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as UIViewController
                                self.show(viewController, sender: nil)
                            }, failure: {
                                
                            })
                        })
                        TSTAPP2_Utilities.showAlert(withMessage: "密碼已修改完成，下次請使用新密碼登入。", action: action, viewController: self)
                        

//                        }
                        
                    }
                    else {
                        TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                    }
                }, failure: { (error: Error) in
                    
                })
            }
            
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "密碼格式錯誤，必須為6-12碼英數字", viewController: self)
        }
        
        
    }
    
//    MARK: -
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.view.endEditing(true)
        
    }


}
