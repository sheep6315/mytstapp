//
//  CheckLock.swift
//  TSTAPP2
//
//  Created by 詮通電腦-江宗澤 on 2017/6/5.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class CheckLock:NSObject {
    
    static let sharedInstance = CheckLock()
    
    private(set) var canAdjustment:String = ""
    private(set) var resultMessage:String = ""
    private(set) var dcb:CheckLockDetail  = CheckLockDetail()
    private(set) var mpg:CheckLockDetail  = CheckLockDetail()
    private(set) var subtitle:String = ""
    private(set) var title:String = ""
    private(set) var content:String = ""
    
    private override init() {}
    
    public func setDataValue(withDic dic:Dictionary<String, Any>) {
        if let data = dic["data"] as? Dictionary<String, Any> {
            canAdjustment = data.stringValue(ofKey: "canAdjustment")
            resultMessage = data.stringValue(ofKey: "resultMessage")
            subtitle = data.stringValue(ofKey: "subtitle")
            title = data.stringValue(ofKey: "title")
            content = data.stringValue(ofKey: "description")
            
            if let dcbDic = data["dcb"] as? Dictionary<String, Any> {
                dcb = CheckLockDetail(withDic: dcbDic)
            }
            if let mpgDic = data["mpg"] as? Dictionary<String, Any> {
                mpg = CheckLockDetail(withDic: mpgDic)
            }
        }
    }
}

class CheckLockDetail:NSObject {
    
    private(set) var code:String             = ""
    private(set) var message:String          = ""
    private(set) var lock:String             = ""
    private(set) var showSwitch:String       = ""
    private(set) var changeSwitch:String     = ""
    private(set) var needSecurityCode:String = ""
    private(set) var resultMessage:String    = ""
    
    override init() {}
    
    init(withDic data:Dictionary<String, Any>) {
        code             = data.stringValue(ofKey: "code")
        message          = data.stringValue(ofKey: "message")
        lock             = data.stringValue(ofKey: "lock")
        showSwitch       = data.stringValue(ofKey: "showSwitch")
        changeSwitch     = data.stringValue(ofKey: "changeSwitch")
        needSecurityCode = data.stringValue(ofKey: "needSecurityCode")
        resultMessage    = data.stringValue(ofKey: "resultMessage")
    }
    
    
}
