//
//  ChooseSendTypeViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/24.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ChooseSendTypeViewController: TSUIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet var radioButton: [UIButton]!
    
    @IBOutlet weak var smsLabel: UILabel!
    
    @IBOutlet weak var mailLabel: UILabel!
    
    @IBOutlet weak var sendBySMS: UIButton!
    
    
    @IBOutlet weak var sendByMailView: UIView!
    @IBOutlet weak var sendByMailViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sendByMail: UIButton!
    
    var msisdn : String = ""
    var customerId : String = ""
    var mail : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let msisdnEncode = TSTAPP2_Utilities.encodeForSecurity(sourceString: msisdn, type: .Phone)
        smsLabel.text = "簡訊傳送(將傳送到：\(msisdnEncode) )"
        
        if mail != "" {
            let mailEncode = TSTAPP2_Utilities.encodeForSecurity(sourceString: mail, type: .Email)
            mailLabel.text = "電子郵件傳送(將傳送到：\(mailEncode) )"
        }
        else {
            sendByMailView.isHidden = true
            sendByMailViewHeight.constant = 0
        }
        
//        self.navigationItem.setHidesBackButton(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapRadioButton(_ sender: UIButton) {
        
        for button in radioButton {
            
            button.isSelected = button.tag == sender.tag ? true:false
        }
    }
        
    @IBAction func didTapNext(_ sender: Any) {
        
        if !sendBySMS.isSelected && !sendByMail.isSelected {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇傳送方式", viewController: self)
        }
        else if sendBySMS.isSelected {
            
            callForgetPwd(sendType: "1")
        }
        else {
            
            callForgetPwd(sendType: "2")
        }
    }
    
    //MARK: API
    func callForgetPwd(sendType :String) {
        
        TSTAPP2_API.sharedInstance.forgetPassword(withMSISDN: msisdn, custId: customerId, notificationType: sendType, notificationEmail: mail, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                let action = UIAlertAction(title: "重新登入", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
//                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                    self.navigationController?.show(storyBoard.instantiateInitialViewController()!, sender: self)
                    if let viewCount = self.navigationController?.viewControllers.count {
                        let vc = self.navigationController?.viewControllers[viewCount - 3]
                        self.navigationController?.popToViewController(vc!, animated: true)
                    }
                })

                TSTAPP2_Utilities.showAlert(withMessage: "您的密碼完成申請，可以使用網站功能。提醒您自下次登入開始，請使用新密碼登入。\n謝謝您的愛用！", action: action, viewController: self)
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (error: Error) in
            
        }
        
    }
}
