//
//  ContactUsIssueQuery.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/9.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class IssueList: NSObject {
    
    private(set) var list: [Issue] = []

    init(withDic dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "code") == "00000" {
            if let data = dic["data"] as? Dictionary<String, Any> {
                if let issueList = data["issueList"] as? [Dictionary<String, Any>] {
                    list = []
                    for issue in issueList {
                        list.append(Issue.init(withDic: issue))
                    }
                }
            }
        }
    }
    
}

class Issue: NSObject {
    
    // 發問時間
    private(set) var date: String = ""
    
    // 問題類型
    private(set) var issueType: String = ""
    
    // 是否讀取
    private(set) var isRead: Bool = false
    
    // 發問內容
    private(set) var issueNote: String = ""
    
    // 回覆時間
    private(set) var replyTime: String = ""
    
    // 回覆內容
    private(set) var replyContent: String = ""
    
    //issueId
    private(set) var issueId: String = ""
    
    //status
    private(set) var status: String = ""
    
    private(set) var displayAskButton: Bool = false
    
    private(set) var issueSubList: [IssueSub] = []
    
    
    init(withDic dic: Dictionary<String, Any>) {
        
        self.date = dic.stringValue(ofKey: "date")
        self.issueType = dic.stringValue(ofKey: "issueType")
        self.isRead = (dic["isRead"] as? String == "Y") ? true : false
        self.issueNote = dic.stringValue(ofKey: "issueNote")
        self.replyTime = dic.stringValue(ofKey: "replyTime")
        self.replyContent = dic.stringValue(ofKey: "replyContent")
        self.issueId = dic.stringValue(ofKey: "issueId")
        self.status = dic.stringValue(ofKey: "status")
        
        self.issueSubList = []
        if let subList = dic["subList"] as? [Dictionary<String, Any>] {
            for subData in subList {
                self.issueSubList.append(IssueSub.init(withDic: subData))
            }
        }
        
        if issueSubList.count > 0 {
            let last = issueSubList.last?.replyContent
            if last != "" {
                displayAskButton = true
            }
        }
        else {
            if replyContent != "" {
                displayAskButton = true
            }
        }
    }
}

class IssueSub: NSObject {
    
    private(set) var date: String = ""
    
    private(set) var issueNote: String = ""
    
    private(set) var replyContent: String = ""
    
    private(set) var replyTime: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        
        self.date = dic.stringValue(ofKey: "date")
        self.issueNote = dic.stringValue(ofKey: "issueNote")
        self.replyContent = dic.stringValue(ofKey: "replyContent")
        self.replyTime = dic.stringValue(ofKey: "replyTime")
        
    }
}



class ContactUsIssueQueryList: NSObject {
    
    static let sharedInstance = ContactUsIssueQueryList()
    
    // 客服問題清單
    private(set) var queryList: [ContactUsIssueQuery] = []
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        self.queryList = []
        let response:Dictionary<String, Any> = dic["GetContactUsIssueQueryRes"] as? [String : Any] ?? ["ResultCode": "99999", "ResultText": "API回傳格式異常"]
        let resultCode = response["ResultCode"] as? String ?? ""
        if resultCode == "00000" {
            let rawData = response["respData"] as! Dictionary<String, Any>
            if let repliesDetails = rawData["RepliesDetails"] as? Array<Dictionary<String, String>> {
                repliesDetails.forEach({ (raw:[String : String]) in
                    let issueQuery = ContactUsIssueQuery(withDic: raw)
                    
                    if issueQuery.parentId != "" {
                        let note = "\(issueQuery.createDate)<br>問：\(issueQuery.issueNote)"
                        var content = issueQuery.replyContent
                        
                        if content != "" {
                            content = "\(issueQuery.replyDate)<br>答：\(content)"
                        }
                        for query in queryList {
                            if query.id == issueQuery.parentId {
                                query.replyContent = "\(query.replyContent)<br>\(note)<br><br>\(content)<style>body{font-size:17px;color:#686868}</style>"
                                if content != "" {
                                    query.displayAskButton = true
                                }
                            }
                        }
                        
                    }
                    else {
                        if issueQuery.replyContent != "" {
                            issueQuery.displayAskButton = true
                        }
                        self.queryList.append(issueQuery)
                    }
                    
//                    self.queryList.append(issueQuery)
                })
            } else if let repliesDetails = rawData["RepliesDetails"] as? Dictionary<String, String> {
                let issueQuery = ContactUsIssueQuery(withDic: repliesDetails)
                self.queryList.append(issueQuery)
            }
        }
    }
}

class ContactUsIssueQuery: NSObject {
    
    // 發問時間
    private(set) var createDate: String = ""
    
    // 問題類型
    private(set) var issueType: String = ""
    
    // 是否讀取
    private(set) var isRead: Bool = false
    
    // 發問內容
    private(set) var issueNote: String = ""
    
    // 回覆時間
    private(set) var replyDate: String = ""
    
    // 回覆內容
//    private(set) var replyContent: String = ""
    var replyContent: String = ""
    
    //id
    private(set) var id: String = ""
    
    //parentId
    private(set) var parentId: String = ""
    
    var displayAskButton: Bool = false
    
    init(withDic dic: Dictionary<String, String>) {
        
        self.id = dic.stringValue(ofKey: "id")
        self.createDate = dic["createDate"] ?? ""
        self.issueType = dic["issueTyep"] ?? ""
        self.isRead = (dic["isRead"] == "true") ? true : false
        self.parentId = dic.stringValue(ofKey: "parentId")
        self.replyDate = dic["replyDate"] ?? ""
        if let issueNote = dic["issueNote"] {
            let decodeData = NSData(base64Encoded: issueNote, options: NSData.Base64DecodingOptions.init(rawValue: 0))
            self.issueNote = NSString(data: (decodeData as Data?)!, encoding: String.Encoding.utf8.rawValue)! as String
        }
        
        if let replyContent = dic["replyContent"] {
            let decodeData = NSData(base64Encoded: replyContent, options: NSData.Base64DecodingOptions.init(rawValue: 0))
            self.replyContent = NSString(data: (decodeData as Data?)!, encoding: String.Encoding.utf8.rawValue)! as String
        }
    }
}
