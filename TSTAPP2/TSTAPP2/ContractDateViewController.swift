//
//  ContractDateViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/9/14.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

class ContractDateViewController: TSUIViewController {
    
    @IBOutlet weak var contractStartDateLabel: UILabel!
    
    @IBOutlet weak var contractExpireDateLabel: UILabel!
    
    @IBOutlet weak var contractBannerImageView: UIImageView!
    
    @IBOutlet weak var contractBannerAspect: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let loginData = LoginData.sharedLoginData()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        self.contractStartDateLabel.text = loginData.custProfile_activationDate.components(separatedBy: " ").first!
        self.contractExpireDateLabel.text = loginData.custProfile_expiredDate.components(separatedBy: " ").first!
        
        if SmartBannerList.sharedInstance.contractBanner.count > 0 {
            //            contractBannerHeight.constant = 145
            if let url = URL(string: (SmartBannerList.sharedInstance.contractBanner.first?.path)!) {
                self.contractBannerImageView.setImageWith(url, placeholderImage: UIImage(named: "img_placeHolder"))
            }
            else {
                self.contractBannerImageView.image = UIImage(named: "img_placeHolder")
            }
            
        }
        else {
            contractBannerAspect = contractBannerAspect.setMultiplier(multiplier: -1)
            //            contractBannerHeight.constant = 0
            //            self.contractBannerImageView.image = UIImage(named: "img_placeHolder")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_CONT_TERM", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "我的專案", label: "合約起訖日", value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "合約起訖日")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_CONT_TERM", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    MARK: - IBAction
    @IBAction func didTapContractBannerButton(_ sender: Any) {
        if SmartBannerList.sharedInstance.contractBanner.count > 0 {
            if let urlString = SmartBannerList.sharedInstance.contractBanner.first?.url {
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "合約起訖日Banner", label: urlString, value: nil)
                
                let smartBanner = SmartBannerList.sharedInstance.contractBanner.first!
                
                UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                                 action: "ACMP_\(SmartBannerList.sharedInstance.cmpId)_\(SmartBannerList.sharedInstance.cmpVersion)_\(smartBanner.displayType)_\(smartBanner.sort)",
                    codeId: SmartBannerList.sharedInstance.cmpId,
                    codeVersion: SmartBannerList.sharedInstance.cmpVersion,
                    codeDisplayType: smartBanner.displayType,
                    codeSort: smartBanner.sort)
                
                if smartBanner.action == AUTO_BROWSER {
                    if let url = URL(string: urlString) {
                        UIApplication.shared.openURL(url)
                    }
                }
                else {
                    TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
                }
                
                Answers.logContentView(withName: "智能推薦",
                                       contentType: "AD",
                                       contentId: urlString,
                                       customAttributes: [:])
                
            }
        }
    }

}
