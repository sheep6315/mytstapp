//
//  CreditCardTransferChangeViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/9.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class CreditCardTransferChangeViewController: TSUIViewController,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var msisdn: UILabel!
    
    @IBOutlet weak var customerId: UILabel!
    
    @IBOutlet weak var cardNumber: UILabel!
    
    @IBOutlet weak var serviceTypeChange: UIButton!
    
    @IBOutlet weak var serviceTypeExpired: UIButton!
    
    @IBOutlet weak var serviceTypeCancel: UIButton!
    
    @IBOutlet var radioButton: [UIButton]!
    
    @IBOutlet weak var cardTypeView: UIView!
    
    @IBOutlet weak var firstNumber: UITextField!
    
    @IBOutlet weak var secondNumber: UITextField!
    
    @IBOutlet weak var thirdNumber: UITextField!
    
    @IBOutlet weak var fourthNumber: UITextField!
    
    @IBOutlet weak var cardNumberView: UIView!
    
    @IBOutlet weak var expiredDateView: UIView!
    
    @IBOutlet weak var monthTextField: UITextField!
    
    @IBOutlet weak var monthButton: UIButton!
    
    @IBOutlet weak var yearTextField: UITextField!
    
    @IBOutlet weak var yearButton: UIButton!
    
    @IBOutlet weak var ccvView: UIView!
    
    @IBOutlet weak var ccvTextField: UITextField!
    
    @IBOutlet weak var checkBox: UIButton!
        
    @IBOutlet var constraints: [NSLayoutConstraint]!
    
    @IBOutlet weak var validDateButton: UIButton!
    
    @IBOutlet weak var validDateTextField: UITextField!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var yearList : Array<String> = []
    var monthList : Array<String> = []
    var seletedYear : String = ""
    var seletedMonth : String = ""
    var checkBoxOn : Bool = false
    var directDebit : DirectDebit = DirectDebit.sharedInstance
    var isShowPrivacy = false

    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        monthTextField.delegate = self
        yearTextField.delegate = self
        
        monthButton.layer.borderWidth = 1
        monthButton.layer.cornerRadius = 5
        monthButton.layer.borderColor = UIColor.lightGray.cgColor
        yearButton.layer.borderWidth = 1
        yearButton.layer.cornerRadius = 5
        yearButton.layer.borderColor = UIColor.lightGray.cgColor
//        ccvTextField.layer.borderColor = UIColor.lightGray.cgColor
//        ccvTextField.layer.cornerRadius = 5
//        ccvTextField.layer.borderWidth = 1
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(gesture)
        
        
        msisdn.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: LoginData.sharedLoginData().custProfile_msisdn, type: .Phone)
        customerId.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: LoginData.sharedLoginData().custProfile_custId, type: .CustomerId)
        cardNumber.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: directDebit.cardNumber, type: .CardNumber)
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        setPickerData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0505", action: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        didTapRadioButton(serviceTypeChange)
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0505", action: "")
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    func setPickerData() {
        
        monthList = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        
        let calendar = Calendar(identifier: .gregorian)
        let year = calendar.component(.year, from: Date())
        
        for i in 0...14 {
            yearList.append("\(year+i)")
        }
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        var pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        yearTextField.inputView = pickerView
        yearTextField.inputAccessoryView = pickBar
        
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 1
        monthTextField.inputView = pickerView
        monthTextField.inputAccessoryView = pickBar
        
        let validDatePickerView = UIPickerView()
        validDatePickerView.delegate = self
        validDatePickerView.dataSource = self
        validDatePickerView.tag = 2
        validDateTextField.inputView = validDatePickerView
        validDateTextField.inputAccessoryView = pickBar
        
    }
    
    //MARK: - 按鈕
    @objc func didTapDone(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapCheckBox(_ sender: Any) {
        
        if checkBoxOn {
            
            checkBox.isSelected = false
        }
        else {
            
            checkBox.isSelected = true
            
                        TSTAPP2_Utilities.showHtmlMessage(withTitle: "服務注意事項", message: "<b>轉帳代繳授權約定條款：</b><br />立授權書人(以下稱授權人)即信用卡持有人，已經由台灣之星官網或門市公告了解並同意台灣之星之「隱私權聲明條款」所載內容。並同意台灣之星蒐集、處理及利用本人之個人資料。授權人茲為便於台灣之星對電信服務之申請人(以下簡稱「用戶」)收取費用，謹授權並同意 貴金融機構為支付該等款項之代理人，並同意遵守如下條款：<br /><br /><p>1、授權人謹授權並同意 貴金融機構按期於付款期限日，自授權人於 貴金融機構發行之信用卡，自動轉帳繳納前列用戶帳單應繳之總額【包括電信服務費用及其他應支付款項（如代收代付款、小額付款等）】，若　貴金融機構因其他原因拒絕授權支付全部費用時，將優先扣抵電信服務費用，其餘未扣抵之部分用戶則需自行依台灣之星規定之其他繳費方式繳清該期應繳款項。<br /><br />2、使用信用卡辦理轉帳代繳，授權人與用戶須為同一人，且信用卡之有效期限至少尚餘兩個月。本授權填寫之內容不全、有誤或未載明用戶之行動電話號碼時，本授權不發生效力。申請或變更轉帳代繳，信用卡處理時間約為7個工作天。<br /><br />3、授權人辦理信用卡轉帳代繳，為便利作業，若信用卡有效期屆滿，視同該信用卡到期後轉帳代繳授權自動展延，授權人應立即告知（請以官網或客服專線）台灣之星新卡之有效期限，未於到期前告知者，台灣之星將主動展延。若造成扣款作業失敗，用戶需採其他繳款方式自行繳款。<br /><br />4、若信用卡因遺失、被竊或其他原因喪失占有，或信用卡因停止使用、不續卡、欠款、未開卡或其他情事，造成扣款失敗，或有金融機構拒絕授權支付全部款項之情事，因此發生之損失及責任概由用戶自行負擔，台灣之星得不經催告逕行終止本轉帳代繳授權，用戶並應自行依台灣之星規定之其他繳款方式繳清該期應繳款項。<br /><br />5、授權人終止本授權時，應向台灣之星提出申請，經台灣之星同意並俟相關金融機構辦妥有關手續後生效；未生效前，用戶仍應依原授權內容繳納屆期之所有帳單款項。<br /><br />6、授權人/用戶對當期應繳之費用有疑義時，被授權之信用卡仍將全額扣繳帳單所列之金額，若用戶有住址遷移、停用等異動事項，應即向本公司辦妥各項手續，否則一切損失及責任，概由用戶自行負擔。如確實有溢繳或繳款不足之情形時，則台灣之星將於下期帳單中自動扣除或增列該款項。<br /><br />7、授權人同意，如其他門號併入本授權扣款之門號合併出帳，自合併出帳開始後，所有門號視同自本授權信用卡轉帳代繳；若分裂帳單後另行出帳之門號即不適用於本授權帳號扣款。如有任何疑問，請以手機直撥123或0908-000-123客服專線洽詢。</p><style>p {padding-left: 1em;line-height: 1.5em;}</style>", viewController: self)
        }
        
        checkBoxOn = !checkBoxOn
    }

    @IBAction func didTapSubmit(_ sender: Any) {
        
        let validDateArray = validDateButton.titleLabel?.text?.components(separatedBy: "/")
        let validYear = validDateArray![1]
        let validMonth = validDateArray![0]
        
        if !serviceTypeExpired.isSelected && !serviceTypeChange.isSelected && !serviceTypeCancel.isSelected {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇服務種類", viewController: self)
        }
        else if checkBoxOn == false {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請勾選本人已詳閱及了解本服務注意事項並同意遵守", viewController: self)
        }
        else if serviceTypeCancel.isSelected {
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "信用卡轉帳申請", action: "異動取消", label: nil, value: nil)
            callSubmitDirectDebitAPI(expiredDate: directDebit.expiredDate, cardNumber: directDebit.cardNumber, cvv: directDebit.ccv, action: "C")
        }
        else if serviceTypeExpired.isSelected {
            
            let expiredDate = "\(validYear)\(validMonth)"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM"
            formatter.timeZone = TimeZone(secondsFromGMT: 8)
            let cardDate = formatter.date(from: "\(validYear)/\(validMonth)")
            let now = formatter.date(from: TSTAPP2_Utilities.nowDateFromGMT(dateFormatter: "yyyy/MM"))!
            let result = cardDate?.compare(now)
            
            if result != ComparisonResult.orderedDescending {
                
                TSTAPP2_Utilities.showAlert(withMessage: "信用卡有效期限必須大於本月", viewController: self)
            }
            else {
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "信用卡轉帳申請", action: "異動展延", label: nil, value: nil)
                
                callSubmitDirectDebitAPI(expiredDate: expiredDate, cardNumber: directDebit.cardNumber, cvv: directDebit.ccv, action: "E")
            }
        }
        else if serviceTypeChange.isSelected{
            
            let cardNumberLength = (firstNumber.text ?? "").count + (secondNumber.text ?? "").count + (thirdNumber.text ?? "").count + (fourthNumber.text ?? "").count
            let ccv = ccvTextField.text?.lengthOfBytes(using: .utf8) ?? 0
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM"
            formatter.timeZone = TimeZone(secondsFromGMT: 8)
            let cardDate = formatter.date(from: "\(validYear)/\(validMonth)")
            let now = formatter.date(from: TSTAPP2_Utilities.nowDateFromGMT(dateFormatter: "yyyy/MM"))!
            let result = cardDate?.compare(now)
            
            var first = ""
            if (firstNumber.text?.lengthOfBytes(using: String.Encoding.utf8))! > 0 {
                let nsstring = firstNumber.text! as NSString
                first = nsstring.substring(to: 1)
            }
            
            if cardNumberLength < 16 {
                
                TSTAPP2_Utilities.showAlert(withMessage: "信用卡卡號不正確", viewController: self)
            }
            else if ccv < 3 {
                
                TSTAPP2_Utilities.showAlert(withMessage: "信用卡卡片後三碼不正確", viewController: self)
            }
            else if result != ComparisonResult.orderedDescending {
                
                TSTAPP2_Utilities.showAlert(withMessage: "信用卡有效期限必須大於本月", viewController: self)
            }
            else if first != "3" && first != "4" && first != "5"{
                
                TSTAPP2_Utilities.showAlert(withMessage: "信用卡類型不符", viewController: self)
            }
            else {
                
               TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "信用卡轉帳申請", action: "異動變更卡號", label: nil, value: nil)
                
                let expiredDate = "\(validYear)\(validMonth)"
                let cardNumber = firstNumber.text! + secondNumber.text! + thirdNumber.text! + fourthNumber.text!
                let ccv = ccvTextField.text!
                
                callSubmitDirectDebitAPI(expiredDate: expiredDate, cardNumber: cardNumber, cvv: ccv, action: "M")
            }
        }
    }
    
    @IBAction func didTapValidDateButton(_ sender: Any) {
//        validDateButton.setTitle("\(monthList.first!)/\(yearList.first!)", for: UIControlState.normal)
//        validDateButton.setTitleColor(UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1), for: UIControlState.normal)
        validDateTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapYear(_ sender: Any) {
        
        seletedYear = seletedYear == "" ? yearList.first! : seletedYear
        yearButton.setTitle(seletedYear, for: .normal)
        yearTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapMonth(_ sender: Any) {
        
        seletedMonth = seletedMonth == "" ? monthList.first! : seletedMonth
        monthButton.setTitle(seletedMonth, for: .normal)
        monthTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapPrivacy(_ sender: Any) {
        
        if !isShowPrivacy {
            self.isShowPrivacy = true
            TSTAPP2_Utilities.showHtmlMessage(withTitle: "隱私權聲明條款", message: "<body>台灣之星電信股份有限公司(以下稱台灣之星)非常重視本網站使用者的隱私權及保護使用者所提供的個人資料，為保障使用者的權益及讓使用者瞭解台灣之星直接或間接蒐集、利用、處理及/或傳輸使用者個人資料等情形，請使用者詳閱下列台灣之星的隱私權保護政策及依「個人資料保護法」之規定所作之說明暨使用者享有之權利或服務：<br /><br /><div class='title'>適用的範圍</div>隱私權保護政策的內容，包括台灣之星如何處理使用者使用網站服務、參加網站活動、申請加入網站會員、進行線上購物及申請門號與加值服務時所蒐集到的身份識別資料。<br /><br /><div class='title'>資料蒐集的事由、目的與範圍</div>台灣之星為履行法定義務、契約義務、提供或辦理各項電信商品或服務，以及遵守通訊傳播監理、司法與其他依法具有司法或行政調查權公務機關之命令及查核，暨台灣之星業務、行政管理以及訴訟、非訟或其他紛爭事件之處理等事由與目的，於所涉業務執行之必要範圍內，而有必要直接或間接蒐集、處理、利用及/或國際傳輸使用者個人資料。<br /><br />為了確認在台灣之星網站（下稱本網站www.tstartel.com）上取得資訊的使用者身份及提供有關行動電話各項服務之用，其範圍如下：<br />台灣之星在使用者使用台灣之星的產品或服務、瀏覽台灣之星網頁、參加宣傳活動或贈獎遊戲、訂閱電子報或使用其他網站服務時，將會請使用者提供個人相關資料，包括但不限於行動電話門號、身分證字號、暱稱、行動電話USIM卡號碼、姓名、電子郵件、職業或行業別、婚姻、出生年月日、性別、教育程度、個人收入、地址⋯⋯等。<br />台灣之星會保留使用者所提供的上述資料，也會保留使用者上網瀏覽或查詢時，在系統上產生的相關記錄，包括IP位址、使用時間、瀏覽器、瀏覽及點選紀錄等。依「個人資料保護法」第八條規定之告知內容：<br /><ol><li>非公務機關名稱：台灣之星電信股份有限公司。</li><li>蒐集之目的：○四○ 行銷/○六三 非公務機關依法定義務所進行個人資料之蒐集處理及利用/○六七 信用卡、現金卡、轉帳卡或電子票證業務/○六九 契約、類似契約或其他法律關係事務/○七二 政令宣導/○八一 個人資料之合法交易業務/○八五 旅外國人急難救助/○九○ 消費者、客戶管理與服務/○九一 消費者保護/一○四 帳務管理及債權交易業務/一二九 會計與相關服務/一三三 經營電信業務與電信加值網路業務/一三五 資(通)訊服務/一三六 資(通)訊與資料庫管理/一三七 資通安全與管理/一四八 網路購物及其他電子商務服務/一五二 廣告或商業行為管理/一五三 影視、音樂與媒體管理/一五四 徵信/一五七 調查、統計與研究分析/一八一 其他經營合於營業登記項目或組織章程所定之業務/一八二 其他諮詢與顧問服務及其他依主管機關公告之特定目的。</li><li>個人資料之類別：辨識個人資料、辨識財務者、政府資料中之辨識者、個人描述、財務交易及其他依主管機關公告之個人資料類別。</li><li>個人資料利用之期間、地區、對象及方式：<ol><li>期間：台灣之星營運期間</li><li>地區：中華民國境內、境外（主管機關禁止者不在此限）。</li><li>對象：台灣之星、與台灣之星有合作或業務往來關係之企業及合作廠商。</li><li>方式：符合法令規定範圍之利用。</li></ol></li><li>使用者在符合電信相關法規之情形下，就其個人資料得依「個人資料保護法」規定行使下列權 利，行使方式依台灣之星作業規範：<ol><li>查詢或請求閱覽。</li><li>請求製給複製本。</li><li>請求補充或更正。</li><li>請求停止蒐集、處理或利用。</li><li>請求刪除。</li><li>請求拒絕行銷。</li></ol></li><li>使用者得自由選擇提供資料（但依法令規定者不在此限），若不提供將影響電信服務之完整性。</li></ol><div class='title'>資料使用的範圍與保護</div>為了保護使用者個人資料之完整及安全，保存使用者個人資料之資料處理系統均已受妥善的維護，並符合相關主管機關嚴格之要求，以保障使用者的個人資料不會被不當取得或破壞。如因業務需要有必要委託第三者時，台灣之星亦會嚴格要求遵守保密義務，並採取必要檢查程序以確定其確實遵守。<br /><br />為了提供使用者更多服務或優惠，如需要求使用者將個人資料提供予台灣之星之優良商業夥伴或合作廠商時，我們會在活動時提供充分說明，使用者可以自由選擇是否接受這項特定服務或優惠。<br /><br />台灣之星對於使用者個人資料之使用，僅在蒐集特定目的及相關法令規定之範圍內為之。<br /><br />由於科技的技術性問題，使得任何資料在網際網路傳輸的過程中，仍然不具有完全的隱密性，因此，使用者亦必須體認到上述的事實。<br /><br />台灣之星充分瞭解到保護使用者資料是我們的責任。因此，使用者的個人資料將會完整儲存在台灣之星的資料庫中，台灣之星將盡最大的努力，以嚴密的保護措施維護並防止未經過授權人員接觸。除非經由使用者的同意或其他法令之特別規定，否則，本網站絕不會將使用者的個人資料揭露給第三人或使用於蒐集目的以外之其他用途。<br /><br /><div class='title'>用戶個人資料的使用與修改</div>使用者在本網站中可以隨時利用您個人申請的帳號和密碼，更改使用者所輸入的任何個人或公司資料。<br /><br /><div class='title'>Cookie、Web Beacon之運用</div>台灣之星為了提供個別化的服務，方便使用者使用或為了統計分析瀏覽模式，以改善服務等目的，且為了讓資訊系統能夠辨識使用者的資訊，資訊系統將以Cookie、Web Beacon或其他方式記錄使用者使用網路的行為。<br /><br />資訊系統伺服器會將所需的資訊經由瀏覽器寫入使用者的儲存設備中，下次瀏覽器在要求伺服器傳回網頁時，會將Cookie的資料先傳給伺服器，伺服器可依據Cookie的資料判斷使用者，網頁伺服器可針對使用者之使用行為來執行不同動作或傳回特定的資訊。<br /><br />Web Beacon稱網路信標是一種1X1像素或電子圖片檔，部分網頁會使用Web Beacon結合Cookie之運用蒐集有關使用者於網站使用的情況，技術運用上可能結合或利用其他工具彙整統計資料，以計算網站或網頁使用人次或辨識使用者在網站的互動情況，並依不同喜好執行不同資訊之傳遞。<br /><br /><div class='title'>隱私權保護政策的例外</div>若使用者因犯罪嫌疑，經政府機關或司法機關依法調查、偵查時，本公司及網站有義務協助配合，並提供使用者之相關資料給予政府或司法機關。<br /><br />本網站的網頁可能提供其他網站的網路連結，您可經由本網站所提供的連結，點選進入其他網站，任何與台灣之星連結的網站，亦可能蒐集使用者個人資料，該連結網站應有個別的隱私權保護政策，台灣之星不負任何連帶責任，亦不保護使用者於該等連結網站中的隱私權；除了使用者主動登錄網站所提供的個人資料，使用者如在台灣之星網站或服務中的討論版等類似場域主動提供之個人資料，這種形式的資料提供，亦不在本隱私權保護政策的範圍之內。<br /><br /><div class='title'>隱私權保護政策的修正</div>台灣之星隱私權保護政策將因應科技發展的趨勢、法規之修訂或其他環境變遷等因素而做適當的修改，以落實保障使用者隱私權之立意。修正過之條款，也將會立即刊登在本網站上。<br /><br />本條款之解釋及適用、以及使用者因使用本服務而與本公司間所生之權利義務關係，應依中華民國法令解釋適用之。其因此所生之爭議，以台灣台北地方法院為第一審管轄法院。<br /><br />使用者若想進一步瞭解台灣之星蒐集、處理、利用及/或傳輸個人資料之管理方針及使用者所享有之權利或服務，請透過客服專線（手機直撥123或撥0908-000-123）致電洽詢。<br /><br /><div class='title'>著作權保護措施</div>使用者如透過本公司之網路連接至網際網路任意上傳、下載、轉貼未經權利人合法授權之影像、圖片、音樂、文章或其他受著作權法保護之著作，或利用P2P、Foxy等軟體非法傳輸上述著作者，本公司將依著作權法「第六章之一：網路服務提供者之民事免責事由（即第90條之4以下）」及98年11月17日經濟部智慧財產局訂定發布之「網路服務提供者民事免責事由實施辦法」等相關規定執行著作權保護措施，詳情請參閱:經濟部智慧財產局網站<br /><a href=\"http://www.tipo.gov.tw/\"><font color=\"#819c21\">http://www.tipo.gov.tw/</font></a>。<br /><p align=\"right\">版次：231115-002</p></body><style>body {color: #666666;line-height: 1.5em;}.title {color: #ad267c;font-size: 1.2em;line-height: 1.8em;}ol {padding-left: 1em;}</style>", viewController: self, completion: {
                self.isShowPrivacy = false
            })
        }
    }
    
    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
//    MARK: - 
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            secondNumber.isUserInteractionEnabled = true
            thirdNumber.isUserInteractionEnabled = true
            fourthNumber.isUserInteractionEnabled = true
        }
    }

    @IBAction func didTextFieldValueChange(_ sender: UITextField) {
        
        let textField = sender
        let length = textField.text?.lengthOfBytes(using: .utf8)
        
        if textField.tag == 0 && length == 4 {
            
            secondNumber.becomeFirstResponder()
        }
        else if textField.tag == 1 && length == 4 {
            
            thirdNumber.becomeFirstResponder()
        }
        else if textField.tag == 2 && length == 4 {
            
            fourthNumber.becomeFirstResponder()
        }
        else if textField.tag == 3 && length == 4 {
            
            fourthNumber.resignFirstResponder()
        }
        else if (textField.tag == 5 && length == 3) {
            
            ccvTextField.resignFirstResponder()
        }
    }
    
//    MARK: -     
    @IBAction func didTapRadioButton(_ sender: UIButton) {
        
        for button in radioButton {
            
            button.isSelected = button.tag == sender.tag ? true:false
        }
        
        if sender == serviceTypeCancel {
            
            hiddenByHeight(view: cardTypeView)
            hiddenByHeight(view: cardNumberView)
            hiddenByHeight(view: ccvView)
            hiddenByHeight(view: expiredDateView)
        }
        else if sender == serviceTypeChange {
            
            showByHeight(view: cardTypeView)
            showByHeight(view: cardNumberView)
            showByHeight(view: ccvView)
            showByHeight(view: expiredDateView)
        }
        else if sender == serviceTypeExpired {
            
            hiddenByHeight(view: cardTypeView)
            hiddenByHeight(view: cardNumberView)
            hiddenByHeight(view: ccvView)
            showByHeight(view: expiredDateView)
        }
    }

    //MARK: - PickDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        var components = 0
        if pickerView.tag == 0 {
            components = 1
        }
        else if pickerView.tag == 1 {
            components = 1
        }
        else if pickerView.tag == 2 {
            components = 2
        }
        
        return components
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
            
        case 0:
            return yearList.count
        case 1:
            return monthList.count
        case 2:
            if component == 0 {
                return monthList.count
            }
            else {
                return yearList.count
            }
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        switch pickerView.tag {
            
        case 0:
            return NSAttributedString(string: yearList[row], attributes: [NSAttributedStringKey.foregroundColor:UIColor.black])
        case 1:
            return NSAttributedString(string: monthList[row], attributes: [NSAttributedStringKey.foregroundColor:UIColor.black])
        case 2:
            let color = UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1)
            if component == 0 {
                return NSAttributedString(string: "\(monthList[row])月", attributes: [NSAttributedStringKey.foregroundColor:color])
            }
            else {
                return NSAttributedString(string: "\(yearList[row])年", attributes: [NSAttributedStringKey.foregroundColor:color])
            }
        default:
            return NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor:UIColor.black])
        }
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        
//        switch pickerView.tag {
//            
//        case 0:
//            return yearList[row]
//        case 1:
//            return monthList[row]
//        default:
//            return ""
//        }
//    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
            
        case 0:
            seletedYear = yearList[row]
            yearButton.setTitle(yearList[row], for: .normal)
            break
        case 1:
            seletedMonth = monthList[row]
            monthButton.setTitle(monthList[row], for: .normal)
            break
        case 2:
            var year = ""
            var month = ""
            
            if validDateButton.titleLabel?.text?.components(separatedBy: "/").count == 2 {
                year = (validDateButton.titleLabel?.text?.components(separatedBy: "/")[1])!
                month = (validDateButton.titleLabel?.text?.components(separatedBy: "/")[0])!
            }
            
            if component == 0 {
                month = monthList[row]
            }
            else {
                year = yearList[row]
            }
            validDateButton.setTitleColor(UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1), for: UIControlState.normal)
            validDateButton.setTitle("\(month)/\(year)", for: UIControlState.normal)

            
        default:
            
            break
        }
    }
    
    //MARK: - custom function
    func hiddenByHeight (view : UIView) {
        
        view.isHidden = true
        
        for constraint in view.constraints {
            
            if constraint.firstAttribute == .height && constraint.constant != 0 {
                
                constraint.identifier = "\(constraint.constant)"
                constraint.constant = 0
            }
        }
        
        for constraint in constraints {
            
            if constraint.firstItem as! UIView == view && constraint.constant != 0 {
                
                constraint.identifier = "\(constraint.constant)"
                constraint.constant = 0
            }
        }
    }
    
    func showByHeight (view : UIView) {
        
        for constraint in view.constraints {
    
            if constraint.firstAttribute == .height && constraint.identifier != nil {
                
                if let float = NumberFormatter().number(from: constraint.identifier!) {
                    
                    constraint.constant = CGFloat(truncating: float)
                }
            }
        }
        
        for constraint in constraints {
            
            if constraint.firstItem as! UIView == view && constraint.identifier != nil{
                
                if let float = NumberFormatter().number(from: constraint.identifier!) {
                    
                    constraint.constant = CGFloat(truncating: float)
                }
            }
        }
        
        view.isHidden = false
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }

    
    //MARK: - API
    func callSubmitDirectDebitAPI (expiredDate : String?, cardNumber : String?, cvv : String?, action : String) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP0505", action: "A0505")
        
        TSTAPP2_API.sharedInstance.submitDirectDebit(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, accountId: LoginData.sharedLoginData().custProfile_accountId, custId: LoginData.sharedLoginData().custProfile_custId, companyId: LoginData.sharedLoginData().custProfile_companyId, channel: "APP", action: action, custName: LoginData.sharedLoginData().custProfile_custName, cardNumber: cardNumber ?? "", cvv2: cvv ?? "", expiredDate: expiredDate ?? "", progressMessage:""/*directDebit.authWaitingMessage*/, showProgress: true, completion: { (dic :Dictionary<String, Any>, response :URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
//                let message = dic.stringValue(ofKey: "message")
//                let attribute = NSMutableAttributedString()
//                attribute.append(NSAttributedString(string: message, attributes: [NSForegroundColorAttributeName : UIColor.purple]))
//                
//                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
//                alert.setValue(attribute, forKey: "attributedMessage")
//                
//                let action = UIAlertAction(title: "確定", style: .cancel) { (action: UIAlertAction!) -> Void in
//                    let _ = self.navigationController?.popViewController(animated: true)
//                }
//                
//                alert.addAction(action)
//                alert.show(viewController: self)
                
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        
        }, failure: { (Error) in
            
        })
        
    }
}
