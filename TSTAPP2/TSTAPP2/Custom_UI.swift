//
//  Custom_UI.swift
//  TSTAPP2
//
//  Created by 詮通電腦-江宗澤 on 2017/12/13.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class CustomButton:UIButton {
    
    var originBackgroundColor:UIColor?
    
    var originTextColor:UIColor?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var isSelected: Bool {
        willSet {
            print("changing from \(isSelected) to \(newValue)")
        }
        
        didSet {
            print("changed from \(oldValue) to \(isSelected)")
        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        coordinator.addCoordinatedAnimations({
            if self.isFocused {
                self.alpha = 1.0 // in focus
            }
            else {
                self.alpha = 1.0 // leaving focus
            }
        }, completion: nil)
        
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                self.backgroundColor = UIColor(red: 1, green: 0, blue: 162/255, alpha: 1)
                self.setTitleColor(UIColor.white, for: .highlighted)
                self.titleLabel?.alpha = 1
            } else {
                self.backgroundColor = UIColor.white
                self.setTitleColor(UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1), for: .normal)
            }
        }
    }
}
