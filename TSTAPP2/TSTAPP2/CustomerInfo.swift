//
//  CustomerInfo.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/18.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class CustomerInfo: NSObject {
    
    static let sharedInstance = CustomerInfo()
    
    private(set) var contractId : String = "" //合約編號
    private(set) var msisdn : String = "" //用戶門號
    private(set) var billingZipCode : String = "" //帳單地址-郵遞區號
    private(set) var billingState : String = "" //帳單地址-縣市
    private(set) var billingCity : String = "" //帳單地址-鄉鎮市區
    private(set) var billingAddr : String = "" //帳單地址-地址
    private(set) var perZipCode : String = "" //戶籍地址-郵遞區號
    private(set) var perState : String = "" //戶籍地址-縣市
    private(set) var perCity : String = "" //戶籍地址-鄉鎮市區
    private(set) var perAddr : String = "" //戶籍地址-地址
    private(set) var maskBillingAddress : String = "" //隱碼後的帳單地址
    private(set) var maskPerAddress : String = "" //隱碼後的戶籍地址
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if dic.stringValue(ofKey: "code") == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                perZipCode = apiData.stringValue(ofKey: "perZipCode")
                perState = apiData.stringValue(ofKey: "perState")
                perCity = apiData.stringValue(ofKey: "perCity")
                perAddr = apiData.stringValue(ofKey: "perAddr")
                maskBillingAddress = apiData.stringValue(ofKey: "maskBillingAddress")
                maskPerAddress = apiData.stringValue(ofKey: "maskPerAddress")
                contractId = apiData.stringValue(ofKey: "contractId")
                billingZipCode = apiData.stringValue(ofKey: "billingZipCode")
                msisdn = apiData.stringValue(ofKey: "msisdn")
                billingState = apiData.stringValue(ofKey: "billingState")
                billingCity = apiData.stringValue(ofKey: "billingCity")
                billingAddr = apiData.stringValue(ofKey: "billingAddr")
            }
        }
    }
}
