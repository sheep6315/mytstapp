//
//  DailyRoaming.swift
//  TSTAPP2
//
//  Created by apple on 2016/10/28.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class DailyRoaming: NSObject {
    
    static let sharedInstance = DailyRoaming()
    
    private(set) var roamingCountryList : Array<RoamingCountryList> = [] //日租國家產品清單
    private(set) var roamingAppliedList : Array<RoamingAppliedList> = [] //日租申請列表
    private(set) var systemTime : String = "" //系統時間
    
    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            
            roamingCountryList = []
            roamingAppliedList = []
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                if let countryList = apiData["roamingCountryList"] as? Array<Dictionary<String, Any>> {
                    
                    for dic in countryList {
                        
                        let country = RoamingCountryList()
                        country.setDataValue(withDic: dic)
                        roamingCountryList.append(country)
                        
                    }
                }
                
                if let appliedList = apiData["roamingAppliedList"] as? Array<Dictionary<String, Any>> {
                    
                    for dic in appliedList {
                        
                        let applied = RoamingAppliedList()
                        applied.setDataValue(withDic: dic)
                        roamingAppliedList.append(applied)
                        
                    }
                }
                
                systemTime = apiData.stringValue(ofKey: "systemTime")
            }
        }
    }
}

class RoamingCountryList : NSObject {
    
    //國家代碼
    private(set) var countryId : String = ""
    //國家名稱
    private(set) var countryName : String = ""
    //產品名稱
    private(set) var productName : String = ""
    //產品費率
    private(set) var rates : String = ""
    //VIP產品費率
    private(set) var vipRates : String = ""
    //格林威治時間
    private(set) var gmtValue : String = ""
    //格林威治NOTE
    private(set) var gmtNote : String = ""
    //地區(洲別)
    private(set) var continentName : String = ""
    //電信國碼(886)
    private(set) var gtValue : String = ""
    private(set) var vendorName : String = ""
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>){
        
        countryId = dic.stringValue(ofKey: "countryId")
        countryName = dic.stringValue(ofKey: "countryName")
        productName = dic.stringValue(ofKey: "productName")
        rates = dic.stringValue(ofKey: "rates")
        vipRates = dic.stringValue(ofKey: "vipRates")
        gmtValue = dic.stringValue(ofKey: "gmtValue")
        gmtNote = dic.stringValue(ofKey: "gmtNote")
        continentName = dic.stringValue(ofKey: "continentName")
        gtValue = dic.stringValue(ofKey: "gtValue")
        vendorName = dic.stringValue(ofKey: "vendorName")
        
    }
}

class RoamingAppliedList : NSObject {
    
    //申請ID
    private(set) var appliedId : String = ""
    //國家代碼
    private(set) var countryId : String = ""
    //申請國家
    private(set) var countryName : String = ""
    //申請天數
    private(set) var applyDays : String = ""
    //生效時間(格式：yyyy/MM/dd HH:mm)
    private(set) var effectiveTime : String = ""
    //當地生效時間(格式：yyyy/MM/dd HH:mm)
    private(set) var localEffectiveTime : String = ""
    //到期時間(格式：yyyy/MM/dd HH:mm)
    private(set) var expiryTime : String = ""
    //當地到期時間(格式：yyyy/MM/dd HH:mm)
    private(set) var localExpiryTime : String = ""
    //狀態
    private(set) var status : String = ""
    //狀態2
    private(set) var statusCode : String = ""
    //格林威治NOTE
    private(set) var gmtNote : String = ""

    //是否開啟Detail
    private(set) var isOpenDetail : Bool = false
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>){
        
        appliedId = dic.stringValue(ofKey: "appliedId")
        countryId = dic.stringValue(ofKey: "countryId")
        countryName = dic.stringValue(ofKey: "countryName")
        applyDays = dic.stringValue(ofKey: "applyDays")
        effectiveTime = dic.stringValue(ofKey: "effectiveTime")
        localEffectiveTime = dic.stringValue(ofKey: "localEffectiveTime")
        expiryTime = dic.stringValue(ofKey: "expiryTime")
        localExpiryTime = dic.stringValue(ofKey: "localExpiryTime")
        status = dic.stringValue(ofKey: "status")
        statusCode = dic.stringValue(ofKey: "statusCode")
        gmtNote = dic.stringValue(ofKey: "gmtNote")
        
    }
}

