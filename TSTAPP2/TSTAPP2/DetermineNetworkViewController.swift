//
//  DetermineNetworkViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/26.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Contacts
import AddressBook

class DetermineNetworkContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contactName: UILabel!
    
    @IBOutlet weak var contactPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class DetermineNetworkViewController: TSUIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    
    @IBOutlet var searchContainer: UIView!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var callButton: UIButton!
    
    @IBOutlet weak var searchButton: UIButton!
    
    var phoneNumber: String = ""
    
    var searchController: UISearchController!
    
    var resultTableViewController: UITableViewController!
    
    var searchResult:[(name:String, phone:String)] = [(name:String, phone:String)]() {
        didSet {
            self.resultTableViewController.tableView.reloadData()
        }
    }
    
    lazy var contacts:[(name:String, phone:String)] = {
        [unowned self] in
        var result:[(name:String, phone:String)] = []
        
        if #available(iOS 9, *) {
            let contactStore = CNContactStore()
            let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: CNContactFormatterStyle.fullName),CNContactPhoneNumbersKey as CNKeyDescriptor]
            let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch)
            
            do {
                try contactStore.enumerateContacts(with: fetchRequest, usingBlock: { ( contact:CNContact, stop) -> Void in
                    contact.phoneNumbers.forEach { (phoneNumber) in
                        result.append((name:CNContactFormatter.string(from: contact, style: CNContactFormatterStyle.fullName) ?? "", phone:(phoneNumber.value as CNPhoneNumber).stringValue))
                    }
                })
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
//        else if #available(iOS 8, *) {
//            let semaphore = DispatchSemaphore(value: 0)
//            var error:Unmanaged<CFError>?
//            var addressBook:ABAddressBook = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
//            ABAddressBookRequestAccessWithCompletion(addressBook, { success, error in
//                if success {
//                    let allContacts = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as Array
//                    allContacts.forEach({ (contact) in
//                        let lastName = ABRecordCopyValue(contact, kABPersonLastNameProperty)?
//                            .takeRetainedValue() as! String? ?? ""
//                        let firstName = ABRecordCopyValue(contact, kABPersonFirstNameProperty)?
//                            .takeRetainedValue() as! String? ?? ""
//                        var fullName: String = ""
//                        if firstName == "" && lastName == "" {
//                            fullName = ABRecordCopyValue(contact, kABPersonOrganizationProperty)?
//                                .takeRetainedValue() as! String? ?? ""
//                        } else {
//                            fullName = "\(firstName) \(lastName)"
//                        }
//                        var phoneValues:ABMutableMultiValue? =
//                            ABRecordCopyValue(contact, kABPersonPhoneProperty).takeRetainedValue()
//                        for i in 0 ..< ABMultiValueGetCount(phoneValues){
//                            let phoneNumber = ABMultiValueCopyValueAtIndex(phoneValues, i).takeRetainedValue() as! String
//                            result.append((name: fullName, phone: phoneNumber))
//                        }
//                    })
//                } else {
//                    print("error")
//                }
//                semaphore.signal()
//            })
//            _ = semaphore.wait(timeout: .distantFuture)
//        }
        return result
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.resultTableViewController = UITableViewController()
        self.resultTableViewController.tableView.register(UINib(nibName: "DetermineNetworkContactTableViewCell", bundle: nil), forCellReuseIdentifier: "contactInfo")
        self.resultTableViewController.tableView.delegate = self
        self.resultTableViewController.tableView.dataSource = self
        
        self.searchController = UISearchController(searchResultsController: self.resultTableViewController)
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.placeholder = "請輸入您欲查詢的門號"
        self.searchController.hidesNavigationBarDuringPresentation = true
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.keyboardType = UIKeyboardType.decimalPad
        
        if #available(iOS 11, *) {
            self.searchContainer.isHidden = true
            searchController.searchBar.barStyle = UIBarStyle.blackTranslucent
            searchController.searchBar.tintColor = UIColor.white
            navigationItem.searchController = self.searchController
        }
        else {
            self.searchContainer.addSubview(searchController.searchBar)
        }
        
        self.definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0507", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "網內外門號查詢", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "網內外門號查詢")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0507", action: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {

        UISkinChange.sharedInstance.changeUIColor(theObject: searchButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: callButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    // MARK: - UISearchControllerDelegate
    
    func willPresentSearchController(_ searchController: UISearchController) {
        DispatchQueue.main.async {
            self.resultLabel.isHidden = true
            self.searchController.searchResultsController?.view.isHidden = false
        }
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if self.searchResult.count < 1 {
            self.searchController.dismiss(animated: true, completion: nil)
            self.didTapSearchButton()
        }
    }
    
    // MARK: - UITableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactInfo", for: indexPath) as! DetermineNetworkContactTableViewCell
        cell.contactName.text = self.searchResult[indexPath.row].name
        cell.contactPhone.text = self.searchResult[indexPath.row].phone
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchController.searchBar.text = self.searchResult[indexPath.row].phone
        self.searchController.dismiss(animated: true, completion: nil)
        self.didTapSearchButton()
    }
    
    // MARK: - UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = self.searchController.searchBar.text else {
            return
        }
        self.resultTableViewController.view.isHidden = false
        
        self.searchResult = self.contacts.filter { (contact: (name: String, phone: String)) -> Bool in
            return (contact.name.localizedCaseInsensitiveContains(searchText) || contact.phone.contains(searchText))
        }
        
        if searchText == "" {
            self.searchResult = self.contacts
        } else if self.searchResult.count == 0 {
            self.searchResult.append((name: "點此查詢", phone: searchText))
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - IBAction
    
    @IBAction func didTapReminder(_ sender: UIButton) {
        let reminderAlertController = UIAlertController(title: "親愛的用戶您好", message: "您可利用本服務輸入您想查詢之行動電話號碼查詢該門號為網內或網外門號，或以手機直撥57016進行查詢。提醒您，因應號碼可攜服務的蓬勃發展，同一門號於不同日期的查詢結果可能不同。", preferredStyle: UIAlertControllerStyle.alert)
        reminderAlertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(reminderAlertController, animated: true, completion: nil)
    }
    
    @IBAction func didTapSearchButton() {
        
        if self.searchController.searchBar.text == "" {
            self.searchController.isActive = true
        } else {
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP0507", action: "A060401")
            
            self.resultLabel.text = "查詢中..."
            self.resultLabel.isHidden = false
            
            var determinePhone:String = self.searchController.searchBar.text!
            if determinePhone.hasPrefix("+886") {
                determinePhone = "0" + determinePhone[..<determinePhone.index(determinePhone.startIndex, offsetBy: 4)]
            }
            
//            let regex = try! NSRegularExpression(pattern: "[\\+\\-\\(\\)\\*\\# ]", options: .caseInsensitive)
//            determinePhone = regex.stringByReplacingMatches(in: determinePhone, options: [], range: NSRange(0..<determinePhone.characters.count), withTemplate: "")
            
            let validator: Validators = Validators(withRules: [
                [
                    "label": "電話號碼",
                    "value": determinePhone,
                    "rules": ["required", "mobile"]
                ]
                ])
            do {
                let verifyResult = try validator.verify()
                if verifyResult {
                    
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "網內外門號查詢", action: nil, label: nil, value: nil)
                    TSTAPP2_API.sharedInstance.onNetOffNet(withMSISDN: determinePhone, showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                        
                        if dic.stringValue(ofKey: "code") == "00000" {
                            
                            if let data = dic["data"] as? [String : String] {
                                
                                //                if response["queryStatus"] == "1" || response["queryStatus"] == "5" {
                                //                    self.callButton.isHidden = false
                                //                    self.phoneNumber = determinePhone
                                //                }
                                //                else {
                                //                    self.callButton.isHidden = true
                                //                }
                                self.resultLabel.text = data["resultMessage"]
                                self.callButton.isHidden = false
                                self.phoneNumber = determinePhone
                                self.resultLabel.isHidden = false

                            }
                            else {
                                self.resultLabel.text = ""
                            }
                        }
                        else {
                            self.resultLabel.text = ""
                            TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                        }
                    }) { (Error) in
                        self.resultLabel.text = ""
                        print("error:")
                        print(Error)
                    }
                }
            } catch {
                self.resultLabel.text = ""
                self.callButton.isHidden = true
                TSTAPP2_Utilities.showAlert(withMessage: validator.errors.first!, viewController: self)
            }
        }
    }
    
    @IBAction func didTapCall(_ sender: Any) {
        
        if phoneNumber != "" {
            UIApplication.shared.openURL(URL(string: "tel://\(phoneNumber)")!)
        }
    }
}
