//
//  DirectDebit.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/14.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class DirectDebit : NSObject {
    
    static let sharedInstance = DirectDebit()
    
    private(set) var msisdn : String = "" //行動電話
    private(set) var cardType : String = "" //信用卡卡別
    private(set) var cardNumber : String = "" //信用卡卡號
    private(set) var expiredDate : String = "" //信用卡有效期限(YYYYMM)
    private(set) var result : String = "" //結果碼
    /*
     1:用戶已存在
     2:用戶不存在
    3:系統錯誤處理失敗
    4:參數錯誤無法處理
    5:處理中
    6:已申請銀行轉帳代繳 */
    private(set) var message : String = "" //訊息
    private(set) var statusDesc : String = "" //狀態說明
    private(set) var warningMessage : String = "" //提示訊息
    private(set) var authWaitingMessage : String = "" //授權等待訊息示文字
    private(set) var waitingMessage : String = "" //等待訊息
    
    private(set) var popularCreditCard : String = ""
    private(set) var firstNumber : String = ""
    private(set) var secondNumber : String = ""
    private(set) var thirdNumber : String = ""
    private(set) var fourthNumber : String = ""
    private(set) var year : String = ""
    private(set) var month : String = ""
    private(set) var ccv : String = ""
    
    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                msisdn = apiData.stringValue(ofKey: "msisdn")
                cardType = apiData.stringValue(ofKey: "cardType")
                cardNumber = apiData.stringValue(ofKey: "cardNumber")
                expiredDate = apiData.stringValue(ofKey: "expiredDate")
                result = apiData.stringValue(ofKey: "result")
                message = apiData.stringValue(ofKey: "message")
                statusDesc = apiData.stringValue(ofKey: "statusDesc")
                warningMessage = apiData.stringValue(ofKey: "warningMessage")
                authWaitingMessage = apiData.stringValue(ofKey: "authWaitingMessage")
                waitingMessage = apiData.stringValue(ofKey: "waitingMessage")
            }
        }
    }
}
