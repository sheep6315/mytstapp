//
//  EbillApplyViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/12/19.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class EbillApplyViewController: TSUIViewController {

    @IBOutlet weak var msisdnLabel: UILabel!
    
    @IBOutlet weak var agreeButton: UIButton!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var applyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.msisdnLabel.text = LoginData.sharedLoginData().custProfile_msisdn
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkVIPStatusAndChangeSkin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: applyButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBAction
    @IBAction func didTapAgreeButton(_ sender: Any) {
        self.agreeButton.isSelected = !self.agreeButton.isSelected
        if self.agreeButton.isSelected {
            TSTAPP2_Utilities.showHtmlMessage(withTitle: "台灣之星 電子帳單服務協定", message: "<!DOCTYPE HTML> <html> <head><style> *{font-family:Helvetica,Arial,sans-serif; font-size:14px; margin:0; padding:0;} ol.main_ol{padding-right:8px;} ol li{list-style-position:outside; line-height:22px;} ol.main_ol>li{list-style-type:cjk-ideographic; margin:0 0 15px 30px; font-weight:bold;} ol.main_ol>li li{font-weight:normal;} </style></head> <body> <ol class=\"main_ol\"> <li>申請對象<div>台灣之星月租型用戶皆可申請【電子帳單服務】。</div></li><li>服務說明 <ol> <li>台灣之星(以下簡稱「本公司」) 【電子帳單服務】(以下簡稱「本服務」)係提供用戶以電子郵件式收取帳單，本服務申請成功後，本公司將自生效首期傳送【電子帳單】至用戶指定之電子郵件信箱並同時取消郵寄實體帳單予用戶。</li> <li>公司戶用戶申請本服務如需稅務申報，請將電子帳單印出後即可作為會計憑證。</li> <li>本服務提供六個月內之帳單瀏覽、帳單列印、線上繳款、帳款查詢等服務。</li> <li>用戶若欲取消本服務，可至本服務網頁申請，本公司將自取消成功後之次期帳單開始恢復郵寄體帳單。</li> <li>用戶申請本服務或變更電子郵件信箱時，應保證其填寫電子郵件信箱正確無誤，以確保將來電帳單通知函及相關訊息能準確送達；若因用戶輸入資料有誤或未主動即時更新資料所致之損害應由用自行負責。</li> <li>用戶對於【電子帳單」內容不得作任何修改，如有修改對本公司均不生效力。若用戶違反法令違反本協定之任何條款，致本公司或其關係企業受有損害時，用戶應負擔損害賠償責任。</li> <li>用戶經由本服務所下載或取得之任何資料應自負風險任何因資料之下載而導致用戶電腦系統之何損壞或資料流失等，皆由用戶自行負責，本公司對於因此所生損失或損害皆不負任何賠償責任。<li> </ol> </li> <li>繳款方式 <ol> <li>用戶可直接列印【電子帳單】（務必使用「雷射印表機」）後，至本公司各繳款通路進行繳費。如因用戶因素（例如：電子信箱空間已滿、Isp業者網路中斷等）致無法收取【電子帳單】，請用戶行至本公司網站或各直營店補印帳單，並應自行承擔因延遲繳款造成之任何費用及損害。</li> <li>用戶亦可辦理轉帳代繳、或利用網站公告繳費方式、或利用其他繳款途徑進行繳款。</li> <li>已辦理轉帳代繳之用戶仍會以原轉帳方式扣款請勿重複繳款。</li> </ol> </li> <li>服務終止或暫停： <div>如有下列情形，本公司得終止或暫停本服務之一部或全部且對用戶不負任何責任：</div> <ol> <li>因天災、不可抗力或其他非可歸責於本公司之事由所致之暫停或中斷。</li> <li>對本服務相關軟硬體設備進行搬遷、更換、保養或維修時。</li> <li>如用戶有門號退租、一退一租、過戶、更名、切結非本人申請、銷號、合併／個別出帳或其他號契約終止等情形時，本公司得逕行停止用戶使用本服務功能，但在用戶原來行動通信契約有效期內，本公司仍將繼續寄發電子帳單。</li> </ol> </li> <li>服務協定之修改 <div>本公司得隨時修改本服務協定條款，如有修改將公佈於本公司網站。修改事項於公佈時即時效，不另行個別通知用戶。</div> </li> </ol></body> </html>", viewController: self)
        }
    }
    
    @IBAction func didTapApplyButton(_ sender: Any) {
        if self.agreeButton.isSelected {
            let validator: Validators = Validators(withRules: [
                [
                    "label": "電子郵件信箱",
                    "value": self.emailField.text ?? "",
                    "rules": ["required", "email"]
                ]
                ])
            do {
                let verifyResult = try validator.verify()
                if verifyResult {
                    self.callApplyEBillAPI()
                }
            } catch {
                TSTAPP2_Utilities.showAlert(withMessage: validator.errors.first!, viewController: self)
            }
            
        } else {
            TSTAPP2_Utilities.showAlert(withMessage: "請勾選本人已清楚了解台灣之星電子帳單服務協定", viewController: self)
        }
    }
    
    // MARK: - API
    func callApplyEBillAPI() {
        let loginData: LoginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.applyEBill(withMSISDN: loginData.custProfile_msisdn, emailAddress: self.emailField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: "已成功申請電子帳單", viewController: self)
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "電子帳單申請", action:"ON",  label: "\(loginData.custProfile_msisdn)/\(loginData.custProfile_contractId)", value: nil)
            } else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (Error) in
            
        }
    }

}
