//
//  ExtAccountViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/14.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ExtAccountViewController: TSUIViewController {
    
    @IBOutlet weak var unBindButton: UIButton!
    
    @IBOutlet weak var extAccountLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        extAccountLabel.text = LoginData.sharedLoginData().custProfile_extEmail
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP020103", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "會員中心", action: "修改FB帳號", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "修改FB帳號")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP020103", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: unBindButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    MARK: - IBAction
    @IBAction func didTapUnbindButton(_ sender: Any) {
        
        let alert = UIAlertController(title: "確定要取消Facebook連結？", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let confirm = UIAlertAction(title: "確定", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
            
            TSTAPP2_Utilities.logoutFacebook()
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP020103", action: "A020103")
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改FB帳號(會員中心)", action: "取消連結", label: nil, value: nil)
            
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.unbindExt(withMSISDN: loginData.custProfile_msisdn, extId: loginData.custProfile_extId, contractId: loginData.custProfile_contractId, extType: loginData.custProfile_extType, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    TSTAPP2_Utilities.showAlertAndLogoutThenShowLoginView(withMessage: "取消連結成功，請重新登入系統。", viewController: self)
                }
                else {
                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
            }, failure: { (error: Error) in
                
            })
        })
        let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
            
        })
        alert.addAction(confirm)
        alert.addAction(cancel)
        
        
        present(alert, animated: true, completion: {() in
            
        })
        
        
    }
    

}
