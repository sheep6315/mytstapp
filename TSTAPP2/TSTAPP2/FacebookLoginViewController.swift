//
//  FacebookLoginViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/13.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

let FACEBOOK_LOGIN_DATA = "FACEBOOK_LOGIN_DATA"

class FacebookLoginViewController: TSUIViewController {
    
    
    @IBOutlet weak var fbLoginButton: UIButton!
    
    var extInfoDic: Dictionary<String, Any>?
    var pswchange = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        if FBSDKAccessToken.current() != nil {
            print("Login")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP01", action: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP01", action: "")
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toAccountBinding" {
            (segue.destination as! AccountBindingViewController).extInfoDic = extInfoDic
        }
        else if segue.identifier == "toChangePassword" {
            (segue.destination as! ChangePasswordViewController).pwschangeStatus = pswchange
        }
    }
 
    
    
//    MARK: - IBAction
    @IBAction func didTapFbLoginButton(_ sender: AnyObject) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP01", action: "A0102")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "執行登入", action: "使用FB登入", label: nil, value: nil)
        
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result: FBSDKLoginManagerLoginResult?, error: Error?) in
            if error != nil {
                print("Process error")
                let description = (error! as NSError).userInfo["FBSDKErrorLocalizedDescriptionKey"]
                print("Process error \(String(describing: description))")
            }
            else if result!.isCancelled {
                print("Cancelled")
            }
            else {
                print("Logged in")
                
                self.requestGraph()
                
//                self.performSegue(withIdentifier: "toAccountLogin", sender: nil)
            }
        }
    }
    
    @IBAction func didTapOtherLogin(_ sender: Any) {
        
//        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP01", action: "A0101")
    }
    
    
//    MARK: -
    func requestGraph() {
        TSTAPP2_Utilities.showPKHUD()
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, birthday, gender", "locale": "zh_TW"]).start(completionHandler: { (connection: FBSDKGraphRequestConnection?, result: Any?, error: Error?) in
            if result != nil {
                var resultDic = result as! Dictionary<String, String>
                print("FBSDKGraphRequest\n\(String(describing: resultDic["id"])) \(String(describing: resultDic["name"])) \(String(describing: resultDic["email"]))")
                
                resultDic["token"] = FBSDKAccessToken.current().tokenString
                self.extInfoDic = resultDic
                UserDefaults.standard.set(resultDic, forKey: FACEBOOK_LOGIN_DATA)
                UserDefaults.standard.synchronize()
                
                let id = resultDic.stringValue(ofKey: "id")
                let token = resultDic.stringValue(ofKey: "token")
                let email = resultDic.stringValue(ofKey: "email")
                let name = resultDic.stringValue(ofKey: "name")
                let birthday = resultDic.stringValue(ofKey: "birthday")
                let gender = resultDic.stringValue(ofKey: "gender")
                
                TSTAPP2_API.sharedInstance.loginExt(withExtId: id, extType: "facebook", extToken: token, email: email, name: name, birthday: birthday, gender: gender, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        if dic["data"] == nil {
//                            self.performSegue(withIdentifier: "toAccountBinding", sender: nil)
                            self.performSegue(withIdentifier: "toTabBar", sender: nil)
                        }
                        else {
                            let apiData = dic["data"] as! Dictionary<String, Any>
                            let custProfile = apiData["custProfile"] as! Dictionary<String, Any>
                            self.pswchange = custProfile.stringValue(ofKey: "pwschange")
                            if self.pswchange != "1" {
                                
                                if let _ = UserDefaults.standard.string(forKey: ACCOUNT_PASSWORD) {
                                    self.performSegue(withIdentifier: "toChangePassword", sender: nil)
                                }
                                else {
                                    self.performSegue(withIdentifier: "toAccountLogin", sender: nil)
                                }
                            }
                            else {
                                LoginData.sharedLoginData().setDataValue(withDic: dic, onlyUpdateCustProfile: false)
                                self.performSegue(withIdentifier: "toTabBar", sender: nil)
                            }
                            
//                            LoginData.sharedLoginData().setDataValue(withDic: dic, onlyUpdateCustProfile: false)
//                            self.performSegue(withIdentifier: "toTabBar", sender: nil)
                        }
                        
//                        let deviceID = UIDevice.current.identifierForVendor!.uuidString
//                        let loginData = LoginData.sharedLoginData()
//                        TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: "", showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//                            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//                            UIApplication.shared.registerUserNotificationSettings(settings)
//                            UIApplication.shared.registerForRemoteNotifications()
//                        }, failure: { (error: Error) in
//
//                        })
                        
                        TSTAPP2_Utilities.registerDevice()
                    }
                    else {
                        FBSDKLoginManager().logOut()
                        TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                    }
                }, failure: { (error: Error) in
                    FBSDKLoginManager().logOut()
                })
            }
            else {
                TSTAPP2_Utilities.hidePKHUD()
            }
        })
    }
    

}
