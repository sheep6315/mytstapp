//
//  GOOGLE_API.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/15.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import CryptoSwift

let MAP_API_HOST = "https://maps.googleapis.com"

class GOOGLE_API: NSObject {
    
    static let sharedInstance = GOOGLE_API()
    
    private override init() {
        //This prevents others from using the default '()' initializer for this class.
    }
    
    func startTask(withURLString urlString: String, data: Data, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        if showProgress {
            TSTAPP2_Utilities.showPKHUD()
        }
        
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: encodedUrlString!)
        
        if let url = url {
            var request: URLRequest = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 60)
            request.httpBody = data
            request.httpMethod = "POST"
            
            let session: URLSession = URLSession.shared
            let task: URLSessionTask = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                
                DispatchQueue.main.async {
                    if showProgress {
                        TSTAPP2_Utilities.hidePKHUD()
                    }
                    
                    let dic = self.dictionary(fromResponseData: data)
                    
                    if error == nil {
                        print("GOOGLE_API \(urlString)\n\(dic.description)")
                        completion(dic, response!)
                    }
                    else {
                        print("GOOGLE_API \(urlString)\n\(String(describing: error?.localizedDescription))")
                        failure(error!)
                    }
                }
                
                
            })
            
            task.resume()
            
        }
        else {
            if showProgress {
                TSTAPP2_Utilities.hidePKHUD()
            }
            failure(NSError(domain: "Not valid URL", code: 1, userInfo: nil))
        }
        
    }
    
    /**
     Parse JSON Data into Dictionary<String, Any>
     */
    func dictionary(fromResponseData data: Data?) -> Dictionary<String, Any> {
        var resultDic: Dictionary<String, Any> = [:]
        
        if data != nil {
            //            var rawString = String(data: data!, encoding: String.Encoding.utf8)
            do {
                resultDic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! Dictionary<String, Any>
                //                print("resultDic \(resultDic.description)")
            } catch let parseError {
                print("parseError \(parseError.localizedDescription)")
            }
            
        }
        
        return resultDic
    }
    
    func getEncryptedData(ofDictionary dic: Dictionary<String, String>) -> String {
        
        let params: [String] = dic.map { (key: String, value: String) -> String in
            return "\(key)=\(value)"
        }
        
        return params.joined(separator: "&")
    }
    
    // MARK: - API
    func getGeocode(withAddress address: String, latlng: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "sensor": "false",
            "address": address,
            "latlng": latlng
        ]
        
        let urlString = MAP_API_HOST + "/maps/api/geocode/json?" + self.getEncryptedData(ofDictionary: dic)
        
        print("GOOGLE_API getGeocode\n\(urlString)")
        
        self.startTask(withURLString: urlString, data: Data(), showProgress: showProgress, completion: completion, failure: failure)
        
    }
}
