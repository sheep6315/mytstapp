//
//  GroupBuy.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/18.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class GroupBuy: NSObject {
    
    static let sharedInstance = GroupBuy()
    
    private(set) var isMaster : Bool = false //是否為粽子頭(Y/N)
    private(set) var groupCnt : String = "" //家族人數
    private(set) var iconUrl : String = "" //圖示網址
    private(set) var groupInfo : String = "" //家族優惠
    private(set) var remindMessage : String = "" //提醒訊息
    private(set) var subtitle: String = "" //小標題
    private(set) var title: String = "" //標題
    private(set) var content: String = "" //內容
    private(set) var showButtonArea : Bool = false //顯示按鈕區塊(Y:顯示/N:不顯示)
    private(set) var moreUrl : String = "" //了解更多按鈕連結
    private(set) var fbUrl : String = "" //FB分享網址
    private(set) var lineText : String = "" //用Line傳送文字
    private(set) var showMember : Bool = false //是否要顯示人數
    private(set) var memberCount : String = "" //參與人數
    private(set) var memberRemainder : String = "" //剩餘人數
    private(set) var showShareButton : Bool = false //顯示分享按鈕
    private(set) var showRecordButton : Bool = false //顯示紀錄按鈕
    private(set) var shareText : String = "" //分享用的文字
    private(set) var recordTitle : String = "" //記錄標題
    private(set) var showCGC : Bool = false //顯示家族省
    private(set) var recordList: [String] = [] //紀錄列表

    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if dic.stringValue(ofKey: "code") == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                isMaster = (apiData.stringValue(ofKey: "isMaster") == "Y")
                groupCnt = apiData.stringValue(ofKey: "groupCnt")
                iconUrl = apiData.stringValue(ofKey: "iconUrl")
                groupInfo = apiData.stringValue(ofKey: "groupInfo")
                remindMessage = apiData.stringValue(ofKey: "remindMessage")
                subtitle = apiData.stringValue(ofKey: "subtitle")
                title = apiData.stringValue(ofKey: "title")
                content = apiData.stringValue(ofKey: "description")
                showButtonArea = (apiData.stringValue(ofKey: "showButtonArea") == "Y")
                moreUrl = apiData.stringValue(ofKey: "moreUrl")
                fbUrl = apiData.stringValue(ofKey: "fbUrl")
                lineText = apiData.stringValue(ofKey: "lineText")
                showMember = (apiData.stringValue(ofKey: "showMember") == "Y")
                memberCount = apiData.stringValue(ofKey: "memberCount")
                memberRemainder = apiData.stringValue(ofKey: "memberRemainder")
                showShareButton = (apiData.stringValue(ofKey: "showShareButton") == "Y")
                showRecordButton = (apiData.stringValue(ofKey: "showRecordButton") == "Y")
                shareText = apiData.stringValue(ofKey: "shareText")
                recordTitle = apiData.stringValue(ofKey: "recordTitle")
                showCGC = (apiData.stringValue(ofKey: "showCGC") == "Y")
                recordList = apiData["recordList"] as? [String] ?? []
            }
        }
    }
}
