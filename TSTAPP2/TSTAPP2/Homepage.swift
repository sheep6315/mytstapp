//
//  Homepage.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/18.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class Homepage: NSObject {
    
    static let sharedInstance = Homepage()
    
    private(set) var promote : PromoteData = PromoteData() //優惠
    private(set) var video : VideoData = VideoData() //廣告專區
    private(set) var news : NewsData = NewsData() //最新消息
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if dic.stringValue(ofKey: "code") == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                if let dic = apiData["promote"] as? Dictionary<String, Any> {
                    
                    let promoteData = PromoteData()
                    promoteData.setDataValue(withDic: dic)
                    promote = promoteData
                }
                
                if let dic = apiData["video"] as? Dictionary<String, Any> {
                    
                    let videoData = VideoData()
                    videoData.setDataValue(withDic: dic)
                    video = videoData
                }
                
                
                if let dic = apiData["news"] as? Dictionary<String, Any> {
                    
                    let newsData = NewsData()
                    newsData.setDataValue(withDic: dic)
                    news = newsData
                }
            }
        }
    }
}

class PromoteData: NSObject {
    
    private(set) var moreUrl : String = "" //更多網址
    private(set) var content : [PromoteContent] = [] //優惠資料

    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        moreUrl = dic.stringValue(ofKey: "moreUrl")
        
        if let list = dic["content"] as? Array<Dictionary<String, Any>> {
            
            for data in list {
                
                let promoteContent = PromoteContent()
                promoteContent.setDataValue(withDic: data)
                content.append(promoteContent)
            }
        }
    }
}

class PromoteContent: NSObject {
    
    private(set) var id : String = "" //代碼
    private(set) var version : String = "" //版本代碼
    private(set) var sort : String = "" //排序
    private(set) var picUrl : String = "" //圖片路徑
    private(set) var url : String = "" //網址
    private(set) var type : String = "" // 1:WebView 2:Brower
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        id = dic.stringValue(ofKey: "id")
        version = dic.stringValue(ofKey: "version")
        sort = dic.stringValue(ofKey: "sort")
        picUrl = dic.stringValue(ofKey: "picUrl")
        url = dic.stringValue(ofKey: "url")
        type = dic.stringValue(ofKey: "type")
    }
}

class VideoData: NSObject {
    
    private(set) var moreUrl : String = "" //更多網址
    private(set) var imgUrl : String = "" //影片縮圖網址
    private(set) var url : String = "" //網址
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        moreUrl = dic.stringValue(ofKey: "moreUrl")
        imgUrl = dic.stringValue(ofKey: "imgUrl")
        url = dic.stringValue(ofKey: "url")
    }
}

class NewsData: NSObject {
    
    private(set) var moreUrl : String = "" //更多網址
    private(set) var content : [NewsContent] = [] //最新消息資料
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        moreUrl = dic.stringValue(ofKey: "moreUrl")
        
        if let list = dic["content"] as? Array<Dictionary<String, Any>> {
            
            for data in list {
                
                let newsContent = NewsContent()
                newsContent.setDataValue(withDic: data)
                content.append(newsContent)
            }
        }
    }
}

class NewsContent: NSObject {
    
    private(set) var id : String = "" //ID
    private(set) var title : String = "" //標題
    private(set) var subTitle : String = "" //子標題
    private(set) var url : String = "" //網址
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        id = dic.stringValue(ofKey: "id")
        title = dic.stringValue(ofKey: "title")
        subTitle = dic.stringValue(ofKey: "subTitle")
        url = dic.stringValue(ofKey: "url")
    }
}
