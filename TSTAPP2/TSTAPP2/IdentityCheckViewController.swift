//
//  IdentityCheckViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/24.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class IdentityCheckViewController: TSUIViewController {

    @IBOutlet weak var msisdnField: UITextField!
    
    @IBOutlet weak var customerIdField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var msisdn : String = ""
    var customerId : String = ""
    var contractId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        
        scrollView.endEditing(true)
    }

    @IBAction func didTapNext(_ sender: Any) {
        
        let msisdnCount = msisdnField.text?.count
        let idCount = customerIdField.text?.count

        if msisdnCount! <= 0 {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請輸入會員帳號", viewController: self)
        }
        else if idCount! <= 0 {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請輸入身分證或統編", viewController: self)
        }
        else {
            
            msisdn = msisdnField.text!
            customerId = customerIdField.text!
            
            callForgetPwd()
        }
    }
    
    //MARK: API
    func callForgetPwd() {
        
        TSTAPP2_API.sharedInstance.forgetPassword(withMSISDN: msisdn, custId: customerId, notificationType: "0", notificationEmail: nil, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                if let data = dic["data"] as? Dictionary<String, Any> {
                    self.contractId = data.stringValue(ofKey: "contractId")
                    
                    let storyBoard = UIStoryboard(name: "ApplyPassword", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ChooseSendTypeViewController") as! ChooseSendTypeViewController
                    
                    vc.mail = data.stringValue(ofKey: "email")
                    vc.customerId = self.customerId
                    vc.msisdn = self.msisdn
                    
                    self.navigationController?.show(vc, sender: self)
                }
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (error: Error) in
            
        }
    }
}
