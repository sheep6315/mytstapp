//
//  LandingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/13.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

let MESSAGE_COUNT = "MESSAGE_COUNT"

protocol AccountMenuDelegate: class {
    func didTapAccountInfoButton(sender: Any?) -> ()
}

class MoreButton: UIButton {
    var moreURLString = ""
}

class AccountMenuViewController: UIViewController {
    @IBOutlet weak var changeNumberButton: UIButton!
    @IBOutlet weak var accountInfoButton: UIButton!
    
    weak var delegate: AccountMenuDelegate?
    
    var accountInfoButtonTarget: Selector?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didTapAccountInfoButton(_ sender: AnyObject) {
        dismiss(animated: true) { 
            
        }
        delegate?.didTapAccountInfoButton(sender: nil)
    }
    
    
}

//MARK: - 廣告上遮罩的按鈕
class AdvertisingImageViewButton: UIButton {
    var smartBanner:SmartBanner?
}

//MARK: - 網路用量圓圈圖
class CircleView: UIView {
    
    var borderWidth: CGFloat = 2
    private(set) var startAngle:CGFloat = 0
    var percent:CGFloat = 0
    var originAngle:CGFloat = CGFloat.pi * 3 / 2
    var endAngle = 2 * CGFloat.pi
    var primaryColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 0.68)
    var secondaryColor = UIColor(red: 1, green: 47/255, blue: 163/255, alpha: 1)
    var currentStrokeValue = CGFloat(0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        borderWidth = self.frame.width * 0.015
        self.layer.cornerRadius = self.frame.width / 2
        let center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        let radius = CGFloat(self.frame.width / 2)
        
        if percent == 0 {
            primaryColor = secondaryColor
            percent = 0.01
        }
        
        let path1 = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle + originAngle, endAngle: ( percent * endAngle ) + originAngle, clockwise: true)

        let path2 = UIBezierPath(arcCenter: center, radius: radius, startAngle: (percent * endAngle) + originAngle, endAngle: startAngle + originAngle, clockwise: true)

        path1.lineWidth = borderWidth
        primaryColor.setStroke()
        path1.stroke()
        path2.lineWidth = borderWidth
        path1.stroke()
        secondaryColor.setStroke()
        path2.stroke()
//        self.addLine(fromPoint: CGPoint(x: 0.7574*self.frame.width, y: 0.2042*self.frame.width), toPoint: CGPoint(x: 0.5532*self.frame.width, y: 0.6517*self.frame.width))
    }
    
    //加斜線
    func addLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let line = CAShapeLayer()
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        line.path = linePath.cgPath
        line.strokeColor = UIColor.white.cgColor
        line.lineWidth = 1
        line.lineJoin = kCALineJoinRound
        self.layer.addSublayer(line)
    }
}

class AdvertisingCell:UITableViewCell {
    
    @IBOutlet weak var myImageView: UIImageView!
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                myImageView.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                myImageView.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    func setAspectConstraint(aspect : CGFloat?) {
        if aspect != nil {
            self.aspectConstraint = NSLayoutConstraint(item: self.myImageView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self.myImageView, attribute: NSLayoutAttribute.height, multiplier: aspect!, constant: 0.0)
        }
    }
}

class LandingViewController: TSUIViewController, AccountMenuDelegate, AdvertisingDelegate, Advertising2Delegate, PromoteDelegate, NewsDelegate, UIPopoverPresentationControllerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var accountInfoButton: UIBarButtonItem!
    @IBOutlet weak var messageButton: UIBarButtonItem!
    
    @IBOutlet weak var promoteVCConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ytPlayerView: YTPlayerView!
    
    @IBOutlet weak var productBackwardButton: UIButton!
    @IBOutlet weak var productForwardButton: UIButton!
    
    @IBOutlet weak var newsVCConstraint: NSLayoutConstraint!
    
//    @IBOutlet weak var advertising2AspectConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var campaignView: UIView!
    @IBOutlet weak var campaignViewTopSpace: NSLayoutConstraint!
    @IBOutlet weak var campaignImageView: UIImageView!
    @IBOutlet weak var campaignImageViewAspect: NSLayoutConstraint!
    @IBOutlet weak var campaignMainButton: UIButton!
    @IBOutlet weak var campaignButtonView: UIView!
    @IBOutlet weak var campaignButtonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var campaignButton1: UIButton!
    @IBOutlet weak var campaignButton2: UIButton!
    @IBOutlet weak var campaignButton3: UIButton!
    
    //MARK: - 新的layout
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet var iconBackgroundView: [UIView]!
    
    @IBOutlet weak var myProjectButton: UIButton!
    
    @IBOutlet weak var nickName: UILabel!
    
    @IBOutlet weak var internetUsageBackgroundView: CircleView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cellPhoneNumber: UILabel!
    
    @IBOutlet weak var unBillView: UIView!
    
    @IBOutlet weak var unBillBuyButton: UIButton!
    
    @IBOutlet weak var dataUsage: UILabel!
    
    @IBOutlet weak var unlimitedImageView: UIImageView!
    
    @IBOutlet weak var dataTotal: UILabel!
    
    @IBOutlet weak var dataUnit: UILabel!
    
    @IBOutlet weak var pstn: UILabel! // 市話
    
    @IBOutlet weak var offNetVoice: UILabel! // 網外
    
    @IBOutlet weak var onNetVoice: UILabel! // 網內
    
    @IBOutlet weak var pstnDesc: UILabel!
    
    @IBOutlet weak var onNetVoiceDesc: UILabel!
    
    @IBOutlet weak var offNetVoiceDesc: UILabel!
    
    @IBOutlet weak var remainingBillingDate: UILabel!
    
    var advertisingPageVC: AdvertisingPageViewController?
    var advertising2CollectionVC: Advertising2CollectionViewController?
    var promoteVC: PromoteTableViewController?
    var newsVC: NewsTableViewController?
    
    let loginData = LoginData.sharedLoginData()
    
    var didSetLandingVC = false
    
    var isGetSmartBanner = false {
        didSet {
            apiResponded()
        }
    }
    var isRecommendProduct = false {
        didSet {
            apiResponded()
        }
    }
    var isGetHomepage = false {
        didSet {
            apiResponded()
        }
    }
    
    var didLoadAPIOnce = false
    
    var advertisingImageList:[UIImage] = []
    var aspectList:[CGFloat?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(LandingViewController.backToRootViewController), name: NSNotification.Name(rawValue: LANDING), object: nil)
        
        let logo = UIImage(named: "icon_logo_small")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        self.navigationItem.titleView?.sizeToFit()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.setupBarButtonItem), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        self.hideCampaignView(animated: false)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        
        //設定畫面的美工
        myProjectButton.layer.borderColor = UIColor.white.cgColor
        unBillView.layer.borderColor = UIColor.lightGray.cgColor
        unBillView.layer.borderWidth = 1
        internetUsageBackgroundView.layer.cornerRadius = internetUsageBackgroundView.frame.height / 2
        iconBackgroundView.forEach { (view) in
            view.layer.cornerRadius = view.frame.width / 2
        }
        
        callAdvertisingDataAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP02", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "首頁")
        setupBarButtonItem()
        checkVIPStatusAndChangeSkin()
        
        loginView.isHidden = LoginData.isLogin()
        if LoginData.isLogin() {
            loadAPI()
            //登入後判斷ui顯示
            cellPhoneNumber.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: loginData.custProfile_msisdn, type: encodeForSecurityType.Phone)
            nickName.text = loginData.custProfile_displayNickname
            
            pstn.text = loginData.unbillData_pstn
            pstnDesc.text = loginData.unbillData_pstnDesc
            onNetVoice.text = loginData.unbillData_onNetVoice
            onNetVoiceDesc.text = loginData.unbillData_onNetVoiceDesc
            offNetVoice.text = loginData.unbillData_offNetVoice
            offNetVoiceDesc.text = loginData.unbillData_offNetVoiceDesc
            dataUsage.text = loginData.unbillData_dataUsage
            unlimitedImageView.isHidden = !loginData.unbillData_isUnlimitedData
            dataTotal.isHidden = loginData.unbillData_isUnlimitedData
            dataTotal.text = loginData.unbillData_dataTotal
            dataUnit.text = loginData.unbillData_dataUnit
            remainingBillingDate.text = loginData.remainingBillingDate
            unBillBuyButton.isHidden = !loginData.unbillData_showBuyButton
        }else {
            //設定假資料
            setDummyData()
        }
        
        //設定未出帳的％數
        if loginData.unbillData_isUnlimitedData {
            internetUsageBackgroundView.percent = 1
        }else {
            let numerator:Float = loginData.unbillData_dataUsage.toFloatValue!
            let denominator:Float = loginData.unbillData_dataTotal.toFloatValue!
            internetUsageBackgroundView.percent = CGFloat(numerator/denominator)
            self.internetUsageBackgroundView.setNeedsDisplay()
            self.internetUsageBackgroundView.updateFocusIfNeeded()
            self.internetUsageBackgroundView.setNeedsLayout()
        }
        
        // 隱藏 navigationBar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP02", action: "")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        // 顯示 navigationBar
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        if LoginData.isLogin() {
            UISkinChange.sharedInstance.changeUIColor(theObject: accountInfoButton, mode: ButtonColorMode.button_account_Info.rawValue)
        }
        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
        
        UISkinChange.sharedInstance.changeUIColor(theObject: campaignButton1, mode: ButtonColorMode.button_purple.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: campaignButton2, mode: ButtonColorMode.button_purple.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: campaignButton3, mode: ButtonColorMode.button_purple.rawValue)
    }
    
    func loadAPI() {
        
        if !didLoadAPIOnce {
            didLoadAPIOnce = true
            callQueryMessageAPI()
        }
    }
    
    func hideCampaignView(animated: Bool) {
        self.campaignViewTopSpace.constant = 0
        self.campaignImageViewAspect = self.campaignImageViewAspect.setMultiplier(multiplier: -1)
        self.campaignButtonViewHeight.constant = 0
        
        self.view.setNeedsUpdateConstraints()
        
        if animated {
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                self.campaignView.alpha = 0
                self.campaignButtonView.alpha = 0
                self.view.layoutIfNeeded()
            }, completion: { (isComplete: Bool) in
                self.campaignView.isHidden = true
            })
        }
        else {
            self.campaignView.alpha = 0
            self.campaignButtonView.alpha = 0
            self.campaignView.isHidden = true
        }
    }
    
    @objc func setupBarButtonItem() {
        if !LoginData.isLogin() {
            
            accountInfoButton.isEnabled = false
            accountInfoButton.image = UIImage(named: "")
            messageButton.isEnabled = false
            messageButton.image = UIImage(named: "")
        }
        else {
            accountInfoButton.image = UIImage(named: "button_account")
            accountInfoButton.isEnabled = true
            accountInfoButton.tintColor = nil
            
            messageButton.isEnabled = true
            messageButton.image = UIImage(named: "button_message")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            messageButton.tintColor = nil
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "popoverAccountMenu" {
            let popoverVC = segue.destination as! AccountMenuViewController
            popoverVC.modalPresentationStyle = UIModalPresentationStyle.popover
            popoverVC.popoverPresentationController!.delegate = self
            let popoverSize = CGSize(width: 132, height: 100)
            popoverVC.preferredContentSize = popoverSize
            popoverVC.delegate = self
        }
        else if segue.identifier == "embedAdvertisingPageVC" {
            advertisingPageVC = segue.destination as? AdvertisingPageViewController
            advertisingPageVC?.advertisingDelegate = self
        }
        else if segue.identifier == "embedAdvertising2CollectionVC" {
            advertising2CollectionVC = segue.destination as? Advertising2CollectionViewController
            advertising2CollectionVC?.advertising2Delegate = self
        }
        else if segue.identifier == "embedPromoteVC" {
            promoteVC = segue.destination as? PromoteTableViewController
            promoteVC?.promoteDelegate = self
        }
        else if segue.identifier == "embedNewsVC" {
            newsVC = segue.destination as? NewsTableViewController
            newsVC?.newsDelegate = self
        }
    }
    
    
    @objc func backToRootViewController(notification: NSNotification) {
        print("backToRootViewController")

        let popedVCArray = navigationController?.popToRootViewController(animated: false)
        if popedVCArray == nil {
            
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: 0), animated: true)
            setAutomaticViewChangeLandingVC()
        }
    }
    
    //MARK: - custom func
    func setDummyData() {
        nickName.text = "親愛的用戶"
        cellPhoneNumber.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: "0987654321", type: encodeForSecurityType.Phone)
        onNetVoice.text = "16"
        onNetVoiceDesc.text = "講到飽"
        offNetVoice.text = "20"
        offNetVoiceDesc.text = "贈送30分鐘/月"
        pstn.text = "1"
        pstnDesc.text = "無優惠"
        dataUsage.text = "3.8"
        unBillBuyButton.isHidden = false
        dataUnit.text = "GB"
        dataTotal.text = "20"
        remainingBillingDate.text = "18"
        unlimitedImageView.isHidden = true
        
    }
    
    
    //MARK: - IBAction
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_MASK_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "我的帳戶", label: "登入", value: nil)
        
        if !TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self) {
            
            TSTAPP2_Utilities.pushToLoginViewController(withViewController: self)
        }
    }
    
    @IBAction func didTapUnBillWarning(_ sender: Any) {
        TSTAPP2_Utilities.showHtmlMessage(withTitle: "注意事項", message: LoginData.sharedLoginData().unbillData_warningWording, viewController: self)
    }
    
    //點擊我的專案
    @IBAction func didTapMyContractButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_CONT")
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案_Button", action: "", label: "", value: nil)
    }
    
    @IBAction func didTapAQButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_EC_APPLY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "我的帳戶", label: "門號申辦", value: nil)
        
        #if SIT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #elseif UAT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #else
            let urlString: String = "https://www.tstartel.com/mCWS/shop.php"
        #endif
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
        //        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @IBAction func didTapAccountButton(_ sender: AnyObject) {
        let aiVC = UIStoryboard.init(name: "AccountInformation", bundle: nil).instantiateViewController(withIdentifier: "AccountInformation")
//        let aiVC = storyboard?.instantiateViewController(withIdentifier: "AccountInformation")
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "會員中心",
                               contentType: "Function",
                               contentId: "AccountInfo",
                               customAttributes: [:])
    }
    
    @IBAction func didTapStoreLocationButton(_ sender: Any) {
        let slVC = UIStoryboard.init(name: "StoreLocation", bundle: nil).instantiateViewController(withIdentifier: "StoreLocationViewController")
        navigationController?.pushViewController(slVC, animated: true)
        
        Answers.logContentView(withName: "服務據點",
                               contentType: "Function",
                               contentId: "StoreLocation",
                               customAttributes: [:])
    }
    
    @IBAction func didTapProductBackwardButton(_ sender: Any) {
        self.advertising2CollectionVC?.scrollBackward()
    }
    
    
    @IBAction func didTapProductForwardButton(_ sender: Any) {
        self.advertising2CollectionVC?.scrollForward()
    }
    
    @IBAction func didTapFacebookButton(_ sender: Any) {
        let urlString = "https://www.facebook.com/tstartel"
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
        
         TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "台灣之星粉絲團", action: nil, label: nil, value: nil)
        
        Answers.logContentView(withName: "台灣之星粉絲團",
                               contentType: "AD",
                               contentId: urlString,
                               customAttributes: [:])
    }
    
    @IBAction func didTapLineButton(_ sender: Any) {
        let urlString: String = "https://line.me/R/ti/p/%40tstar"
        UIApplication.shared.openURL(URL(string: urlString)!)
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "台灣之星官方好友", action: nil, label: nil, value: nil)
        
        Answers.logContentView(withName: "台灣之星官方好友",
                               contentType: "AD",
                               contentId: urlString,
                               customAttributes: [:])
    }
    
    @IBAction func didTapUnbillBuyButton(_ sender: UIButton) {
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "加購", urlString: loginData.unbillData_buyUrl, viewController: self)
    }
    
    //MARK: - UITableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return advertisingImageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisingCell") as! AdvertisingCell
        cell.myImageView.image = advertisingImageList[indexPath.row]
        cell.setAspectConstraint(aspect: self.aspectList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                tableViewHeight.constant = tableView.contentSize.height
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didTapAdvertisement(sender: SmartBannerList.sharedInstance.homeSmartBanner[indexPath.row])
    }
    
    //MARK: AccountMenuDelegate
    func didTapAccountInfoButton(sender: Any?) {
        let aiVC = UIStoryboard.init(name: "AccountInformation", bundle: nil).instantiateViewController(withIdentifier: "AccountInformation")
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "會員中心",
                               contentType: "Function",
                               contentId: "AccountInfo",
                               customAttributes: [:])
    }
    
    @IBAction func didTapMessageButton(_ sender: Any) {
        let aiVC = UIStoryboard.init(name: "Message", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController")
        (aiVC as? MessageViewController)?.landingVC = self
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "訊息中心",
            contentType: "Function",
            contentId: "Message",
            customAttributes: [:])
    }
    
    
    //MARK: AdvertisingDelegate
    func didTapAdvertisement(sender: Any?) {
        if let smartBanner = sender as? SmartBanner {
            print("didTapAdvertising \(smartBanner.url)")
            let urlString = smartBanner.url
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "智能推薦", action: "\(smartBanner.titleText);\(urlString)", label: nil, value: nil)
            
            UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                             action: "ACMP_\(SmartBannerList.sharedInstance.cmpId)_\(SmartBannerList.sharedInstance.cmpVersion)_\(smartBanner.displayType)_\(smartBanner.sort)",
                                             codeId: SmartBannerList.sharedInstance.cmpId,
                                             codeVersion: SmartBannerList.sharedInstance.cmpVersion,
                                             codeDisplayType: smartBanner.displayType,
                                             codeSort: smartBanner.sort)
            
            Answers.logContentView(withName: "智能推薦",
                                   contentType: "AD",
                                   contentId: urlString,
                                   customAttributes: [:])
            
            if smartBanner.action == AUTO_BROWSER {
                if let url = URL(string: urlString) {
                    UIApplication.shared.openURL(url)
                }
            }
            else {//if smartBanner.action == AUTO_WEBVIEW {
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
            }

        }
    }
    
    @IBAction func didTapAdvertisementButton(_ sender: AdvertisingImageViewButton) {
        if let smartBanner = sender.smartBanner as SmartBanner? {
            print("didTapAdvertising \(smartBanner.url)")
            let urlString = smartBanner.url
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "智能推薦", action: "\(smartBanner.titleText);\(urlString)", label: nil, value: nil)
            
            UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                             action: "ACMP_\(SmartBannerList.sharedInstance.cmpId)_\(SmartBannerList.sharedInstance.cmpVersion)_\(smartBanner.displayType)_\(smartBanner.sort)",
                codeId: SmartBannerList.sharedInstance.cmpId,
                codeVersion: SmartBannerList.sharedInstance.cmpVersion,
                codeDisplayType: smartBanner.displayType,
                codeSort: smartBanner.sort)
            
            Answers.logContentView(withName: "智能推薦",
                                   contentType: "AD",
                                   contentId: urlString,
                                   customAttributes: [:])
            
            if smartBanner.action == AUTO_BROWSER {
                if let url = URL(string: urlString) {
                    UIApplication.shared.openURL(url)
                }
            }
            else {//if smartBanner.action == AUTO_WEBVIEW {
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
            }
            
        }
    }
    
    //MARK: Advertising2Delegate
    func didTapAdvertisement2(sender: Any?) {
        if let product = sender as? Product {
            print("didTapAdvertising2 \(product.url)")
            let urlString = product.url
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "推薦商品", action: "\(urlString);\(product.productName)", label: nil, value: nil)
            
            UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                             action: "APRD_\(product.productId)",
                                             codeId: product.productId,
                                             codeVersion: "",
                                             codeDisplayType: "",
                                             codeSort: "")
            
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "推薦商品", urlString: urlString, viewController: self)
            
            Answers.logContentView(withName: "推薦商品",
                                   contentType: "AD",
                                   contentId: urlString,
                                   customAttributes: [:])
        }
    }
    
    //MARK: PromoteDelegate
    func didTapPromote(sender: Any?) {
        if let promoteContent = sender as? PromoteContent {
            print("didTapPromote \(promoteContent.url)")
            let urlString = promoteContent.url
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "優惠Banner", action: "\(urlString)", label: nil, value: nil)
            
            UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                             action: "APRM_\(promoteContent.id)_\(promoteContent.version)_\(promoteContent.sort)",
                                             codeId: promoteContent.id,
                                             codeVersion: promoteContent.version,
                                             codeDisplayType: "",
                                             codeSort: promoteContent.sort)
            
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "找優惠", urlString: urlString, viewController: self)
            
            Answers.logContentView(withName: "優惠",
                                   contentType: "AD",
                                   contentId: urlString,
                                   customAttributes: [:])
        }
    }
    
    //MARK: NewsDelegate
    func didTapNews(sender: Any?) {
        if let newsContent = sender as? NewsContent {
            print("didTapNews \(newsContent.url)")
            let urlString = newsContent.url
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "最新消息", urlString: urlString, viewController: self)
        }
    }
    
//    MARK: Campaign
    @IBAction func didTapCampaignMainButton(_ sender: Any) {
        let campaign = CampaignInfo.sharedInstance
        
        let text2 = campaign.text2
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP02", action: campaign.ubaActionCode)
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "限時活動", action: "Banner", label: text2, value: nil)
        
        Answers.logContentView(withName: "限時活動Banner",
                               contentType: "AD",
                               contentId: "CampaignBanner",
                               customAttributes: [:])
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: text2, appendToken: true, openBrowser: true, viewController: self)
    }
    
    @IBAction func didTapCampaignButton(_ sender: Any) {
        let tag = (sender as! UIButton).tag
        let campaign = CampaignInfo.sharedInstance
        
        if campaign.buttonList.count > tag {
            let buttonData = campaign.buttonList[tag]
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP02", action: buttonData.ubaActionCode)
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "限時活動", action: "Button", label: "\(buttonData.text1)\(buttonData.text2)", value: nil)
            
            Answers.logContentView(withName: "限時活動Button",
                                   contentType: "AD",
                                   contentId: "CampaignButton",
                                   customAttributes: [:])
            
            let type = buttonData.type
            
            if type == "BUTTON" {
                let text2 = buttonData.text2
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: text2, appendToken: true, openBrowser: true, viewController: self)
            }
            else if type == "SHARE" {
                let text2 = buttonData.text2
                let activity = UIActivityViewController(activityItems: [text2], applicationActivities: nil)
                
                self.present(activity, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        popoverPresentationController.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
    }
    
//    MARK: - API
    //未入帳API
    func callAdvertisingDataAPI() {
        
        TSTAPP2_Utilities.showPKHUD()

        TSTAPP2_API.sharedInstance.getSmartBanner(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, companyId: loginData.custProfile_companyId, contractStartDate: loginData.custProfile_activationDate, contractExpireDate: loginData.custProfile_expiredDate, projectName: loginData.custProfile_projectName, isRenewal: loginData.ivrQualify_isRenewal, isBL: loginData.isBL, isUnlimitedData: loginData.unbillData_isUnlimitedData ? "Y" : "N", birthday: loginData.custProfile_birthday, vipDegree: loginData.custProfile_vipDegree, retensionType: loginData.custProfile_retensionType, custType: loginData.custProfile_custType, customerContribution: loginData.custProfile_customerContribution, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            self.isGetSmartBanner = true
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let smartBanner = SmartBannerList.sharedInstance
                smartBanner.setDataValue(withDic: dic)
                
                self.advertisingImageList = [UIImage](repeatElement(UIImage(), count: smartBanner.homeSmartBanner.count))
                self.aspectList = [CGFloat?](repeatElement(nil, count: smartBanner.homeSmartBanner.count))
                
                let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.scrollView.frame.size.width, height: self.view.frame.size.height)
                activityIndicator.startAnimating()
                
                DispatchQueue.global().async(execute: {
                    
                    for i in 0..<smartBanner.homeSmartBanner.count {
                        let url = URL(string: smartBanner.homeSmartBanner[i].path)
                        let data = try? Data(contentsOf: url!)
                        
                        DispatchQueue.main.sync(execute: {
                            if data != nil {
                                if let image: UIImage = UIImage(data: data!) {
                                    let aspect = image.size.width / image.size.height
                                    self.advertisingImageList[i] = image
                                    self.aspectList[i] = aspect
                                    if i == smartBanner.homeSmartBanner.count - 1 {
                                        activityIndicator.removeFromSuperview()
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                        })
                    }
                })
            }
            else {
                
            }
            
        }) { (error: Error) in
            self.isGetSmartBanner = true
            self.tableViewHeight.constant = 0
        }
        
        if TSTAPP2_Utilities.versionStatus() != VersionStatus.notReleased {
            TSTAPP2_API.sharedInstance.recommendProduct(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                
                self.isRecommendProduct = true
                
                if dic.stringValue(ofKey: "code") == "00000" {
                    let recommendProduct = RecommendProducts.sharedInstance
                    recommendProduct.setDataValue(withDic: dic)
                    self.productBackwardButton.isHidden = false
                    self.productForwardButton.isHidden = false
                    self.advertising2CollectionVC?.productArray = recommendProduct.productArray
                    self.advertising2CollectionVC?.resetScrollPosition()
                    self.advertising2CollectionVC?.collectionView?.reloadData()
                }
                else {
                    self.productBackwardButton.isHidden = true
                    self.productForwardButton.isHidden = true
                }
            }) { (error: Error) in
                self.productBackwardButton.isHidden = true
                self.productForwardButton.isHidden = true
                self.isRecommendProduct = true
            }
        }
        else {
            productBackwardButton.isHidden = true
            productForwardButton.isHidden = true
            self.isRecommendProduct = true
        }
        
        TSTAPP2_API.sharedInstance.getHomepage(withMSISDN: loginData.custProfile_msisdn, version: nil, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            self.isGetHomepage = true
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                let homePage = Homepage.sharedInstance
                homePage.setDataValue(withDic: dic)
                
                let promoteCellHeight = ((self.view.frame.width * 3) / 4) + 1
                self.promoteVCConstraint.constant = (CGFloat(homePage.promote.content.count) * promoteCellHeight)
                
                self.promoteVC?.promoteData = homePage.promote
                self.promoteVC?.tableView.reloadData()
                
                let urlString = homePage.video.url
                if urlString.components(separatedBy: "v=").count == 2 {
                    let videoID = urlString.components(separatedBy: "v=")[1]
                    self.ytPlayerView.load(withVideoId: videoID)
                }
                
                self.newsVCConstraint.constant = 0
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
                self.newsVC?.newsData = homePage.news
                self.newsVC?.tableView.reloadData()
            }
            else {
                self.newsVCConstraint.constant = 0
            }
        }) { (errpr: Error) in
            self.isGetHomepage = true
            self.newsVCConstraint.constant = 0
        }
        
    }
    
    func callQueryMessageAPI() {
        
        let messageCount = UserDefaults.standard.integer(forKey: MESSAGE_COUNT) 
        
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.queryMessage(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                let pm = PushMessages.sharedInstance
                pm.setDataValue(withDic: dic)
                
                if pm.messages.count > messageCount {
                    self.messageButton.image = UIImage(named: "button_message_unread.png")
                }
                else {
                    self.messageButton.image = UIImage(named: "button_message.png")
                }
            }
        }) { (error: Error) in
        
        }
    }
    
    func apiResponded() {
        if isGetSmartBanner && isRecommendProduct && isGetHomepage {
            TSTAPP2_Utilities.hidePKHUD()
//            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: 0), animated: true)
            
            isGetSmartBanner = false
            isRecommendProduct = false
            isGetHomepage = false
            
//            AutomaticViewChange.sharedInstance.landingVC = self
            setAutomaticViewChangeLandingVC()
            
            didLoadAPIOnce = false
            
        }
    }
    
    func setAutomaticViewChangeLandingVC() {
        if !didSetLandingVC {
            didSetLandingVC = true
            AutomaticViewChange.sharedInstance.landingVC = self
            didSetLandingVC = false
        }
    }

}
