//
//  LoginData.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/7.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class LoginData: NSObject, NSCoding {
    
    private(set) var hasData = "N"                           //Y, N
    
    private(set) var token = ""
    private(set) var isBL = ""
    private(set) var blUrl = ""
    private(set) var isUnlimitedData = ""
    private(set) var isPostpaid = ""                         //是否為月租型 Y:月租 N:預付
    private(set) var roleType = ""
    
    //MARK: - custProfile
    private(set) var custProfile_uid = ""                    //會員編號
    private(set) var custProfile_contractId = ""             //網內用戶合約編號
    private(set) var custProfile_msisdn = ""                 //網內用戶門號、網外用戶Email或OpenId帳號
    private(set) var custProfile_custId = ""                 //身分證號/統編
    private(set) var custProfile_custName = ""               //姓名/公司名稱
    
    private(set) var custProfile_accountId = ""              //合併帳單之帳號Billing account
    private(set) var custProfile_imsi = ""                   //用戶的IMSI
    private(set) var custProfile_birthday = ""               //用戶的生日，格式為：YYYYMMDD
    private(set) var custProfile_operatorId = ""             //Postpaid (Vibo/Tstartel)=01, Prepaid (Vibo/Tstartel)=04
    private(set) var custProfile_companyId = ""              //Postpaid (Vibo) = 001, Postpaid (Tstartel) = 025,
                                                             //Prepaid (Vibo) = 004, Prepaid (Tstartel) = 026
    
    private(set) var custProfile_billingCycle = ""           //帳單週期(BC)
    private(set) var custProfile_gender = ""                 //性別 男=M, 女=F
    private(set) var custProfile_custType = ""               //身分 個人戶 = 0, 公司戶 = 1
    private(set) var custProfile_userState = ""              //用戶門號狀態 Normal = 0, Suspended = 1
    private(set) var custProfile_vipDegree = ""              //VIP會員等級
    
    private(set) var custProfile_vipStartDate = ""           //VIP會員起始日
    private(set) var custProfile_vipEndDate = ""             //VIP會員結束日
    private(set) var custProfile_picUrl = ""                 //大頭照URL
    private(set) var custProfile_email = ""                  //會員中心Email
    private(set) var custProfile_emailFlag = ""              //Email 是否認證
    
    private(set) var custProfile_pwschange = ""              //是否完成首登(DB) 0:未完成, 1:已完成, 2:忘記密碼，需強制變更
    private(set) var custProfile_customerContribution = ""   //客戶貢獻度
    private(set) var custProfile_projectCode = ""            //專案代碼
    private(set) var custProfile_projectName = ""            //專案名稱
    private(set) var custProfile_retensionType = ""          //續約類別
    
    private(set) var custProfile_realFee = ""                //實收資費
    private(set) var custProfile_activationDate = ""         //合約起始日
    private(set) var custProfile_expiredDate = ""            //合約到期日
    private(set) var custProfile_isMainMsisdn = ""           //是否為主帳號(Y/N)
    private(set) var custProfile_billAcctNumber = ""         //帳務帳號
    
    private(set) var custProfile_memberType = ""             //會員類別
    private(set) var custProfile_isVIP = ""                  //是否為VIP Y or N
    private(set) var custProfile_displayNickname = ""        //顯示的暱稱
    private(set) var custProfile_nickname = ""               //暱稱
    private(set) var custProfile_gsmToLteFlag = ""           //3G轉4G狀態
    
    private(set) var custProfile_gsmToLteDate = ""           //3G轉4G狀態日期
    private(set) var custProfile_extId = ""                  //最新一筆第三方帳號
    private(set) var custProfile_extType = ""                //最新一筆第三方類別
    private(set) var custProfile_extName = ""                //最新一筆第三方姓名
    private(set) var custProfile_extEmail = ""               //最新一筆第三方email
    
    private(set) var custProfile_extList: [Dictionary<String, Any>] = []//第三方帳號清單
    private(set) var custProfile_planName = ""               //資費名稱
    private(set) var remainingBillingDate = ""               //距離下一個帳週的天數
    
    //MARK: - custInfoBrief
    private(set) var custInfoBrief_contractId = ""           //合約編號
    private(set) var custInfoBrief_msisdn = ""               //用戶門號
    private(set) var custInfoBrief_billingZipCode = ""       //帳單地址-郵遞區號
    private(set) var custInfoBrief_billingState = ""         //帳單地址-縣市
    private(set) var custInfoBrief_billingCity = ""          //帳單地址-鄉鎮市區
    
    private(set) var custInfoBrief_billingAddr = ""          //帳單地址-地址
    private(set) var custInfoBrief_perZipCode = ""           //戶籍地址-郵遞區號
    private(set) var custInfoBrief_perState = ""             //戶籍地址-縣市
    private(set) var custInfoBrief_perCity = ""              //戶籍地址-鄉鎮市區
    private(set) var custInfoBrief_perAddr = ""              //戶籍地址-地址
    
    private(set) var custInfoBrief_contractStatus = ""       //合約狀態
    private(set) var custInfoBrief_maskBillingAddress = ""   //隱碼後的帳單地址
    private(set) var custInfoBrief_maskPerAddress = ""       //隱碼後的戶籍地址
    
    //MARK: - ivrQualify
    private(set) var ivrQualify_earlyRenewDay = ""           //用戶可提前續約日期
    private(set) var ivrQualify_renewalCode = ""             //續約資格代碼
    private(set) var ivrQualify_renewalMessage = ""          //續約資格訊息
    private(set) var ivrQualify_extendDate = ""              //
    private(set) var ivrQualify_extendFlag = ""              //用戶可提前續約日期
    
    private(set) var ivrQualify_displayRenewalMessage = ""   //顯示的續約資格訊息
    private(set) var ivrQualify_displayDiscInfo = ""         //顯示的續約資格說明
    private(set) var ivrQualify_isRenewal = ""               //是否可續約(Y:可續約/N:不可續約)
    
    //Project 即時專案資訊
//    private(set) var project_projectCode = ""                //專案代碼
//    private(set) var project_projectName = ""                //專案名稱
//    private(set) var project_projectRate = ""                //資費
//    private(set) var project_billDate = ""                   //出帳日
//    private(set) var project_paymentEndDate = ""             //繳費截止日
//    
//    private(set) var project_startDate = ""                  //合約起始繳費截止日(yyyy/MM/dd)
//    private(set) var project_endDate = ""                    //合約結束日期(yyyy/MM/dd)
//    private(set) var project_contractStartDate = ""          //合約起始日期(yyyy-MM-dd)
//    private(set) var project_contractExpireDate = ""         //合約結束日期(yyyy-MM-dd)ww
//    private(set) var project_unlimitedData = ""              //是否吃到飽(Y:吃到飽/N:非吃到飽)
    
    
    //MARK: - EBill 電子帳單狀態
    private(set) var eBill_emailAddress = ""
    private(set) var eBill_memberType = ""
    private(set) var eBill_statusText = ""
    private(set) var eBill_showApplyEbill = ""
    private(set) var eBill_showMessage = ""
    
    //MARK: - urlConfig
    private(set) var urlConfig_BL = ""
    private(set) var urlConfig_PROMO_ZONE = ""
    
    //MARK: - billType
    private(set) var billType_address = ""
    private(set) var billType_addressEdit = ""
    private(set) var billType_addressShow = ""
    private(set) var billType_billType = ""
    private(set) var billType_billTypeEdit = ""
    
    private(set) var billType_billTypeMsg = ""
    private(set) var billType_billingAddr = ""
    private(set) var billType_billingCity = ""
    private(set) var billType_billingState = ""
    private(set) var billType_billingZipCode = ""
    
    private(set) var billType_ebillApply = ""
    private(set) var billType_email = ""
    private(set) var billType_maskEmail = ""
    private(set) var billType_emailEdit = ""
    private(set) var billType_emailShow = ""
    
    private(set) var billType_memberType = ""
    private(set) var billType_showMessage = ""
    
    //MARK: - unbillData
    private(set) var unbillData_onNetVoice : String = ""
    private(set) var unbillData_offNetVoice : String = ""
    private(set) var unbillData_pstn : String = ""
    private(set) var unbillData_dataUsage : String = ""
    private(set) var unbillData_dataTotal : String = ""
    private(set) var unbillData_dataUnit : String = ""
    private(set) var unbillData_onNetVoiceDesc : String = ""
    private(set) var unbillData_offNetVoiceDesc : String = ""
    private(set) var unbillData_pstnDesc : String = ""
    private(set) var unbillData_onNetSMSDesc : String = ""
    private(set) var unbillData_offNetSMSDesc : String = ""
    private(set) var unbillData_showBuyButton : Bool = false
    private(set) var unbillData_buyUrl : String = ""
    private(set) var unbillData_warningWording : String = ""
    private(set) var unbillData_isUnlimitedData : Bool = false
    
    //MARK: -
    
    private static var sharedInstance = LoginData()
    
    private var didInitFromUserDefaults = false
    
    private override init() {
        //This prevents others from using the default '()' initializer for this class.
    }

    class func sharedLoginData() -> LoginData {
        if !sharedInstance.didInitFromUserDefaults && UserDefaults.standard.object(forKey: "TSTAPP2_LoginData") != nil {
            sharedInstance.didInitFromUserDefaults = true
            let data = UserDefaults.standard.object(forKey: "TSTAPP2_LoginData") as! Data
            sharedInstance = NSKeyedUnarchiver.unarchiveObject(with: data) as! LoginData
            print("init sharedInstance with userDefault data")
        }
        return sharedInstance
    }
    
    class func cleanLoginData() -> LoginData {
        return LoginData()
    }
    
    class func isLogin() -> Bool {
        
        let isMaintain = TSTAPP2_Utilities.isMaintain(showAlert: false, withViewController: nil)
        
        return (UserDefaults.standard.object(forKey: "TSTAPP2_LoginData") != nil) && !isMaintain
    }
    
    class func isFacebookLogin() -> Bool {
        return UserDefaults.standard.object(forKey: FACEBOOK_LOGIN_DATA) != nil
    }
    
    func initAllValue() {
        hasData = "N"
        token = ""
        isBL = ""
        blUrl = ""
        isUnlimitedData = ""
        isPostpaid = ""
        roleType = ""
        
        //custProfile
        custProfile_uid = ""
        custProfile_contractId = ""
        custProfile_msisdn = ""
        custProfile_custId = ""
        custProfile_custName = ""
        
        custProfile_accountId = ""
        custProfile_imsi = ""
        custProfile_birthday = ""
        custProfile_operatorId = ""
        custProfile_companyId = ""
        
        custProfile_billingCycle = ""
        custProfile_gender = ""
        custProfile_custType = ""
        custProfile_userState = ""
        custProfile_vipDegree = ""
        
        custProfile_vipStartDate = ""
        custProfile_vipEndDate = ""
        custProfile_picUrl = ""
        custProfile_email = ""
        custProfile_emailFlag = ""
        
        custProfile_pwschange = ""
        custProfile_customerContribution = ""
        custProfile_projectCode = ""
        custProfile_projectName = ""
        custProfile_retensionType = ""
        
        custProfile_realFee = ""
        custProfile_activationDate = ""
        custProfile_expiredDate = ""
        custProfile_isMainMsisdn = ""
        custProfile_billAcctNumber = ""
        
        custProfile_memberType = ""
        custProfile_isVIP = ""
        custProfile_displayNickname = ""
        custProfile_nickname = ""
        custProfile_gsmToLteFlag = ""
        
        custProfile_gsmToLteDate = ""
        custProfile_extId = ""
        custProfile_extType = ""
        custProfile_extName = ""
        custProfile_extEmail = ""
        
        custProfile_planName = ""
        remainingBillingDate = ""
        
        //custInfoBrief
        custInfoBrief_contractId = ""
        custInfoBrief_msisdn = ""
        custInfoBrief_billingZipCode = ""
        custInfoBrief_billingState = ""
        custInfoBrief_billingCity = ""
        
        custInfoBrief_billingAddr = ""
        custInfoBrief_perZipCode = ""
        custInfoBrief_perState = ""
        custInfoBrief_perCity = ""
        custInfoBrief_perAddr = ""
        
        custInfoBrief_contractStatus = ""
        custInfoBrief_maskBillingAddress = ""
        custInfoBrief_maskPerAddress = ""
        
        //ivrQualify
        ivrQualify_earlyRenewDay = ""
        ivrQualify_renewalCode = ""
        ivrQualify_renewalMessage = ""
        ivrQualify_extendDate = ""
        ivrQualify_extendFlag = ""
        
        ivrQualify_displayRenewalMessage = ""
        ivrQualify_displayDiscInfo = ""
        ivrQualify_isRenewal = ""
        
        //Project
//        project_projectCode = ""
//        project_projectName = ""
//        project_projectRate = ""
//        project_billDate = ""
//        project_paymentEndDate = ""
//        
//        project_startDate = ""
//        project_endDate = ""
//        project_contractStartDate = ""
//        project_contractExpireDate = ""
//        project_unlimitedData = ""
        
        //EBill
        eBill_emailAddress = ""
        eBill_memberType = ""
        eBill_statusText = ""
        eBill_showApplyEbill = ""
        eBill_showMessage = ""
        
        //urlConfig
        urlConfig_BL = ""
        urlConfig_PROMO_ZONE = ""
        
        //billType
        billType_address = ""
        billType_addressEdit = ""
        billType_addressShow = ""
        billType_billType = ""
        billType_billTypeEdit = ""
        
        billType_billTypeMsg = ""
        billType_billingAddr = ""
        billType_billingCity = ""
        billType_billingState = ""
        billType_billingZipCode = ""
        
        billType_ebillApply = ""
        billType_email = ""
        billType_maskEmail = ""
        billType_emailEdit = ""
        billType_emailShow = ""
        
        billType_memberType = ""
        billType_showMessage = ""
        
        //unbillData
        unbillData_onNetVoice = ""
        unbillData_offNetVoice = ""
        unbillData_pstn = ""
        unbillData_dataUsage = ""
        unbillData_dataTotal = ""
        unbillData_dataUnit = ""
        unbillData_onNetVoiceDesc = ""
        unbillData_offNetVoiceDesc = ""
        unbillData_pstnDesc = ""
        unbillData_onNetSMSDesc = ""
        unbillData_offNetSMSDesc = ""
        unbillData_showBuyButton = false
        unbillData_buyUrl = ""
        unbillData_warningWording = ""
        unbillData_isUnlimitedData = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        hasData = aDecoder.decodeObject(forKey: "hasData") as? String ?? ""
        token = aDecoder.decodeObject(forKey: "token") as! String
        isBL = aDecoder.decodeObject(forKey: "isBL") as! String
        blUrl = aDecoder.decodeObject(forKey: "blUrl") as? String ?? ""
        isUnlimitedData = aDecoder.decodeObject(forKey: "isUnlimitedData") as! String
        isPostpaid = aDecoder.decodeObject(forKey: "isPostpaid") as! String
        roleType = aDecoder.decodeObject(forKey: "roleType") as! String
        
        //custProfile
        custProfile_uid = aDecoder.decodeObject(forKey: "custProfile_uid") as! String
        custProfile_contractId = aDecoder.decodeObject(forKey: "custProfile_contractId") as! String
        custProfile_msisdn = aDecoder.decodeObject(forKey: "custProfile_msisdn") as! String
        custProfile_custId = aDecoder.decodeObject(forKey: "custProfile_custId") as! String
        custProfile_custName = aDecoder.decodeObject(forKey: "custProfile_custName") as! String
        
        custProfile_accountId = aDecoder.decodeObject(forKey: "custProfile_accountId") as! String
        custProfile_imsi = aDecoder.decodeObject(forKey: "custProfile_imsi") as! String
        custProfile_birthday = aDecoder.decodeObject(forKey: "custProfile_birthday") as! String
        custProfile_operatorId = aDecoder.decodeObject(forKey: "custProfile_operatorId") as! String
        custProfile_companyId = aDecoder.decodeObject(forKey: "custProfile_companyId") as! String
        
        custProfile_billingCycle = aDecoder.decodeObject(forKey: "custProfile_billingCycle") as! String
        custProfile_gender = aDecoder.decodeObject(forKey: "custProfile_gender") as! String
        custProfile_custType = aDecoder.decodeObject(forKey: "custProfile_custType") as! String
        custProfile_userState = aDecoder.decodeObject(forKey: "custProfile_userState") as! String
        custProfile_vipDegree = aDecoder.decodeObject(forKey: "custProfile_vipDegree") as! String
        
        custProfile_vipStartDate = aDecoder.decodeObject(forKey: "custProfile_vipStartDate") as! String
        custProfile_vipEndDate = aDecoder.decodeObject(forKey: "custProfile_vipEndDate") as! String
        custProfile_picUrl = aDecoder.decodeObject(forKey: "custProfile_picUrl") as! String
        custProfile_email = aDecoder.decodeObject(forKey: "custProfile_email") as! String
        custProfile_emailFlag = aDecoder.decodeObject(forKey: "custProfile_emailFlag") as! String
        
        custProfile_pwschange = aDecoder.decodeObject(forKey: "custProfile_pwschange") as! String
        custProfile_customerContribution = aDecoder.decodeObject(forKey: "custProfile_customerContribution") as! String
        custProfile_projectCode = aDecoder.decodeObject(forKey: "custProfile_projectCode") as! String
        custProfile_projectName = aDecoder.decodeObject(forKey: "custProfile_projectName") as! String
        custProfile_retensionType = aDecoder.decodeObject(forKey: "custProfile_retensionType") as! String
        
        custProfile_realFee = aDecoder.decodeObject(forKey: "custProfile_realFee") as! String
        custProfile_activationDate = aDecoder.decodeObject(forKey: "custProfile_activationDate") as! String
        custProfile_expiredDate = aDecoder.decodeObject(forKey: "custProfile_expiredDate") as! String
        custProfile_isMainMsisdn = aDecoder.decodeObject(forKey: "custProfile_isMainMsisdn") as! String
        custProfile_billAcctNumber = aDecoder.decodeObject(forKey: "custProfile_billAcctNumber") as! String
        
        custProfile_memberType = aDecoder.decodeObject(forKey: "custProfile_memberType") as! String
        custProfile_isVIP = aDecoder.decodeObject(forKey: "custProfile_isVIP") as! String
        custProfile_displayNickname = aDecoder.decodeObject(forKey: "custProfile_displayNickname") as! String
        custProfile_nickname = aDecoder.decodeObject(forKey: "custProfile_nickname") as! String
        custProfile_gsmToLteFlag = aDecoder.decodeObject(forKey: "custProfile_gsmToLteFlag") as? String ?? ""
        
        custProfile_gsmToLteDate = aDecoder.decodeObject(forKey: "custProfile_gsmToLteDate") as? String ?? ""
        custProfile_extId = aDecoder.decodeObject(forKey: "custProfile_extId") as! String
        custProfile_extType = aDecoder.decodeObject(forKey: "custProfile_extType") as! String
        custProfile_extName = aDecoder.decodeObject(forKey: "custProfile_extName") as! String
        custProfile_extEmail = aDecoder.decodeObject(forKey: "custProfile_extEmail") as! String
        
        custProfile_planName = aDecoder.decodeObject(forKey: "custProfile_planName") as? String ?? ""
        remainingBillingDate = aDecoder.decodeObject(forKey: "remainingBillingDate") as? String ?? ""
        //custInfoBrief
        custInfoBrief_contractId = aDecoder.decodeObject(forKey: "custInfoBrief_contractId") as! String
        custInfoBrief_msisdn = aDecoder.decodeObject(forKey: "custInfoBrief_msisdn") as! String
        custInfoBrief_billingZipCode = aDecoder.decodeObject(forKey: "custInfoBrief_billingZipCode") as! String
        custInfoBrief_billingState = aDecoder.decodeObject(forKey: "custInfoBrief_billingState") as! String
        custInfoBrief_billingCity = aDecoder.decodeObject(forKey: "custInfoBrief_billingCity") as! String
        
        custInfoBrief_billingAddr = aDecoder.decodeObject(forKey: "custInfoBrief_billingAddr") as! String
        custInfoBrief_perZipCode = aDecoder.decodeObject(forKey: "custInfoBrief_perZipCode") as! String
        custInfoBrief_perState = aDecoder.decodeObject(forKey: "custInfoBrief_perState") as! String
        custInfoBrief_perCity = aDecoder.decodeObject(forKey: "custInfoBrief_perCity") as! String
        custInfoBrief_perAddr = aDecoder.decodeObject(forKey: "custInfoBrief_perAddr") as! String
        
        custInfoBrief_contractStatus = aDecoder.decodeObject(forKey: "custInfoBrief_contractStatus") as! String
        custInfoBrief_maskBillingAddress = aDecoder.decodeObject(forKey: "custInfoBrief_maskBillingAddress") as! String
        custInfoBrief_maskPerAddress = aDecoder.decodeObject(forKey: "custInfoBrief_maskPerAddress") as! String
        
        //ivrQualify
        ivrQualify_earlyRenewDay = aDecoder.decodeObject(forKey: "ivrQualify_earlyRenewDay") as! String
        ivrQualify_renewalCode = aDecoder.decodeObject(forKey: "ivrQualify_renewalCode") as! String
        ivrQualify_renewalMessage = aDecoder.decodeObject(forKey: "ivrQualify_renewalMessage") as! String
        ivrQualify_extendDate = aDecoder.decodeObject(forKey: "ivrQualify_extendDate") as! String
        ivrQualify_extendFlag = aDecoder.decodeObject(forKey: "ivrQualify_extendFlag") as! String
        
        ivrQualify_displayRenewalMessage = aDecoder.decodeObject(forKey: "ivrQualify_displayRenewalMessage") as! String
        ivrQualify_displayDiscInfo = aDecoder.decodeObject(forKey: "ivrQualify_displayDiscInfo") as! String
        ivrQualify_isRenewal = aDecoder.decodeObject(forKey: "ivrQualify_isRenewal") as! String
        
        //Project
//        project_projectCode = aDecoder.decodeObject(forKey: "project_projectCode") as! String
//        project_projectName = aDecoder.decodeObject(forKey: "project_projectName") as! String
//        project_projectRate = aDecoder.decodeObject(forKey: "project_projectRate") as! String
//        project_billDate = aDecoder.decodeObject(forKey: "project_billDate") as! String
//        project_paymentEndDate = aDecoder.decodeObject(forKey: "project_paymentEndDate") as! String
//        
//        project_startDate = aDecoder.decodeObject(forKey: "project_startDate") as! String
//        project_endDate = aDecoder.decodeObject(forKey: "project_endDate") as! String
//        project_contractStartDate = aDecoder.decodeObject(forKey: "project_contractStartDate") as! String
//        project_contractExpireDate = aDecoder.decodeObject(forKey: "project_contractExpireDate") as! String
//        project_unlimitedData = aDecoder.decodeObject(forKey: "project_unlimitedData") as! String
        
        //EBill
        eBill_emailAddress = aDecoder.decodeObject(forKey: "eBill_emailAddress") as? String ??  ""
        eBill_memberType = aDecoder.decodeObject(forKey: "eBill_memberType") as? String ?? ""
        eBill_statusText = aDecoder.decodeObject(forKey: "eBill_statusText") as? String ?? ""
        eBill_showApplyEbill = aDecoder.decodeObject(forKey: "eBill_showApplyEbill") as? String ?? ""
        eBill_showMessage = aDecoder.decodeObject(forKey: "eBill_showMessage") as? String ?? ""
        
        //urlConfig
        urlConfig_BL = aDecoder.decodeObject(forKey: "urlConfig_BL") as? String ?? ""
        urlConfig_PROMO_ZONE = aDecoder.decodeObject(forKey: "urlConfig_PROMO_ZONE") as? String ?? ""
        
        //billType
        billType_address = aDecoder.decodeObject(forKey: "billType_address") as? String ?? ""
        billType_addressEdit = aDecoder.decodeObject(forKey: "billType_addressEdit") as? String ?? ""
        billType_addressShow = aDecoder.decodeObject(forKey: "billType_addressShow") as? String ?? ""
        billType_billType = aDecoder.decodeObject(forKey: "billType_billType") as? String ?? ""
        billType_billTypeEdit = aDecoder.decodeObject(forKey: "billType_billTypeEdit") as? String ?? ""
        
        billType_billTypeMsg = aDecoder.decodeObject(forKey: "billType_billTypeMsg") as? String ?? ""
        billType_billingAddr = aDecoder.decodeObject(forKey: "billType_billingAddr") as? String ?? ""
        billType_billingCity = aDecoder.decodeObject(forKey: "billType_billingCity") as? String ?? ""
        billType_billingState = aDecoder.decodeObject(forKey: "billType_billingState") as? String ?? ""
        billType_billingZipCode = aDecoder.decodeObject(forKey: "billType_billingZipCode") as? String ?? ""
        
        billType_ebillApply = aDecoder.decodeObject(forKey: "billType_ebillApply") as? String ?? ""
        billType_email = aDecoder.decodeObject(forKey: "billType_email") as? String ?? ""
        billType_maskEmail = aDecoder.decodeObject(forKey: "billType_maskEmail") as? String ?? ""
        billType_emailEdit = aDecoder.decodeObject(forKey: "billType_emailEdit") as? String ?? ""
        billType_emailShow = aDecoder.decodeObject(forKey: "billType_emailShow") as? String ?? ""
        
        billType_memberType = aDecoder.decodeObject(forKey: "billType_memberType") as? String ?? ""
        billType_showMessage = aDecoder.decodeObject(forKey: "billType_showMessage") as? String ?? ""
        
        unbillData_onNetVoice = aDecoder.decodeObject(forKey: "unbillData_onNetVoice") as? String ?? ""
        unbillData_offNetVoice = aDecoder.decodeObject(forKey: "unbillData_offNetVoice") as? String ?? ""
        unbillData_pstn = aDecoder.decodeObject(forKey: "unbillData_pstn") as? String ?? ""
        unbillData_dataUsage = aDecoder.decodeObject(forKey: "unbillData_dataUsage") as? String ?? ""
        unbillData_dataTotal = aDecoder.decodeObject(forKey: "unbillData_dataTotal") as? String ?? ""
        unbillData_dataUnit = aDecoder.decodeObject(forKey: "unbillData_dataUnit") as? String ?? ""
        unbillData_onNetVoiceDesc = aDecoder.decodeObject(forKey: "unbillData_onNetVoiceDesc") as? String ?? ""
        unbillData_offNetVoiceDesc = aDecoder.decodeObject(forKey: "unbillData_offNetVoiceDesc") as? String ?? ""
        unbillData_pstnDesc = aDecoder.decodeObject(forKey: "unbillData_pstnDesc") as? String ?? ""
        unbillData_onNetSMSDesc = aDecoder.decodeObject(forKey: "unbillData_onNetSMSDesc") as? String ?? ""
        unbillData_offNetSMSDesc = aDecoder.decodeObject(forKey: "unbillData_offNetSMSDesc") as? String ?? ""
        unbillData_showBuyButton = aDecoder.decodeObject(forKey: "unbillData_showBuyButton") as? Bool ?? false
        unbillData_buyUrl = aDecoder.decodeObject(forKey: "unbillData_buyUrl") as? String ?? ""
        unbillData_warningWording = aDecoder.decodeObject(forKey: "unbillData_warningWording") as? String ?? ""
        unbillData_isUnlimitedData = aDecoder.decodeObject(forKey: "unbillData_isUnlimitedData") as? Bool ?? false
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(token, forKey: "hasData")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(isBL, forKey: "isBL")
        aCoder.encode(blUrl, forKey: "blUrl")
        aCoder.encode(isUnlimitedData, forKey: "isUnlimitedData")
        aCoder.encode(isPostpaid, forKey: "isPostpaid")
        aCoder.encode(roleType, forKey: "roleType")
        
        //custProfile
        aCoder.encode(custProfile_uid, forKey: "custProfile_uid")
        aCoder.encode(custProfile_contractId, forKey: "custProfile_contractId")
        aCoder.encode(custProfile_msisdn, forKey: "custProfile_msisdn")
        aCoder.encode(custProfile_custId, forKey: "custProfile_custId")
        aCoder.encode(custProfile_custName, forKey: "custProfile_custName")
        
        aCoder.encode(custProfile_accountId, forKey: "custProfile_accountId")
        aCoder.encode(custProfile_imsi, forKey: "custProfile_imsi")
        aCoder.encode(custProfile_birthday, forKey: "custProfile_birthday")
        aCoder.encode(custProfile_operatorId, forKey: "custProfile_operatorId")
        aCoder.encode(custProfile_companyId, forKey: "custProfile_companyId")
        
        aCoder.encode(custProfile_billingCycle, forKey: "custProfile_billingCycle")
        aCoder.encode(custProfile_gender, forKey: "custProfile_gender")
        aCoder.encode(custProfile_custType, forKey: "custProfile_custType")
        aCoder.encode(custProfile_userState, forKey: "custProfile_userState")
        aCoder.encode(custProfile_vipDegree, forKey: "custProfile_vipDegree")
        
        aCoder.encode(custProfile_vipStartDate, forKey: "custProfile_vipStartDate")
        aCoder.encode(custProfile_vipEndDate, forKey: "custProfile_vipEndDate")
        aCoder.encode(custProfile_picUrl, forKey: "custProfile_picUrl")
        aCoder.encode(custProfile_email, forKey: "custProfile_email")
        aCoder.encode(custProfile_emailFlag, forKey: "custProfile_emailFlag")
        
        aCoder.encode(custProfile_pwschange, forKey: "custProfile_pwschange")
        aCoder.encode(custProfile_customerContribution, forKey: "custProfile_customerContribution")
        aCoder.encode(custProfile_projectCode, forKey: "custProfile_projectCode")
        aCoder.encode(custProfile_projectName, forKey: "custProfile_projectName")
        aCoder.encode(custProfile_retensionType, forKey: "custProfile_retensionType")
        
        aCoder.encode(custProfile_realFee, forKey: "custProfile_realFee")
        aCoder.encode(custProfile_activationDate, forKey: "custProfile_activationDate")
        aCoder.encode(custProfile_expiredDate, forKey: "custProfile_expiredDate")
        aCoder.encode(custProfile_isMainMsisdn, forKey: "custProfile_isMainMsisdn")
        aCoder.encode(custProfile_billAcctNumber, forKey: "custProfile_billAcctNumber")
        
        aCoder.encode(custProfile_memberType, forKey: "custProfile_memberType")
        aCoder.encode(custProfile_isVIP, forKey: "custProfile_isVIP")
        aCoder.encode(custProfile_displayNickname, forKey: "custProfile_displayNickname")
        aCoder.encode(custProfile_nickname, forKey: "custProfile_nickname")
        aCoder.encode(custProfile_gsmToLteFlag, forKey: "custProfile_gsmToLteFlag")
        
        aCoder.encode(custProfile_gsmToLteDate, forKey: "custProfile_gsmToLteDate")
        aCoder.encode(custProfile_extId, forKey: "custProfile_extId")
        aCoder.encode(custProfile_extType, forKey: "custProfile_extType")
        aCoder.encode(custProfile_extName, forKey: "custProfile_extName")
        aCoder.encode(custProfile_extEmail, forKey: "custProfile_extEmail")
        
        aCoder.encode(custProfile_planName, forKey: "custProfile_planName")
        aCoder.encode(remainingBillingDate, forKey: "remainingBillingDate")
        
        
        //custInfoBrief
        aCoder.encode(custInfoBrief_contractId, forKey: "custInfoBrief_contractId")
        aCoder.encode(custInfoBrief_msisdn, forKey: "custInfoBrief_msisdn")
        aCoder.encode(custInfoBrief_billingZipCode, forKey: "custInfoBrief_billingZipCode")
        aCoder.encode(custInfoBrief_billingState, forKey: "custInfoBrief_billingState")
        aCoder.encode(custInfoBrief_billingCity, forKey: "custInfoBrief_billingCity")
        
        aCoder.encode(custInfoBrief_billingAddr, forKey: "custInfoBrief_billingAddr")
        aCoder.encode(custInfoBrief_perZipCode, forKey: "custInfoBrief_perZipCode")
        aCoder.encode(custInfoBrief_perState, forKey: "custInfoBrief_perState")
        aCoder.encode(custInfoBrief_perCity, forKey: "custInfoBrief_perCity")
        aCoder.encode(custInfoBrief_perAddr, forKey: "custInfoBrief_perAddr")
        
        aCoder.encode(custInfoBrief_contractStatus, forKey: "custInfoBrief_contractStatus")
        aCoder.encode(custInfoBrief_maskBillingAddress, forKey: "custInfoBrief_maskBillingAddress")
        aCoder.encode(custInfoBrief_maskPerAddress, forKey: "custInfoBrief_maskPerAddress")
        
        //ivrQualify
        aCoder.encode(ivrQualify_earlyRenewDay, forKey: "ivrQualify_earlyRenewDay")
        aCoder.encode(ivrQualify_renewalCode, forKey: "ivrQualify_renewalCode")
        aCoder.encode(ivrQualify_renewalMessage, forKey: "ivrQualify_renewalMessage")
        aCoder.encode(ivrQualify_extendDate, forKey: "ivrQualify_extendDate")
        aCoder.encode(ivrQualify_extendFlag, forKey: "ivrQualify_extendFlag")
        
        aCoder.encode(ivrQualify_displayRenewalMessage, forKey: "ivrQualify_displayRenewalMessage")
        aCoder.encode(ivrQualify_displayDiscInfo, forKey: "ivrQualify_displayDiscInfo")
        aCoder.encode(ivrQualify_isRenewal, forKey: "ivrQualify_isRenewal")
        
        //Project
//        aCoder.encode(project_projectCode, forKey: "project_projectCode")
//        aCoder.encode(project_projectName, forKey: "project_projectName")
//        aCoder.encode(project_projectRate, forKey: "project_projectRate")
//        aCoder.encode(project_billDate, forKey: "project_billDate")
//        aCoder.encode(project_paymentEndDate, forKey: "project_paymentEndDate")
//        
//        aCoder.encode(project_startDate, forKey: "project_startDate")
//        aCoder.encode(project_endDate, forKey: "project_endDate")
//        aCoder.encode(project_contractStartDate, forKey: "project_contractStartDate")
//        aCoder.encode(project_contractExpireDate, forKey: "project_contractExpireDate")
//        aCoder.encode(project_unlimitedData, forKey: "project_unlimitedData")
        
        //EBill
        aCoder.encode(eBill_emailAddress, forKey: "eBill_emailAddress")
        aCoder.encode(eBill_memberType, forKey: "eBill_memberType")
        aCoder.encode(eBill_statusText, forKey: "eBill_statusText")
        aCoder.encode(eBill_showApplyEbill, forKey: "eBill_showApplyEbill")
        aCoder.encode(eBill_showMessage, forKey: "eBill_showMessage")
        
        //urlConfig
        aCoder.encode(urlConfig_BL, forKey: "urlConfig_BL")
        aCoder.encode(urlConfig_PROMO_ZONE, forKey: "urlConfig_PROMO_ZONE")
        
        //billType
        aCoder.encode(billType_address, forKey: "billType_address")
        aCoder.encode(billType_addressEdit, forKey: "billType_addressEdit")
        aCoder.encode(billType_addressShow, forKey: "billType_addressShow")
        aCoder.encode(billType_billType, forKey: "billType_billType")
        aCoder.encode(billType_billTypeEdit, forKey: "billType_billTypeEdit")
        
        aCoder.encode(billType_billTypeMsg, forKey: "billType_billTypeMsg")
        aCoder.encode(billType_billingAddr, forKey: "billType_billingAddr")
        aCoder.encode(billType_billingCity, forKey: "billType_billingCity")
        aCoder.encode(billType_billingState, forKey: "billType_billingState")
        aCoder.encode(billType_billingZipCode, forKey: "billType_billingZipCode")
        
        aCoder.encode(billType_ebillApply, forKey: "billType_ebillApply")
        aCoder.encode(billType_email, forKey: "billType_email")
        aCoder.encode(billType_maskEmail, forKey: "billType_maskEmail")
        aCoder.encode(billType_emailEdit, forKey: "billType_emailEdit")
        aCoder.encode(billType_emailShow, forKey: "billType_emailShow")
        
        aCoder.encode(billType_memberType, forKey: "billType_memberType")
        aCoder.encode(billType_showMessage, forKey: "billType_showMessage")
        
        //unbillData
        aCoder.encode(unbillData_onNetVoice, forKey: "unbillData_onNetVoice")
        aCoder.encode(unbillData_offNetVoice, forKey: "unbillData_offNetVoice")
        aCoder.encode(unbillData_pstn, forKey: "unbillData_pstn")
        aCoder.encode(unbillData_dataUsage, forKey: "unbillData_dataUsage")
        aCoder.encode(unbillData_dataTotal, forKey: "unbillData_dataTotal")
        aCoder.encode(unbillData_dataUnit, forKey: "unbillData_dataUnit")
        aCoder.encode(unbillData_onNetVoiceDesc, forKey: "unbillData_onNetVoiceDesc")
        aCoder.encode(unbillData_offNetVoiceDesc, forKey: "unbillData_offNetVoiceDesc")
        aCoder.encode(unbillData_pstnDesc, forKey: "unbillData_pstnDesc")
        aCoder.encode(unbillData_onNetSMSDesc, forKey: "unbillData_onNetSMSDesc")
        aCoder.encode(unbillData_offNetSMSDesc, forKey: "unbillData_offNetSMSDesc")
        aCoder.encode(unbillData_showBuyButton, forKey: "unbillData_showBuyButton")
        aCoder.encode(unbillData_buyUrl, forKey: "unbillData_buyUrl")
        aCoder.encode(unbillData_warningWording, forKey: "unbillData_warningWording")
        aCoder.encode(unbillData_isUnlimitedData, forKey: "unbillData_isUnlimitedData")
        
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>, onlyUpdateCustProfile: Bool) {
        if (dic["code"] as! String) == "00000" {
            
            guard (dic["data"] as? Dictionary<String, Any>) != nil else {
                return
            }
            
            hasData = "Y"
            
            let apiData = dic["data"] as! Dictionary<String, Any>
            
            var custProfile: Dictionary<String, Any> = [:]
            if !onlyUpdateCustProfile {
                
                guard (apiData["custProfile"] as? Dictionary<String, Any>) != nil else {
                    return
                }
                
                custProfile = apiData["custProfile"] as! Dictionary<String, Any>
            }
            else {
                custProfile = apiData
            }
            
            if !onlyUpdateCustProfile {
                token = apiData.stringValue(ofKey: "token")
                isBL = apiData.stringValue(ofKey: "isBL")
                blUrl = apiData.stringValue(ofKey: "blUrl")
                let tempIsUnlimitedData = apiData.stringValue(ofKey: "isUnlimitedData")
                if tempIsUnlimitedData != "Y" || tempIsUnlimitedData != "N" {
                    isUnlimitedData = "E"
                }
                else {
                    isUnlimitedData = apiData.stringValue(ofKey: "isUnlimitedData")
                }
                isPostpaid = apiData.stringValue(ofKey: "isPostpaid")
                roleType = apiData.stringValue(ofKey: "roleType")
            }
            
            //custProfile
            custProfile_uid = custProfile.stringValue(ofKey: "uid")
            custProfile_contractId = custProfile.stringValue(ofKey: "contractId")
            custProfile_msisdn = custProfile.stringValue(ofKey: "msisdn")
            custProfile_custId = custProfile.stringValue(ofKey: "custId")
            custProfile_custName = custProfile.stringValue(ofKey: "custName")
            
            custProfile_accountId = custProfile.stringValue(ofKey: "acctId")
            custProfile_imsi = custProfile.stringValue(ofKey: "imsi")
            custProfile_birthday = custProfile.stringValue(ofKey: "birthday")
            custProfile_operatorId = custProfile.stringValue(ofKey: "operatorId")
            custProfile_companyId = custProfile.stringValue(ofKey: "companyId")
            
            custProfile_billingCycle = custProfile.stringValue(ofKey: "billingCycle")
            custProfile_gender = custProfile.stringValue(ofKey: "gender")
            custProfile_custType = custProfile.stringValue(ofKey: "custType")
            custProfile_userState = custProfile.stringValue(ofKey: "userState")
            custProfile_vipDegree = custProfile.stringValue(ofKey: "vipDegree")
            
            custProfile_vipStartDate = custProfile.stringValue(ofKey: "vipStartDate")
            custProfile_vipEndDate = custProfile.stringValue(ofKey: "vipEndDate")
            custProfile_picUrl = custProfile.stringValue(ofKey: "picUrl")
            let custProfileEmail = custProfile.stringValue(ofKey: "email")
            if custProfileEmail.replacingOccurrences(of: " ", with: "") != "" {
                custProfile_email = custProfile.stringValue(ofKey: "email")
            }
            custProfile_emailFlag = custProfile.stringValue(ofKey: "emailFlag")
            
            custProfile_pwschange = custProfile.stringValue(ofKey: "pwschange")
            custProfile_customerContribution = custProfile.stringValue(ofKey: "customerContribution")
            custProfile_projectCode = custProfile.stringValue(ofKey: "projectCode")
            custProfile_projectName = custProfile.stringValue(ofKey: "projectName")
            custProfile_retensionType = custProfile.stringValue(ofKey: "retensionType")
            custProfile_realFee = custProfile.stringValue(ofKey: "realFee")
            
            custProfile_activationDate = custProfile.stringValue(ofKey: "activationDate")
            custProfile_expiredDate = custProfile.stringValue(ofKey: "expiredDate")
            custProfile_isMainMsisdn = custProfile.stringValue(ofKey: "isMainMsisdn")
            custProfile_billAcctNumber = custProfile.stringValue(ofKey: "billAcctNumber")
            custProfile_memberType = custProfile.stringValue(ofKey: "memberType")
            
            custProfile_isVIP = "N"//custProfile.stringValue(ofKey: "isVIP")
//            if custProfile_isVIP == "Y" {
//                custProfile_isVIP = "N"
//            }
//            else {
//                custProfile_isVIP = "Y"
//            }
            custProfile_displayNickname = custProfile.stringValue(ofKey: "displayNickname")
            custProfile_nickname = custProfile.stringValue(ofKey: "nickname")
            custProfile_gsmToLteFlag = custProfile.stringValue(ofKey: "gsmToLteFlag")
            custProfile_gsmToLteDate = custProfile.stringValue(ofKey: "gsmToLteDate")
            
            custProfile_extId = custProfile.stringValue(ofKey: "extId")
            custProfile_extType = custProfile.stringValue(ofKey: "extType")
            custProfile_extName = custProfile.stringValue(ofKey: "extName")
            custProfile_extEmail = custProfile.stringValue(ofKey: "extEmail")
            custProfile_extList = custProfile["extList"] as? [Dictionary<String, Any>] ?? []
            custProfile_planName = custProfile.stringValue(ofKey: "planName")
            remainingBillingDate = custProfile.stringValue(ofKey: "remainingBillingDate")
            
            if let fbLoginData = UserDefaults.standard.object(forKey: FACEBOOK_LOGIN_DATA) as? Dictionary<String, String> {
                
                for extData in custProfile_extList {
                    if extData.stringValue(ofKey: "email") == fbLoginData.stringValue(ofKey: "email") {
                        custProfile_extId = extData.stringValue(ofKey: "extId")
                        custProfile_extType = extData.stringValue(ofKey: "extType")
                        custProfile_extName = extData.stringValue(ofKey: "custName")
                        custProfile_extEmail = extData.stringValue(ofKey: "email")
                        break
                    }
                }
            }
            
            
            if !onlyUpdateCustProfile {
                let custInfoBrief = apiData["custInfoBrief"] as? Dictionary<String, Any>
                let ivrQualify = apiData["ivrQualify"] as? Dictionary<String, Any>
//                let project = apiData["project"] as! Dictionary<String, Any>
                let eBill = apiData["ebill"] as? Dictionary<String, Any>
                let urlConfig = apiData["urlConfig"] as? Dictionary<String, Any>
                let billType = apiData["billType"] as? Dictionary<String, Any>
                let unbillData = apiData["unbillData"] as? Dictionary<String, Any>
                
                //custInfoBrief
                if let custInfoBrief = custInfoBrief {
                    custInfoBrief_contractId = custInfoBrief.stringValue(ofKey: "contractId")
                    custInfoBrief_msisdn = custInfoBrief.stringValue(ofKey: "msisdn")
                    custInfoBrief_billingZipCode = custInfoBrief.stringValue(ofKey: "billingZipCode")
                    custInfoBrief_billingState = custInfoBrief.stringValue(ofKey: "billingState")
                    custInfoBrief_billingCity = custInfoBrief.stringValue(ofKey: "billingCity")
                    
                    custInfoBrief_billingAddr = custInfoBrief.stringValue(ofKey: "billingAddr")
                    custInfoBrief_perZipCode = custInfoBrief.stringValue(ofKey: "perZipCode")
                    custInfoBrief_perState = custInfoBrief.stringValue(ofKey: "perState")
                    custInfoBrief_perCity = custInfoBrief.stringValue(ofKey: "perCity")
                    custInfoBrief_perAddr = custInfoBrief.stringValue(ofKey: "perAddr")
                    
                    custInfoBrief_contractStatus = custInfoBrief.stringValue(ofKey: "contractStatus")
                    custInfoBrief_maskBillingAddress = custInfoBrief.stringValue(ofKey: "maskBillingAddress")
                    custInfoBrief_maskPerAddress = custInfoBrief.stringValue(ofKey: "maskPerAddress")
                    
                }
                
                //ivrQualify
                if let ivrQualify = ivrQualify {
                    ivrQualify_earlyRenewDay = ivrQualify.stringValue(ofKey: "earlyRenewDay")
                    ivrQualify_renewalCode = ivrQualify.stringValue(ofKey: "renewalCode")
                    ivrQualify_renewalMessage = ivrQualify.stringValue(ofKey: "renewalMessage")
                    ivrQualify_extendDate = ivrQualify.stringValue(ofKey: "extendDate")
                    ivrQualify_extendFlag = ivrQualify.stringValue(ofKey: "extendFlag")
                    
                    ivrQualify_displayRenewalMessage = ivrQualify.stringValue(ofKey: "displayRenewalMessage")
                    ivrQualify_displayDiscInfo = ivrQualify.stringValue(ofKey: "displayDiscInfo")
                    ivrQualify_isRenewal = ivrQualify.stringValue(ofKey: "isRenewal")
                }
                
                //unBill
                if let unbillData = unbillData {
                    unbillData_onNetVoice = unbillData.stringValue(ofKey: "onNetVoice")
                    unbillData_offNetVoice = unbillData.stringValue(ofKey: "offNetVoice")
                    unbillData_pstn = unbillData.stringValue(ofKey: "pstn")
                    unbillData_dataUsage = unbillData.stringValue(ofKey: "dataUsage")
                    unbillData_dataTotal = unbillData.stringValue(ofKey: "dataTotal")
                    unbillData_dataUnit = unbillData.stringValue(ofKey: "dataUnit")
                    unbillData_onNetVoiceDesc = unbillData.stringValue(ofKey: "onNetVoiceDesc")
                    unbillData_offNetVoiceDesc = unbillData.stringValue(ofKey: "offNetVoiceDesc")
                    unbillData_pstnDesc = unbillData.stringValue(ofKey: "pstnDesc")
                    unbillData_onNetSMSDesc = unbillData.stringValue(ofKey: "onNetSMSDesc")
                    unbillData_offNetSMSDesc = unbillData.stringValue(ofKey: "offNetSMSDesc")
                    unbillData_showBuyButton = unbillData.stringValue(ofKey: "showBuyButton") == "Y" ? true : false
                    unbillData_buyUrl = unbillData.stringValue(ofKey: "buyUrl")
                    unbillData_warningWording = unbillData.stringValue(ofKey: "warningWording")
                    unbillData_isUnlimitedData = unbillData.stringValue(ofKey: "isUnlimitedData") == "Y" ? true : false
                }
                
                //Project
//                project_projectCode = project.stringValue(ofKey: "projectCode")
//                project_projectName = project.stringValue(ofKey: "projectName")
//                project_projectRate = project.stringValue(ofKey: "projectRate")
//                project_billDate = project.stringValue(ofKey: "billDate")
//                project_paymentEndDate = project.stringValue(ofKey: "paymentEndDate")
//                
//                project_startDate = project.stringValue(ofKey: "startDate")
//                project_endDate = project.stringValue(ofKey: "endDate")
//                project_contractStartDate = project.stringValue(ofKey: "contractStartDate")
//                project_contractExpireDate = project.stringValue(ofKey: "contractExpireDate")
//                project_unlimitedData = project.stringValue(ofKey: "unlimitedData")
                
                //EBill
                if let eBill = eBill {
                    eBill_emailAddress = eBill.stringValue(ofKey: "emailAddress")
                    eBill_memberType = eBill.stringValue(ofKey: "memberType")
                    eBill_statusText = eBill.stringValue(ofKey: "statusText")
                    eBill_showApplyEbill = eBill.stringValue(ofKey: "showApplyEbill")
                    eBill_showMessage = eBill.stringValue(ofKey: "showMessage")
                }
                
                //urlConfig
                if let urlConfig = urlConfig {
                    urlConfig_BL = urlConfig.stringValue(ofKey: "BL")
                    urlConfig_PROMO_ZONE = urlConfig.stringValue(ofKey: "PROMO_ZONE")
                }
                
                //billType
                if let billType = billType {
                    billType_address = billType.stringValue(ofKey: "address")
                    billType_addressEdit = billType.stringValue(ofKey: "addressEdit")
                    billType_addressShow = billType.stringValue(ofKey: "addressShow")
                    billType_billType = billType.stringValue(ofKey: "billType")
                    billType_billTypeEdit = billType.stringValue(ofKey: "billTypeEdit")
                    
                    billType_billTypeMsg = billType.stringValue(ofKey: "billTypeMsg")
                    billType_billingAddr = billType.stringValue(ofKey: "billingAddr")
                    billType_billingCity = billType.stringValue(ofKey: "billingCity")
                    billType_billingState = billType.stringValue(ofKey: "billingState")
                    billType_billingZipCode = billType.stringValue(ofKey: "billingZipCode")
                    
                    billType_ebillApply = billType.stringValue(ofKey: "ebillApply")
                    billType_email = billType.stringValue(ofKey: "email")
                    billType_maskEmail = billType.stringValue(ofKey: "maskEmail")
                    billType_emailEdit = billType.stringValue(ofKey: "emailEdit")
                    billType_emailShow = billType.stringValue(ofKey: "emailShow")
                    
                    billType_memberType = billType.stringValue(ofKey: "memberType")
                    billType_showMessage = billType.stringValue(ofKey: "showMessage")
                }
            }
            
            saveData()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        }
        else {
            hasData = "N"
        }
    }
    
    func setBillTypeDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            guard (dic["data"] as? Dictionary<String, Any>) != nil else {
                return
            }
            
            let billType = dic["data"] as? Dictionary<String, Any>
            if let billType = billType {
                billType_address = billType.stringValue(ofKey: "address")
                billType_addressEdit = billType.stringValue(ofKey: "addressEdit")
                billType_addressShow = billType.stringValue(ofKey: "addressShow")
                billType_billType = billType.stringValue(ofKey: "billType")
                billType_billTypeEdit = billType.stringValue(ofKey: "billTypeEdit")
                
                billType_billTypeMsg = billType.stringValue(ofKey: "billTypeMsg")
                billType_billingAddr = billType.stringValue(ofKey: "billingAddr")
                billType_billingCity = billType.stringValue(ofKey: "billingCity")
                billType_billingState = billType.stringValue(ofKey: "billingState")
                billType_billingZipCode = billType.stringValue(ofKey: "billingZipCode")
                
                billType_ebillApply = billType.stringValue(ofKey: "ebillApply")
                billType_email = billType.stringValue(ofKey: "email")
                billType_maskEmail = billType.stringValue(ofKey: "maskEmail")
                billType_emailEdit = billType.stringValue(ofKey: "emailEdit")
                billType_emailShow = billType.stringValue(ofKey: "emailShow")
                
                billType_memberType = billType.stringValue(ofKey: "memberType")
                billType_showMessage = billType.stringValue(ofKey: "showMessage")
                
                saveData()
            }
        }
    }
    
    func saveData() {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: self), forKey: "TSTAPP2_LoginData")
        UserDefaults.standard.synchronize()
        print("TSTAPP2_LoginData saved")
    }
    
    func deleteData() {
        initAllValue()
        didInitFromUserDefaults = false
        UserDefaults.standard.removeObject(forKey: "TSTAPP2_LoginData")
        UserDefaults.standard.synchronize()
        print("TSTAPP2_LoginData deleted")
    }
    
    func setRoleType(value: String) {
        roleType = value
        saveData()
    }

}
