//
//  MailBoxHistoryViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/4.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let historyCellHeight: CGFloat = 75
let historyCellVerticalMargin: CGFloat = 24
let historyCellAskAgainHeight: CGFloat = 44

class MailBoxHistoryCell: UITableViewCell {
    
    @IBOutlet weak var createDateLabel: UILabel!
    
    @IBOutlet weak var issueTypeLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var askAgainButton: UIButton!
    
    @IBOutlet weak var askAgainHeight: NSLayoutConstraint!
    
    @IBOutlet weak var arrow: UIImageView!
    
    var isExpand = false
}


class MailBoxHistoryViewController: TSUIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var historyTableView: UITableView!
    
    @IBOutlet weak var historyTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var noDataLabel: UILabel!
    
    var issueQueryList: [ContactUsIssueQuery] = []
    
    var issueDisplayList: [Issue] = []
    
    var issueId: String = ""
    
    var issue: IssueList = IssueList.init(withDic: [:])
    
    // MARK: - ENUM
    private enum EnumPickerFieldsTag: Int {
        case Range
        case Status
    }
    
    private enum EnumMailBoxHistoryRange: String {
        case LastWeek = "最近一個禮拜"
        case LastMonth = "最近一個月"
        case LastThreeMonths = "最近三個月"
        case All = "全部"
        
        static let maps = [LastWeek, LastMonth, LastThreeMonths, All]
    }
    
    private enum EnumMailBoxHistoryStatus: String {
        case All = "全部"
        case Unread = "未讀之訊息"
        
        static let maps = [All, Unread]
    }
    
    // MARK: - search fields
    @IBOutlet weak var rangeField: UITextField!
    
    @IBOutlet weak var statusField: UITextField!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if !LoginData.isLogin() {
            let alertController = UIAlertController(title: "尚未登入", message: "請先登入", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (_:UIAlertAction) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        self.historyTableView.rowHeight = UITableViewAutomaticDimension
        self.historyTableView.estimatedRowHeight = historyCellHeight
        self.setSearchFieldsKeyboard()
        self.callGetContactUsIssueQueryAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP050903", action: "")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050903", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    func setSearchFieldsKeyboard() {
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.didTapDone))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: false)
        
        
        let rangeFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        rangeFieldRightIconImage.contentMode = .scaleAspectFit
        rangeFieldRightIconImage.frame.size = CGSize(width: rangeFieldRightIconImage.frame.size.width + 16, height: rangeFieldRightIconImage.frame.size.height)
        var pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = EnumPickerFieldsTag.Range.rawValue
        pickerView.selectRow(EnumMailBoxHistoryRange.maps.index(of: EnumMailBoxHistoryRange.All)!, inComponent: 0, animated: false)
        pickerView.delegate?.pickerView!(pickerView, didSelectRow: EnumMailBoxHistoryRange.maps.index(of: EnumMailBoxHistoryRange.All)!, inComponent: 0)
        self.rangeField.rightViewMode = .always
        self.rangeField.rightView = rangeFieldRightIconImage
        self.rangeField.inputView = pickerView
        self.rangeField.inputAccessoryView = toolBar
        
        let statusFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        statusFieldRightIconImage.contentMode = .scaleAspectFit
        statusFieldRightIconImage.frame.size = CGSize(width: statusFieldRightIconImage.frame.size.width + 16, height: statusFieldRightIconImage.frame.size.height)
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = EnumPickerFieldsTag.Status.rawValue
        pickerView.selectRow(EnumMailBoxHistoryStatus.maps.index(of: EnumMailBoxHistoryStatus.Unread)!, inComponent: 0, animated: false)
        pickerView.delegate?.pickerView!(pickerView, didSelectRow: EnumMailBoxHistoryStatus.maps.index(of: EnumMailBoxHistoryStatus.Unread)!, inComponent: 0)
        self.statusField.rightViewMode = .always
        self.statusField.rightView = statusFieldRightIconImage
        self.statusField.inputView = pickerView
        self.statusField.inputAccessoryView = toolBar
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let pickerView = EnumPickerFieldsTag(rawValue: pickerView.tag)!
        switch pickerView {
        case .Range:
            return EnumMailBoxHistoryRange.maps[row].rawValue
        case .Status:
            return EnumMailBoxHistoryStatus.maps[row].rawValue
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let pickerView = EnumPickerFieldsTag(rawValue: pickerView.tag)!
        switch pickerView {
        case .Range:
            self.rangeField.text = EnumMailBoxHistoryRange.maps[row].rawValue
            break
        case .Status:
            self.statusField.text = EnumMailBoxHistoryStatus.maps[row].rawValue
            break
        }
        self.reloadTableViewData()
    }
    
    private func reloadTableViewData() {
        
        if self.rangeField.text != "" && self.statusField.text != "" {
            var isRead: Bool
            let selectedStatus = EnumMailBoxHistoryStatus(rawValue: self.statusField.text!)!
            switch selectedStatus {
            case .All:
                isRead = true
                break
            case .Unread:
                isRead = false
                break
            }
            
            var startDate: Date
            let selectedRange = EnumMailBoxHistoryRange(rawValue: self.rangeField.text!)!
            switch selectedRange {
            case .All:
                startDate = Date(timeIntervalSince1970: 0)
                break
            case .LastWeek:
                startDate = Date(timeIntervalSinceNow: -7 * 24 * 60 * 60)
                break
            case .LastMonth:
                startDate = Date(timeIntervalSinceNow: -30 * 24 * 60 * 60)
                break
            case .LastThreeMonths:
                startDate = Date(timeIntervalSinceNow: -90 * 24 * 60 * 60)
                break
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
            self.issueDisplayList = []
            self.issue.list.forEach({ (issueData: Issue) in
                let issueDate: Date = dateFormatter.date(from: issueData.date)!
                switch issueDate.compare(startDate) {
                case .orderedSame:
                    fallthrough
                case .orderedDescending:
                    if isRead {
                        self.issueDisplayList.append(issueData)
                    } else if !issueData.isRead {
                        self.issueDisplayList.append(issueData)
                    }
                    break
                default:
                    break
                }
            })
            
            self.historyTableHeightConstraint.constant = CGFloat(self.issueDisplayList.count) * historyCellHeight
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
            
            self.historyTableView.reloadData()
            
            if self.issueDisplayList.count == 0 {
                self.historyTableView.isHidden = true
                self.noDataLabel.isHidden = false
            } else {
                self.noDataLabel.isHidden = true
                self.historyTableView.isHidden = false
            }
        }
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let pickerView = EnumPickerFieldsTag(rawValue: pickerView.tag)!
        switch pickerView {
        case .Range:
            return EnumMailBoxHistoryRange.maps.count
        case .Status:
            return EnumMailBoxHistoryStatus.maps.count
        }
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.issueDisplayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as! MailBoxHistoryCell
        
        cell.askAgainButton.addTarget(self, action: #selector(self.didTapAskAgainButton(_:)), for: UIControlEvents.touchUpInside)
        UISkinChange.sharedInstance.changeUIColor(theObject: cell.askAgainButton, mode: ButtonColorMode.backgroundImage.rawValue)
        
//        cell.createDateLabel.text = self.issueDisplayList[indexPath.row].createDate
//        cell.issueTypeLabel.text = self.issueDisplayList[indexPath.row].issueType
        
        
        cell.createDateLabel.text = self.issue.list[indexPath.row].date
        cell.issueTypeLabel.text = self.issue.list[indexPath.row].issueType
        
        
//        let reply = (self.issueDisplayList[indexPath.row].replyContent == "") ? "" : "<br>\(self.issueDisplayList[indexPath.row].replyDate)<br>答: \(self.issueDisplayList[indexPath.row].replyContent)"
//        
//        let askAndReplyString = "\(self.issueDisplayList[indexPath.row].createDate)<br>問: \(self.issueDisplayList[indexPath.row].issueNote)\(reply)<style>body{font-size:17px;color:#686868}</style>"
//
//        let attributedString = try! NSAttributedString(data: askAndReplyString.data(using: String.Encoding.unicode)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
//        
//        cell.contentLabel.attributedText = attributedString
//        cell.contentLabel.sizeToFit()
        
//        let reply = (self.issueDisplayList[indexPath.row].replyContent == "") ? "" : "<br>\(self.issueDisplayList[indexPath.row].replyDate)<br>答: \(self.issueDisplayList[indexPath.row].replyContent)"
//        
//        if reply == "" {
//            cell.askAgainHeight.constant = 0
//        }
//        
//        let askAndReplyString = "\(self.issueDisplayList[indexPath.row].createDate)<br>問: \(self.issueDisplayList[indexPath.row].issueNote)\(reply)<style>body{font-size:17px;color:#686868}</style>"
//
//        let attributedString = try! NSAttributedString(data: askAndReplyString.data(using: String.Encoding.unicode)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
//        
//        cell.contentLabel.attributedText = attributedString
//        cell.contentLabel.sizeToFit()
        
        cell.isExpand = false
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var cellHeight: CGFloat = historyCellHeight
        
        let historyCell = tableView.cellForRow(at: indexPath) as? MailBoxHistoryCell
        
        if historyCell != nil {
            if (historyCell?.isExpand)! {
                
                cellHeight += (historyCell?.contentLabel.frame.size.height)! + historyCellVerticalMargin + (historyCell?.askAgainHeight.constant)!
            }
        }
        
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let historyCell = tableView.cellForRow(at: indexPath) as! MailBoxHistoryCell
        historyCell.isExpand = !historyCell.isExpand

//        historyCell.createDateLabel.text = self.issueDisplayList[indexPath.row].createDate
//        historyCell.issueTypeLabel.text = self.issueDisplayList[indexPath.row].issueType
//        let reply = (self.issueDisplayList[indexPath.row].replyContent == "") ? "" : "<br>\(self.issueDisplayList[indexPath.row].replyDate)<br>答: \(self.issueDisplayList[indexPath.row].replyContent)"
//        
//        let askAndReplyString = "\(self.issueDisplayList[indexPath.row].createDate)<br>問: \(self.issueDisplayList[indexPath.row].issueNote)\(reply)<style>body{font-size:17px;color:#686868}</style>"
//        
//        let attributedString = try! NSAttributedString(data: askAndReplyString.data(using: String.Encoding.unicode)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
//        
//        historyCell.contentLabel.attributedText = attributedString
//        historyCell.contentLabel.sizeToFit()
        
//        let reply = (self.issueDisplayList[indexPath.row].replyContent == "") ? "" : "<br><br>\(self.issueDisplayList[indexPath.row].replyDate)<br>答: \(self.issueDisplayList[indexPath.row].replyContent)"
        
//        if reply == "" {
//            historyCell.askAgainHeight.constant = 0
//        }
        
        var reply = ""
        if self.issue.list[indexPath.row].replyContent == "" {
            reply = "<br><br>\(self.issue.list[indexPath.row].replyTime)"
        }
        else {
            reply = "<br><br>\(self.issue.list[indexPath.row].replyTime)<br>答：\(self.issue.list[indexPath.row].replyContent)"
        }
        
        for issueSub in self.issue.list[indexPath.row].issueSubList {
            let date = issueSub.date
            let note = issueSub.issueNote
            let replyContent = issueSub.replyContent
            let replyTime = issueSub.replyTime
            
            if replyContent == "" {
                reply = reply + "<br><br>\(date)<br>問：\(note)"
            }
            else {
                reply = reply + "<br><br>\(date)<br>問：\(note)<br><br>\(replyTime)<br>答：\(replyContent)"
            }
            
//            reply = reply + "<br><br>\(date)<br>問：\(note)<br><br>\(replyTime)<br>答：\(replyContent)"
        }
        
        
        if self.issue.list[indexPath.row].displayAskButton == false {
            historyCell.askAgainHeight.constant = 0
        }
        else {
            historyCell.askAgainHeight.constant = historyCellAskAgainHeight
        }
        
        let date = self.issue.list[indexPath.row].date
        let note = self.issue.list[indexPath.row].issueNote
        let askAndReplyString = "\(date)<br>問: \(note)\(reply)<style>body{font-size:17px;color:#686868}</style>"
        
        let attributedString = try! NSAttributedString(data: askAndReplyString.data(using: String.Encoding.unicode)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        
        historyCell.contentLabel.attributedText = attributedString
        historyCell.contentLabel.sizeToFit()
        
        self.issueId = self.issue.list[indexPath.row].issueId
        historyCell.arrow.image = historyCell.isExpand ? UIImage(named: "icon_arrow_up") : UIImage(named: "icon_arrow_down")
        
        var tableHeight: CGFloat = 0
        for cell in tableView.visibleCells {
            tableHeight += historyCellHeight
            if (cell as! MailBoxHistoryCell).isExpand {
                tableHeight += (cell as! MailBoxHistoryCell).contentLabel.frame.size.height + historyCellVerticalMargin + (cell as! MailBoxHistoryCell).askAgainHeight.constant
            }
        }
        
        self.historyTableHeightConstraint.constant = tableHeight
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
            tableView.beginUpdates()
            tableView.endUpdates()
            }, completion: { (isComplete: Bool) in
                self.callContactUsReplyRead(self.issueId)
        })
    }
    
    // MARK: - IBAction
    @objc func didTapDone() {
        self.view.endEditing(true)
    }
    
    @IBAction func didTapEditRange(_ sender: Any) {
        self.rangeField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditStatue(_ sender: Any) {
        self.statusField.becomeFirstResponder()
    }
    
    @objc func didTapAskAgainButton(_ sender: AnyObject) {
        let button = sender as! UIButton
        
        let alert = UIAlertController(title: "再發問", message: nil, preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            
            textField.placeholder = "請輸入問題"
        }
 
        alert.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) -> Void in

            let textField = alert.textFields![0] as UITextField
            
            if let text = textField.text {
                
                self.callContactUsIssueAddAgain(withIssueID: self.issueId, issueNote: text)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        
        alert.show(viewController: self)
        print("\(button.tag)")
    }
    
    // MARK: - API
    func callGetContactUsIssueQueryAPI() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.queryIssue(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, startDate: dateFormatter.string(from: Date(timeIntervalSince1970: 0)), showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                self.issue = IssueList.init(withDic: dic)
                self.reloadTableViewData()
            }
            else {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            
        }) { (error: Error) in
            
        }
        
    }
    
    func callContactUsIssueAddAgain(withIssueID issueId: String, issueNote :String) {
        
        TSTAPP2_API.sharedInstance.addIssueAgain(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, issueId: issueId, issueNote: issueNote, showProgress: true, completion:{ (dic :Dictionary<String, Any>, response) in
            
            if let apiResponse = dic["data"] as? [Dictionary<String, Any>] {
                
                if apiResponse[0].stringValue(ofKey: "status") == "0000" {
                    
                    TSTAPP2_Utilities.showAlert(withMessage: apiResponse[0].stringValue(ofKey: "message"), viewController: self)
                }
            }
            
        }) { (Error) in
            
            print("error: \(Error)")
        }

//        TSTAPP2_API.sharedInstance.contactUsIssueAddAgain(withIssueId: issueId, issueNote: issueNote, showProgress: true, completion: { (dic :Dictionary<String, Any>, response) in
//            
//            if let apiResponse = dic["data"] as? [Dictionary<String, Any>] {
//                
//                if apiResponse[0].stringValue(ofKey: "status") == "0000" {
//                    
//                    TSTAPP2_Utilities.showAlert(withMessage: apiResponse[0].stringValue(ofKey: "message"), viewController: self)
//                }
//            }
//            
//        }) { (Error) in
//            
//            print("error: \(Error)")
//        }
    }
    
    func callContactUsReplyRead(_ issueId: String) {
        
        TSTAPP2_API.sharedInstance.replyIssueRead(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, issueId: issueId, showProgress: false, completion: { (dic :Dictionary<String, Any>, reponse :URLResponse) in
            
            
        }) { (Error) in
            
        }
    }
}
