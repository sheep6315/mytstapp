//
//  MailBoxOptions.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2017/5/17.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class MailBoxOptions: NSObject {
    
    static let sharedInstance = MailBoxOptions()
    
    // 門號相關類別清單
    private(set) var genCategoryList: [CmsCategory] = []
    
    // 通訊品質類別清單
    private(set) var netCategoryList: [CmsCategory] = []
    
    // 訊號強度清單
    private(set) var receptionList: [Reception] = []
    
    // 訊號類型清單
    private(set) var lteTypeList: [LteType] = []
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "code") == "00000" {
            let apiData = dic["data"] as! Dictionary<String, Any>
            
            
            if let genList = apiData["genCategoryList"] as? Array<Dictionary<String, Any>> {
                genList.forEach({ (genCategory:[String : Any]) in
                    self.genCategoryList.append(CmsCategory(withDic: genCategory))
                })
            }
            
            if let netList = apiData["netCategoryList"] as? Array<Dictionary<String, Any>> {
                netList.forEach({ (netCategory:[String : Any]) in
                    self.netCategoryList.append(CmsCategory(withDic: netCategory))
                })
            }
            
            if let receptionList = apiData["receptionList"] as? Array<Dictionary<String, String>> {
                receptionList.forEach({ (reception:[String : String]) in
                    self.receptionList.append(Reception(withDic: reception))
                })
            }
            
            if let lteTypeList = apiData["lteTypeList"] as? Array<Dictionary<String, String>> {
                lteTypeList.forEach({ (lteType:[String : String]) in
                    self.lteTypeList.append(LteType(withDic: lteType))
                })
            }
        }
    }
}

// MARK: - 客服信箱分類
class CmsCategory: NSObject {
    
    // Call Type
    private(set) var callType: String = ""
    
    // 類別名稱
    private(set) var crmName: String = ""
    
    // 類別值
    private(set) var crmValue: String = ""
    
    // 子類別清單
    private(set) var child: [CmsCategory] = []
    
    init(withDic dic: Dictionary<String, Any>) {
        self.callType = dic["callType"] as? String ?? ""
        self.crmName = dic["crmName"] as? String ?? ""
        self.crmValue = dic["crmValue"] as? String ?? ""
        if let childList = dic["childList"] as? Array<Dictionary<String, Any>> {
            var childs: [CmsCategory] = []
            childList.forEach({ (data: [String : Any]) in
                childs.append(CmsCategory(withDic: data))
            })
            self.child = childs
        }
    }
}

// MARK: - 訊號強度
class Reception: NSObject {
    
    private(set) var itemValue: String = ""
    
    private(set) var itemName: String = ""
    
    init(withDic dic: Dictionary<String, String>) {
        self.itemValue = dic["itemValue"] ?? ""
        self.itemName = dic["itemName"] ?? ""
    }
}

// MARK: - 訊號類型
class LteType: NSObject {
    
    private(set) var itemValue: String = ""
    
    private(set) var itemName: String = ""
    
    init(withDic dic: Dictionary<String, String>) {
        self.itemValue = dic["itemValue"] ?? ""
        self.itemName = dic["itemName"] ?? ""
    }
}
