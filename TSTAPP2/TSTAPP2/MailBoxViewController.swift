//
//  MailBoxViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/1.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class MailBoxViewController: TSUIViewController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var historyButton: UIBarButtonItem!
    
    @IBOutlet weak var segmentedFormType: UISegmentedControl!
    
    @IBOutlet weak var segmentedPlaceType: UISegmentedControl!
    
    @IBOutlet weak var agreePolicy: UIButton!
    
    @IBOutlet weak var captchaImageView: UIImageView!
    
    lazy var keyboardToolBar:UIToolbar = {
        [unowned self] in
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.didTapDone))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: false)
        return toolBar
        }()
    
    // MARK: - picker data
    var signalTypeList: [LteType] = []
    
    var signalStrengthList: [Reception] = []
    
    var phoneNumberIssueList: [CmsCategory] = []
    
    var networkQualityIssueList: [CmsCategory] = []
    
    var cityList: [City] = []
    
    // MARK: - ENUM
    
    private enum EnumServiceMainFormType: Int {
        case PhoneNumberQuestion = 0
        case NetworkQualityQuestion = 1
    }
    
    private enum EnumPlaceType: Int {
        case Standard = 0
        case Customize = 1
    }
    
    private enum EnumPickerTag: Int {
        case SignalType
        case SignalStrength
        case MainIssue
        case SubIssue
        case City
        case County
    }
    
    // MARK: - constraints
    @IBOutlet weak var issueTypeHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var signalTypeHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var signalTypeTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var signalStrengthHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var signalStrengthTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var placeHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var placeTopConstraint: NSLayoutConstraint!
    
    // MARK: - form fields
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var mobileField: UITextField!
    
    @IBOutlet weak var dayPhonePrefixField: UITextField!
    
    @IBOutlet weak var dayPhoneNumberField: UITextField!
    
    @IBOutlet weak var dayPhoneExtensionField: UITextField!
    
    @IBOutlet weak var nightPhonePrefixField: UITextField!
    
    @IBOutlet weak var nightPhoneNumberField: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var mainIssueField: UITextField!
    
    @IBOutlet weak var subIssueField: UITextField!
    
    @IBOutlet weak var signalTypeField: UITextField!
    
    @IBOutlet weak var signalStrengthField: UITextField!
    
    @IBOutlet weak var placeCityField: UITextField!
    
    @IBOutlet weak var placeCountyField: UITextField!
    
    @IBOutlet weak var placeAddressField: UITextField!
    
    @IBOutlet weak var descriptionField: UITextView!
    
    @IBOutlet weak var captchaField: UITextField!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var isFisrtLoad : Bool = true //判斷是否第一次load ,給Uba用
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.descriptionField.layer.borderWidth = 0.5
        self.descriptionField.layer.cornerRadius = 6
        self.descriptionField.layer.borderColor = UIColor.lightGray.cgColor
        self.descriptionField.text = ""
        self.didTapFormType(NSNull())
        self.callMailBoxOptionsAPI()
        self.setPickerKeyboard()
        
//        if LoginData.isLogin() {
//            let userData = LoginData.sharedLoginData()
//            self.nameField.text = userData.custProfile_custName
//            self.mobileField.text = userData.custInfoBrief_msisdn
//            self.emailField.text = userData.custProfile_email.replacingOccurrences(of: " ", with: "")
//            
//            self.mobileField.isUserInteractionEnabled = false
//        }
        
        let captchaImg = Captcha.sharedInstance
        captchaImg.captchaSize = CGSize(width: 100, height: 50)
        self.captchaImageView.image = captchaImg.captchaImage.image
        let tapCaptchaGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.didTapRefreshCaptcha))
        self.captchaImageView.addGestureRecognizer(tapCaptchaGestureRecognizer)
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        contentScrollView.addGestureRecognizer(gesture)
        
        if !LoginData.isLogin() {
            historyButton.isEnabled = false
            historyButton.title = ""
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "客服信箱")
        
        if LoginData.isLogin() {
            let userData = LoginData.sharedLoginData()
            self.nameField.text = userData.custProfile_custName
            self.mobileField.text = userData.custProfile_msisdn
            self.emailField.text = userData.eBill_emailAddress.replacingOccurrences(of: " ", with: "")
            
            self.mobileField.isUserInteractionEnabled = false
        }
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let selectedFormType = EnumServiceMainFormType(rawValue: self.segmentedFormType.selectedSegmentIndex)!
        
        if selectedFormType == .NetworkQualityQuestion {
            
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050902", action: "")
        }
        else if selectedFormType == .PhoneNumberQuestion{
            
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050901", action: "")
        }
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: segmentedFormType)
        UISkinChange.sharedInstance.changeUIColor(theObject: segmentedPlaceType)
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    //    MARK: -
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(contentScrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(contentScrollView, hasTabBar: true, notification: notification)
    }
    
    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    private func setPickerKeyboard() {
        var pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.SignalType.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.signalTypeField.rightViewMode = .always
        self.signalTypeField.rightView = self.pickerFieldRightIconImage()
        self.signalTypeField.inputView = pickerView
        
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.SignalStrength.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.signalStrengthField.rightViewMode = .always
        self.signalStrengthField.rightView = self.pickerFieldRightIconImage()
        self.signalStrengthField.inputView = pickerView
        
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.MainIssue.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.mainIssueField.rightViewMode = .always
        self.mainIssueField.rightView = self.pickerFieldRightIconImage()
        self.mainIssueField.inputView = pickerView
        
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.SubIssue.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.subIssueField.rightViewMode = .always
        self.subIssueField.rightView = self.pickerFieldRightIconImage()
        self.subIssueField.inputView = pickerView
        
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.City.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.placeCityField.rightViewMode = .always
        self.placeCityField.rightView = self.pickerFieldRightIconImage()
        self.placeCityField.inputView = pickerView
        
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.County.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.placeCountyField.rightViewMode = .always
        self.placeCountyField.rightView = self.pickerFieldRightIconImage()
        self.placeCountyField.inputView = pickerView
    }
    
    private func pickerFieldRightIconImage() -> UIImageView {
        let rightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        rightIconImage.contentMode = .scaleAspectFit
        rightIconImage.frame.size = CGSize(width: rightIconImage.frame.size.width + 16, height: rightIconImage.frame.size.height)
        return rightIconImage
    }

    private func truncatePickerFields() {
        
        if let signalTypePickerView = self.signalTypeField.inputView as? UIPickerView {
            signalTypePickerView.selectRow(0, inComponent: 0, animated: false)
            self.signalTypeField.text = ""
        }
        
        if let signalStrengthPickerView = self.signalStrengthField.inputView as? UIPickerView {
            signalStrengthPickerView.selectRow(0, inComponent: 0, animated: false)
            signalStrengthField.text = ""
        }
        
        if let mainIssuePickerView = self.mainIssueField.inputView as? UIPickerView {
            mainIssuePickerView.selectRow(0, inComponent: 0, animated: false)
            self.mainIssueField.text = ""
        }
        
        if let subIssuePickerView = self.subIssueField.inputView as? UIPickerView {
            subIssuePickerView.selectRow(0, inComponent: 0, animated: false)
            self.subIssueField.text = ""
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "mailBoxHistory" && !LoginData.isLogin() {
//            let alertController = UIAlertController(title: "", message: "請先登入會員", preferredStyle: .alert)
//            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
//            self.present(alertController, animated: true, completion: nil)
//            return false
            
//            self.navigationController?.popViewController(animated: true)
//            let _ = (self.navigationController?.viewControllers[0] as! ServicesViewController).loginAndNotSuspended()
            
            TSTAPP2_Utilities.checkIfOnlyFacebookLogin()
            
//            let isMaintain = TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self)
            
//            let _ = TSTAPP2_Utilities.isForceAccountBindingAndPushToBindingView(hideBackButton: false, withViewController: self)
            
            let _ = TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self)
            
            return false
        }
        return true
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.inputAccessoryView = self.keyboardToolBar
        if textField.text == "", let pickerView = textField.inputView as? UIPickerView {
            if pickerView.numberOfRows(inComponent: 0) > 0 {
                pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.inputAccessoryView = self.keyboardToolBar
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldChange = true
        
        if textField.tag == 5 {
            if textField.text!.lengthOfBytes(using: String.Encoding.utf8) >= 5 && string != "" {
                shouldChange = false
            }
        }
        else if textField.tag == 6 {
            if textField.text!.lengthOfBytes(using: String.Encoding.utf8) >= 6 && string != "" {
                shouldChange = false
            }
        }
        else if textField.tag == 10 {
            if textField.text!.lengthOfBytes(using: String.Encoding.utf8) >= 10 && string != "" {
                shouldChange = false
            }
        }
        else if textField.tag == 12 {
            if textField.text!.lengthOfBytes(using: String.Encoding.utf8) >= 12 && string != "" {
                shouldChange = false
            }
        }
        else if textField.tag == 20 {
            if textField.text!.lengthOfBytes(using: String.Encoding.utf8) >= 20 && string != "" {
                shouldChange = false
            }
        }
        else if textField.tag == 500 {
            if textField.text!.lengthOfBytes(using: String.Encoding.utf8) >= 500 && string != "" {
                shouldChange = false
            }
        }
        
        
        return shouldChange
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let displayPickerView = EnumPickerTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .SignalType:
            return self.signalTypeList.count
        case .SignalStrength:
            return self.signalStrengthList.count
        case .MainIssue:
            let selectedFormType = EnumServiceMainFormType(rawValue: self.segmentedFormType.selectedSegmentIndex)!
            switch selectedFormType {
            case .PhoneNumberQuestion:
                return self.phoneNumberIssueList.count
            case .NetworkQualityQuestion:
                return self.networkQualityIssueList.count
            }
        case .SubIssue:
            if self.mainIssueField.text == "" {
                return 1
            } else {
                let mainIssuePickerView = self.mainIssueField.inputView as? UIPickerView
                let mainIssue = self.networkQualityIssueList[(mainIssuePickerView?.selectedRow(inComponent: 0))!]
                return mainIssue.child.count
            }
        case .City:
            return self.cityList.count
        case .County:
            if self.placeCityField.text == "" {
                return 1
            } else {
                let cityPickerView = self.placeCityField.inputView as? UIPickerView
                let city = self.cityList[(cityPickerView?.selectedRow(inComponent: 0))!]
                return city.county.count
            }
        }
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let displayPickerView = EnumPickerTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .SignalType:
            return self.signalTypeList[row].itemName
        case .SignalStrength:
            return self.signalStrengthList[row].itemName
        case .MainIssue:
            let selectedFormType = EnumServiceMainFormType(rawValue: self.segmentedFormType.selectedSegmentIndex)!
            switch selectedFormType {
            case .PhoneNumberQuestion:
                return self.phoneNumberIssueList[row].crmName
            case .NetworkQualityQuestion:
                return self.networkQualityIssueList[row].crmName
            }
        case .SubIssue:
            if self.mainIssueField.text == "" {
                return "請先選擇意見類別大項"
            } else {
                let mainIssuePickerView = self.mainIssueField.inputView as? UIPickerView
                let mainIssue = self.networkQualityIssueList[(mainIssuePickerView?.selectedRow(inComponent: 0))!]
                return mainIssue.child[row].crmName
            }
        case .City:
            return self.cityList[row].name
        case .County:
            if self.placeCityField.text == "" {
                return "請先選擇縣市"
            } else {
                let cityPickerView = self.placeCityField.inputView as? UIPickerView
                let city = self.cityList[(cityPickerView?.selectedRow(inComponent: 0))!]
                return city.county[row].name
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        let displayPickerView = EnumPickerTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .SignalType:
            self.signalTypeField.text = self.signalTypeList[row].itemName
            break
        case .SignalStrength:
            self.signalStrengthField.text = self.signalStrengthList[row].itemName
            break
        case .MainIssue:
            let selectedFormType = EnumServiceMainFormType(rawValue: self.segmentedFormType.selectedSegmentIndex)!
            switch selectedFormType {
            case .PhoneNumberQuestion:
                self.mainIssueField.text = self.phoneNumberIssueList[row].crmName
                break
            case .NetworkQualityQuestion:
                self.mainIssueField.text = self.networkQualityIssueList[row].crmName
                let subIssuePickerView = self.subIssueField.inputView as? UIPickerView
                subIssuePickerView?.selectRow(0, inComponent: 0, animated: false)
                self.subIssueField.text = ""
                break
            }
            break
        case .SubIssue:
            if self.mainIssueField.text != "" {
                let mainIssuePickerView = self.mainIssueField.inputView as? UIPickerView
                let mainIssue = self.networkQualityIssueList[(mainIssuePickerView?.selectedRow(inComponent: 0))!]
                self.subIssueField.text = mainIssue.child[row].crmName
            }
            break
        case .City:
            if self.placeCityField.text != self.cityList[row].name {
                self.placeCityField.text = self.cityList[row].name
                let countyPickerView = self.placeCountyField.inputView as? UIPickerView
                self.placeCountyField.text = ""
                countyPickerView?.reloadComponent(0)
                countyPickerView?.selectRow(0, inComponent: 0, animated: false)
            }
            break
        case .County:
            if self.placeCityField.text != "" {
                let cityPickerView = self.placeCityField.inputView as? UIPickerView
                let city = self.cityList[(cityPickerView?.selectedRow(inComponent: 0))!]
                self.placeCountyField.text = city.county[row].name
            }
            break
        }
    }

    // MARK: - IBAction
    
    @IBAction func didTapFormType(_ sender: AnyObject) {
        
        self.contentScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        
        let selectedFormType = EnumServiceMainFormType(rawValue: self.segmentedFormType.selectedSegmentIndex)!
        switch selectedFormType {
        case .NetworkQualityQuestion:
            
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP050902", action: "")
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050901", action: "")
            self.issueTypeHeightConstraint.constant = 81
            self.subIssueField.isHidden = false
            self.signalTypeHeightConstraint.constant = 44
            self.signalTypeTopConstraint.constant = 1
            self.signalStrengthHeightConstraint.constant = 44
            self.signalStrengthTopConstraint.constant = 1
            self.placeHeightConstraint.constant = 118
            self.placeTopConstraint.constant = 1
            if  self.cityList.count == 0 {
                self.callZipCodeAPI()
            }
            break
        case .PhoneNumberQuestion:
            
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP050901", action: "")
            
            if !isFisrtLoad {
                
                UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050902", action: "")
            }
            isFisrtLoad = false
            fallthrough
        default:
            self.issueTypeHeightConstraint.constant = 44
            self.subIssueField.isHidden = true
            self.signalTypeHeightConstraint.constant = 0
            self.signalTypeTopConstraint.constant = 0
            self.signalStrengthHeightConstraint.constant = 0
            self.signalStrengthTopConstraint.constant = 0
            self.placeHeightConstraint.constant = 0
            self.placeTopConstraint.constant = 0
            break
        }
        self.view.updateConstraintsIfNeeded()
        self.view.endEditing(true)
        self.truncatePickerFields()
    }
    
    @IBAction func didTapPlaceType(_ sender: UISegmentedControl) {
        let placeType = EnumPlaceType(rawValue: self.segmentedPlaceType.selectedSegmentIndex)!
        switch placeType {
        case .Customize:
            self.placeCityField.isHidden = true
            self.placeCountyField.isHidden = true
            self.placeAddressField.frame.size.height = 68
            self.placeAddressField.frame.origin.y = 42
            break
        case .Standard:
            fallthrough
        default:
            self.placeCityField.isHidden = false
            self.placeCountyField.isHidden = false
            self.placeAddressField.frame.size.height = 30
            self.placeAddressField.frame.origin.y = 79

            break
        }
    }
    
    @objc func didTapRefreshCaptcha() {
        let captchaImg = Captcha.sharedInstance
        self.captchaImageView.image = captchaImg.refresh().image
    }
    
    @IBAction func didTapAgreePolicy(_ sender: UIButton) {
        self.agreePolicy.isSelected = !self.agreePolicy.isSelected
        if self.agreePolicy.isSelected {
            TSTAPP2_Utilities.showHtmlMessage(withTitle: "隱私權聲明條款", message: "<body>台灣之星電信股份有限公司(以下稱台灣之星)非常重視本網站使用者的隱私權及保護使用者所提供的個人資料，為保障使用者的權益及讓使用者瞭解台灣之星直接或間接蒐集、利用、處理及/或傳輸使用者個人資料等情形，請使用者詳閱下列台灣之星的隱私權保護政策及依「個人資料保護法」之規定所作之說明暨使用者享有之權利或服務：<br /><br /><div class='title'>適用的範圍</div>隱私權保護政策的內容，包括台灣之星如何處理使用者使用網站服務、參加網站活動、申請加入網站會員、進行線上購物及申請門號與加值服務時所蒐集到的身份識別資料。<br /><br /><div class='title'>資料蒐集的事由、目的與範圍</div>台灣之星為履行法定義務、契約義務、提供或辦理各項電信商品或服務，以及遵守通訊傳播監理、司法與其他依法具有司法或行政調查權公務機關之命令及查核，暨台灣之星業務、行政管理以及訴訟、非訟或其他紛爭事件之處理等事由與目的，於所涉業務執行之必要範圍內，而有必要直接或間接蒐集、處理、利用及/或國際傳輸使用者個人資料。<br /><br />為了確認在台灣之星網站（下稱本網站www.tstartel.com）上取得資訊的使用者身份及提供有關行動電話各項服務之用，其範圍如下：<br />台灣之星在使用者使用台灣之星的產品或服務、瀏覽台灣之星網頁、參加宣傳活動或贈獎遊戲、訂閱電子報或使用其他網站服務時，將會請使用者提供個人相關資料，包括但不限於行動電話門號、身分證字號、暱稱、行動電話USIM卡號碼、姓名、電子郵件、職業或行業別、婚姻、出生年月日、性別、教育程度、個人收入、地址⋯⋯等。<br />台灣之星會保留使用者所提供的上述資料，也會保留使用者上網瀏覽或查詢時，在系統上產生的相關記錄，包括IP位址、使用時間、瀏覽器、瀏覽及點選紀錄等。依「個人資料保護法」第八條規定之告知內容：<br /><ol><li>非公務機關名稱：台灣之星電信股份有限公司。</li><li>蒐集之目的：○四○ 行銷/○六三 非公務機關依法定義務所進行個人資料之蒐集處理及利用/○六七 信用卡、現金卡、轉帳卡或電子票證業務/○六九 契約、類似契約或其他法律關係事務/○七二 政令宣導/○八一 個人資料之合法交易業務/○八五 旅外國人急難救助/○九○ 消費者、客戶管理與服務/○九一 消費者保護/一○四 帳務管理及債權交易業務/一二九 會計與相關服務/一三三 經營電信業務與電信加值網路業務/一三五 資(通)訊服務/一三六 資(通)訊與資料庫管理/一三七 資通安全與管理/一四八 網路購物及其他電子商務服務/一五二 廣告或商業行為管理/一五三 影視、音樂與媒體管理/一五四 徵信/一五七 調查、統計與研究分析/一八一 其他經營合於營業登記項目或組織章程所定之業務/一八二 其他諮詢與顧問服務及其他依主管機關公告之特定目的。</li><li>個人資料之類別：辨識個人資料、辨識財務者、政府資料中之辨識者、個人描述、財務交易及其他依主管機關公告之個人資料類別。</li><li>個人資料利用之期間、地區、對象及方式：<ol><li>期間：台灣之星營運期間</li><li>地區：中華民國境內、境外（主管機關禁止者不在此限）。</li><li>對象：台灣之星、與台灣之星有合作或業務往來關係之企業及合作廠商。</li><li>方式：符合法令規定範圍之利用。</li></ol></li><li>使用者在符合電信相關法規之情形下，就其個人資料得依「個人資料保護法」規定行使下列權 利，行使方式依台灣之星作業規範：<ol><li>查詢或請求閱覽。</li><li>請求製給複製本。</li><li>請求補充或更正。</li><li>請求停止蒐集、處理或利用。</li><li>請求刪除。</li><li>請求拒絕行銷。</li></ol></li><li>使用者得自由選擇提供資料（但依法令規定者不在此限），若不提供將影響電信服務之完整性。</li></ol><div class='title'>資料使用的範圍與保護</div>為了保護使用者個人資料之完整及安全，保存使用者個人資料之資料處理系統均已受妥善的維護，並符合相關主管機關嚴格之要求，以保障使用者的個人資料不會被不當取得或破壞。如因業務需要有必要委託第三者時，台灣之星亦會嚴格要求遵守保密義務，並採取必要檢查程序以確定其確實遵守。<br /><br />為了提供使用者更多服務或優惠，如需要求使用者將個人資料提供予台灣之星之優良商業夥伴或合作廠商時，我們會在活動時提供充分說明，使用者可以自由選擇是否接受這項特定服務或優惠。<br /><br />台灣之星對於使用者個人資料之使用，僅在蒐集特定目的及相關法令規定之範圍內為之。<br /><br />由於科技的技術性問題，使得任何資料在網際網路傳輸的過程中，仍然不具有完全的隱密性，因此，使用者亦必須體認到上述的事實。<br /><br />台灣之星充分瞭解到保護使用者資料是我們的責任。因此，使用者的個人資料將會完整儲存在台灣之星的資料庫中，台灣之星將盡最大的努力，以嚴密的保護措施維護並防止未經過授權人員接觸。除非經由使用者的同意或其他法令之特別規定，否則，本網站絕不會將使用者的個人資料揭露給第三人或使用於蒐集目的以外之其他用途。<br /><br /><div class='title'>用戶個人資料的使用與修改</div>使用者在本網站中可以隨時利用您個人申請的帳號和密碼，更改使用者所輸入的任何個人或公司資料。<br /><br /><div class='title'>Cookie、Web Beacon之運用</div>台灣之星為了提供個別化的服務，方便使用者使用或為了統計分析瀏覽模式，以改善服務等目的，且為了讓資訊系統能夠辨識使用者的資訊，資訊系統將以Cookie、Web Beacon或其他方式記錄使用者使用網路的行為。<br /><br />資訊系統伺服器會將所需的資訊經由瀏覽器寫入使用者的儲存設備中，下次瀏覽器在要求伺服器傳回網頁時，會將Cookie的資料先傳給伺服器，伺服器可依據Cookie的資料判斷使用者，網頁伺服器可針對使用者之使用行為來執行不同動作或傳回特定的資訊。<br /><br />Web Beacon稱網路信標是一種1X1像素或電子圖片檔，部分網頁會使用Web Beacon結合Cookie之運用蒐集有關使用者於網站使用的情況，技術運用上可能結合或利用其他工具彙整統計資料，以計算網站或網頁使用人次或辨識使用者在網站的互動情況，並依不同喜好執行不同資訊之傳遞。<br /><br /><div class='title'>隱私權保護政策的例外</div>若使用者因犯罪嫌疑，經政府機關或司法機關依法調查、偵查時，本公司及網站有義務協助配合，並提供使用者之相關資料給予政府或司法機關。<br /><br />本網站的網頁可能提供其他網站的網路連結，您可經由本網站所提供的連結，點選進入其他網站，任何與台灣之星連結的網站，亦可能蒐集使用者個人資料，該連結網站應有個別的隱私權保護政策，台灣之星不負任何連帶責任，亦不保護使用者於該等連結網站中的隱私權；除了使用者主動登錄網站所提供的個人資料，使用者如在台灣之星網站或服務中的討論版等類似場域主動提供之個人資料，這種形式的資料提供，亦不在本隱私權保護政策的範圍之內。<br /><br /><div class='title'>隱私權保護政策的修正</div>台灣之星隱私權保護政策將因應科技發展的趨勢、法規之修訂或其他環境變遷等因素而做適當的修改，以落實保障使用者隱私權之立意。修正過之條款，也將會立即刊登在本網站上。<br /><br />本條款之解釋及適用、以及使用者因使用本服務而與本公司間所生之權利義務關係，應依中華民國法令解釋適用之。其因此所生之爭議，以台灣台北地方法院為第一審管轄法院。<br /><br />使用者若想進一步瞭解台灣之星蒐集、處理、利用及/或傳輸個人資料之管理方針及使用者所享有之權利或服務，請透過客服專線（手機直撥123或撥0908-000-123）致電洽詢。<br /><br /><div class='title'>著作權保護措施</div>使用者如透過本公司之網路連接至網際網路任意上傳、下載、轉貼未經權利人合法授權之影像、圖片、音樂、文章或其他受著作權法保護之著作，或利用P2P、Foxy等軟體非法傳輸上述著作者，本公司將依著作權法「第六章之一：網路服務提供者之民事免責事由（即第90條之4以下）」及98年11月17日經濟部智慧財產局訂定發布之「網路服務提供者民事免責事由實施辦法」等相關規定執行著作權保護措施，詳情請參閱:經濟部智慧財產局網站<br /><a href=\"http://www.tipo.gov.tw/\"><font color=\"#819c21\">http://www.tipo.gov.tw/</font></a>。<br /><p align=\"right\">版次：231115-002</p></body><style>body {color: #666666;line-height: 1.5em;}.title {color: #ad267c;font-size: 1.2em;line-height: 1.8em;}ol {padding-left: 1em;}</style>", viewController: self)
        }
    }
    
    @IBAction func didTapEditMainIssue(_ sender: Any) {
        self.mainIssueField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditSubIssue(_ sender: Any) {
        self.subIssueField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditSignalType(_ sender: Any) {
        self.signalTypeField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditSignalStrength(_ sender: Any) {
        self.signalStrengthField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditPlaceCity(_ sender: Any) {
        self.placeCityField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditPlaceCounty(_ sender: Any) {
        self.placeCountyField.becomeFirstResponder()
    }
    
    @objc func didTapDone() {
        self.view.endEditing(true)
    }
    
    @IBAction func didTapSubmit(_ sender: AnyObject) {
        
        let selectedFormType = EnumServiceMainFormType(rawValue: self.segmentedFormType.selectedSegmentIndex)!
        
        var verifyRules: [Dictionary<String, Any>] = [
            [
                "attribute": "name",
                "label": "姓名",
                "value": self.nameField.text!,
                "rules": ["required"]
            ],
            [
                "attribute": "mobile",
                "label": "手機門號",
                "value": self.mobileField.text!,
                "rules": ["required" ,"mobile"]
            ],
            [
                "attribute": "dayPhonePrefix",
                "label": "聯絡電話(日) 區碼",
                "value": self.dayPhonePrefixField.text!,
                "rules": ["required", "number"]
            ],
            [
                "attribute": "dayPhoneNumber",
                "label": "聯絡電話(日) 電話",
                "value": self.dayPhoneNumberField.text!,
                "rules": ["required", "number"]
            ],
            [
                "attribute": "dayPhoneExtension",
                "label": "聯絡電話(日) 分機",
                "value": self.dayPhoneExtensionField.text!,
                "rules": ["number"]
            ],
            [
                "attribute": "nightPhonePrefix",
                "label": "聯絡電話(夜) 區碼",
                "value": self.nightPhonePrefixField.text!,
                "rules": ["number"]
            ],
            [
                "attribute": "nightPhoneNumber",
                "label": "聯絡電話(夜) 電話",
                "value": self.nightPhoneNumberField.text!,
                "rules": ["number"]
            ],
            [
                "attribute": "email",
                "label": "電子郵件",
                "value": self.emailField.text!,
                "rules": ["required", "email"]
            ],
            [
                "attribute": "description",
                "label": "意見描述",
                "value": self.descriptionField.text!,
                "rules": ["required"]
            ]
        ]
        
        switch selectedFormType {
        case .PhoneNumberQuestion:
            
            var mainIssueValue = ""
            if let pickerView = self.mainIssueField.inputView as? UIPickerView {
                mainIssueValue = self.phoneNumberIssueList[pickerView.selectedRow(inComponent: 0)].callType
            }
            verifyRules.append([
                "attribute": "mainIssue",
                "label": "意見類別",
                "value": (self.mainIssueField.text == "") ? "" : mainIssueValue,
                "rules": ["required"]
                ])
            break
        case .NetworkQualityQuestion:
            
            var mainIssueValue = ""
            var subIssueValue = ""
            if let mainIssuePickerView = self.mainIssueField.inputView as? UIPickerView {
                mainIssueValue = self.networkQualityIssueList[mainIssuePickerView.selectedRow(inComponent: 0)].callType
                if let subIssuePickerView = self.subIssueField.inputView as? UIPickerView {
                    subIssueValue = self.networkQualityIssueList[mainIssuePickerView.selectedRow(inComponent: 0)].child[subIssuePickerView.selectedRow(inComponent: 0)].callType
                }
            }
            verifyRules.append(
                [
                    "attribute": "mainIssue",
                    "label": "意見類別大項",
                    "value": (self.mainIssueField.text == "") ? "" : mainIssueValue,
                    "rules": ["required"]
                ]
            )
            verifyRules.append([
                "attribute": "subIssue",
                "label": "意見類別小項",
                "value": (self.subIssueField.text == "") ? "" : subIssueValue,
                "rules": ["required"]
                ])
            
            var signalTypeValue = ""
            if let signalTypePickerView = self.signalTypeField.inputView as? UIPickerView {
                signalTypeValue = self.signalTypeList[signalTypePickerView.selectedRow(inComponent: 0)].itemValue
            }
            verifyRules.append([
                "attribute": "signalType",
                "label": "訊號種類",
                "value": (self.signalTypeField.text == "") ? "" : signalTypeValue,
                "rules": ["required"]
                ])
            
            var signalStrengthValue = ""
            if let signalStrengthPickerView = self.signalStrengthField.inputView as? UIPickerView {
                signalStrengthValue = self.signalStrengthList[signalStrengthPickerView.selectedRow(inComponent: 0)].itemValue
            }
            verifyRules.append([
                "attribute": "signalStrength",
                "label": "手機收訊格數",
                "value": (self.signalStrengthField.text == "") ? "" : signalStrengthValue,
                "rules": ["required"]
                ])
            
            let placeType = EnumPlaceType(rawValue: self.segmentedPlaceType.selectedSegmentIndex)!
            switch placeType {
            case .Standard:
                var placeCityValue = ""
                var placeCountyValue = ""
                if let placeCityPickerView = self.placeCityField.inputView as? UIPickerView {
                    placeCityValue = self.cityList[placeCityPickerView.selectedRow(inComponent: 0)].name
                    if let placeCountyPickerView = self.placeCountyField.inputView as? UIPickerView {
                        placeCountyValue = self.cityList[placeCityPickerView.selectedRow(inComponent: 0)].county[placeCountyPickerView.selectedRow(inComponent: 0)].name
                    }
                }
                
                verifyRules.append([
                    "attribute": "placeCity",
                    "label": "縣市",
                    "value": (self.placeCityField.text == "") ? "" : placeCityValue,
                    "rules": ["required"]
                    ])
                verifyRules.append([
                    "attribute": "placeCounty",
                    "label": "鄉鎮",
                    "value": (self.placeCountyField.text == "") ? "" : placeCountyValue,
                    "rules": ["required"]
                    ])
                verifyRules.append([
                    "attribute": "placeAddress",
                    "label": "詳細地址",
                    "value": self.placeAddressField.text!,
                    "rules": ["required"]
                    ])
                break
            case .Customize:
                verifyRules.append([
                    "attribute": "customizePlaceAddress",
                    "label": "詳細地址",
                    "value": self.placeAddressField.text!,
                    "rules": ["required"]
                    ])
                break
            }
            break
        }
        
        let validator = Validators(withRules: verifyRules)
        let alertController = UIAlertController(title: "提示訊息", message: "", preferredStyle: .alert)
        do {
            let verifyResult = try validator.verify()
            if verifyResult == true {
                let captcha = Captcha.sharedInstance
                if self.captchaField.text != captcha.captchaString {
                    alertController.message = "驗證碼輸入錯誤"
                } else if !self.agreePolicy.isSelected {
                    alertController.message = "請勾選本人已詳閱及瞭解隱私權說明"
                } else {
                    
                    
                    alertController.message = "客服信箱服務時段為週一至週日9:00 – 23:00，我們將於收到您的問題反應後24個工作小時內回覆您的問題，如遇不可抗力之天災或大型促銷活動期間，因信件數量較多，我們將儘速協助處理您的問題，謝謝您 !\n\n若您於回覆時間內尚未收到台灣之星客服信箱信件回覆的時候，建議您可以優先至您的電子郵件信箱中的垃圾信件裡查看，嘗試在郵件信箱裡搜尋“台灣之星客服中心”確認是否有無收到信件。"
                    alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (_ : UIAlertAction) in
                        let requestData = validator.data
                        switch selectedFormType {
                        case .PhoneNumberQuestion:
                            
                            UbaRecord.sharedInstance.sendUba(withPage: "APP050901", action: "A030401")
                            
                            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "客服信箱", action:requestData["mainIssue"]!,  label: nil, value: nil)
                            
                            TSTAPP2_API.sharedInstance.addContactUs(withType: "GEN", name: requestData["name"]!, mobile: requestData["mobile"]!, dayPhonePrefix: requestData["dayPhonePrefix"]!, dayPhoneNumber: requestData["dayPhoneNumber"]!, dayPhoneExtension: requestData["dayPhoneExtension"]!, nightPhonePrefix: requestData["nightPhonePrefix"]!, nightPhoneNumber: requestData["nightPhoneNumber"]!, email: requestData["email"]!, description: requestData["description"]!, issueIdentify1: requestData["mainIssue"]!, issueIdentify2: "", issueIdentify3: "", signalType: "", signalStrength: "", addressType1City: "", addressType1Section: "", addressType1Addr: "", addressType2Addr: "", showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                                if dic.stringValue(ofKey: "code") == "00000" {
                                    let _ = self.navigationController?.popViewController(animated: true)
                                }
                                else {
                                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                                }
                                
                            }, failure: { (Error) in
                                print(Error)
                                TSTAPP2_Utilities.showAlert(withMessage: "請確認網路狀態", viewController: self)
                            })
                            
                            break
                        case .NetworkQualityQuestion:
                            
                            UbaRecord.sharedInstance.sendUba(withPage: "APP050902", action: "A030402")
                            
                            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "客服信箱", action:requestData["mainIssue"]!,  label: nil, value: nil)
                            
                            TSTAPP2_API.sharedInstance.addContactUs(withType: "NET", name: requestData["name"]!, mobile: requestData["mobile"]!, dayPhonePrefix: requestData["dayPhonePrefix"]!, dayPhoneNumber: requestData["dayPhoneNumber"]!, dayPhoneExtension: requestData["dayPhoneExtension"]!, nightPhonePrefix: requestData["nightPhonePrefix"]!, nightPhoneNumber: requestData["nightPhoneNumber"]!, email: requestData["email"]!, description: requestData["description"]!, issueIdentify1: "", issueIdentify2: requestData["mainIssue"]!, issueIdentify3: requestData["subIssue"]!, signalType: requestData["signalType"]!, signalStrength: requestData["signalStrength"]!, addressType1City: (requestData["placeCity"] ?? ""), addressType1Section: (requestData["placeCounty"] ?? ""), addressType1Addr: (requestData["placeAddress"] ?? ""), addressType2Addr: (requestData["customizePlaceAddress"] ?? ""), showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                                if dic.stringValue(ofKey: "code") == "00000" {
                                    let _ = self.navigationController?.popViewController(animated: true)
                                }
                                else {
                                    TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                                }
                            }, failure: { (Error) in
                                print(Error)
                                TSTAPP2_Utilities.showAlert(withMessage: "請確認網路狀態", viewController: self)
                            })
                            
                            print(requestData)
                            break
                        }
                    }))
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
            }
        } catch {
            alertController.message = validator.errors.first
        }
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - API
    func callZipCodeAPI() {
        let zipCode = ZipCode.sharedInstance
        if zipCode.towns.count > 0 {
            self.cityList = zipCode.towns
        } else {
            TSTAPP2_API.sharedInstance.getZipCode(showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    zipCode.setDataValue(withDic: dic)
                    self.cityList = zipCode.towns
                } else {
                    let errorAlertController = UIAlertController(title: nil, message: dic.stringValue(ofKey: "message"), preferredStyle: .alert)
                    errorAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
                    self.present(errorAlertController, animated: true, completion: nil)
                }
            }) { (Error) in
                print("error: \(Error)")
            }
        }
    }
    
    func callMailBoxOptionsAPI() {
        
        let mailBoxOpts = MailBoxOptions.sharedInstance
        if mailBoxOpts.genCategoryList.count > 0 {
            self.phoneNumberIssueList = mailBoxOpts.genCategoryList
            self.networkQualityIssueList = mailBoxOpts.netCategoryList
            self.signalStrengthList = mailBoxOpts.receptionList
            self.signalTypeList = mailBoxOpts.lteTypeList
        } else {
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.getCmsCategory(withMSISDN: loginData.custProfile_msisdn, showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    mailBoxOpts.setDataValue(withDic: dic)
                    
                    self.phoneNumberIssueList = mailBoxOpts.genCategoryList
                    self.networkQualityIssueList = mailBoxOpts.netCategoryList
                    self.signalStrengthList = mailBoxOpts.receptionList
                    self.signalTypeList = mailBoxOpts.lteTypeList
                } else {
//                    let errorAlertController = UIAlertController(title: nil, message: dic.stringValue(ofKey: "message"), preferredStyle: .alert)
//                    errorAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
//                    self.present(errorAlertController, animated: true, completion: nil)
                    
                    TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                }
            }) { (Error) in
                print("error: \(Error)")
                TSTAPP2_Utilities.showAlertAndPop(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
            }
        }
    }
}
