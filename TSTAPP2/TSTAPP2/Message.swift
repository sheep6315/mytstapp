//
//  Message.swift
//  TSTAPP2
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

//查詢推撥訊息
class PushMessages: NSObject {
    
    static let sharedInstance = PushMessages()
    
//    private(set) var messages: [Message] = []
    var messages: [Message] = []
    
    private override init () {
        
    }
    
    func setDataValue(withDic dic: Dictionary<String, Any>) {
        messages = []
        let code = dic.stringValue(ofKey: "code")
        if code == "00000" {
            if let rawData = dic["data"] as? [Dictionary<String, Any>] {
                rawData.forEach({ (data: [String : Any]) in
                    let message = Message()
                    message.setDataValue(withDic: data)
                    messages.append(message)
                })
            }
        }
    }
}

class Message: NSObject {
    
    static let sharedInstance = Message()
    
    private(set) var messageSeq : String = "" //推播訊息流水號
    private(set) var appId : String = "" //APP系統代碼
    private(set) var systemId : String = "" //系統代碼
    private(set) var batchId : String = "" //批號
    private(set) var deviceType : String = "" //手機系統
    private(set) var id : String = ""
    /*
        門號_合約編號(TSTAPP)
        工號(NTS)
        AD帳號
        NT帳號
     */
    private(set) var messageTitle : String = "" //推播標題
    private(set) var messageBody : String = "" //推播內文
    private(set) var action : String = "" //動作
    private(set) var actionParam : String = "" //動作參數
    private(set) var status : String = ""
    //狀態 (N:未發送/Y:發送成功/D:刪除/F:發送失敗)
    var readStatus : String = ""
    //推播讀取狀態 (Y:已讀/N:未讀)
    private(set) var sendTime : String = "" //發送時間
    private(set) var resultCode : String = "" //發送結果代碼
    private(set) var resultMsg : String = "" //發送結果訊息
    private(set) var createUser : String = "" //登入使用者
    private(set) var createTime : String = "" //建立時間
    private(set) var updateUser : String = "" //異動人員
    private(set) var updateTime : String = "" //更新時間
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        messageTitle = dic.stringValue(ofKey: "messageTitle")
        messageBody = dic.stringValue(ofKey: "messageBody")
        action = dic.stringValue(ofKey: "action")
        actionParam = dic.stringValue(ofKey: "actionParam")
        status = dic.stringValue(ofKey: "status")
        readStatus = dic.stringValue(ofKey: "readStatus")
        sendTime = dic.stringValue(ofKey: "sendTime")
        resultCode = dic.stringValue(ofKey: "resultCode")
        resultMsg = dic.stringValue(ofKey: "resultMsg")
        createUser = dic.stringValue(ofKey: "createUser")
        createTime = dic.stringValue(ofKey: "createTime")
        updateUser = dic.stringValue(ofKey: "updateUser")
        updateTime = dic.stringValue(ofKey: "updateTime")
        messageSeq = dic.stringValue(ofKey: "messageSeq")
        appId = dic.stringValue(ofKey: "appId")
        systemId = dic.stringValue(ofKey: "systemId")
        batchId = dic.stringValue(ofKey: "batchId")
        deviceType = dic.stringValue(ofKey: "deviceType")
    }
}
