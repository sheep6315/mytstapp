//
//  MessageViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/23.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

//message CELL
//let messageCellHeight = CGFloat(75)

class HistoryPushMessageCell: UITableViewCell {
    
    var expand: Bool = false
    
    @IBOutlet weak var readIndicatorImageView: UIImageView!
    
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var messageContentLabel: UILabel!
    
    @IBOutlet weak var messageTimeLabel: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var pushMessageButton: UIButton!
}

class MessageCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var delete: UIButton!
    
}

class MessageViewController: TSUIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var pushMessageBaseView: UIView!
    
    @IBOutlet weak var pushMessageScrollView: UIScrollView!
    
    @IBOutlet weak var pushMessageTableView: UITableView!
    
    @IBOutlet weak var pushMessageTableHeight: NSLayoutConstraint!

    @IBOutlet weak var noMessageLabel: UILabel!
    
//    @IBOutlet weak var scrollView: UIScrollView!
//    
//    @IBOutlet weak var tableView: UITableView!
//    
//    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var disableBackToRoot = false
    
    var messageList : [PushMsg] = []
    
    var pushMessages = PushMessages.sharedInstance
    
    let messageCellHeight = CGFloat(88)
    
    var landingVC: LandingViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pushMessageTableView.isHidden = true
        
        pushMessageTableView.rowHeight = UITableViewAutomaticDimension
        pushMessageTableView.estimatedRowHeight = messageCellHeight
        
        noMessageLabel.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0202", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "訊息推播", action: nil, label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "訊息推播")
        
        TSTAPP2_Utilities.checkIfOnlyFacebookLogin()
        
        let isMaintain = TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self)
        
        if !isMaintain {
            callQueryMessage()
        }
        
//        disableBackToRoot = TSTAPP2_Utilities.isForceAccountBindingAndPushToBindingView(hideBackButton: true, withViewController: self)
//        if disableBackToRoot {
//            return
//        }
        
//        callQueryMessage()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0202", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    MARK: - IBAction
    
    @IBAction func didTapEditButton(_ sender: Any) {
        pushMessageTableView.setEditing(!pushMessageTableView.isEditing, animated: true)
    }
    
    @IBAction func didTapPushMessageButton(_ sender: Any) {
        let row = (sender as! UIButton).tag
        let action = pushMessages.messages[row].action
        let actionParam = pushMessages.messages[row].actionParam
        
        landingVC?.didLoadAPIOnce = true
        navigationController?.popToRootViewController(animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.35) {
            TSTAPP2_Utilities.changeFunctionPage(withAction: action, actionParam: actionParam)
        }
        
    }
    
    func didTapDelete(_ sender: AnyObject) {
        
//        let button = sender as! UIButton
//        let messageId = messageList[button.tag].messageid
        
//        callDeleteMsgAPI(messageId: messageId, index: button.tag)
    }

    //MARK: - callAPI
    func callQueryMessage() {
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.queryMessage(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                let pm = PushMessages.sharedInstance
                pm.setDataValue(withDic: dic)
                self.pushMessages = pm
                
                var count = 0
                
                if self.pushMessages.messages.count > 0 {
                    count = self.pushMessages.messages.count
                    self.pushMessageTableView.isUserInteractionEnabled = true
                    
                    self.noMessageLabel.isHidden = true
                }
                else {
                    self.pushMessageTableView.isUserInteractionEnabled = false
                    self.noMessageLabel.isHidden = false
                }
                
                self.pushMessageTableHeight.constant = CGFloat(count) * self.messageCellHeight
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
                
                self.pushMessageTableView.reloadData()
                self.pushMessageTableView.isHidden = false
                
                UserDefaults.standard.set(pm.messages.count, forKey: MESSAGE_COUNT)
            }
            else {
                
            }
        }) { (error: Error) in
            
        }
    }
    
//    MARK: - Update Table Height
    func updateTableHeight(indexPath: IndexPath) {
        var tableHeight: CGFloat = 0
        
            if pushMessages.messages.count > 0 {
            
                for cell in pushMessageTableView.visibleCells {
                    tableHeight += CGFloat(messageCellHeight)
                    if (cell as! HistoryPushMessageCell).expand {
                        tableHeight += (cell as! HistoryPushMessageCell).messageContentLabel.frame.height + 16
                    }
                }
                
                pushMessageTableHeight.constant = tableHeight
                self.view.setNeedsUpdateConstraints()
                
                UIView.animate(withDuration: 0.5, delay: 0, options: .beginFromCurrentState, animations: {
                    self.view.layoutIfNeeded()
                    self.pushMessageTableView.beginUpdates()
                    self.pushMessageTableView.endUpdates()
                }) { (isComplete: Bool) in
                    if let cell = self.pushMessageTableView.cellForRow(at: indexPath) as? HistoryPushMessageCell {
                        if !cell.expand {
                            cell.messageContentLabel.isHidden = true
                        }
                    }
                }
        }
    }
    
    
    //MARK: - tableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height = messageCellHeight
        
        if tableView.tag == 0 {
            if let cell = tableView.cellForRow(at: indexPath) as? HistoryPushMessageCell {
                if cell.expand {
                    height += cell.messageContentLabel.frame.height + 16
                }
            }
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if let cell = tableView.cellForRow(at: indexPath) as? HistoryPushMessageCell {
            cell.expand = !cell.expand
            cell.arrowImageView.image = cell.expand ? UIImage(named: "icon_arrow_up") : UIImage(named: "icon_arrow_down")
            if cell.expand {
                cell.messageContentLabel.isHidden = false
            }
            
            if pushMessages.messages[indexPath.row].readStatus == "N" {
                pushMessages.messages[indexPath.row].readStatus = "Y"
                let loginData = LoginData.sharedLoginData()
                TSTAPP2_API.sharedInstance.updateReadStatus(withMSISDN: loginData.custProfile_msisdn, messageSeq: pushMessages.messages[indexPath.row].messageSeq, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
                        cell.readIndicatorImageView.isHidden = true
                    }
                }, failure: { (error: Error) in
                    
                })
            }
        }
        
        updateTableHeight(indexPath: indexPath)
        
//        let action = messgeList[indexPath.row].action
//        let actionParam = messgeList[indexPath.row].actionParam
//        
//        if action != "" {
//            
//            let url = "tstarcs://?action=\(action)&actionParam=\(actionParam)"
//            UIApplication.shared.openURL(NSURL(string: url) as! URL)
//        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.deleteMessage(withMSISDN: loginData.custProfile_msisdn, messageSeq: pushMessages.messages[indexPath.row].messageSeq, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                self.pushMessages.messages.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                self.updateTableHeight(indexPath: indexPath)
                
                UserDefaults.standard.set(self.pushMessages.messages.count, forKey: MESSAGE_COUNT)
                
                if self.pushMessages.messages.count > 0 {
                    
                    self.noMessageLabel.isHidden = true
                }
                else {
                    self.noMessageLabel.isHidden = false
                }
                
            }, failure: { (error: Error) in

            })
        }
    }
    
    //    MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var count = 0
        
        if pushMessages.messages.count > 0 {
            count = pushMessages.messages.count
        }
        else {
            tableView.isHidden = true
        }
    
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()

        if tableView.tag == 0 {
            
            cell = tableView.dequeueReusableCell(withIdentifier: "HistoryPushMessageCell", for: indexPath) as! HistoryPushMessageCell
            (cell as! HistoryPushMessageCell).messageTitleLabel.text = pushMessages.messages[indexPath.row].messageTitle
            (cell as! HistoryPushMessageCell).messageTimeLabel.text = pushMessages.messages[indexPath.row].sendTime
            
            let color = UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1)
            let attributedString = NSMutableAttributedString(string: pushMessages.messages[indexPath.row].messageBody, attributes: [NSAttributedStringKey.foregroundColor:color])
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1.2
            attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
            (cell as! HistoryPushMessageCell).messageContentLabel.attributedText = attributedString

            
            
            //(cell as! HistoryPushMessageCell).messageContentLabel.text = pushMessages.messages[indexPath.row].messageBody
            (cell as! HistoryPushMessageCell).pushMessageButton.tag = indexPath.row
            
            if pushMessages.messages[indexPath.row].readStatus == "Y" {
                (cell as! HistoryPushMessageCell).readIndicatorImageView.isHidden = true
            }
            else {
                (cell as! HistoryPushMessageCell).readIndicatorImageView.isHidden = false
            }
        }
        
        return cell
    }

}
