//
//  MyAccountViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/24.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import FBSDKShareKit
import Crashlytics

let SHOW_MYACCONUT_TOP_MESSAGE = "SHOW_MYACCONUT_TOP_MESSAGE"

//VAS CELL
let vasCellHeight = 44
let vasCellExpandHeight = vasCellHeight * 4

class VasCell: UITableViewCell {
    @IBOutlet weak var vasButton: UIButton!
    var expand = false
}

class GroupBuyMemoViewController: UIViewController {
    
    @IBOutlet weak var recordListContentWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.recordListContentWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(GroupBuy.sharedInstance.recordList.joined(separator: "<br />"))</span>", baseURL: nil)
        
    }
}

class MyAccountViewController: TSUIViewController, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate {
    
    @IBOutlet weak var nickName: UILabel!
//    @IBOutlet weak var nickNameHeight: NSLayoutConstraint!
    
    @IBOutlet weak var greetingWord: UILabel!
    
    @IBOutlet weak var accountInfoButton: UIBarButtonItem!
    @IBOutlet weak var messageButton: UIBarButtonItem!
    @IBOutlet weak var locationButton: UIBarButtonItem!
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet var btnList: [UIButton]! //畫面上需要增加紫色邊框的按鈕
    
    @IBOutlet var iconBackgroundView: [UIView]! //會員訊息據點圓設定用
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var myScrollView: UIScrollView!
    
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    // MARK: - IB GroupBuy 家族省
    @IBOutlet weak var groupBuyView: UIView!
    
    @IBOutlet weak var groupBuyViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupBuyTitleWebView: UIWebView!
    
    @IBOutlet weak var groupBuyTitleHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupBuyContentWebView: UIWebView!
    
    @IBOutlet weak var groupBuyContentHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupBuyMoreButton: CustomButton!
    
    @IBOutlet weak var groupBuyRecordButton: CustomButton!
    
    @IBOutlet weak var groupBuyShareButton: CustomButton!
    
    @IBOutlet weak var groupBuyMemberView: UIView!
    
    @IBOutlet weak var groupBuyMemberViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var groupBuyShareButtonCenterXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupBuyRecordButtonCenterXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupBuyMoreButtonCenterXConstraint: NSLayoutConstraint!

    @IBOutlet weak var memberCount: UILabel!
    
    @IBOutlet weak var memberRemainder: UILabel!
    
    // MARK: -
    @IBOutlet weak var vasTableView: UITableView! //Value Added Services Table
    @IBOutlet weak var vasTableHeight: NSLayoutConstraint!
    
    //MARK: - carrier 電信帳單
    
    @IBOutlet weak var carrierBillingVIew: UIView!
    
    @IBOutlet weak var carrierBillingServiceView: UIView!
    
    @IBOutlet weak var carrierBillingServiceViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var carrierBillingTitleWebView: UIWebView!
    
    @IBOutlet weak var carrierBillingTitleHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var carrierBillingContentWebView: UIWebView!
    
    @IBOutlet weak var carrierBillingContentHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - Relation 預付卡
    @IBOutlet weak var relationView: UIView!
    
    @IBOutlet weak var relationViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var relationViewTopGap: NSLayoutConstraint!
    
    @IBOutlet weak var relationTitleWebView: UIWebView!
    
    @IBOutlet weak var relationContentWebView: UIWebView!
    
    @IBOutlet weak var relationButton: CustomButton!
    
    @IBOutlet weak var relationButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var relationTitleHeight: NSLayoutConstraint!
    
    @IBOutlet weak var relationContentHeight: NSLayoutConstraint!
    
    //MARK: - AddValue 加值服務
    
    @IBOutlet weak var addValueView: UIView!
    
    @IBOutlet weak var addValueServiceView: UIView!
    
    @IBOutlet weak var addValueServiceHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addValueAppliedView: UIView!
    
    @IBOutlet weak var addValueAppliedViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addValuePromoView: UIView!
    
    @IBOutlet weak var addValueTitleWebView: UIWebView!
    
    @IBOutlet weak var addValueTitleHeight: NSLayoutConstraint!
    
    @IBOutlet weak var addValueContentWebView: UIWebView!
    
    @IBOutlet weak var addValueContentHeight: NSLayoutConstraint!
    
    //MARK: - 漸層用
    var gradientLayer: CAGradientLayer!
    
    
    //MARK: -
    var expand = false
    
    var disableBackToRoot = false
    
    var topMessageURL = ""
    
    var blURL: String = "http://doc.tstartel.com/BP/index.html"
    
    var shouldCleanUnbillInfo = false
    
    var didLoadAPIOnce: Bool = false
    
    var isGetSmartBannerAPI = false {
        didSet {
//            apiResponded()
        }
    }
    var isQueryUnbillInfoAPI = false {
        didSet {
            apiResponded()
        }
    }
    var isGetGroupBuyAPI = false {
        didSet {
//            apiResponded()
        }
    }
    
    var isQueryProjectAgreement = false {
        didSet {
//            apiResponded()
        }
    }
    
    var isVerifyDIY = false {
        didSet {
//            apiResponded()
        }
    }
    
    var isCallGetBLURL = false {
        didSet {
//            apiResponded()
        }
    }
    
    var isRelationQueryAPI = false {
        didSet {
//            apiResponded()
        }
    }
    
    enum EnumTableViewTag: Int {
        case Test
        case BestLow
        case ProjectContract
    }
    
    enum EnumWebViewTag: Int {
        case CarrierBillingTitle
        case CarrierBillingContent
        case GroupBuyTitle
        case GroupBuyContent
        case RelationTitle
        case RelationContent
        case AddValueTitle
        case AddValueContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(MyAccountViewController.backToRootViewController), name: NSNotification.Name(rawValue: MY_ACCOUNT), object: nil)
        
        vasTableView.rowHeight = UITableViewAutomaticDimension
        vasTableView.estimatedRowHeight = 44
        vasTableHeight.constant = 0

        self.groupBuyTitleWebView.tag = EnumWebViewTag.GroupBuyTitle.rawValue
        self.groupBuyContentWebView.tag = EnumWebViewTag.GroupBuyContent.rawValue
        relationTitleWebView.tag = EnumWebViewTag.RelationTitle.rawValue
        relationContentWebView.tag = EnumWebViewTag.RelationContent.rawValue
        addValueTitleWebView.tag = EnumWebViewTag.AddValueTitle.rawValue
        addValueContentWebView.tag = EnumWebViewTag.AddValueContent.rawValue
        carrierBillingTitleWebView.tag = EnumWebViewTag.CarrierBillingTitle.rawValue
        carrierBillingContentWebView.tag = EnumWebViewTag.CarrierBillingContent.rawValue
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        groupBuyView.isHidden = true
        groupBuyViewHeightConstraint.constant = 0
        
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        myScrollView.setContentOffset(CGPoint(x: myScrollView.contentOffset.x, y: 0), animated: true)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP03", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "我的帳戶")
        
        //reset group buy view
        self.groupBuyView.isHidden = true
        
        relationView.isHidden = true
        relationViewHeight.constant = 0
        relationViewTopGap.constant = 0
        
        self.addValueServiceView.alpha = 0
        addValuePromoView.alpha = 0
        
        if !LoginData.isLogin() {
            accountInfoButton.isEnabled = false
            accountInfoButton.tintColor = UIColor.clear
            
            messageButton.isEnabled = false
            let messageButtonImage = messageButton.image?.withRenderingMode(UIImageRenderingMode.automatic)
            messageButton.image = messageButtonImage
            messageButton.tintColor = UIColor.clear
            
            locationButton.isEnabled = false
            let locationButtonImage = locationButton.image?.withRenderingMode(UIImageRenderingMode.automatic)
            locationButton.image = locationButtonImage
            locationButton.tintColor = UIColor.clear
            loginView.isHidden = false
        }
        else {
            
            //hide navigation bar button
            accountInfoButton.isEnabled = false
            accountInfoButton.tintColor = UIColor.clear
            
            messageButton.isEnabled = false
            let messageButtonImage = messageButton.image?.withRenderingMode(UIImageRenderingMode.automatic)
            messageButton.image = messageButtonImage
            messageButton.tintColor = UIColor.clear
            
            locationButton.isEnabled = false
            let locationButtonImage = locationButton.image?.withRenderingMode(UIImageRenderingMode.automatic)
            locationButton.image = locationButtonImage
            locationButton.tintColor = UIColor.clear
            
            loginView.isHidden = true
            
            carrierBillingServiceView.isHidden = false
            carrierBillingServiceViewHeight.constant = 89
            
        }
        
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
        
//        checkVIPStatusAndChangeSkin()
        
        
        //設定畫面的美工
        btnList.forEach { (btn) in
            btn.layer.borderColorFromUIColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1)
        }
        
        iconBackgroundView.forEach { (view) in
            view.layer.cornerRadius = view.frame.width / 2
        }
        
        if LoginData.isLogin() {
            nickName.text = LoginData.sharedLoginData().custProfile_displayNickname
            TSTAPP2_Utilities.showPKHUD()
            loadAPI()
            
        }
        else {
            setDummyData()
            setDummyAddValueInfo()
            
            //callGetGroupBuyAPI
            self.groupBuyView.isHidden = true
            self.groupBuyViewHeightConstraint.constant = 0
            
            //callRelationQueryAPI
            self.relationView.isHidden = true
            self.relationViewHeight.constant = 0
            self.relationViewTopGap.constant = 0
            
        }
        if nickName.frame.height > 0 {
            topViewHeight.constant = 102 + nickName.frame.height + greetingWord.frame.height
        }
        
        
        // 隱藏 navigationBar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        TSTAPP2_Utilities.checkIfOnlyFacebookLogin()
        
        let _ = TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self)
        
        if LoginData.isLogin() {
        
            TSTAPP2_Utilities.showPKHUD()
            callGetGroupBuyAPI()
            callPrepaidFamilyAPI()
            callGetAppliedAddValueSvc()
            callCarrierBillingAPI()
            
        }
        else {
            
            setDummyAddValueInfo()
            
            //callGetGroupBuyAPI
            self.groupBuyView.isHidden = true
            self.groupBuyViewHeightConstraint.constant = 0
            
            //callRelationQueryAPI
            self.relationView.isHidden = true
            self.relationViewHeight.constant = 0
            self.relationViewTopGap.constant = 0
            
        }
        
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP03", action: "")
        
        // 顯示navigationBar
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
//        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
//        UISkinChange.sharedInstance.changeUIColor(theObject: buyButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: relationButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: groupBuyMoreButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: groupBuyShareViaFBButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: groupBuyShareViaLineButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: applyButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: projectContractButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: myContractButton, mode: ButtonColorMode.button_purple.rawValue)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

    }
    
//    MARK: -
    
    @objc func backToRootViewController() {
        print("backToRootViewController")
        if !disableBackToRoot {
            let poppedVCArray = navigationController?.popToRootViewController(animated: false)
            
            if poppedVCArray == nil {
                myScrollView.setContentOffset(CGPoint(x: myScrollView.contentOffset.x, y: 0), animated: true)
                
                let avc = AutomaticViewChange.sharedInstance
                avc.myAccountVC = self
            }
        }
    }
    
    func setDummyAddValueInfo() {
        addValueServiceView.isHidden = false
        
        addValueServiceHeight.constant = 45
        addValueAppliedViewHeight.constant = AddValueContentViewHeight
        addValueAppliedView.isHidden = false
        
        self.addValueServiceHeight.constant += 2 * (44 + 1)
        
        var count: CGFloat = 0
        for _ in 0..<2 {
            let addValueTryView = UINib(nibName: "AddValueContent", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as! AddValueTryView
            if count == 0 {
                addValueTryView.textLabel.text = "LiTV頻道免費看30天"
            }
            else if count == 1 {
                addValueTryView.textLabel.text = "KKBOX免費聽30天"
            }
            addValueTryView.autoresizingMask = .flexibleWidth
            addValueTryView.frame = CGRect(x: 0, y: 1 + (count * (AddValueContentViewHeight + 1)), width: CGFloat(self.view.frame.width), height: AddValueContentViewHeight)
            addValueTryView.tryButton.tag = Int(count)
            addValueTryView.tryButton.setTitle(" 立即體驗 ", for: UIControlState.normal)
            addValueTryView.tryButton.addTarget(self, action: #selector(self.didTapAddValueTryButton(button:)), for: UIControlEvents.touchUpInside)
            addValueTryView.tryButton.layer.cornerRadius = 15
            addValueTryView.tryButton.layer.borderColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1).cgColor
            addValueTryView.tryButton.layer.borderWidth = 1
//            UISkinChange.sharedInstance.changeUIColor(theObject: addValueTryView.tryButton, mode: ButtonColorMode.button_purple.rawValue)
            
            self.addValuePromoView.addSubview(addValueTryView)
            count += 1
        }
        
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
            self.addValueServiceView.alpha = 1
        }, completion: { (isComplete: Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                self.addValueAppliedView.alpha = 1
                self.addValuePromoView.alpha = 1
            }, completion: { (isComplete: Bool) in
                
            })
        })
    }
    
    //MARK: - custom func
    func loadAPI() {
        
        if !didLoadAPIOnce {
            didLoadAPIOnce = true
            callGetGroupBuyAPI()
            callPrepaidFamilyAPI()
            callGetAppliedAddValueSvc()
            callCarrierBillingAPI()
        }
        else {
            TSTAPP2_Utilities.hidePKHUD()
        }
        
    }
    
    func setDummyData() {
        nickName.text = "親愛的用戶"
        self.relationViewTopGap.constant = 0
        self.addValueTitleWebView.loadHTMLString("<span style='font-family:sans-serif;'><font color='#333333'>加值服務</font><br><big><big><b><font color='#333333'>影音娛樂</font></b></big></big></span>", baseURL: nil)
        self.addValueContentWebView.loadHTMLString("<span style='font-family:sans-serif;'><font color='#333333'>沒有試用?怎麼保證你會喜歡?<br/>我們以最優惠的價格，提供最頂級的影音內容，讓您免費體驗，並擁有充裕的時間選擇。</font></span>", baseURL: nil)
        self.carrierBillingTitleWebView.loadHTMLString("<span style='font-family:sans-serif;'><font color='#333333'>電信帳單代收服務</font><br><big><big><b><font color='#333333'>Google/Apple小額付款</font></b></big></big></span>", baseURL: nil)
        self.carrierBillingContentWebView.loadHTMLString("<span style='font-family:sans-serif;'><font color='#333333'>取代您的信用卡，購買APP或在APP內消費，免收海外手續費，自控額度，電信帳單輕鬆管理。</font></span>", baseURL: nil)
    }
    
    func openShareActivityVC(activityItems:[Any]) {
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
//    MARK: - IBAction
    @IBAction func didTapAccountInfoButton(_ sender: Any) {
        let aiVC = UIStoryboard.init(name: "AccountInformation", bundle: nil).instantiateViewController(withIdentifier: "AccountInformation")
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "會員中心",
                               contentType: "Function",
                               contentId: "AccountInfo",
                               customAttributes: [:])
    }
    
    @IBAction func didTapMessageButton(_ sender: Any) {
        let aiVC = UIStoryboard.init(name: "Message", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController")
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "訊息中心",
                               contentType: "Function",
                               contentId: "Message",
                               customAttributes: [:])
    }
    
    @IBAction func didTapStoreLocationButton(_ sender: Any) {
        let slVC = UIStoryboard.init(name: "StoreLocation", bundle: nil).instantiateViewController(withIdentifier: "StoreLocationViewController")
        navigationController?.pushViewController(slVC, animated: true)
        
        Answers.logContentView(withName: "服務據點",
                               contentType: "Function",
                               contentId: "StoreLocation",
                               customAttributes: [:])
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_MASK_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "我的帳戶", label: "登入", value: nil)
        
        if !TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self) {
        
            TSTAPP2_Utilities.pushToLoginViewController(withViewController: self)
        }
    }
    
    @IBAction func didTapAQButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_EC_APPLY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "我的帳戶", label: "門號申辦", value: nil)
        
        #if SIT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #elseif UAT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #else
            let urlString: String = "https://www.tstartel.com/mCWS/shop.php"
        #endif
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func didTapVASButton(_ sender: AnyObject) {
        let button = sender as! UIButton
        print("\(button.tag)")
    }
    
    @IBAction func didTapGroupBuyMore(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "A030303")
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "家族省", action: "了解更多", label: "\(LoginData.sharedLoginData().custProfile_msisdn)/\(LoginData.sharedLoginData().custProfile_contractId)", value: nil)
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "了解更多", urlString: GroupBuy.sharedInstance.moreUrl, viewController: self)
        
        Answers.logContentView(withName: "家族省-了解更多",
                               contentType: "AD",
                               contentId: GroupBuy.sharedInstance.moreUrl,
                               customAttributes: [:])

    }
    
    @IBAction func didTapGroupBuyShare(_ sender: Any) {
        openShareActivityVC(activityItems: [GroupBuy.sharedInstance.shareText])
    }
    
    @IBAction func didTapGroupBuyMemo(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_CGC_HIS")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "家族省", action: "優惠歷程", label: nil, value: nil)
        
        Answers.logContentView(withName: "家族省",
                               contentType: "function",
                               contentId: "GroupBuyMeno",
                               customAttributes: [:])
        
        let alert = UIAlertController(title: "優惠歷程", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.default) { (action: UIAlertAction) in
            alert.dismiss(animated: true, completion: {
                
            })
        }
        let storyBoard = UIStoryboard(name: "TStarWebView", bundle: nil)
        let webViewVC = storyBoard.instantiateViewController(withIdentifier: "TstarWebViewController") as! TstarWebViewController
        webViewVC.preferredContentSize.height = 300
        alert.setValue(webViewVC, forKey: "contentViewController")
        alert.addAction(action)
        self.present(alert, animated: true, completion: {
            webViewVC.contentWebView.backgroundColor = UIColor.clear
            webViewVC.contentWebView.scalesPageToFit = false
            webViewVC.contentWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(GroupBuy.sharedInstance.recordList.joined(separator: "<br /><p height='30'>"))</span>", baseURL: nil)
        })
    }
    
    @IBAction func didTapRelationButton(_ sender: Any) {
        let buttonURL = RelationQuery.sharedInstance.buttonUrl
        if buttonURL != "" {
            
            let relationQuery = RelationQuery.sharedInstance
            
            if relationQuery.isPrepaidFamily == "N" {
                
                UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "A030701")
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "親子卡專區", action: "立即前往了解", label: "\(LoginData.sharedLoginData().custProfile_msisdn)\(LoginData.sharedLoginData().custProfile_contractId)", value: nil)
                
                Answers.logContentView(withName: "親子專區-立即前往了解",
                                       contentType: "AD",
                                       contentId: buttonURL,
                                       customAttributes: [:])
                
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: buttonURL, viewController: self)
            }
            else if relationQuery.isPrepaidFamily == "Y" {
                UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "A030702")
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "親子卡專區", action: "立即前往專區", label: nil, value: nil)
                
                Answers.logContentView(withName: "親子專區-立即前往專區",
                                       contentType: "Function",
                                       contentId: buttonURL,
                                       customAttributes: [:])
                
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: buttonURL, appendToken: true, openBrowser: true, viewController: self)
            }
        }
    }
    
    @IBAction func didTapCarrierBillingButton(_ sender: Any) {
        
        Answers.logContentView(withName: "電信帳單代收",
                               contentType: "Function",
                               contentId: "CarrierBilling",
                               customAttributes: [:])
        
        TSTAPP2_API.sharedInstance.checkLock(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, showProgress: true, completion: { (dic, response) in
            if dic.stringValue(ofKey: "code") == "00000" {
                let checkLock = CheckLock.sharedInstance
                checkLock.setDataValue(withDic: dic)
                if checkLock.canAdjustment == "N" {
                    TSTAPP2_Utilities.showAlert(withMessage: checkLock.resultMessage, viewController: self)
                }
                else if checkLock.canAdjustment == "Y" {
                    self.performSegue(withIdentifier: "toCarrierBilling", sender: self)
                }
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (error) in
            
        }
    }
    
    @IBAction func didTapCarrierBillingSwitchButton(_ sender: Any) {
        self.performSegue(withIdentifier: "toCarrierBillingSetting", sender: self)
    }
    
    
    @IBAction func didTapAppliedAddValueButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_APLD_AVSVC")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "已申裝之服務", action: nil, label: nil, value: nil)
    }
    
    @objc func didTapAddValueTryButton(button: UIButton) {
        let tag = button.tag
        let addValuePromo = AppliedAddValue.sharedInstance.promoAddValueSvcList[tag]
        if addValuePromo.svcUrl != "" {
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: addValuePromo.svcUrl, viewController: self)
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: addValuePromo.svcUBAActionCode)
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: addValuePromo.svcGACode, action: nil, label: nil, value: nil)
        }
        
    }
    
    //MARK: AdvertisingDelegate
    func didTapAdvertisement(sender: Any?) {
        if let smartBanner = sender as? SmartBanner {
            print("didTapAdvertising \(smartBanner.url)")
            let urlString = smartBanner.url
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "智能推薦", action: "\(smartBanner.titleText);\(urlString)", label: nil, value: nil)
            
            UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                             action: "ACMP_\(SmartBannerList.sharedInstance.cmpId)_\(SmartBannerList.sharedInstance.cmpVersion)_\(smartBanner.displayType)_\(smartBanner.sort)",
                codeId: SmartBannerList.sharedInstance.cmpId,
                codeVersion: SmartBannerList.sharedInstance.cmpVersion,
                codeDisplayType: smartBanner.displayType,
                codeSort: smartBanner.sort)
            
            if smartBanner.action == AUTO_BROWSER {
                if let url = URL(string: urlString) {
                    UIApplication.shared.openURL(url)
                }
            }
            else {
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
            }
            
            Answers.logContentView(withName: "智能推薦",
                                   contentType: "AD",
                                   contentId: urlString,
                                   customAttributes: [:])
            
        }
    }
    
//    MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let currentTableView = EnumTableViewTag(rawValue: tableView.tag)!
        switch currentTableView {
        case .BestLow:
            fallthrough
        case .ProjectContract:
            return 1
        default:
            return 0
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        
        // Configure the cell...
        let currentTableView = EnumTableViewTag(rawValue: tableView.tag)!
        switch currentTableView {
        case .BestLow:
            cell = tableView.dequeueReusableCell(withIdentifier: "bestLowCell", for: indexPath)
            break
        case .ProjectContract:
            cell = tableView.dequeueReusableCell(withIdentifier: "projectContractCell", for: indexPath)
            break
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "vasCell", for: indexPath)
            (cell as! VasCell).vasButton.tag = indexPath.row
            (cell as! VasCell).vasButton.addTarget(self, action: #selector(MyAccountViewController.didTapVASButton(_:)), for: UIControlEvents.touchUpInside)
            break
        }
        
        return cell
    }
    
//    MARK: - Table view delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 44
        
        let currentTableView = EnumTableViewTag(rawValue: tableView.tag)!
        switch currentTableView {
        case .BestLow:
            break
        case .ProjectContract:
            break
        default:
            let theCell = tableView.cellForRow(at: indexPath) as? VasCell
            if theCell != nil {
                if theCell!.expand {
                    height = CGFloat(vasCellExpandHeight)
                }
            }
            break
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let currentTableView = EnumTableViewTag(rawValue: tableView.tag)!
        switch currentTableView {
        case .BestLow:
            UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_BL")
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "保證最低價最近一期出帳明細", action: nil, label: nil, value: nil)
            self.performSegue(withIdentifier: "toBestLow", sender: self)
            break
        case .ProjectContract:
            self.performSegue(withIdentifier: "toProjectContract", sender: self)
            break
        default:
            //VAS TABLE
            let vasCell = tableView.cellForRow(at: indexPath)! as! VasCell
            vasCell.expand = !vasCell.expand
            if vasCell.expand {
                
            }
            else {
                
            }
            
            var x = 0
            for cell in tableView.visibleCells {
                if (cell as! VasCell).expand {
                    x += 1
                }
            }
            
            self.vasTableHeight.constant = 132 + CGFloat(x) * CGFloat(vasCellExpandHeight - vasCellHeight)
            self.view.setNeedsUpdateConstraints()
            
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
                tableView.beginUpdates()
                tableView.endUpdates()
            }, completion: { (isComplete: Bool) in
                
            })
            break
        }
    }
    
    // MARK: - WebViewDelegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let currentWebViewTag = EnumWebViewTag(rawValue: webView.tag)!
        switch currentWebViewTag {
        case .GroupBuyTitle:
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            groupBuyTitleHeightConstraint.constant = webView.scrollView.contentSize.height
            break
        case .GroupBuyContent:
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            groupBuyContentHeightConstraint.constant = groupBuyContentWebView.scrollView.contentSize.height
            groupBuyViewHeightConstraint.constant = groupBuyTitleHeightConstraint.constant + webView.scrollView.contentSize.height + groupBuyMoreButton.frame.height + (GroupBuy.sharedInstance.showMember ? 89 : 0) + 48
            break
        case .RelationTitle:
            relationTitleHeight.constant = relationTitleWebView.scrollView.contentSize.height
            break
        case .RelationContent:
            relationContentHeight.constant = webView.scrollView.contentSize.height

            self.relationViewHeight.constant = self.relationTitleHeight.constant + self.relationContentHeight.constant + self.relationButtonHeight.constant + 48
            self.relationViewTopGap.constant = 16
            break
        case .AddValueTitle:
            addValueTitleHeight.constant = addValueTitleWebView.scrollView.contentSize.height
            break
        case .AddValueContent:
            addValueContentHeight.constant = addValueContentWebView.scrollView.contentSize.height
            break
        case .CarrierBillingTitle:
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            carrierBillingTitleHeightConstraint.constant = webView.scrollView.contentSize.height
            break
        case .CarrierBillingContent:
            webView.frame.size.height = 1
            webView.frame.size = webView.sizeThatFits(.zero)
            carrierBillingContentHeightConstraint.constant = webView.scrollView.contentSize.height
            break
        }
    }
    
    // MARK: - API
    //家族省 api
    func callGetGroupBuyAPI() {
        let userData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.getGroupBuy(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, showProgress: false, completion: { (dic:Dictionary<String, Any>, URLResponse) in
            
            self.isGetGroupBuyAPI = true
            
            var displayGroupBuyView = false
            if dic.stringValue(ofKey: "code") == "00000" {
                if (dic["data"] as? Dictionary<String, Any>) != nil {
                    displayGroupBuyView = true
                }
            }
            
            if displayGroupBuyView {
                
                self.groupBuyView.alpha = 0
                
                let groupBuyData = GroupBuy.sharedInstance
                groupBuyData.setDataValue(withDic: dic)
                
                if groupBuyData.showCGC {
                    self.groupBuyView.isHidden = false
                    self.groupBuyTitleWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(groupBuyData.subtitle)<br>\(groupBuyData.title)</span>", baseURL: nil)
                    self.groupBuyContentWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(groupBuyData.content)</span>", baseURL: nil)
                }else {
                    self.groupBuyView.isHidden = true
                    self.groupBuyViewHeightConstraint.constant = 0
                    self.relationViewTopGap.constant = 0
                }
                
                //設定家族省三個按鈕置中的位置
                var groupBuyButtons:[NSLayoutConstraint] = []
                groupBuyButtons.append(self.groupBuyMoreButtonCenterXConstraint)
                
                if groupBuyData.showRecordButton {
                    self.groupBuyRecordButton.isHidden = false
                    groupBuyButtons.append(self.groupBuyRecordButtonCenterXConstraint)

                }else {
                    self.groupBuyRecordButton.isHidden = true
                }
                
                if groupBuyData.showShareButton {
                    self.groupBuyShareButton.isHidden = false
                    groupBuyButtons.append(self.groupBuyShareButtonCenterXConstraint)

                }else {
                    self.groupBuyShareButton.isHidden = true
                }
                
                //判斷顯示人數
                if groupBuyData.showMember {
                    
                    self.groupBuyMemberView.isHidden = false
                    self.groupBuyMemberViewHeight.constant = 89
                    self.memberCount.text = groupBuyData.memberCount
                    self.memberRemainder.text = groupBuyData.memberRemainder
                }else {
                    
                    self.groupBuyMemberView.isHidden = true
                    self.groupBuyMemberViewHeight.constant = 0
                }
                
                var groupBuyButtonCenterX:[CGFloat] = []
                switch groupBuyButtons.count {
                case 3:
                    let margin = (self.groupBuyView.frame.size.width - (self.groupBuyMoreButton.frame.size.width*3))/4
                    groupBuyButtonCenterX = [(self.groupBuyMoreButton.frame.size.width+margin) * -1, 0, (self.groupBuyMoreButton.frame.size.width+margin)]
                    break
                case 2:
                    let margin = (self.groupBuyView.frame.size.width - (self.groupBuyMoreButton.frame.size.width*2))/3
                    groupBuyButtonCenterX = [(self.groupBuyMoreButton.frame.size.width+margin) * -1 / 2, (self.groupBuyMoreButton.frame.size.width+margin) / 2]
                    break
                default:
                    groupBuyButtonCenterX = [0]
                    break
                }
                
                for (buttonIdx, centerXConstraint) in groupBuyButtons.enumerated() {
                    centerXConstraint.constant = groupBuyButtonCenterX[buttonIdx]
                }
                
            } else {
                self.groupBuyView.isHidden = true
            }
            
            
            if !self.groupBuyView.isHidden {
                UIView.animate(withDuration: 0.5, animations: { 
                    self.groupBuyView.alpha = 1
                })
            }
            
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
        }) { (Error) in
            print("error:")
            print(Error)
            self.groupBuyViewHeightConstraint.constant = 0
            self.groupBuyView.isHidden = true
            self.relationViewTopGap.constant = 0
            self.isGetGroupBuyAPI = true
            
        }
    }
    
    func callPrepaidFamilyAPI() {
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.prepaidFamily(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, custId: loginData.custProfile_custId, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            self.isRelationQueryAPI = true
            if dic.stringValue(ofKey: "code") == "00000" {
                
                let relationQuery = RelationQuery.sharedInstance
                relationQuery.setDataValue(withDic: dic)
                
                
                if relationQuery.showPFS == "Y" {
                    self.relationView.isHidden = false
                    self.relationTitleWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(relationQuery.subtitle)<br>\(relationQuery.title)</span>", baseURL: nil)
                    self.relationContentWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(relationQuery.content)</span>", baseURL: nil)
                    self.relationButton.setTitle(relationQuery.buttonText, for: .normal)
                }
                else {
                    self.relationView.isHidden = true
                    self.relationViewHeight.constant = 0
                    self.relationViewTopGap.constant = 0
                }
                
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
                
            }
            else {
                self.relationView.isHidden = true
            }
        }) { (error: Error) in
            self.isRelationQueryAPI = true
            
            self.relationView.isHidden = true
            self.relationViewHeight.constant = 0
            self.relationViewTopGap.constant = 0
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    func callGetAppliedAddValueSvc () {
        for subView in self.addValuePromoView.subviews {
            subView.removeFromSuperview()
        }
        addValueAppliedView.isHidden = true
        addValueAppliedView.alpha = 0
        
        TSTAPP2_API.sharedInstance.getAppliedAddValueSvc(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                self.addValueServiceView.isHidden = false
                
                let appliedAddValue = AppliedAddValue.sharedInstance
                appliedAddValue.setDataValue(withDic: dic)
                
                self.addValueTitleWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(appliedAddValue.subtitle)<br>\(appliedAddValue.title)</span>", baseURL: nil)
                self.addValueContentWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(appliedAddValue.content)</span>", baseURL: nil)
                
                if appliedAddValue.appliedAddValueSvcList.count == 0 {
                    self.addValueServiceHeight.constant = 45 - AddValueContentViewHeight
                    self.addValueAppliedViewHeight.constant = 0
                    self.addValueAppliedView.isHidden = true
                }
                else {
                    self.addValueServiceHeight.constant = 45
                    self.addValueAppliedViewHeight.constant = AddValueContentViewHeight
                    self.addValueAppliedView.isHidden = false
                }
                
                let promoCount = CGFloat(appliedAddValue.promoAddValueSvcList.count)
                self.addValueServiceHeight.constant += promoCount * (44 + 1)
                
                var count: CGFloat = 0
                for promoAddValue in appliedAddValue.promoAddValueSvcList {
                    let addValueTryView = UINib(nibName: "AddValueContent", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as! AddValueTryView
                    addValueTryView.textLabel.text = promoAddValue.svcName
                    addValueTryView.autoresizingMask = .flexibleWidth
                    addValueTryView.frame = CGRect(x: 0, y: 1 + (count * (AddValueContentViewHeight + 1)), width: CGFloat(self.addValueView.frame.width), height: AddValueContentViewHeight)
                    addValueTryView.tryButton.tag = Int(count)
                    addValueTryView.tryButton.setTitle(" \(promoAddValue.buttonText) ", for: UIControlState.normal)
                    addValueTryView.tryButton.layer.cornerRadius = 15
                    addValueTryView.tryButton.layer.borderColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1).cgColor
                    addValueTryView.tryButton.layer.borderWidth = 1
                    
                    addValueTryView.tryButton.addTarget(self, action: #selector(self.didTapAddValueTryButton(button:)), for: UIControlEvents.touchUpInside)
                    
                    self.addValuePromoView.addSubview(addValueTryView)
                    count += 1
                }
                
                self.view.setNeedsUpdateConstraints()
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                    self.view.layoutIfNeeded()
                    self.addValueServiceView.alpha = 1
                }, completion: { (isComplete: Bool) in
                    UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                        self.addValueAppliedView.alpha = 1
                        self.addValuePromoView.alpha = 1
                    }, completion: { (isComplete: Bool) in
                        
                    })
                })
            }
            else {
                self.addValueServiceView.isHidden = true
                self.addValueServiceHeight.constant = 0
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
            }
            
        }, failure: { (error: Error) in
            self.addValueServiceView.isHidden = true
            self.addValueServiceHeight.constant = 0
            self.addValueView.isHidden = true
            self.addValueView.frame.size.height = 0
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
        })

    }
    
    func callCarrierBillingAPI() {
        let checkLock = CheckLock.sharedInstance
        TSTAPP2_API.sharedInstance.checkLock(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, showProgress: true, completion: { (dic, response) in
            if dic.stringValue(ofKey: "code") == "00000" {
                checkLock.setDataValue(withDic: dic)
                self.carrierBillingTitleWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(checkLock.subtitle)<br>\(checkLock.title)</span>", baseURL: nil)
                self.carrierBillingContentWebView.loadHTMLString("<span style='font-family:sans-serif;'>\(checkLock.content)</span>", baseURL: nil)
            }
            else {
                
            }
        }) { (error) in
            self.carrierBillingVIew.isHidden = true
            self.carrierBillingVIew.frame.size.height = 0
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
        }
    }
    
    func apiResponded() {
        if isQueryUnbillInfoAPI {

            TSTAPP2_Utilities.hidePKHUD()
            isGetSmartBannerAPI = false
            isQueryUnbillInfoAPI = false
            isGetGroupBuyAPI = false
            isQueryProjectAgreement = false
            isCallGetBLURL = false
            isVerifyDIY = false
            isRelationQueryAPI = false
            let avc = AutomaticViewChange.sharedInstance
            avc.myAccountVC = self
        }
    }
}
