//
//  MyBillBaseViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/9/22.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class MyBillBaseViewController: TSUIViewController {

    @IBOutlet weak var containerViewBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if tabBarController != nil {
            containerViewBottom.constant = -tabBarController!.tabBar.frame.height
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
