//
//  MyBillViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/24.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import EventKit
import EventKitUI
import Crashlytics


//Roam CELL
let myBillCellHeight = 45
let myBillCellExpandHeight = 540

class MyBillCell: UITableViewCell {
    
    @IBOutlet weak var msisdn: UILabel!
    
    @IBOutlet weak var billDate: UILabel!
    
    @IBOutlet weak var expiredDate: UILabel!
    
    @IBOutlet weak var previousPay: UILabel!
    
    @IBOutlet weak var previousPayed: UILabel!
    
    @IBOutlet weak var append: UILabel!
    
//    @IBOutlet weak var overage: UILabel!
    
    @IBOutlet weak var pay: UILabel!
    
    @IBOutlet weak var itemDesc: UILabel!
    
    @IBOutlet weak var revenueMny: UILabel!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var cellContentView: UIView!
    
    @IBOutlet weak var itemView: UIView!
    
    @IBOutlet weak var itemViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var arrow: UIImageView!
    
    @IBOutlet weak var noticeLabel: UILabel!
    
    @IBOutlet weak var noticeButton: UIButton!
    
    var expand = false
}

class MyBillViewController: TSUIViewController, UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate,UIPickerViewDelegate , UIPickerViewDataSource {

    @IBOutlet weak var segment: UISegmentedControl!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var billView: UIView!
    
    @IBOutlet weak var bestLowView: UIView!
    
    @IBOutlet weak var bestLowViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bestLowViewTopGap: NSLayoutConstraint!
    
    @IBOutlet weak var remindButton: UIButton!
    
    @IBOutlet weak var unpaid: UILabel!
    
    @IBOutlet weak var paymentExpiredDate: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var paymentButton: UIButton!
    
    @IBOutlet weak var noPaymentButton: UIButton!
    
    @IBOutlet weak var remindTextField: UITextField!
    
    @IBOutlet weak var loginView: UIView!
    
    @IBOutlet var iconBackgroundView: [UIView]!
    
    var billingDetail : Array<BillingDetail> = []
    var remindDay : [String] = []
    var shouldPerformSegue = false
    
    var disableBackToRoot = false
    
    @IBOutlet weak var informationView: UIView!
    
    @IBOutlet weak var informationLabel: UILabel!
    
    @IBOutlet weak var nickName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(MyBillViewController.backToRootViewController), name: NSNotification.Name(rawValue: MY_BILL), object: nil)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.delegate = self
        tableView.dataSource = self
       
        setPickerData()
//        isNeedLogin()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        //UI美工
        paymentButton.layer.borderWidth = 1
        paymentButton.layer.borderColor = UIColor.purple.cgColor
        remindButton.layer.borderWidth = 1
        remindButton.layer.borderColor = UIColor.purple.cgColor
        noPaymentButton.isHidden = true
        iconBackgroundView.forEach { (view) in
            view.layer.cornerRadius = view.frame.width / 2
        }
        
        if LoginData.sharedLoginData().isBL != "Y" {
            bestLowView.isHidden = true
            bestLowViewHeight.constant = 0
            bestLowViewTopGap.constant = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP04", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "我的帳單")
        
        checkVIPStatusAndChangeSkin()
        
        // 隱藏 navigationBar
        self.navigationController?.isNavigationBarHidden = true
        
        informationView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        TSTAPP2_Utilities.checkIfOnlyFacebookLogin()
        
        let isMaintain = TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self)
        
        if !isMaintain {
            isNeedLogin()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // 顯示 navigationBar
        self.navigationController?.isNavigationBarHidden = false
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP04", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
//        UISkinChange.sharedInstance.changeUIColor(theObject: paymentButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    func isNeedLogin() {
        
        if LoginData.isLogin() {
            nickName.text = LoginData.sharedLoginData().custProfile_displayNickname
            loginView.isHidden = true
            callQueryBillAPI()
        }
        else {
            nickName.text = "親愛的用戶"
            loginView.isHidden = false
            informationView.isHidden = true
            unpaid.text = "388 元"
            paymentExpiredDate.text =  "2017 / 08 / 29"
            paymentButton.setTitle("請立即繳款", for: .normal)
            paymentButton.isHidden = false
            noPaymentButton.isHidden = true
            
            let dummyBillingInfo = BillInfo.sharedInstance
            let dummyDic = ["code": "00000",
                            "data":[
                
                "billingDetail" :[["billDate": "201708",
                            "paymentEndDate": "",
                            "mnyLastpay": "0",
                            "mnyLast": "0",
                            "mnyNow": "0",
                            "mnyPay": "0"],
                            ["billDate": "201707",
                "paymentEndDate": "",
                "mnyLastpay": "0",
                "mnyLast": "0",
                "mnyNow": "0",
                "mnyPay": "0"],["billDate": "201706",
                "paymentEndDate": "",
                "mnyLastpay": "0",
                "mnyLast": "0",
                "mnyNow": "0",
                "mnyPay": "0"],["billDate": "201705",
                "paymentEndDate": "",
                "mnyLastpay": "0",
                "mnyLast": "0",
                "mnyNow": "0",
                "mnyPay": "0"],["billDate": "201704",
                "paymentEndDate": "",
                "mnyLastpay": "0",
                "mnyLast": "0",
                "mnyNow": "0",
                "mnyPay": "0"],["billDate": "201703",
                "paymentEndDate": "",
                "mnyLastpay": "0",
                "mnyLast": "0",
                "mnyNow": "0",
                "mnyPay": "0"]]]] as [String : Any]
            
            dummyBillingInfo.setDataValue(withDic: dummyDic)
            
            billingDetail = dummyBillingInfo.billingDetail
            tableView.reloadData()
            
        }

    }

    //MARK: -
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return shouldPerformSegue
    }
    
    //MARK: - API
    func callQueryBillAPI() {
        
        TSTAPP2_API.sharedInstance.queryBill(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, custId: LoginData.sharedLoginData().custProfile_custId, accountId: LoginData.sharedLoginData().custProfile_accountId, showProgress: true, completion: {(dic :Dictionary<String, Any>, response :URLResponse) in
            
            let bill = BillInfo.sharedInstance
            bill.setDataValue(withDic: dic)
            
            if dic.stringValue(ofKey: "code") == "00000" && bill.billingMessage == "" {
                
                self.informationView.isHidden = true
                
                self.billingDetail = bill.billingDetail
                
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let pay = Float(bill.totDebt) ?? 0
                
                self.unpaid.text = "\(numberFormatter.string(from: NSNumber(value: pay))!) 元"
                
                if bill.showPaymentButton == "Y" {
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyyMMdd"
                    let date = dateFormatter.date(from: bill.dueDate)
                    dateFormatter.dateFormat = "yyyy / MM / dd"
                    if date != nil {
                        self.paymentExpiredDate.text = dateFormatter.string(from: date!)
                    }
                    else {
                        self.paymentExpiredDate.text = bill.dueDate
                    }
                    self.paymentButton.setTitle(bill.paymentText, for: .normal)
                    self.paymentButton.isHidden = false
                    self.noPaymentButton.isHidden = true
                    
                    self.shouldPerformSegue = true
                }
                else {
                    self.shouldPerformSegue = false
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyyMMdd"
                    let date = dateFormatter.date(from: bill.dueDate)
                    dateFormatter.dateFormat = "yyyy/MM/dd"
                    if date != nil {
                        self.paymentExpiredDate.text = dateFormatter.string(from: date!)
                    }
                    else {
                        self.paymentExpiredDate.text = bill.dueDate
                    }
                    
                    self.paymentButton.isHidden = true
                    self.noPaymentButton.isHidden = false
                    self.noPaymentButton.setTitle(bill.paymentText, for: .normal)
                }
                
                self.tableViewHeight.constant = CGFloat(bill.billingDetail.count) * CGFloat(myBillCellHeight)
                self.tableView.reloadData()
            }
            else {
                var infoText = ""
                
                if dic.stringValue(ofKey: "code") == "00000" && bill.billingMessage != ""{
                    infoText = bill.billingMessage
                }
                else {
                    infoText = dic.stringValue(ofKey: "message")
                }
                
                self.informationView.isHidden = false
                self.informationLabel.text = infoText
                
                self.shouldPerformSegue = false
            }
            
            let avc = AutomaticViewChange.sharedInstance
            avc.myBillVC = self
            
        }, failure: {(Error) in
            self.informationView.isHidden = false
        })
    }

    //MARK: 
    @objc func backToRootViewController() {
        print("backToRootViewController")
        if !disableBackToRoot {
            let popedVCArray = navigationController?.popToRootViewController(animated: false)
            
            if popedVCArray == nil {
                scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: 0), animated: true)
                
                let avc = AutomaticViewChange.sharedInstance
                avc.myBillVC = self
            }
        }
    }
    
    func setPickerData(){
        
        for i in 1...30 {
            remindDay.append(String(i))
        }
        
        remindTextField.delegate = self
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let done = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let cancel = UIBarButtonItem(barButtonSystemItem:.cancel, target: self, action: #selector(self.didTapCancel(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,done,cancel], animated: false)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        remindTextField.inputView = pickerView
        remindTextField.inputAccessoryView = pickBar;
    }
    
    //MARK: - IBAction
    
    @IBAction func didTapAccountInfoButton(_ sender: Any) {
        let aiVC = UIStoryboard.init(name: "AccountInformation", bundle: nil).instantiateViewController(withIdentifier: "AccountInformation")
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "會員中心",
                               contentType: "Function",
                               contentId: "AccountInfo",
                               customAttributes: [:])
    }
    
    @IBAction func didTapMessageButton(_ sender: Any) {
        let aiVC = UIStoryboard.init(name: "Message", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController")
        navigationController?.pushViewController(aiVC, animated: true)
        
        Answers.logContentView(withName: "訊息中心",
                               contentType: "Function",
                               contentId: "Message",
                               customAttributes: [:])
    }
    
    @IBAction func didTapStoreLocationButton(_ sender: Any) {
        let slVC = UIStoryboard.init(name: "StoreLocation", bundle: nil).instantiateViewController(withIdentifier: "StoreLocationViewController")
        navigationController?.pushViewController(slVC, animated: true)
        
        Answers.logContentView(withName: "服務據點",
                               contentType: "Function",
                               contentId: "StoreLocation",
                               customAttributes: [:])
    }
    
    @IBAction func didTapLoginButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP04", action: "AA_MASK_LOGIN")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "我的帳單", label: "登入", value: nil)
        
        if !TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self) {
            TSTAPP2_Utilities.pushToLoginViewController(withViewController: self)
        }
    }
    
    @IBAction func didTapBestLowButton(_ sender: UIButton) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP03", action: "AA_BL")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "保證最低價最近一期出帳明細", action: nil, label: nil, value: nil)
        self.performSegue(withIdentifier: "toBestLow", sender: self)
    }
    
    
    @IBAction func didTapAQButton(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP04", action: "AA_EC_APPLY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "登入遮罩頁", action: "我的帳單", label: "門號申辦", value: nil)
        
        #if SIT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #elseif UAT
            let urlString: String = "https://uattsp.tstartel.com/mCWS/shop.php"
        #else
            let urlString: String = "https://www.tstartel.com/mCWS/shop.php"
        #endif
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @objc func didTapDone(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
        
        let eventStore = EKEventStore()
        let calendar = eventStore.defaultCalendarForNewEvents
        let event = EKEvent(eventStore: eventStore)
        
        event.calendar = calendar
        event.title = "台灣之星繳費提醒"
        event.notes = "台灣之星敬上"
        event.url = NSURL(string: "http://www.tstartel.com") as URL?
        
        let day = Int(remindTextField.text!)! < 10 ? "0\(remindTextField.text!)" : "\(remindTextField.text!)"
        let date = "\(TSTAPP2_Utilities.nowDateFromGMT(dateFormatter: "yyyy/MM"))/\(day)"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "zh_TW")
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        event.startDate = dateFormatter.date(from: date)!
        event.endDate = dateFormatter.date(from: date)!.addingTimeInterval(60*60*24)
        
        let rule = EKRecurrenceRule(recurrenceWith: .monthly, interval: 1, end: EKRecurrenceEnd.init(occurrenceCount: 12))
        event.addRecurrenceRule(rule)
        
        do {
            try eventStore.save(event, span: .thisEvent)
        } catch let e as NSError {
            print(e)
            return
        }
        
        TSTAPP2_Utilities.showAlert(withMessage: "行事曆提醒功能已經設定完成。", viewController: self)
    }
    
    @objc func didTapCancel(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapRemind(_ sender: Any) {
        
        let eventStore = EKEventStore()
    
        eventStore.requestAccess(to: .event, completion: {(granted, error) in
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP04", action: "AA_CALENDAR")
            
            DispatchQueue.main.async {
                
                if error != nil {
                    // 錯誤
                } else if !granted {
                    // 拒絕權限
                    TSTAPP2_Utilities.showAlert(withMessage: "存取權限不足導致無法使用,請於設定->隱私權->行事曆中開啟", viewController: self)
                } else {
                    // 有權限
                    let action = UIAlertAction(title: "確定", style: .default) { (UIAlertAction) in
                        self.remindTextField.becomeFirstResponder()
                    }
                    
                    TSTAPP2_Utilities.showAlert(withMessage: "此功能可將當月的「繳款到期日提醒」\n同步加入您的手機行事曆，您可自行於手機行事曆修改每月提醒日期\n或是刪除此提醒。",action: action, viewController: self)
                }
            }
        })
    }
    
    @IBAction func didTapPayment(_ sender: Any) {
        
        if LoginData.sharedLoginData().custProfile_isMainMsisdn == "Y" {
            UbaRecord.sharedInstance.sendUba(withPage: "APP04", action: "A0401")
            
            let alert = UIAlertController(title: "選擇付款方式", message: nil, preferredStyle: .actionSheet)
            
            let barCode = UIAlertAction(title: "超商繳費條碼", style: .default, handler: { (UIAlertAction) in
                self.performSegue(withIdentifier: "toPaymentBarcode", sender: nil)
            })
            let bank = UIAlertAction(title: "銀行帳戶繳款", style: .default, handler: { (UIAlertAction) in
            
                self.performSegue(withIdentifier: "toPaymentBank", sender: nil)
            })
            let credit = UIAlertAction(title: "信用卡繳款", style: .default, handler: { (UIAlertAction) in
            
                self.performSegue(withIdentifier: "toPaymentCreditCard", sender: nil)
            })
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            
            alert.addActions(array: [barCode, credit, bank, cancel])
            alert.show(viewController: self)
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "此門號目前已申請合併帳單，請以主門號登入服務。", viewController: self)
        }
    }
    
    @objc func didTapNoticeButton(button: UIButton) {
        let urlString = billingDetail[button.tag].detailUrl
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP04", action: "AA_DOWN_BIL_DET")
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "下載通話明細", action: nil, label: nil, value: nil)
        }
    }
    
    //MARK: - PickDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
       return remindDay.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return remindDay[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        remindTextField.text = remindDay[row]
    }
    
    //MARK: - tableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = CGFloat(myBillCellHeight)
        
        if tableView.tag == 0 {
            let theCell = tableView.cellForRow(at: indexPath) as? MyBillCell
            if theCell != nil {
                if theCell!.expand {
                   height += (theCell?.cellContentView.frame.height)!
                    
                }
            }
        }
        
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if tableView.tag == 0 {
            let theCell = tableView.cellForRow(at: indexPath)! as! MyBillCell
            theCell.expand = !theCell.expand
            
            theCell.arrow.image = theCell.expand ? UIImage(named: "icon_arrow_up") : UIImage(named: "icon_arrow_down")
            
            var tableHeight: CGFloat = 0
            for cell in tableView.visibleCells {
//                if cell.tag != theCell.tag && (cell as! MyBillCell).expand {
//                    (cell as! MyBillCell).expand = false
//                    (cell as! MyBillCell).arrow.image = (cell as! MyBillCell).expand ? UIImage(named: "icon_arrow_up") : UIImage(named: "icon_arrow_down")
//                }
                
                tableHeight += CGFloat(myBillCellHeight)
                if (cell as! MyBillCell).expand {
                    tableHeight += (cell as! MyBillCell).cellContentView.frame.height
                }
            }
            
            tableViewHeight.constant = tableHeight
            self.view.setNeedsUpdateConstraints()
                        
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
                tableView.beginUpdates()
                tableView.endUpdates()
            }, completion: { (isComplete: Bool) in
                
            })
        }
    }
    
    //    MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return billingDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = MyBillCell()
        //myBillCell
        if tableView.tag == 0 {
        
            cell = tableView.dequeueReusableCell(withIdentifier: "myBillCell", for: indexPath) as! MyBillCell
            
            if !LoginData.isLogin() {
                cell.expand = false
            }
            
            let detail = billingDetail[indexPath.row]
            
            var year = ""
            var month = ""
            let str = detail.billDate as NSString
            
            if str != "" {
                
                year = str.substring(with: NSMakeRange(0, 4)) as String
                month = str.substring(with: NSMakeRange(4, 2)) as String
            }
            
            cell.tag = indexPath.row
            
            cell.title.text = "\(year) 年 \(month) 月"
            
            cell.msisdn.text = TSTAPP2_Utilities.encodeForSecurity(sourceString: LoginData.sharedLoginData().custProfile_msisdn, type: encodeForSecurityType.Phone)
            cell.billDate.text = detail.billDate
            cell.expiredDate.text = Int(detail.mnyPay) == 0 ? "本期無需繳款":detail.paymentEndDate
            cell.previousPay.text = detail.mnyLast
            cell.previousPayed.text = detail.mnyLastpay
            cell.append.text = detail.mnyNow
            cell.pay.text = detail.mnyPay
//            cell.overage.text = ""
            cell.noticeLabel.text = detail.detailText//"本服務僅提供簡要帳單資訊，實際帳單及通話明細，請至電腦版官網查詢(myTSTAR→我的帳單→帳單查詢與繳款)"
            
            if detail.detailType == "LINK" {
                cell.noticeButton.tag = indexPath.row
                cell.noticeButton.addTarget(self, action: #selector(didTapNoticeButton(button:)), for: UIControlEvents.touchUpInside)
            }
            else if detail.detailType == "TEXT" {
//                cell.noticeButton.isHidden = true
            }
            
            cell.arrow.image = UIImage(named: "icon_arrow_down")
            
            for subView in cell.itemView.subviews {
                subView.removeFromSuperview()
            }
            
            var y = CGFloat(10)
            
            for charge in detail.chargeItemList {
                
                let label = UILabel(frame: CGRect(x: 16, y: y, width: 290, height: 30))
                label.text = charge.itemGroupName
                label.font = UIFont.systemFont(ofSize: 17)
                label.textColor = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1)
                cell.itemView.addSubview(label)
                y  =  y + 30
                
                for item in charge.itemList {
                    
                    let cellWidth = self.view.frame.width
                    let revenueMnyLabelWidthPreset = CGFloat(80)
                    let revenueMnyLabel = UILabel(frame: CGRect(x: 0, y: y, width: revenueMnyLabelWidthPreset + 1, height: 20))
                    
                    let revenueValue = Float(item.revenueMny) ?? 0
                    let formattedString = NumberFormatter.localizedString(from: NSNumber(value: revenueValue), number: NumberFormatter.Style.decimal)
                    
                    revenueMnyLabel.text = formattedString
                    revenueMnyLabel.font = UIFont.systemFont(ofSize: 15)
                    revenueMnyLabel.textColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1)
                    revenueMnyLabel.textAlignment = .right
                    revenueMnyLabel.sizeToFit()
                    
                    revenueMnyLabel.frame = CGRect(x: cellWidth - 16 - revenueMnyLabel.frame.width, y: revenueMnyLabel.frame.origin.y, width: revenueMnyLabel.frame.width, height: revenueMnyLabel.frame.height)
                    
                    
                    cell.itemView.addSubview(revenueMnyLabel)
                    
                    let labelText = item.itemDesc
                    let attrLabelText = NSAttributedString(string: labelText, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)])
                    let constraintRect = CGSize(width: cellWidth - 24 - 16 - revenueMnyLabel.frame.width, height: .greatestFiniteMagnitude)
                    let boundingBox = attrLabelText.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
                    
                    
                    let itemLabel = UILabel(frame: CGRect(x: 24, y: y, width: cellWidth - 24 - 16 - revenueMnyLabel.frame.width, height: boundingBox.height))
                    itemLabel.text = item.itemDesc
                    itemLabel.font = UIFont.systemFont(ofSize: 15)
                    itemLabel.textColor = UIColor(red: 104/255, green: 104/255, blue: 104/255, alpha: 1)
                    itemLabel.numberOfLines = 0
                    itemLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
//                    itemLabel.sizeToFit()
                    cell.itemView.addSubview(itemLabel)
                    
                    cell.itemView.layoutIfNeeded()
                    
                    y  =  y + itemLabel.frame.height + 4
                }
            }
            
            cell.itemViewHeight.constant = y + 10
        }
        
        return cell
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if let pickerView = textField.inputView as? UIPickerView {
            if pickerView.numberOfRows(inComponent: 0) > 0 {
                pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
            }
        }
    }
}
