//
//  MyContractViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/9/14.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

class MyContractViewController: TSUIViewController {
    
    @IBOutlet weak var projectContractView: UIView!
    
    @IBOutlet weak var projectContractViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var projectContractTableView: UITableView!
    
    @IBOutlet weak var projectNameLabel: UILabel!
    
    @IBOutlet weak var projectNameTopSpace: NSLayoutConstraint!
    
    @IBOutlet weak var projectNameHeight: NSLayoutConstraint!
    
    @IBOutlet weak var projectContentTextView: UITextView!
    
    @IBOutlet weak var projectContentHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var projectButtonAreaHeight: NSLayoutConstraint!
    
    @IBOutlet weak var applyButton: UIButton!
    
    //續約列表
    @IBOutlet weak var renewView: UIView!
    
    @IBOutlet weak var renewMessage: UILabel!
    
    @IBOutlet weak var renewButton: UIButton!
    
    @IBOutlet weak var displayDiscInfo: UILabel!
    
    var isVerifyDIY: Bool = false {
        didSet {
            apiResponded()
        }
    }
    var isQueryProjectAgreement: Bool = false {
        didSet {
            apiResponded()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        
        if LoginData.isLogin() {
            TSTAPP2_Utilities.showPKHUD()
            projectNameLabel.text = LoginData.sharedLoginData().custProfile_projectName
            callVerifyDIYAPI()
            callGetContractAPI()
        }
        
        let loginData = LoginData.sharedLoginData()
        
        displayDiscInfo.text = loginData.ivrQualify_displayDiscInfo
        
        if loginData.ivrQualify_displayRenewalMessage != "" {
            self.renewMessage.text = loginData.ivrQualify_displayRenewalMessage
        }else {
            renewView.isHidden = true
        }
        
        if loginData.ivrQualify_isRenewal == "Y" {
            renewButton.isHidden = false
        }else {
            renewButton.isHidden = true
            renewButton.frame.size.width = 0
        }
        
        //美工
        renewButton.layer.borderColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1).cgColor
        renewButton.layer.borderWidth = 1
        applyButton.layer.borderColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1).cgColor
        applyButton.layer.borderWidth = 1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_CONT", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "我的專案", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "我的專案")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_CONT", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func checkVIPStatusAndChangeSkin() {
//        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
//        UISkinChange.sharedInstance.changeUIColor(theObject: applyButton, mode: ButtonColorMode.button_purple.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: projectContractButton, mode: ButtonColorMode.button_purple.rawValue)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
//    MARK: - IBAction
    @IBAction func didTapApplyButton(_ sender: Any) {
        
        if LoginData.sharedLoginData().isBL == "Y" {
            let urlString = UrlMap.sharedInstance.BL
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
            
            Answers.logContentView(withName: VerifyDIY.sharedInstance.buttonText,
                                   contentType: "function",
                                   contentId: urlString,
                                   customAttributes: [:])
        }
        else {
            
            let diyData = VerifyDIY.sharedInstance
            if diyData.isDIY {
                UbaRecord.sharedInstance.sendUba(withPage: "AP_CONT", action: "A0404")
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "變更我的自由配", label: "\(diyData.buttonLinkUrl)", value: nil)
                
                TSTAPP2_Utilities.pushToWebViewControler(withTitle: diyData.buttonText, urlString: diyData.buttonLinkUrl, appendToken: true, viewController: self)
                
                Answers.logContentView(withName: diyData.buttonText,
                                       contentType: "function",
                                       contentId: diyData.buttonLinkUrl,
                                       customAttributes: [:])
            } else {
                
                UbaRecord.sharedInstance.sendUba(withPage: "AP_CONT", action: "A0405")
                
                let storyBoard = UIStoryboard(name: "ProjectContract", bundle: nil)
                let contractChangeVC = storyBoard.instantiateViewController(withIdentifier: "changeContract")
                self.show(contractChangeVC, sender: self)
                
                Answers.logContentView(withName: diyData.buttonText,
                                       contentType: "function",
                                       contentId: "ChangeContract",
                                       customAttributes: [:])
            }
        }
    }
    
    @IBAction func didTapProjectContractButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_CONT", action: "AA_RT_QUALIFY")
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "續約資格查詢", label: nil, value: nil)
        
        self.performSegue(withIdentifier: "toProjectContract", sender: self)
        
        Answers.logContentView(withName: "續約資格查詢",
                               contentType: "function",
                               contentId: "ProjectContract",
                               customAttributes: [:])
    }
    
    @IBAction func didTapContractDateButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_CONT", action: "AA_CONT_TERM")
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "合約起訖日", label: nil, value: nil)
        
        let storyBoard = UIStoryboard(name: "ProjectContract", bundle: nil)
        let contractDateVC = storyBoard.instantiateViewController(withIdentifier: "ContractDateViewController")
        self.show(contractDateVC, sender: self)
        
    }
    
    @IBAction func didTapRenewButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_RT_QUALIFY", action: "AA_RENEWAL")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "續約資格查詢", label: "立即續約", value: nil)
        
        let urlString = UrlMap.sharedInstance.RT
        
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
    }
    
//    MARK: - API
    func callVerifyDIYAPI() {
        let userData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.verifyDIY(withMSISDN: userData.custProfile_msisdn, projectCode: userData.custProfile_projectCode, isBL: (userData.isBL == "Y"), showProgress: false, completion:  { (dic:Dictionary<String, Any>, URLResponse) in
            
            self.isVerifyDIY = true
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let diyData: VerifyDIY = VerifyDIY.sharedInstance
                diyData.setDataValue(withDic: dic)
                
                if diyData.showChangeButton == "Y" {
                    self.applyButton.isHidden = false
                    self.applyButton.setTitle(diyData.buttonText, for: .normal)
                }
                else if userData.isBL == "Y" {
                    self.applyButton.setTitle("專案重點", for: UIControlState.normal)
                    self.applyButton.isHidden = false
                }
                else {
                    self.applyButton.isHidden = true
                }
            }
            else {
                self.applyButton.isHidden = true
            }
            
        }) { (Error) in
            print("error: \(Error)")
            self.isVerifyDIY = true
            self.applyButton.isHidden = true
        }
    }
    
    func callGetContractAPI() {
        let userData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.queryProjectAgreement(withMSISDN: userData.custProfile_msisdn, projectCode: userData.custProfile_projectCode, showProgress: false, completion: { (dic:Dictionary<String, Any>, URLResponse) in
            
            self.isQueryProjectAgreement = true
            
            //            var errorMsg: String = ""
            if dic.stringValue(ofKey: "code") == "00000" {
//                self.hideProjectContractView(hide: false, animate: true)
                
                let apiData = dic["data"] as! Dictionary<String, Any>
                //                self.projectNameLabel.text = apiData["projectDesc"] as? String ?? userData.custProfile_projectName
                self.projectContentTextView.text = apiData["offerContent"] as? String ?? ""
                self.projectContentTextView.sizeToFit()
                self.projectContentHeightConstraint.constant = self.projectContentTextView.frame.height + 8
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
                return
            } else {
                //                errorMsg = dic["errorMessage"] as! String
//                self.hideProjectContractView(hide: true, animate: true)
            }
            
            //            if errorMsg != "" {
            //                let errorMsgAlertController = UIAlertController(title: nil, message: errorMsg, preferredStyle: .alert)
            //                errorMsgAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            //                self.present(errorMsgAlertController, animated: true, completion: nil)
            //            }
            
        }) { (Error) in
            print("error: \(Error)")
            self.isQueryProjectAgreement = true
//            self.hideProjectContractView(hide: true, animate: true)
        }
    }
    
    func apiResponded() {
        if isVerifyDIY && isQueryProjectAgreement {
            
            TSTAPP2_Utilities.hidePKHUD()
        }
    }

}
