//
//  NavigationItemSwizzling.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/17.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class TSUIViewController: UIViewController {
    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

protocol SelfAware: class {
    static func awake()
}

//class NothingToSeeHere {
//    
//    static func harmlessFunction() {
//        
//        let typeCount = Int(objc_getClassList(nil, 0))
//        let types = UnsafeMutablePointer<AnyClass?>.allocate(capacity: typeCount)
//        let safeTypes = AutoreleasingUnsafeMutablePointer<AnyClass?>(types)
//        objc_getClassList(safeTypes, Int32(typeCount))
//        for index in 0 ..< typeCount { (types[index] as? SelfAware.Type)?.awake() }
//        types.deallocate(capacity: typeCount)
//        
//    }
//    
//}

extension UIApplication {
    
    private static let runOnce: Void = {
//        NothingToSeeHere.harmlessFunction()
    }()
    
    override open var next: UIResponder? {
        // Called before applicationDidFinishLaunching
        UIApplication.runOnce
        return super.next
    }
    
}

fileprivate let swizzling: (UINavigationItem.Type) -> () = { navigationItem in
    print("swizzling")
    let originalSelector = #selector(getter: UINavigationItem.backBarButtonItem)
    let swizzledSelector = #selector(UINavigationItem.noTitleBackBarButtonItem)
    
    let originalMethod = class_getInstanceMethod(navigationItem, originalSelector)
    let swizzledMethod = class_getInstanceMethod(navigationItem, swizzledSelector)
    
    let didAddMethod = class_addMethod(navigationItem, originalSelector, method_getImplementation(swizzledMethod!), method_getTypeEncoding(swizzledMethod!))
    
    if didAddMethod {
        class_replaceMethod(navigationItem, swizzledSelector, method_getImplementation(originalMethod!), method_getTypeEncoding(originalMethod!))
    } else {
        method_exchangeImplementations(originalMethod!, swizzledMethod!)
    }
}

extension UINavigationItem: SelfAware {
    static func awake() {
        swizzling(self)
    }

//    open override class func initialize() {
//        
//        // make sure this isn't a subclass
//        if self !== UINavigationItem.self {
//            return
//        }
//        swizzling(self)
//    }
    
    // MARK: - Method Swizzling
    
    struct AssociatedKeys {
        static var ArrowBackButtonKey = "noTitleArrowBackButtonKey"
    }
    
    @objc func noTitleBackBarButtonItem() -> UIBarButtonItem? {
        print("noTitleBackBarButtonItem")
        if let item = self.noTitleBackBarButtonItem() {
            return item
        }
        
        if let item = objc_getAssociatedObject(self, &AssociatedKeys.ArrowBackButtonKey) as? UIBarButtonItem {
            return item
        } else {
            let newItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            objc_setAssociatedObject(self, &AssociatedKeys.ArrowBackButtonKey, newItem as UIBarButtonItem?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return newItem
        }
    }
}

class NavigationItemSwizzling: UINavigationItem {

}
