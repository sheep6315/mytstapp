//
//  PaymentBankViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/28.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

class PaymentBankViewController: TSUIViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var msisdnLabel: UILabel!
    
    @IBOutlet weak var billDateLabel: UILabel!
    
    @IBOutlet weak var endDateLabel: UILabel!
    
    @IBOutlet weak var needPayLabel: UILabel!
    
    @IBOutlet weak var bankTextField: UITextField!
    
    @IBOutlet weak var bankButton: UIButton!
    
    @IBOutlet weak var cardNumber: UITextField!
    
    @IBOutlet weak var checkBox: UIButton!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var bankList: [Dictionary<String, String>] = []
    var bankId: String = ""
    var checkBoxOn : Bool = false
    var msisdn: String = ""
    var billDate: String = ""
    var endDate: String = ""
    var needPay: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        bankButton.layer.borderWidth = 1
        bankButton.layer.cornerRadius = 5
        bankButton.layer.borderColor = UIColor.lightGray.cgColor
        
        cardNumber.layer.borderWidth = 1
        cardNumber.layer.cornerRadius = 5
        cardNumber.layer.borderColor = UIColor.lightGray.cgColor
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(gesture)
        
        bankTextField.delegate = self
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP040104", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳單", action: "銀行帳戶繳款", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "銀行帳戶繳款")
        
        checkVIPStatusAndChangeSkin()
        
        let (maintainStatus, maintainTitle, maintainMessage) = TSTAPP2_Utilities.isMEGAMaintain()
        
        if maintainStatus == true {
            TSTAPP2_Utilities.showAlertAndPop(withTitle: maintainTitle, message: maintainMessage, viewController: self)
        }
        else {
            callBackInfo()
            setPickerData()
            
            if BillInfo.sharedInstance.billingDetail.count > 0 {
                
                setLabel()
            }
            else {
                
                callQueryBillAPI()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP040104", action: "")
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    //MARK: - didTap function
    @IBAction func didTapSubmit(_ sender: Any) {
        
        if bankId == "" {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇銀行", viewController: self)
        }
        else if cardNumber.text == "" {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請輸入銀行帳號", viewController: self)
        }
        else if !checkBoxOn {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請先同意服務說明及注意事項", viewController: self)
        }
        else {
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP040104", action: "A050102")
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "銀行帳戶繳款", label: nil, value: nil)
            callDoPaymentByMegaServiceAPI()
        }
    }
    
    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapBankButton(_ sender: Any) {
        
        bankTextField.becomeFirstResponder()
    }
    
    @objc func didTapDone(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapCheckBox(_ sender: Any) {
        
        if checkBoxOn {
            
            checkBox.isSelected = false
        }
        else {
            
            checkBox.isSelected = true            
            TSTAPP2_Utilities.showHtmlMessage(withTitle: "銀行帳戶繳款服務說明", message: "1.本人即申請人，已知悉並同意台灣之星依『個人資料保護法第8條』所為之告知事項。依「個人資料保護法」第八條規定之告知內容：<br/>1、非公務機關名稱：台灣之星電信股份有限公司。<br/>2、蒐集之目的：客戶管理、統計調查與分析、資訊與資料庫管理、經營電信業務與電信加值網路業務、徵信、行銷(不包括直銷至個人)、行銷(包括直銷至個人)、其他合於營業登記項目或章程所定業務之需要、其他諮詢與顧問服務及依主管機關公告之特定目的。<br/>3、個人資料之類別：辨識個人資料、辨識財務者、政府資料中之辨識者、個人描述、財務交易及其他依主管機關公告之個人資料類別。<br/>4、個人資料利用之期間、地區、對象及方式：<br/>（1）期間：依法令、各契約規定之期間。<br/>（2）地區：中華民國境內、境外（主管機關禁止者不在此限）。<br/>（3）對象：台灣之星、與台灣之星有合作或業務往來之台灣之星關係企業及合作廠商。<br/>（4）方式：符合法令規定範圍之利用。<br/>5、使用者在符合電信相關法規之情形下，就其個人資料得依「個人資料保護法」規定行使下列權利，行使方式依台灣之星作業規範：<br/>（1）查詢或請求閱覽。<br/>（2）請求製給複製本。<br/>（3）請求補充或更正。<br/>（4）請求停止蒐集、處理或利用。<br/>（5）請求刪除。<br/>6、使用者得自由選擇提供資料（但依法令規定者不在此限），若不提供將影響電信服務之完整性。<br/>2.您使用本服務所輸入之相關資料，將由帳務代理、轉出、轉入金融機構及財金資訊股份有限公司在完成服務之特定目的內，蒐集、處理、利用及國際傳輸您的個人資料。<br/>3.本服務僅提供台灣之星月租型用戶本使用其名下銀行帳戶繳付帳單費用。<br/>4.本服務將進入SSL安全鑰匙加密機制，您可以放心使用台灣之星此項線上繳款服務。<br/>5.繳費並銷帳成功後您可於「繳款紀錄查詢」查詢到您的繳款紀錄，倘若使用本服務有錯誤或對款項之計算暨退補費等發生疑義，請洽台灣之星或轉出金融機構處理。<br/>6.為了讓您有更多的繳款選擇，同時提供「信用卡繳款」功能，歡迎多加利用!<br/><br/>注意事項：<br/>1.本服務不受每日轉帳新臺幣（下同）3萬元限制，但每一轉出帳戶每日不得超過10萬元，每月不得超過20萬元。如轉出金融機構之限額低於前述規定，則依轉出金融機構之規定辦理。<br/>2.轉帳時間依營業時間區分如下：營業日： <br/>15：30 前轉帳為當日帳；<br/>15：30 後轉帳則視為次一營業日之交易。非營業日：轉帳一律視為次一營業日之交易 。<br/>3.在您確認繳款同時將連線到銀行端執行扣款授權作業，偶有因網路擁塞造成連線逾時，屆時請您查詢銀行帳戶確認是否扣款成功，避免重覆繳款。<br/>4.完成線上繳款程序，一律無法再取消該筆交易。<br/>5.外籍人士/華僑恕無法使用此服務。<br/>6.若您因帳款逾期未繳而無法正常通話，請先登入網上繳款服務後，選擇「無用戶密碼繳款」完成繳費。", viewController: self)
        }
        
        checkBoxOn = !checkBoxOn
    }
    
    //MARK: - custom function
    func setLabel() {
        
        var needPayInDecimalFormat = ""
        
        if BillInfo.sharedInstance.billingDetail.count > 0 {
            
            let detail = BillInfo.sharedInstance
            msisdn = LoginData.sharedLoginData().custProfile_msisdn
            billDate = detail.billDate
            endDate = detail.dueDate
            needPay = detail.totDebt
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let pay = Int(detail.totDebt) ?? 0
            needPayInDecimalFormat = numberFormatter.string(from: NSNumber(value: pay))!
        }
        
        msisdnLabel.text = msisdn
        billDateLabel.text = billDate
        endDateLabel.text = endDate
        needPayLabel.text = needPayInDecimalFormat
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }
    
    func setPickerData(){
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        bankTextField.inputView = pickerView
        bankTextField.inputAccessoryView = pickBar
    }
    
    //MARK: - PickDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return bankList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        var str: String = ""
        
        if bankList.count > row {
        
            str = "\(bankList[row]["bankName"]!) \(bankList[row]["bankId"]!)"
        }
        
        return str
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        if bankList.count > row {
            
            let str = "\(bankList[row]["bankName"]!) \(bankList[row]["bankId"]!)"
            bankId = bankList[row]["bankId"]!
            bankButton.setTitle(str, for: .normal)
        }
        
//        for (key, value) in bankList[row] {
//            
//            bankId = key
//            bankButton.setTitle("\(key) \(value)", for: .normal)
//        }
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let pickerView = textField.inputView as? UIPickerView {
            if pickerView.numberOfRows(inComponent: 0) > 0 {
                pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
            }
        }
    }
    
    //MARK: - API
    func callBackInfo() {
        
        TSTAPP2_API.sharedInstance.getBankInfo(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                if let bankArray = (dic["data"] as? Dictionary<String, Any>)?["bankList"] as? [Dictionary<String, String>] {
                    for bank in bankArray {
                        self.bankList.append(bank)
                    }
                }
            }
        }) { (error: Error) in
            
        }
        
//        TSTAPP2_API.sharedInstance.getDepositBankInfo(showProgress: true, completion: {(dic :Dictionary<String, Any>, response :URLResponse) in
//            
//            if let apiData = dic["GetDepositBankInfoRes"] as? Dictionary<String ,Any> {
//                
//                if apiData.stringValue(ofKey: "ResultCode") == "00000" {
//                    
//                    if let banks = apiData["Banks"] as? [String] {
//                        
//                        for bank in banks {
//                            
//                            let array = bank.components(separatedBy: "/")
//                            let dic = [array[0] : array[1]]
//                            
//                            self.bankList.append(dic)
//                        }
//                    }
//                }
//                
//            }
//            
//        }, failure: {(Error) in
//        
//        })
    }
    
    func callDoPaymentByMegaServiceAPI() {
        
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.doBank(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, custId: loginData.custProfile_custId, accountId: loginData.custProfile_accountId, isMainMsisdn: loginData.custProfile_isMainMsisdn, bankId: "\(bankId)0000", bankAccount: cardNumber.text!, payAmount: needPay, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            
            let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
                let _ = self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(action)
            
            if dic.stringValue(ofKey: "code") == "00000" {
                alert.title = "銀行帳戶繳款/成功"
                alert.message = dic.stringValue(ofKey: "message")
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "銀行帳戶繳款", label: "繳款成功", value: nil)
                
                Answers.logPurchase(withPrice: NSDecimalNumber(string: self.needPay), currency: "TWD", success: 1, itemName: "銀行帳戶繳款", itemType: "Payment", itemId: "Bank", customAttributes: nil)
            }
            else {
                alert.title = "銀行帳戶繳款/失敗"
                alert.message = dic.stringValue(ofKey: "message")
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "銀行帳戶繳款", label: "繳款失敗", value: nil)
                
                Answers.logPurchase(withPrice: NSDecimalNumber(string: self.needPay), currency: "TWD", success: 0, itemName: "銀行帳戶繳款", itemType: "Payment", itemId: "Bank", customAttributes: ["Bank Payment Faliure": "\(dic.stringValue(ofKey: "code")) \(dic.stringValue(ofKey: "message")))"])
            }
            
            alert.show(viewController: self)
            
        }) { (error: Error) in
            
        }
        
//        TSTAPP2_API.sharedInstance.doPaymentByMegaService(withMSISDN: msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, taxIDNumber: LoginData.sharedLoginData().custProfile_custId, bankID: "\(bankId)0000", bankAccount: cardNumber.text!, payAmount: needPay, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//            
//            if let apiData = dic["DoPaymentByMegaServiceRes"] as? Dictionary<String, Any> {
//                
//                if apiData.stringValue(ofKey: "ResultCode") == "00000"{
//                    
//                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
//                    let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
//                        let _ = self.navigationController?.popViewController(animated: true)
//                    })
//                    alert.addAction(action)
//                    
//                    if apiData.stringValue(ofKey: "Status") == "0" {
//                        
//                        var statusMsg = apiData.stringValue(ofKey: "StatusMsg")
//                        
//                        if statusMsg != "" {
//                            
//                            let msg = statusMsg.replacingOccurrences(of: " ", with: "") as NSString
//                            let start = msg.range(of: "[")
//                            let end = msg.range(of: "]")
//                            
//                            if start.location != NSNotFound && end.location != NSNotFound {
//                                
//                                statusMsg = msg.replacingCharacters(in: NSMakeRange(start.location, end.location - start.location + 1), with: "") as String
//                            }
//                        }
//                        
//                        alert.title = "銀行帳戶繳款/成功"
//                        alert.message = statusMsg
//                    }
//                    else {
//                        
//                        var statusMsg = apiData.stringValue(ofKey: "StatusMsg")
//                        
//                        if statusMsg != "" {
//                            
//                            let msg = statusMsg.replacingOccurrences(of: " ", with: "") as NSString
//                            let start = msg.range(of: "[")
//                            let end = msg.range(of: "]")
//                            
//                            if start.location != NSNotFound && end.location != NSNotFound {
//                                
//                                statusMsg = msg.replacingCharacters(in: NSMakeRange(start.location, end.location - start.location + 1), with: "") as String
//                            }
//                        }
//                        
//                        alert.title = "銀行帳戶繳款/失敗"
//                        alert.message = statusMsg
//                    }
//                    
//                    alert.show(viewController: self)
//                }
//                else {
//                    
//                    TSTAPP2_Utilities.showAlert(withMessage: apiData.stringValue(ofKey: "ResultText"), viewController: self)
//                }
//            }
//            
//        }, failure: { (Error) in
//            
//        })
    }
    
    func callQueryBillAPI() {
        
        TSTAPP2_API.sharedInstance.queryBill(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, custId: LoginData.sharedLoginData().custProfile_custId, accountId: LoginData.sharedLoginData().custProfile_accountId, showProgress: true, completion: {(dic :Dictionary<String, Any>, response :URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let bill = BillInfo.sharedInstance
                bill.setDataValue(withDic: dic)
                
                self.setLabel()
            }
            
        }, failure: {(Error) in })
    }
}
