//
//  PaymentBarcodeViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/12/5.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class PaymentBarcodeViewController: TSUIViewController {
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var barcode1Image: UIImageView!
    
    @IBOutlet weak var barcode2Image: UIImageView!
    
    @IBOutlet weak var barcode3Image: UIImageView!
    
    @IBOutlet weak var barcode1Label: UILabel!
    
    @IBOutlet weak var barcode2Label: UILabel!
    
    @IBOutlet weak var barcode3Label: UILabel!
    
    @IBOutlet weak var barcode1MemoLabel: UILabel!
    
    @IBOutlet weak var barcode2MemoLabel: UILabel!
    
    @IBOutlet weak var barcode3MemoLabel: UILabel!
    
    @IBOutlet weak var snapshotButton: UIButton!
    
    var brightness: CGFloat!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.brightness = UIScreen.main.brightness
        self.callQueryBillAPI()
        UIScreen.main.brightness = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP040102", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳單", action: "超商繳費條碼", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "超商繳費條碼")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP040102", action: "")
        UIScreen.main.brightness = self.brightness
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: snapshotButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if (error != nil) {
            let alertController = UIAlertController(title: "儲存失敗", message: "請確定 \"設定>隱私權>照片>台灣之星\" 已開啟或檢查手機剩餘容量", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "儲存成功", message: "條碼已儲存至相機膠卷", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - IBAction
    @IBAction func didTapNoticeButton(_ sender: Any) {
        let noticeAlertController = UIAlertController(title: "貼心提醒", message: "若於門市繳款時發生條碼無法刷讀，請提供門號與身分證號予門市人員，\n門市人員將為您完成服務。", preferredStyle: .alert)
        noticeAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
        self.present(noticeAlertController, animated: true, completion: nil)
    }
    
    @IBAction func didTapSnapshotButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP040102", action: "A040102")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "超商繳費條碼", label: "儲存帳單條碼", value: nil)
        
        var image: UIImage? = nil
        let savedContentOffset = self.contentScrollView.contentOffset
        let savedFrame = self.contentScrollView.frame
        
            UIGraphicsBeginImageContextWithOptions(self.contentScrollView.contentSize, false, UIScreen.main.scale)
        self.contentScrollView.contentOffset = CGPoint.zero
        self.contentScrollView.frame = CGRect(x: 0, y: 0, width: self.contentScrollView.contentSize.width, height: self.contentScrollView.contentSize.height)
        
        self.contentScrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()
        self.contentScrollView.contentOffset = savedContentOffset
        self.contentScrollView.frame = savedFrame
        UIGraphicsEndImageContext()
        if image != nil {
            UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    // MARK: - API
    func callQueryBillAPI() {
        
        TSTAPP2_API.sharedInstance.queryBill(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, custId: LoginData.sharedLoginData().custProfile_custId, accountId: LoginData.sharedLoginData().custProfile_accountId, showProgress: true, completion: {(dic :Dictionary<String, Any>, response :URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let bill = BillInfo.sharedInstance
                bill.setDataValue(withDic: dic)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                if bill.showPaymentButton == "Y" {
                    self.callGetBarcodeAPI(withBillDate: bill.billDate)
                } else {
                    TSTAPP2_Utilities.showAlertAndPop(withMessage: "無需繳款", viewController: self)
                }
            }
            
        }, failure: {(Error) in })
    }
    
    func callGetBarcodeAPI(withBillDate billDate: String) {
        TSTAPP2_API.sharedInstance.getBarcodeInfo(withBillDate: billDate, showProgress: true, completion: { (dic: Dictionary<String, Any>, response) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                if let data = dic["data"] as? Dictionary<String, Any> {
                    
                    let barcode1 = data.stringValue(ofKey: "barcode1")
                    let barcode2 = data.stringValue(ofKey: "barcode2")
                    let barcode3 = data.stringValue(ofKey: "barcode3")
                    self.barcode1Label.text = barcode1
                    self.barcode2Label.text = barcode2
                    self.barcode3Label.text = barcode3
                    self.barcode1MemoLabel.text = data.stringValue(ofKey: "memo1")
                    self.barcode2MemoLabel.text = data.stringValue(ofKey: "memo2")
                    self.barcode3MemoLabel.text = data.stringValue(ofKey: "memo3")
                    
                    let barcodeWriter: ZXMultiFormatWriter = ZXMultiFormatWriter()
                    var barcodeResult: ZXBitMatrix
                    
                    do {
                        barcodeResult = try barcodeWriter.encode(barcode1, format: kBarcodeFormatCode39, width: Int32(self.barcode1Image.frame.size.width), height: 80)
                        self.barcode1Image.image = UIImage(cgImage: ZXImage.init(matrix: barcodeResult).cgimage)
                        
                        barcodeResult = try barcodeWriter.encode(barcode2, format: kBarcodeFormatCode39, width: Int32(self.barcode2Image.frame.size.width), height: 80)
                        self.barcode2Image.image = UIImage(cgImage: ZXImage.init(matrix: barcodeResult).cgimage)
                        
                        barcodeResult = try barcodeWriter.encode(barcode3, format: kBarcodeFormatCode39, width: Int32(self.barcode3Image.frame.size.width), height: 80)
                        self.barcode3Image.image = UIImage(cgImage: ZXImage.init(matrix: barcodeResult).cgimage)
                    } catch {
                        
                    }
                }
            }
            else {
                
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
        }) { (Error) in
            
        }
    }
}
