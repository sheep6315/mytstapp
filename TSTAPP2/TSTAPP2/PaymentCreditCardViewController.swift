//
//  PaymentCreditCardViewController.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/30.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

let PROMOTE_CREDITCARD = "PROMOTE_CREDITCARD"
let CARD_NUMBER = "CARD_NUMBER"

class PaymentCreditCardViewController: TSUIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var msisdnLabel: UILabel!
    
    @IBOutlet weak var billDateLabel: UILabel!
    
    @IBOutlet weak var endDateLabel: UILabel!
    
    @IBOutlet weak var needPayLabel: UILabel!
    
    @IBOutlet weak var firstNumber: UITextField!
    
    @IBOutlet weak var secondNumber: UITextField!
    
    @IBOutlet weak var thirdNumber: UITextField!
    
    @IBOutlet weak var fourthNumber: UITextField!
    
    @IBOutlet weak var ccvTextField: UITextField!
    
    @IBOutlet weak var saveCardCheckBox: UIButton!
    
    @IBOutlet weak var noticeCheckBox: UIButton!
    
    @IBOutlet weak var expiredDateTextField: UITextField!
    
    @IBOutlet weak var expiredDateButton: UIButton!
    
    @IBOutlet weak var submitButton: UIButton!
    
    //custom AlertView
    @IBOutlet weak var customView: UIView!
    
    @IBOutlet weak var customViewCheckBox: UIButton!
    
    @IBOutlet weak var customViewSubmitButton: UIButton!
    
    @IBOutlet weak var customViewCancelButton: UIButton!
    
    var month: [String] = []
    var year: [String] = []
    var selectedMonth: String = ""
    var selectedYear: String = ""
    var msisdn: String = ""
    var billDate: String = ""
    var endDate: String = ""
    var needPay: String = ""
    var isPay: Bool = false //有沒有使用付款api

    override func viewDidLoad() {
        super.viewDidLoad()

        expiredDateButton.layer.borderWidth = 1
        expiredDateButton.layer.cornerRadius = 5
        expiredDateButton.layer.borderColor = UIColor.init(red: 184/255, green: 184/255, blue: 184/255, alpha: 0.5).cgColor
        
        expiredDateTextField.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapScrollView(_:)))
        gesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(gesture)
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        customView.isHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP040103", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳單", action: "信用卡繳款", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "信用卡繳款")
        
        checkVIPStatusAndChangeSkin()
        
        let (maintainStatus, maintainTitle, maintainMessage) = TSTAPP2_Utilities.isCTCBMaintain()
        
        if maintainStatus == true {
            TSTAPP2_Utilities.showAlertAndPop(withTitle: maintainTitle, message: maintainMessage, viewController: self)
        }
        else {
            setPickerData()
            
            if let cardNumber = UserDefaults.standard.object(forKey: CARD_NUMBER) as? String {
                
                let decrypt = TSTAPP2_Utilities.getAESDecrypt(withString: cardNumber)
                let card = decrypt.components(separatedBy: "-")
                
                self.firstNumber.text = card[0]
                self.secondNumber.text = card[1]
                self.thirdNumber.text = card[2]
                self.fourthNumber.text = card[3]
            }
            
            if BillInfo.sharedInstance.billingDetail.count > 0 {
                
                setLabel()
            }
            else {
                
                callQueryBillAPI()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP040103", action: "")
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: customViewCancelButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: customViewSubmitButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    //MARK: - 按鈕
    @objc func didTapDone(button: UIBarButtonItem) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapTransfer(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP040103", action: "A040103")
        customView.isHidden = true
        
        let avc = AutomaticViewChange.sharedInstance
        avc.setTabIndex(withAction: AUTO_CREDITCARDTRANSFER, actionParam: nil)
    }
    
    @IBAction func didTapSaveCard(_ sender: Any) {

        let isOn = saveCardCheckBox.isSelected
        
        if isOn {
            saveCardCheckBox.isSelected = false
        }
        else {
            saveCardCheckBox.isSelected = true
        }
    }
    
    @IBAction func didTapNotice(_ sender: Any) {
        
        let isOn = noticeCheckBox.isSelected
        
        if isOn {
            noticeCheckBox.isSelected = false
        }
        else {
            
            noticeCheckBox.isSelected = true            
            TSTAPP2_Utilities.showHtmlMessage(withTitle: "信用卡繳款服務說明", message: "1.本人即申請人，已知悉並同意台灣之星依『個人資料保護法第8條』所為之告知事項。<br/>依「個人資料保護法」第八條規定之告知內容：<br/>1、非公務機關名稱：台灣之星電信股份有限公司。<br/>2、蒐集之目的：客戶管理、統計調查與分析、資訊與資料庫管理、經營電信業務與電信加值網路業務、徵信、行銷(不包括直銷至個人)、行銷(包括直銷至個人)、其他合於營業登記項目或章程所定業務之需要、其他諮詢與顧問服務及依主管機關公告之特定目的。<br/>3、個人資料之類別：辨識個人資料、辨識財務者、政府資料中之辨識者、個人描述、財務交易及其他依主管機關公告之個人資料類別。<br/>4、個人資料利用之期間、地區、對象及方式：<br/>（1）期間：依法令、各契約規定之期間。<br/>（2）地區：中華民國境內、境外（主管機關禁止者不在此限）。<br/>（3）對象：台灣之星、與台灣之星有合作或業務往來之台灣之星關係企業及合作廠商。<br/>（4）方式：符合法令規定範圍之利用。<br/>5、使用者在符合電信相關法規之情形下，就其個人資料得依「個人資料保護法」規定行使下列權利，行使方式依台灣之星作業規範：<br/>（1）查詢或請求閱覽。<br/>（2）請求製給複製本。<br/>（3）請求補充或更正。<br/>（4）請求停止蒐集、處理或利用。<br/>（5）請求刪除。<br/>6、使用者得自由選擇提供資料（但依法令規定者不在此限），若不提供將影響電信服務之完整性。2.網路繳款僅提供台灣之星月租型用戶本人之信用卡繳費，並使用其名下信用卡(VISA、MASTER、JCB、聯合信用卡)透過網路刷卡繳付帳單費用。<br/>3. 本服務將進入SSL安全鑰匙加密機制，您可以安心使用台灣之星網路刷卡繳款服務。<br/>4.繳費並銷帳成功後您可於「繳款紀錄查詢」查詢到您的繳款紀錄。<br/><br/>注意事項：<br/>1.如用戶使用非本人之信用卡繳費，並經持卡人或原發卡銀行舉發，應由用戶本人負擔全部責任；如致本公司退費予持卡人或原發卡銀行時，逕依帳務催款程序向用戶催討，並願負擔該筆款項。<br/>2.用戶本人如以信用卡繳款但經授權失敗時，請向原發卡銀行查詢扣款失敗之原因。<br/>3.刷卡一經銀行同意授權，視同該筆消費已成立， 無法任意取消或變更。<br/>4.若您因帳款逾期未繳而無法正常通話，請先登入網上繳款服務後，選擇「無用戶密碼繳款」完成繳費。", viewController: self)
        }
    }
    
    @IBAction func didTapExpiredDate(_ sender: Any) {
        
        expiredDateTextField.becomeFirstResponder()
    }
    
    @IBAction func didTapSubmit(_ sender: Any) {
        
        let cardNumberLength = (firstNumber.text ?? "").count + (secondNumber.text ?? "").count + (thirdNumber.text ?? "").count + (fourthNumber.text ?? "").count
        let ccv = ccvTextField.text?.lengthOfBytes(using: .utf8) ?? 0
        
        if cardNumberLength < 16 {
            
            TSTAPP2_Utilities.showAlert(withMessage: "信用卡卡號不正確", viewController: self)
        }
        else if ccv < 3 {
            
            TSTAPP2_Utilities.showAlert(withMessage: "信用卡卡片後三碼不正確", viewController: self)
        }
        else if noticeCheckBox.isSelected == false {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請勾選本人已詳閱及了解本服務注意事項並同意遵守", viewController: self)
        }
        else if selectedYear == "" || selectedMonth == "" {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇有效日期", viewController: self)
        }
        else {
            
            //繳款邏輯： call繳款api -> if 成功 判斷是否推廣代繳 ＆ 是否儲存卡號
            UbaRecord.sharedInstance.sendUba(withPage: "APP040103", action: "A050101")
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "信用卡繳款", label: nil, value: nil)
            submitButton.isUserInteractionEnabled = false
            callDoPaymentByCTCBServiceAPI()
        }
    }

    @objc func didTapScrollView (_ recognizer:UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func didTapCustomViewCheckBox(_ sender: Any) {
        
        let isOn = customViewCheckBox.isSelected
        
        if isOn {
            customViewCheckBox.isSelected = false
        }
        else {
            customViewCheckBox.isSelected = true
        }
    }
    
    @IBAction func didTapCustomViewCancel(_ sender: Any) {
        
        if customViewCheckBox.isSelected {
            
            UserDefaults.standard.set("Y", forKey: PROMOTE_CREDITCARD)
            UserDefaults.standard.synchronize()
        }
        customView.isHidden = true
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapCustomViewSubmit(_ sender: Any) {
        
        if customViewCheckBox.isSelected {
            
            UserDefaults.standard.set("Y", forKey: PROMOTE_CREDITCARD)
            UserDefaults.standard.synchronize()
        }
        
//        let storyboard = UIStoryboard(name: "CreditCardTransfer", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditCardTransferViewController") as UIViewController
//        self.navigationController?.show(viewController, sender: nil)
        
        customView.isHidden = true
        
        let avc = AutomaticViewChange.sharedInstance
        avc.setTabIndex(withAction: AUTO_CREDITCARDTRANSFER, actionParam: nil)
        
    }
    
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        
        let textField = sender
        let length = textField.text?.lengthOfBytes(using: .utf8)
        
        if textField.tag == 0 && length == 4 {
            
            secondNumber.becomeFirstResponder()
        }
        else if textField.tag == 1 && length == 4 {
            
            thirdNumber.becomeFirstResponder()
        }
        else if textField.tag == 2 && length == 4 {
            
            fourthNumber.becomeFirstResponder()
        }
        else if textField.tag == 3 && length == 4 {
            
            fourthNumber.resignFirstResponder()
        }
        else if textField.tag == 5 && length == 3 {
            
            ccvTextField.resignFirstResponder()
        }
        
    }
    
    //MARK: - custom function
    func setLabel() {
        
        var needPayInDecimalFormat = ""
        
        if BillInfo.sharedInstance.billingDetail.count > 0 {
            
            let detail = BillInfo.sharedInstance
            msisdn = LoginData.sharedLoginData().custProfile_msisdn
            billDate = detail.billDate
            endDate = detail.dueDate
            needPay = detail.totDebt
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            let pay = Int(detail.totDebt) ?? 0
            needPayInDecimalFormat = numberFormatter.string(from: NSNumber(value: pay))!
        }
        
        msisdnLabel.text = msisdn
        billDateLabel.text = billDate
        endDateLabel.text = endDate
        needPayLabel.text = needPayInDecimalFormat
    
    }
    
    func setPickerData() {
        
        month = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        
        let calendar = Calendar(identifier: .gregorian)
        let yearNow = calendar.component(.year, from: Date())
        
        for i in 0...14 {
            year.append("\(yearNow+i)")
        }
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        expiredDateTextField.inputView = pickerView
        expiredDateTextField.inputAccessoryView = pickBar
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }
    
    func checkSaveCard() {
        
        if saveCardCheckBox.isSelected {
            
            let cardNumber = "\(firstNumber.text!)-\(secondNumber.text!)-\(thirdNumber.text!)-\(fourthNumber.text!)"
            UserDefaults.standard.set(cardNumber, forKey: CARD_NUMBER)
            UserDefaults.standard.synchronize()
        }
    }
    
    //MARK: - PickDelegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch component {
            
        case 0:
            return year.count
        case 1:
            return month.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch component {
            
        case 0:
            return year[row]
        case 1:
            return month[row]
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        let yearRow = pickerView.selectedRow(inComponent: 0)
        let monthRow = pickerView.selectedRow(inComponent: 1)
        
        selectedYear = year[yearRow]
        selectedMonth = month[monthRow]
        
        expiredDateButton.setTitle("\(selectedYear)/\(selectedMonth)", for: .normal)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let pickerView = textField.inputView as? UIPickerView {
            if pickerView.numberOfRows(inComponent: 0) > 0 {
                pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
            }
        }
    }
    
    //MARK: - API
    func callDoPaymentByCTCBServiceAPI() {
        
        let cardNumber = firstNumber.text! + secondNumber.text! + thirdNumber.text! + fourthNumber.text!
        let cvv2 = ccvTextField.text!
        let expireDate = selectedYear + selectedMonth
        
        let loginData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.doCreditCard(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, accountId: loginData.custProfile_accountId, custId: loginData.custProfile_custId, isMainMsisdn: loginData.custProfile_isMainMsisdn, purchAmt: needPay, cardNumber: cardNumber, cvv2: cvv2, expiredDate: expireDate, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                
                if self.saveCardCheckBox.isSelected {
                    let cardNumber = "\(self.firstNumber.text!)-\(self.secondNumber.text!)-\(self.thirdNumber.text!)-\(self.fourthNumber.text!)"
                    let encrypt = TSTAPP2_API.sharedInstance.getAESEncrypt(withString: cardNumber)
                    UserDefaults.standard.set(encrypt, forKey: CARD_NUMBER)
                    UserDefaults.standard.synchronize()
                }
                
                self.isPay = true
                
                let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
                    if UserDefaults.standard.object(forKey: PROMOTE_CREDITCARD) as? String == nil {
                        self.callQueryDirectDebitAPI()
                    }
                })
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "信用卡繳款", label: "繳款成功", value: nil)
                
                TSTAPP2_Utilities.showAlert(withTitle: "信用卡繳款/成功", message: dic.stringValue(ofKey: "message"), action: action, viewController: self)
                
                Answers.logPurchase(withPrice: NSDecimalNumber(string: self.needPay), currency: "TWD", success: 1, itemName: "信用卡繳款", itemType: "Payment", itemId: "CreditCard", customAttributes: nil)
            }
            else {
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "立即繳款", action: "信用卡繳款", label: "繳款失敗", value: nil)
                
                TSTAPP2_Utilities.showAlert(withTitle: "信用卡繳款/失敗", message: dic.stringValue(ofKey: "message"), viewController: self)
                
                Answers.logPurchase(withPrice: NSDecimalNumber(string: self.needPay), currency: "TWD", success: 0, itemName: "信用卡繳款", itemType: "Payment", itemId: "CreditCard", customAttributes: ["Credit Card Payment Faliure Code": "\(dic.stringValue(ofKey: "code")) \(dic.stringValue(ofKey: "message")))"])
            }
            self.submitButton.isUserInteractionEnabled = true
        }) { (error: Error) in
            
            TSTAPP2_Utilities.showPKHUD()
            TSTAPP2_Utilities.hidePKHUD(afterDelay: 10.0) { (isComplete: Bool) in
                self.submitButton.isUserInteractionEnabled = true
                TSTAPP2_Utilities.showAlert(withTitle: "", message: "系統忙碌中，請稍後再試。", viewController: self)
            }
        }
    
//        TSTAPP2_API.sharedInstance.doPaymentByCTCBService(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, taxIDNumber: LoginData.sharedLoginData().custProfile_custId, cardNumber: cardNumber, expDate: expireDate, payAmount: needPay, showProgress: true, completion: {(dic: Dictionary<String, Any>, response: URLResponse) in
//        
//        
//            if let apiData = dic["DoPaymentByCTCBServiceRes"] as? Dictionary<String, Any> {
//                
//                if apiData.stringValue(ofKey: "ResultCode") == "00000"{
//                    
//                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
//                    let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
//                        let _ = self.navigationController?.popViewController(animated: true)
//                    })
//                    alert.addAction(action)
//                    
//                    if apiData.stringValue(ofKey: "Status") == "0" {
//                        
//                        var statusMsg = apiData.stringValue(ofKey: "StatusMsg")
//                        
//                        if statusMsg != "" {
//                            
//                            let msg = statusMsg.replacingOccurrences(of: " ", with: "") as NSString
//                            let start = msg.range(of: "[")
//                            let end = msg.range(of: "]")
//                            
//                            if start.location != NSNotFound && end.location != NSNotFound {
//                                
//                                statusMsg = msg.replacingCharacters(in: NSMakeRange(start.location, end.location - start.location + 1), with: "") as String
//                            }
//                        }
//                        
//                        alert.title = "信用卡繳款/成功"
//                        alert.message = statusMsg
//                        self.isPay = true
//                        
//                        if self.saveCardCheckBox.isSelected {
//                            
//                            let cardNumber = "\(self.firstNumber.text!)-\(self.secondNumber.text!)-\(self.thirdNumber.text!)-\(self.fourthNumber.text!)"
//                            let encrypt = TSTAPP2_API.sharedInstance.getAESEncrypt(withString: cardNumber)
//                            UserDefaults.standard.set(encrypt, forKey: CARD_NUMBER)
//                            UserDefaults.standard.synchronize()
//                        }
//                        
//                        if let promote = UserDefaults.standard.object(forKey: PROMOTE_CREDITCARD) as? String {
//                            
//                            if promote != "Y" {
//                                
//                                self.callQueryDirectDebitAPI()
//                            }
//                        }
//                    }
//                    else {
//                        
//                        var statusMsg = apiData.stringValue(ofKey: "StatusMsg")
//                        
//                        if statusMsg != "" {
//                            
//                            let msg = statusMsg.replacingOccurrences(of: " ", with: "") as NSString
//                            let start = msg.range(of: "[")
//                            let end = msg.range(of: "]")
//                            
//                            if start.location != NSNotFound && end.location != NSNotFound {
//                                
//                                statusMsg = msg.replacingCharacters(in: NSMakeRange(start.location, end.location - start.location + 1), with: "") as String
//                            }
//                        }
//                        
//                        alert.title = "信用卡繳款/失敗"
//                        alert.message = statusMsg
//                    }
//                    
//                    alert.show(viewController: self)
//                }
//                else {
//                    
//                    TSTAPP2_Utilities.showAlert(withMessage: apiData.stringValue(ofKey: "ResultText"), viewController: self)
//                }
//            }
//
//        }, failure: {(Error) in
//        
//        })
    }
    
    func callQueryDirectDebitAPI() {
        
        TSTAPP2_API.sharedInstance.queryDirectDebit(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, accountId: LoginData.sharedLoginData().custProfile_accountId, custId: LoginData.sharedLoginData().custProfile_custId, companyId: LoginData.sharedLoginData().custProfile_companyId, channel: "APP", isMainMsisdn: LoginData.sharedLoginData().custProfile_isMainMsisdn, showProgress: true, completion:{ (dic :Dictionary<String, Any>, response :URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                let debit = DirectDebit.sharedInstance
                debit.setDataValue(withDic: dic)
                
                if self.isPay {
                    
                    //有使用付款api
                    if debit.result != "5" || debit.result != "1" {
                        
                        self.customView.isHidden = false
                    }
                    else {
                        let _ = self.navigationController?.popViewController(animated: true)
                    }
                }
                else {
                    
                    //直接點信用卡轉帳按鈕
                    if debit.result == "5" || debit.result == "2" {
                        
                        let storyboard = UIStoryboard(name: "CreditCardTransfer", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditCardTransferViewController") as UIViewController
                        self.navigationController?.show(viewController, sender: nil)
                    }
                    else {
                        
                        let storyboard = UIStoryboard(name: "CreditCardTransferChange", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditCardTransferChangeViewController") as UIViewController
                        self.navigationController?.show(viewController, sender: nil)
                    }
                }
            }
            else {
                let _ = self.navigationController?.popViewController(animated: true)
            }
            
        }, failure: { (Error) in
            let _ = self.navigationController?.popViewController(animated: true)
        })

    }
    
    func callQueryBillAPI() {
        
        TSTAPP2_API.sharedInstance.queryBill(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, custId: LoginData.sharedLoginData().custProfile_custId, accountId: LoginData.sharedLoginData().custProfile_accountId, showProgress: true, completion: {(dic :Dictionary<String, Any>, response :URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let bill = BillInfo.sharedInstance
                bill.setDataValue(withDic: dic)
                
                self.setLabel()
            }
            
        }, failure: {(Error) in })
    }
}
