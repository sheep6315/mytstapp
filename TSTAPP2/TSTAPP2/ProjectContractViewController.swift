//
//  ProjectContractViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/21.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

class ProjectContractViewController: TSUIViewController {
    
    @IBOutlet weak var projectNameLabel: UILabel!
    
    @IBOutlet weak var projectContentTextView: UITextView!
    
    @IBOutlet weak var contractStartDateLabel: UILabel!
    
    @IBOutlet weak var contractExpireDateLabel: UILabel!
    
    @IBOutlet weak var contractEarlyRenewDateView: UIView!
    @IBOutlet weak var contractEarlyRenewDateLabel: UILabel!
    
    @IBOutlet weak var contractRenewalMessageView: UIView!
    @IBOutlet weak var contractRenewalMessageLabel: UILabel!
    
    @IBOutlet weak var displayDiscInfoLabel: UILabel!
    
    @IBOutlet weak var renewalWording: UILabel!
    
    @IBOutlet weak var applyButton: UIButton!
    
    
    @IBOutlet weak var projectContentHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var contractBannerAspect: NSLayoutConstraint!
    
    @IBOutlet weak var contractBannerImageView: UIImageView!
    
    @IBOutlet weak var renewButton: UIButton!
    @IBOutlet weak var renewButtonWidth: NSLayoutConstraint!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        if !LoginData.isLogin() {
            let alertController = UIAlertController(title: "尚未登入", message: "請先登入", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (_:UIAlertAction) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
//        self.callVerifyDIYAPI()
//        self.callGetContractAPI()
        let loginData = LoginData.sharedLoginData()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
//        self.contractStartDateLabel.text = loginData.custProfile_activationDate.components(separatedBy: " ").first!
//        self.contractExpireDateLabel.text = loginData.custProfile_expiredDate.components(separatedBy: " ").first!
        
        
        if loginData.ivrQualify_earlyRenewDay.components(separatedBy: " ").first! != "" {
            self.contractEarlyRenewDateLabel.text = loginData.ivrQualify_earlyRenewDay.components(separatedBy: " ").first!
        }
        else {
            contractEarlyRenewDateView.isHidden = true
        }
        
        if loginData.ivrQualify_displayRenewalMessage != "" {
            self.contractRenewalMessageLabel.text = loginData.ivrQualify_displayRenewalMessage
        }
        else {
            contractRenewalMessageView.isHidden = true
        }
        
        self.displayDiscInfoLabel.text = loginData.ivrQualify_displayDiscInfo
        
        if SmartBannerList.sharedInstance.contractBanner.count > 0 {
            if let url = URL(string: (SmartBannerList.sharedInstance.contractBanner.first?.path)!) {
                self.contractBannerImageView.setImageWith(url, placeholderImage: UIImage(named: "img_placeHolder"))
            }
            else {
                self.contractBannerImageView.image = UIImage(named: "img_placeHolder")
            }

        }
        else {
            contractBannerAspect = contractBannerAspect.setMultiplier(multiplier: -1)
        }
        
        if loginData.ivrQualify_isRenewal == "Y" {
            renewButton.isHidden = false
        }
        else {
            renewButton.isHidden = true
            renewButtonWidth.constant = 0
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "AP_RT_QUALIFY", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "我的專案", label: "續約資格查詢", value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "續約資格查詢")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "AP_RT_QUALIFY", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: applyButton, mode: ButtonColorMode.button_purple.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: renewButton, mode: ButtonColorMode.button_purple.rawValue)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBAction
    @IBAction func didTapApplyButton(_ sender: Any) {
        
        let diyData = VerifyDIY.sharedInstance
        if diyData.isDIY {
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: diyData.buttonText, urlString: diyData.buttonLinkUrl, appendToken: true, viewController: self)
        } else {
//            let storyBoard = UIStoryboard(name: "ProjectContract", bundle: nil)
//            let contractChangeVC = storyBoard.instantiateViewController(withIdentifier: "changeContract")
//            self.show(contractChangeVC, sender: self)
            self.performSegue(withIdentifier: "toChangeContract", sender: nil)
        }
    }
    
    @IBAction func didTapContractBannerButton(_ sender: Any) {
        if SmartBannerList.sharedInstance.contractBanner.count > 0 {
            if let urlString = SmartBannerList.sharedInstance.contractBanner.first?.url {
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "續約資格查詢Banner", label: urlString, value: nil)
                
                let smartBanner = SmartBannerList.sharedInstance.contractBanner.first!
                
                UbaRecord.sharedInstance.sendUba(withPage: urlString,
                                                 action: "ACMP_\(SmartBannerList.sharedInstance.cmpId)_\(SmartBannerList.sharedInstance.cmpVersion)_\(smartBanner.displayType)_\(smartBanner.sort)",
                                                 codeId: SmartBannerList.sharedInstance.cmpId,
                                                 codeVersion: SmartBannerList.sharedInstance.cmpVersion,
                                                 codeDisplayType: smartBanner.displayType,
                                                 codeSort: smartBanner.sort)
                
                if smartBanner.action == AUTO_BROWSER {
                    if let url = URL(string: urlString) {
                        UIApplication.shared.openURL(url)
                    }
                }
                else {
                    TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
                }
                
                Answers.logContentView(withName: "智能推薦",
                                       contentType: "AD",
                                       contentId: urlString,
                                       customAttributes: [:])

            }
        }
    }
    
    @IBAction func didTapRenewButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "AP_RT_QUALIFY", action: "AA_RENEWAL")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的專案", action: "續約資格查詢", label: "立即續約", value: nil)
        
        let urlString = UrlMap.sharedInstance.RT
        
        
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
    }
    

    // MARK: - API
    func callGetContractAPI() {
        let userData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.queryProjectAgreement(withMSISDN: userData.custProfile_msisdn, projectCode: userData.custProfile_projectCode, showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
            
//            var errorMsg: String = ""
            if dic.stringValue(ofKey: "code") == "00000" {
                let apiData = dic["data"] as! Dictionary<String, Any>
                self.projectNameLabel.text = apiData["projectDesc"] as? String ?? userData.custProfile_projectName
                self.projectContentTextView.text = apiData["offerContent"] as? String ?? ""
                self.projectContentTextView.sizeToFit()
                self.projectContentHeightConstraint.constant = self.projectContentTextView.frame.height + 8
                self.view.setNeedsUpdateConstraints()
                self.view.layoutIfNeeded()
                return
            } else {
//                errorMsg = dic["errorMessage"] as! String
            }
            
//            if errorMsg != "" {
//                let errorMsgAlertController = UIAlertController(title: nil, message: errorMsg, preferredStyle: .alert)
//                errorMsgAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
//                self.present(errorMsgAlertController, animated: true, completion: nil)
//            }
            
        }) { (Error) in
            print("error: \(Error)")
        }
    }
    
    func callVerifyDIYAPI() {
        let userData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.verifyDIY(withMSISDN: userData.custProfile_msisdn, projectCode: userData.custProfile_projectCode, isBL: (userData.isBL == "Y"), showProgress: true, completion:  { (dic:Dictionary<String, Any>, URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let diyData: VerifyDIY = VerifyDIY.sharedInstance
                diyData.setDataValue(withDic: dic)
                if diyData.isDIY {
                    self.applyButton.setTitle(diyData.buttonText, for: .normal)
                } else {
                    self.applyButton.setTitle("資費變更申請", for: .normal)
                }
            }
            
        }) { (Error) in
            print("error: \(Error)")
        }
    }
}
