//
//  PromoteTableViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/25.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

protocol PromoteDelegate: class {
    func didTapPromote(sender: Any?) -> ()
}

class PromoteCell: UITableViewCell {
    var promoteData: PromoteContent = PromoteContent()
    
    @IBOutlet weak var promoteImageView: UIImageView!
    
}

class PromoteTableViewController: UITableViewController {
    
    weak var promoteDelegate: PromoteDelegate?
    
    var promoteData: PromoteData = PromoteData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.rowHeight = ((tableView.frame.width * 3) / 4) + 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return promoteData.content.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromoteCell", for: indexPath) as! PromoteCell

        // Configure the cell...
        
        let picURLString = promoteData.content[indexPath.row].picUrl
        let picURL = URL(string: picURLString)
        if picURL != nil {
            cell.promoteImageView.backgroundColor = UIColor.white
            cell.promoteImageView.setImageWith(picURL!, placeholderImage: UIImage(named: "img_placeHolder"))
        }
        else {
            cell.promoteImageView.image = UIImage(named: "img_placeHolder")
        }

        return cell
    }
    
    //MARK: - Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let promoteContent = self.promoteData.content[indexPath.row]
        promoteDelegate?.didTapPromote(sender: promoteContent)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
//    MARK: - IBAction
    @IBAction func didTapMoreButton(_ sender: Any) {
        let urlString = promoteData.moreUrl
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "找優惠", urlString: urlString, viewController: self)
    }
    
    

}
