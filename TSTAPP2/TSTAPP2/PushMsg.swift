//
//  PushMsg.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/23.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class PushMsgList: NSObject {
    
    static let sharedInstance = PushMsgList()
    
    private(set) var pushMsgList : [PushMsg] = []
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if dic.stringValue(ofKey: "status") == "00000" {
            
            pushMsgList = []
            
            if let apiData = dic["data"] as? Array<Dictionary<String, Any>> {
                
                for msg in apiData {
                    
                    let pushMsg = PushMsg()
                    pushMsg.setDataValue(withDic: msg)
                    pushMsgList.append(pushMsg)
                }
            }
        }
    }
}

class PushMsg: NSObject {

    private(set) var action : String = ""
    private(set) var actionParam : String = ""
    private(set) var messageid : String = ""
    private(set) var msgBody : String = ""
    private(set) var msgTitle : String = ""
    private(set) var msisdn : String = ""
    private(set) var time : String = ""
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        action = dic.stringValue(ofKey: "action")
        actionParam = dic.stringValue(ofKey: "actionParam")
        messageid = dic.stringValue(ofKey: "messageid")
        msgBody = dic.stringValue(ofKey: "msgBody")
        msgTitle = dic.stringValue(ofKey: "msgTitle")
        time = dic.stringValue(ofKey: "time")
        msisdn = dic.stringValue(ofKey: "msisdn")
    }
}
