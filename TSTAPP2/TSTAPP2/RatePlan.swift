//
//  RatePlan.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/30.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class RatePlanList: NSObject {
    
    // 目前專案費率
    private(set) var currentRatePlan: String = ""
    
    // 可變更專案清單
    private(set) var list: [RatePlan] = []
    
    init(withDic dic: Dictionary<String, Any>) {
//        let response:Dictionary<String, Any> = dic["GetRatePlanRes"] as? [String : Any] ?? ["ResultCode": "99999", "ResultText": "API回傳格式異常"]
//        if response.stringValue(ofKey: "status") == "0" {
//            self.currentRatePlan = response.stringValue(ofKey: "currentRatePlan")
//            if let respData = response["respData"] as? Array<Dictionary<String, Any>> {
//                var ratePlanList:[RatePlan]  = []
//                respData.forEach({ (rawData: [String : Any]) in
//                    ratePlanList.append(RatePlan(withDic: rawData))
//                })
//                self.list = ratePlanList
//            }
//        }

        
        currentRatePlan = LoginData.sharedLoginData().custProfile_planName
        
        if let data = dic["data"] as? Dictionary<String, Any> {
            if let chgRatePlanList = data["chgRatePlanList"] as? [Dictionary<String, Any>] {
                var ratePlanList:[RatePlan]  = []
                chgRatePlanList.forEach({ (rawData: [String : Any]) in
                    ratePlanList.append(RatePlan(withDic: rawData))
                })
                self.list = ratePlanList
            }
        }
    }
}

class RatePlan: NSObject {

    // 帳單週期
    private(set) var billCycle: String = ""
    
    // 原專案名稱
    private(set) var oriProjName: String = ""
    
    // 專案名稱
    private(set) var chgProjName: String = ""
    
    // 來源系統
    private(set) var fromSystem: String = ""
    
    // 搭配手機
    private(set) var handSet: String = ""
    
    private(set) var package: String = ""
    
    // 合約金額
    private(set) var ratePlan: String = ""
    
    private(set) var renew: String = ""
    
    private(set) var renew2: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "ratePlan") != "" {
            self.billCycle = dic.stringValue(ofKey: "billCycle")
            self.oriProjName = dic.stringValue(ofKey: "oriProjName")
            self.chgProjName = dic.stringValue(ofKey: "chgProjName")
            self.fromSystem = dic.stringValue(ofKey: "fromSystem")
            self.handSet = dic.stringValue(ofKey: "handSet")
            self.package = dic.stringValue(ofKey: "package")
            self.ratePlan = dic.stringValue(ofKey: "ratePlan")
            self.renew = dic.stringValue(ofKey: "renew")
            self.renew2 = dic.stringValue(ofKey: "renew2")
        }
    }
}
