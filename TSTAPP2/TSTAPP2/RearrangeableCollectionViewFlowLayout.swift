//
//  RearrangeableCollectionViewFlowLayout.swift
//  DraggableCollectionView
//
//  Created by Tony on 2017/3/8.
//  Copyright © 2017年 Tony Hsu. All rights reserved.
//

import UIKit

protocol RearrangeableCollectionViewDelegate: UICollectionViewDelegate {
    func canMoveItem(indexPath: IndexPath) -> Bool
    func moveDataItem(from source: IndexPath, to destination: IndexPath)
    func swapDataItem(from source: IndexPath, to destination: IndexPath)
    func didEndDragging();
}

extension RearrangeableCollectionViewDelegate {
    func canMoveItem(indexPath: IndexPath) -> Bool {
        return true
    }
}

enum DraggingAxis {
    case free
    case x
    case y
    case xy
}

class RearrangeableCollectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    
}

class RearrangeableCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newImageView: UIImageView!
    
    var baseBackgroundColor : UIColor?
    
    var dragging: Bool = false {
        
        didSet {
            
            if dragging == true {
                
                self.baseBackgroundColor = self.backgroundColor
                self.backgroundColor = UIColor.lightGray//purpleColor
                self.bgView.backgroundColor = UIColor.lightGray
                self.alpha = 1
                
            } else {
                self.bgView.backgroundColor = self.baseBackgroundColor
                self.backgroundColor = self.baseBackgroundColor
                self.alpha = 1
                
            }
        }
    }
    
}

class RearrangeableCollectionViewFlowLayout: UICollectionViewFlowLayout, UIGestureRecognizerDelegate {
    
    var draggable: Bool = false
    var animating: Bool = false
    
    var collectionViewFrameInCanvas: CGRect = CGRect.zero
    
    var hitTestRectagles = [String: CGRect]()
    
    var canvas: UIView? {
        didSet {
            if canvas != nil {
                
            }
        }
    }
    
    var axis: DraggingAxis = .free
    
    struct DraggingItem {
        var offset = CGPoint.zero
        var sourceCell: UICollectionViewCell
        var substituteView: UIView
        var originalIndexPath: IndexPath
    }
    
    struct DestinationItem {
        var offset = CGPoint.zero
        var sourceCell: UICollectionViewCell
        var substituteView: UIView
        var originalIndexPath: IndexPath
    }
    
    var draggingItem: DraggingItem?
    
    var destinationItem: DestinationItem?
    
    var tempDestinationIndexPath: IndexPath = IndexPath(item: 0, section: 0)
    var destinationIndexPath: IndexPath?
    
    var originalDraggingIndexPath: IndexPath?
    var lastSameSectionIndexPath: IndexPath?
    
    
    var type = 1
    
    
//    MARK: - 
    
    override init() {
        super.init()
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepare() {
        super.prepare()
        self.calculateBorders()
    }
    
//    MARK: -
    
    func setup() {
        if let collectionView = self.collectionView {
            let longPressGestureRecogniser = UILongPressGestureRecognizer(target: self, action: #selector(RearrangeableCollectionViewFlowLayout.handleGesture(_:)))
            
            longPressGestureRecogniser.minimumPressDuration = 0.3
            longPressGestureRecogniser.delegate = self
            
            collectionView.addGestureRecognizer(longPressGestureRecogniser)
            
            if self.canvas == nil {
                self.canvas = self.collectionView!.superview
            }
            
        }
    }
    
    
    fileprivate func calculateBorders() {
        
        if let collectionView = self.collectionView {
            
            collectionViewFrameInCanvas = collectionView.frame
            
            if self.canvas != collectionView.superview {
                collectionViewFrameInCanvas = self.canvas!.convert(collectionViewFrameInCanvas, from: collectionView)
            }
            
            var leftRect = collectionViewFrameInCanvas
            leftRect.size.width = 20
            hitTestRectagles["left"] = leftRect
            
            var rightRect = collectionViewFrameInCanvas
            rightRect.origin.x = rightRect.size.width - 20
            rightRect.size.width = 20
            hitTestRectagles["left"] = rightRect
            
            var topRect = collectionViewFrameInCanvas
            topRect.size.height = 20
            hitTestRectagles["top"] = topRect
            
            var bottomRect = collectionViewFrameInCanvas
            bottomRect.origin.y = bottomRect.origin.y + bottomRect.size.height - 20
            bottomRect.size.height = 20
            hitTestRectagles["bottom"] = bottomRect
            
        }
    }
    
    
//    MARK: - UIGestureRecognizerDelegate
    
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if draggable == false {
            return false
        }
        
        guard let canvas = self.canvas else { return false }
        
        guard let collectionView = self.collectionView else { return false }
        
        
        let pointPressedInCanvas = gestureRecognizer.location(in: canvas)
        
        let pointPressedInCollection = gestureRecognizer.location(in: collectionView)//collectionView.convert(pointPressedInCanvas, from: canvas)
        
        if let indexPath = collectionView.indexPathForItem(at: pointPressedInCollection) {
            
            if collectionView.delegate is RearrangeableCollectionViewDelegate {
                let theDelegate = collectionView.delegate as! RearrangeableCollectionViewDelegate
                if theDelegate.canMoveItem(indexPath: indexPath) == false {
                    return false
                }
            }
            
            if let cell = collectionView.cellForItem(at: indexPath) {
                
                if let rCell = cell as? RearrangeableCollectionViewCell {
                    rCell.dragging = true
                }
                
                let cellInCanvasFrame = canvas.convert(cell.frame, from: collectionView)
                
                
                UIGraphicsBeginImageContextWithOptions(cell.bounds.size, cell.isOpaque, 0)
                cell.layer.render(in: UIGraphicsGetCurrentContext()!)
                let img = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                let substituteImage = UIImageView(image: img)
                
                substituteImage.frame = cellInCanvasFrame
                substituteImage.alpha = 0.7
                
                let offset = CGPoint(x: pointPressedInCanvas.x - cellInCanvasFrame.origin.x, y: pointPressedInCanvas.y - cellInCanvasFrame.origin.y)
                
                draggingItem = DraggingItem(offset: offset, sourceCell: cell, substituteView: substituteImage, originalIndexPath: indexPath)
                
                originalDraggingIndexPath = indexPath
            }
            
        }
        
        return (draggingItem != nil)
    }
    
        
//        MARK: -
        
    func checkForDraggingAtTheEdgeAndAnimatePaging(_ gestureRecognizer: UILongPressGestureRecognizer) {
        
        if animating == true {
            return
        }
        
        if let draggingItem = draggingItem {
            
            var nextPageRect : CGRect = self.collectionView!.bounds
            
            if self.scrollDirection == UICollectionViewScrollDirection.horizontal {
                
                if draggingItem.substituteView.frame.intersects(hitTestRectagles["left"]!) {
                    
                    nextPageRect.origin.x -= nextPageRect.size.width
                    
                    if nextPageRect.origin.x < 0.0 {
                        nextPageRect.origin.x = 0.0
                    }
                }
                else if draggingItem.substituteView.frame.intersects(hitTestRectagles["right"]!) {
                    
                    nextPageRect.origin.x += nextPageRect.size.width
                    
                    if nextPageRect.origin.x + nextPageRect.size.width > self.collectionView!.contentSize.width {
                        nextPageRect.origin.x = self.collectionView!.contentSize.width - nextPageRect.size.width
                    }
                }
            }
            else if self.scrollDirection == UICollectionViewScrollDirection.vertical {
                
                if draggingItem.substituteView.frame.intersects(hitTestRectagles["top"]!) {
                    
                    nextPageRect.origin.y -= nextPageRect.size.height
                    
                    if nextPageRect.origin.y < 0.0 {
                        nextPageRect.origin.y = 0.0
                    }
                }
                else if draggingItem.substituteView.frame.intersects(hitTestRectagles["bottom"]!) {
                    
                    nextPageRect.origin.y += nextPageRect.size.height
                    
                    if nextPageRect.origin.y + nextPageRect.size.height > self.collectionView!.contentSize.height {
                        nextPageRect.origin.y = self.collectionView!.contentSize.height - nextPageRect.size.height
                    }
                }
            }
            
            if !nextPageRect.equalTo(self.collectionView!.bounds){
                
                let delayTime = DispatchTime.now() + Double(Int64(0.8 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    
                    self.animating = false
                    self.handleGesture(gestureRecognizer)
                });
                
                self.animating = true
                self.collectionView!.scrollRectToVisible(nextPageRect, animated: true)
            }
        }
        
    }
        
        
    @objc func handleGesture(_ gesture: UILongPressGestureRecognizer) {
    
        guard var draggingItem = draggingItem else {
            return
        }
        
        
        func endDraggingAction(_ draggingItem: DraggingItem) {
            
            collectionView?.isUserInteractionEnabled = false
            
            if type == 0 {
            
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    draggingItem.substituteView.frame.origin = (self.canvas?.convert(draggingItem.sourceCell.frame.origin, from: self.collectionView))!
                }) { (isComplete: Bool) in
                    draggingItem.substituteView.removeFromSuperview()
                    draggingItem.sourceCell.isHidden = false
                    if let rCell = draggingItem.sourceCell as? RearrangeableCollectionViewCell {
                        rCell.dragging = false
                    }
                    if let cv = self.collectionView, cv.delegate is RearrangeableCollectionViewDelegate {
                        cv.reloadData()
                    }
                    
                    self.draggingItem = nil
                    
                    self.collectionView?.isUserInteractionEnabled = true
                }
            }
            else if type == 1 {
                
                if let dIndexPath = destinationIndexPath {
                    let lastDestinationCell = collectionView!.cellForItem(at: dIndexPath)
                    lastDestinationCell?.contentView.backgroundColor = UIColor.clear
                }
                
                if draggingItem.originalIndexPath.section == destinationIndexPath?.section {
                    
                    UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        draggingItem.substituteView.frame.origin = (self.canvas?.convert(draggingItem.sourceCell.frame.origin, from: self.collectionView))!
                        
                        draggingItem.substituteView.alpha = 1
                        
                    }) { (isComplete: Bool) in
                        
                        self.draggingActionComplete()
                        
                    }
                    
                    
                }
                else {
                    
                    if let delegate = self.collectionView!.delegate as? RearrangeableCollectionViewDelegate {
                        delegate.swapDataItem(from: draggingItem.originalIndexPath, to: destinationIndexPath!)
                    }
                    
                    
                    destinationItem?.sourceCell.isHidden = true
                    
                    
                    
                    self.canvas?.insertSubview(destinationItem!.substituteView, belowSubview: draggingItem.substituteView)
                    
                    UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                        draggingItem.substituteView.frame.origin = (self.canvas?.convert(self.destinationItem!.sourceCell.frame.origin, from: self.collectionView))!
                        
                        draggingItem.substituteView.alpha = 1
                        
                        self.destinationItem!.substituteView.frame.origin = (self.canvas?.convert(draggingItem.sourceCell.frame.origin, from: self.collectionView))!
                    }) { (isComplete: Bool) in
                        
                        self.destinationItem?.sourceCell.isHidden = false
                        self.destinationItem?.substituteView.removeFromSuperview()
                        
                        self.destinationItem = nil
                        
                        self.draggingActionComplete()
                    }
                    
                    
                }
            }
            
        }
        
        let dragPointOnCanvas = gesture.location(in: self.canvas)
        
//        print("gesture \(gesture.state.rawValue)")
        
        switch gesture.state {
            
        case .began:
            
            draggingItem.sourceCell.isHidden = true
            self.canvas?.addSubview(draggingItem.substituteView)
            
            var imageViewFrame = draggingItem.substituteView.frame
            var point = CGPoint.zero
            point.x = dragPointOnCanvas.x - draggingItem.offset.x
            point.y = dragPointOnCanvas.y - draggingItem.offset.y
            
            imageViewFrame.origin = point
            draggingItem.substituteView.frame = imageViewFrame
            
            destinationIndexPath = draggingItem.originalIndexPath
            
            break
            
        case .changed:
            // Update the representation image
            var imageViewFrame = draggingItem.substituteView.frame
            var point = CGPoint(x: dragPointOnCanvas.x - draggingItem.offset.x, y: dragPointOnCanvas.y - draggingItem.offset.y)
            if self.axis == .x {
                point.y = imageViewFrame.origin.y
            }
            if self.axis == .y {
                point.x = imageViewFrame.origin.x
            }
            
            
            imageViewFrame.origin = point
            draggingItem.substituteView.frame = imageViewFrame
            
            
            var dragPointOnCollectionView = gesture.location(in: self.collectionView)
            
            if self.axis == .x {
                dragPointOnCollectionView.y = draggingItem.substituteView.center.y
            }
            if self.axis == .y {
                dragPointOnCollectionView.x = draggingItem.substituteView.center.x
            }
            
            if let indexPath : IndexPath = self.collectionView?.indexPathForItem(at: dragPointOnCollectionView) {
                
                checkForDraggingAtTheEdgeAndAnimatePaging((gesture))
                
                print("indexPath \(indexPath), originalIndexPath \(draggingItem.originalIndexPath)")
                
                
                
                
                if type == 0 {
                    if indexPath != draggingItem.originalIndexPath {
                        
                        // If we have a collection view controller that implements the delegate we call the method first
                        
                        moveItemInSameSection(indexPath: indexPath)
                        
                    }
                }
                else if type == 1{
                    
                    if let dIndexPath = destinationIndexPath {
                        let lastDestinationCell = collectionView!.cellForItem(at: dIndexPath)
                        lastDestinationCell?.contentView.backgroundColor = UIColor.clear
                    }
                    
                    if indexPath.section != draggingItem.originalIndexPath.section {
                        
                        //跨section拖曳後，將原section復原並重新設定draggingItem indexPath
                        if originalDraggingIndexPath != nil && lastSameSectionIndexPath != nil {
                            if let delegate = self.collectionView!.delegate as? RearrangeableCollectionViewDelegate {
                                delegate.moveDataItem(from: lastSameSectionIndexPath!, to: originalDraggingIndexPath!)
                            }
                            
                            self.collectionView!.moveItem(at: lastSameSectionIndexPath!, to: originalDraggingIndexPath!)
                            
                            self.draggingItem!.originalIndexPath = originalDraggingIndexPath!
                            
                            lastSameSectionIndexPath = nil
                        }
                        
                        destinationIndexPath = indexPath
                    
                        //防止拖曳速度過快時，無法取得destinationCell的問題
                        guard collectionView!.cellForItem(at: indexPath) != nil else {
                            return
                        }
                        
                        let destinationCell = collectionView!.cellForItem(at: indexPath)
                    
                        destinationCell?.contentView.backgroundColor = UIColor.lightGray
                        
                        
                        let offset = CGPoint(x: destinationCell!.frame.width/2, y: destinationCell!.frame.height/2)
                        
                        let cellInCanvasFrame = canvas!.convert(destinationCell!.frame, from: collectionView)
                        
                        UIGraphicsBeginImageContextWithOptions(destinationCell!.bounds.size, destinationCell!.isOpaque, 0)
                        destinationCell!.layer.render(in: UIGraphicsGetCurrentContext()!)
                        let img = UIGraphicsGetImageFromCurrentImageContext()
                        UIGraphicsEndImageContext()
                        
                        let substituteImage = UIImageView(image: img)
                        
                        substituteImage.frame = cellInCanvasFrame
                        
                        self.destinationItem = DestinationItem(offset: offset, sourceCell: destinationCell!, substituteView: substituteImage, originalIndexPath: destinationIndexPath!)
                    }
                    else {
                        lastSameSectionIndexPath = indexPath
                        
                        destinationIndexPath = indexPath
                        
                        moveItemInSameSection(indexPath: indexPath)
                    }
                }
                
            }
            break
            
        case .ended:
            endDraggingAction(draggingItem)
            break
            
        case .cancelled:
            endDraggingAction(draggingItem)
            break
            
        case .failed:
            endDraggingAction(draggingItem)
            
            break
            
        case .possible:
            break
            
        }
    
    
    }
    
    
    //MARK: - Dragging Action
    
    func moveItemInSameSection(indexPath: IndexPath) {
        if let delegate = self.collectionView!.delegate as? RearrangeableCollectionViewDelegate {
            delegate.moveDataItem(from: (draggingItem?.originalIndexPath)!, to: indexPath)
        }
        
        self.collectionView!.moveItem(at: (draggingItem?.originalIndexPath)!, to: indexPath)
        
        self.draggingItem!.originalIndexPath = indexPath
    }
    
    func draggingActionComplete() {
        
        if let cv = self.collectionView, cv.delegate is RearrangeableCollectionViewDelegate {
            cv.reloadData()
        }
        
        if let rCell = draggingItem?.sourceCell as? RearrangeableCollectionViewCell {
            rCell.dragging = false
        }
        
        self.draggingItem?.sourceCell.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: { 
            self.draggingItem?.substituteView.alpha = 0
        }) { (isComplete: Bool) in
            
            self.draggingItem?.substituteView.removeFromSuperview()
            
            self.draggingItem = nil
            
            self.collectionView?.isUserInteractionEnabled = true
            
            self.destinationIndexPath = nil
            
            self.originalDraggingIndexPath = nil
            
            if let delegate = self.collectionView!.delegate as? RearrangeableCollectionViewDelegate {
                delegate.didEndDragging()
            }
        }
    }
    
    
}
