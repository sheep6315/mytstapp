//
//  RecommendProducts.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/18.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class Product: NSObject {
    
    private(set) var productId : String = "" //設備編號(APP不使用)
    private(set) var displayWeight : String = "" //排序權重(APP不使用)
    private(set) var materialId : String = "" //料號編號(APP不使用)
    private(set) var productName : String = "" //商品名稱
    private(set) var path : String = "" //圖檔路徑
    private(set) var price : String = "" //價格
    private(set) var titleText : String = "" //標題
    private(set) var url : String = "" //網址路徑
    private(set) var mobileVendorNameNew : String = "" //廠牌名稱(APP不使用)
    private(set) var stampType : String = "" //貼標種類 0:不處理 1:星星圖示 3:紅圈圖示
    private(set) var stampText : String = "" //貼標文字,有”貼標種類”才顯示
    private(set) var monthlyFee : String = "" //實收月租費(APP不使用)
    private(set) var discountPrice : String = "" //商品專案價(APP不使用)
    private(set) var priceNew : String = "" //商品自訂價格(APP不使用)
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        productId = dic.stringValue(ofKey: "productId")
        displayWeight = dic.stringValue(ofKey: "displayWeight")
        materialId = dic.stringValue(ofKey: "materialId")
        productName = dic.stringValue(ofKey: "productName")
        path = dic.stringValue(ofKey: "path")
        price = dic.stringValue(ofKey: "price")
        url = dic.stringValue(ofKey: "url")
        mobileVendorNameNew = dic.stringValue(ofKey: "mobileVendorNameNew")
        stampType = dic.stringValue(ofKey: "stampType")
        stampText = dic.stringValue(ofKey: "stampText")
        monthlyFee = dic.stringValue(ofKey: "monthlyFee")
        discountPrice = dic.stringValue(ofKey: "discountPrice")
        priceNew = dic.stringValue(ofKey: "priceNew")
    }
}

class RecommendProducts: NSObject {
    
    static let sharedInstance = RecommendProducts()
    
    private(set) var productArray: [Product] = []
    
  
    override init () {
        
    }
    
    func clear() {
        productArray = []
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if dic.stringValue(ofKey: "code") == "00000" {
            clear()
            if let productInfoArray = dic["data"] as? Array<Dictionary<String, Any>>  {
                for dic in productInfoArray {
                    let product = Product()
                    product.setDataValue(withDic: dic)
                    productArray.append(product)
                }
            }
        }

    }
    
}
