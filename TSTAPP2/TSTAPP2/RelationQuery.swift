//
//  RelationQuery.swift
//  TSTAPP2
//
//  Created by apple on 2017/2/21.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class RelationQuery: NSObject {
    
    static let sharedInstance = RelationQuery()
    
    ///已關連預付卡數量
    private(set) var relatedCount: String = ""
    
    ///還可關連預付卡數量
    private(set) var canRelatedCount: String = ""
    
    ///小標題
    private(set) var subtitle: String = ""
    
    ///標題
    private(set) var title: String = ""
    
    ///內容
    private(set) var content: String = ""
    
    ///按鈕文字
    private(set) var buttonText: String = ""
    
    ///按鈕連結
    private(set) var buttonUrl: String = ""
    
    //是否顯示親子專區Y/N
    private(set) var showPFS: String = ""
    
    //是否去親子專區Y/N
    private(set) var isPrepaidFamily: String = ""
    
    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                relatedCount = apiData.stringValue(ofKey: "relatedCount")
                canRelatedCount = apiData.stringValue(ofKey: "canRelatedCount")
                subtitle = apiData.stringValue(ofKey: "subtitle")
                title = apiData.stringValue(ofKey: "title")
                content = apiData.stringValue(ofKey: "description")
                buttonText = apiData.stringValue(ofKey: "buttonText")
                buttonUrl = apiData.stringValue(ofKey: "buttonUrl")
                showPFS = apiData.stringValue(ofKey: "showPFS")
                isPrepaidFamily = apiData.stringValue(ofKey: "isPrepaidFamily")
            }
        }
    }
}
