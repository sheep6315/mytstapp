//
//  RoamingAppliedViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/4/17.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class RoamingAppliedViewController: TSUIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var noDataView: UIView!
    
    var list: [RoamingAppliedList] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        list = DailyRoaming.sharedInstance.roamingAppliedList
        noDataView.isHidden = list.count <= 0 ? false : true
   
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        if list.count > 0 {
            tableViewHeight.constant = CGFloat(44 * list.count + 236)
        }
        else {
            tableViewHeight.constant = CGFloat(0)
        }
        
        self.view.setNeedsUpdateConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func didTapCellButton(_ sender: AnyObject) {
        let button = sender as! UIButton
        
        if list[button.tag].statusCode == "A" {
            self.callDailyRoamingCancelMessageAPI(appliedId: list[button.tag].appliedId, appliedIndex: button.tag)
        }
        else{
            let alert = UIAlertController(title: "", message: "是否取消日租漫遊服務預約?", preferredStyle: .alert)
            let submit = UIAlertAction(title: "確定", style: .default, handler: { (UIAlertAction) in
                
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "國際漫遊數據設定日租預約申請", action: "日租優惠申請 取消", label: nil, value: nil)
                self.callCancelDailyRoamingAPI(appliedId: self.list[button.tag].appliedId, countryId: self.list[button.tag].countryId, statusCode: self.list[button.tag].statusCode)
            })
            
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            
            alert.addAction(submit)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - API
    func callDailyRoamingCancelMessageAPI(appliedId:String, appliedIndex:Int) {
        
        TSTAPP2_API.sharedInstance.getDailyRoamingCancelMessage(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, appliedId: appliedId, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            let alert = UIAlertController(title: "", message: dic.stringValue(ofKey: "errorMessage"), preferredStyle: .alert)
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let action = UIAlertAction(title: "確定", style: .default, handler: { (UIAlertAction) in
                    
                    self.callCancelDailyRoamingAPI(appliedId: self.list[appliedIndex].appliedId, countryId: self.list[appliedIndex].countryId, statusCode: self.list[appliedIndex].statusCode)
                })
                
                let cacel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
                
                alert.addAction(action)
                alert.addAction(cacel)
            }
            else{
                let action = UIAlertAction(title: "確定", style: .cancel, handler:nil)
                
                alert.addAction(action)
            }
            
            self.present(alert, animated: true, completion: nil)
        }) { (Error) in
            
        }
        
    }
    
    func callCancelDailyRoamingAPI(appliedId:String,countryId:String,statusCode:String) {
        
        TSTAPP2_API.sharedInstance.cancelDailyRoaming(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, appliedId: appliedId, countryId: countryId, statusCode: statusCode, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "errorCode") == "0000" {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "errorMessage"), viewController: self)
            }
            else{
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "errorMessage"), viewController: self)
            }
            
        }) { (Error) in
            
        }
    }
    
    //MARK: - tableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 44
        let theCell = tableView.cellForRow(at: indexPath) as? RoamCell
        if theCell != nil {
            if theCell!.expand {
                height += (theCell?.cellContentView.frame.height)!
            }
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let roamCell = tableView.cellForRow(at: indexPath)! as! RoamCell
        roamCell.expand = !roamCell.expand
        roamCell.arrow.image = roamCell.expand ? UIImage(named: "icon_arrow_up") : UIImage(named: "icon_arrow_down")
        
        var tableHeight: CGFloat = 0
        for cell in tableView.visibleCells {
            tableHeight += CGFloat(roamCellHeight)
            if (cell as! RoamCell).expand {
                tableHeight += (cell as! RoamCell).cellContentView.frame.height
            }
        }
        
        tableViewHeight.constant = tableHeight
        self.view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: { (isComplete: Bool) in
            
        })
    }
    
    //MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = RoamCell()
        cell = tableView.dequeueReusableCell(withIdentifier: "roamCell", for: indexPath) as! RoamCell
        if indexPath.row == 0 {
            cell.expand = true
            cell.arrow.image = UIImage(named: "icon_arrow_up")
        }
        cell.cellButton.tag = indexPath.row
        cell.cellButton.addTarget(self, action: #selector(self.didTapCellButton(_:)), for: UIControlEvents.touchUpInside)
        
        cell.appliedCountry.text = list[indexPath.row].countryName
        cell.applyDays.text = list[indexPath.row].applyDays
        cell.effectiveTime.text = list[indexPath.row].effectiveTime
        cell.localEffectiveTime.text = list[indexPath.row].localEffectiveTime
        cell.effectiveTimeNote.text = "("+list[indexPath.row].gmtNote+")"
        cell.expiredTime.text = list[indexPath.row].expiryTime
        cell.localExpiredTime.text = list[indexPath.row].localExpiryTime
        cell.expiredTimeNote.text = "("+list[indexPath.row].gmtNote+")"
        cell.status.text = list[indexPath.row].status
        UISkinChange.sharedInstance.changeUIColor(theObject: cell.cellButton, mode: ButtonColorMode.backgroundImage.rawValue)
        
        let formatter = DateFormatter.init()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        formatter.timeZone = TimeZone(secondsFromGMT: 8)
        
        let effectiveDate = formatter.date(from: list[indexPath.row].effectiveTime+":00")
        let expiredDate = formatter.date(from: list[indexPath.row].expiryTime+":00")
        let systemDate = formatter.date(from: DailyRoaming.sharedInstance.systemTime)
        let systemTimeAdd26h = Calendar(identifier: .gregorian).date(byAdding: .hour, value: 26, to: systemDate!)
        let systemTimeAdd2h = Calendar(identifier: .gregorian).date(byAdding: .hour, value: 2, to: systemDate!)
        
        let intervalBeforeEffect = Double(effectiveDate!.timeIntervalSince1970) - Double(systemDate!.timeIntervalSince1970)
        let intervalBeforeEffect2h = Double(effectiveDate!.timeIntervalSince1970) - Double(systemTimeAdd2h!.timeIntervalSince1970)
        let intervalBeforeExpire26h = Double(expiredDate!.timeIntervalSince1970) - Double(systemTimeAdd26h!.timeIntervalSince1970)
        //申請中 取消中
        if list[indexPath.row].status == "申請中" || list[indexPath.row].status == "取消中" {
            cell.cellButton.isHidden = true
        }
        else if intervalBeforeEffect > 0 && intervalBeforeEffect2h < 0  {
            //生效時間 < 系統時間 + 2h
            cell.cellButton.isHidden = true
        }
        else if intervalBeforeEffect < 0 && intervalBeforeExpire26h < 0 {
            //生效時間 < 系統時間
            //到期時間 < 系統時間 + 26h
            cell.cellButton.isHidden = true
        }
        else{
            cell.cellButton.isHidden = false
        }
        //cell.cellButton.isHidden = false
        
        return cell
    }
}
