//
//  RoamingViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/27.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

//Roam CELL
let roamCellHeight = 44
let roamCellExpandHeight = 350
let appliedViewHeight = 655

class RoamCell: UITableViewCell {
    
    
    @IBOutlet weak var cellContentView: UIView!
    
    @IBOutlet weak var cellButton: UIButton!

    @IBOutlet weak var applyDays: UILabel!
    
    @IBOutlet weak var effectiveTime: UILabel!
    
    @IBOutlet weak var localEffectiveTime: UILabel!
    
    @IBOutlet weak var effectiveTimeNote: UILabel!
    
    @IBOutlet weak var expiredTime: UILabel!
    
    @IBOutlet weak var localExpiredTime: UILabel!
    
    @IBOutlet weak var expiredTimeNote: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var appliedCountry: UILabel!
    
    @IBOutlet weak var arrow: UIImageView!
    
    var expand = false

}

class RoamingViewController: TSUIViewController,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dailyView: UIView!
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var segementController: UISegmentedControl!
    
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countryButton: UIButton!
    
    @IBOutlet weak var applyProductLabel: UILabel!
    
    @IBOutlet weak var productRateLabel: UILabel!
    @IBOutlet weak var vipOnlyLabel: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    
    @IBOutlet weak var vipOnlyLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var beginDateButton: UIButton!
    @IBOutlet weak var beginDateTextField: UITextField!
    @IBOutlet weak var beginTimeTextField: UITextField!
    @IBOutlet weak var beginTimeButton: UIButton!
    @IBOutlet weak var beginLocalTimeLabel: UILabel!
    @IBOutlet weak var beginLocalTimeNoteLabel: UILabel!
    
    @IBOutlet weak var applyDaysButton: UIButton!
    @IBOutlet weak var applyDaysTextField: UITextField!
    
    @IBOutlet weak var expiredDateLabel: UILabel!
    @IBOutlet weak var expiredLocalTimeLabel: UILabel!
    @IBOutlet weak var expiredLocalTimeNoteLabel: UILabel!
    
    @IBOutlet weak var submitButton: UIButton!
    
    //一般計量View
        
    @IBOutlet weak var switchButton: UISwitch!
    
    @IBOutlet weak var greetingView: UIView!
    
    @IBOutlet weak var greetingLabel: UILabel!
    
    @IBOutlet weak var greetingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var countryAndRateButton: UIButton!
    
    #if SIT
    let RateUrl: String = "https://uattsp.tstartel.com/mCWS/nationRoaming.php?showTab=1"
    let NationRoaming: String = "https://uattsp.tstartel.com/mCWS/nationRoaming.php?showTab=2"
    #elseif UAT
    let RateUrl: String = "https://uattsp.tstartel.com/mCWS/nationRoaming.php?showTab=1"
    let NationRoaming: String = "https://uattsp.tstartel.com/mCWS/nationRoaming.php?showTab=2"
    #else
    let RateUrl: String = "https://www.tstartel.com/mCWS/nationRoaming.php?showTab=1"
    let NationRoaming: String = "https://www.tstartel.com/mCWS/nationRoaming.php?showTab=2"
    #endif
    
    //1 = prodution , 2 = uat
//    let RateUrl: String = "https://www.tstartel.com/mCWS/nationRoaming.php?showTab=1"
    //let RateUrlUat: String = "https://uattsp.tstartel.com/mCWS/nationRoaming.php?showTab=1"
    
//    let NationRoaming: String = "https://www.tstartel.com/mCWS/nationRoaming.php?showTab=2"
    //let NationRoamingUat: String = "https://uattsp.tstartel.com/mCWS/nationRoaming.php?showTab=2"
    
    var seletedCountry: RoamingCountryList? = nil
    var countryList: Array<RoamingCountryList> = []
    var systemTime: String = ""
    var beginTimeList: Array<String> = []
    var applyDaysList: Array<String> = []
    var beginDate: String = ""
    var beginTime: String = ""
    var applyDay: String = ""
    
    var didSelectCountry: Bool = false
    
    //一般計量
    var greeting :String = ""
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        greetingViewHeight.constant = 0
        
        generalView.isHidden = true
        dailyView.isHidden = false
     
        self.countryButton.layer.borderWidth = 1
        self.countryButton.layer.cornerRadius = 5
        self.countryButton.layer.borderColor = UIColor.lightGray.cgColor
        self.beginDateButton.layer.borderWidth = 1
        self.beginDateButton.layer.cornerRadius = 5
        self.beginDateButton.layer.borderColor = UIColor.lightGray.cgColor
        self.beginTimeButton.layer.borderWidth = 1
        self.beginTimeButton.layer.cornerRadius = 5
        self.beginTimeButton.layer.borderColor = UIColor.lightGray.cgColor
        self.applyDaysButton.layer.borderWidth = 1
        self.applyDaysButton.layer.cornerRadius = 5
        self.applyDaysButton.layer.borderColor = UIColor.lightGray.cgColor
        
        var formatter = DateFormatter.init()
        formatter.dateFormat = "yyyy/MM/dd"
        beginDate = formatter.string(from: Date())
        self.beginDateButton.setTitle(beginDate, for: .normal)
        formatter = DateFormatter.init()
        formatter.dateFormat = "HH"
        beginTime = formatter.string(from: Date())+":00"
        
        if beginTime.contains(" AM") {
            beginTime = beginTime.replacingOccurrences(of: " AM", with: "")
        }
        else if beginTime.contains(" PM") {
            beginTime = beginTime.replacingOccurrences(of: " PM", with: "")
        }
        
        self.beginTimeButton.setTitle(beginTime, for: .normal)
        self.applyDay = "1"
        
            TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        
        //setPickerData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP050203", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "國際漫遊數據設定日租預約申請", label: "日租優惠申請", value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "日租優惠申請")
        
        checkVIPStatusAndChangeSkin()
        
        callDailyRoamingAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if segementController.selectedSegmentIndex == 0 {
            
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050203", action: "")
        }
        else if segementController.selectedSegmentIndex == 1 {
            
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050202", action: "")
        }
        
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: segementController)
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: rateButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: countryAndRateButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: switchButton)
    }
    
    func setPickerData(){
        
        for i in 0...23 {
            let hour = i < 10 ? "0\(i):00" : "\(i):00"
            beginTimeList.append(hour)
        }
        //String(format: "%02d:00", i)
        for i in 1...30 {
            applyDaysList.append(String(i))
        }
        
        let pickBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 44))
        let itemC = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.didTapDone(button:)))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        pickBar.setItems([flex,itemC], animated: false)
        
        var pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 0
        countryTextField.inputView = pickerView
        countryTextField.inputAccessoryView = pickBar;
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy 年 MM 月 dd 日"
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.locale = Locale(identifier: "zh_TW")
        datePickerView.date = Date()
        datePickerView.minimumDate = Date()
        let calendar = NSCalendar(identifier: .gregorian)
        var component = DateComponents()
        component.month = 3
        datePickerView.maximumDate = calendar?.date(byAdding: component, to: Date(), options: .init(rawValue: 0))
        datePickerView.addTarget(self, action: #selector(self.datePickerChanged(datePicker:)), for: .valueChanged)
        
        beginDateTextField.inputView = datePickerView
        beginDateTextField.inputAccessoryView = pickBar;
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 2
        beginTimeTextField.inputView = pickerView
        beginTimeTextField.inputAccessoryView = pickBar;
        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.tag = 3
        applyDaysTextField.inputView = pickerView
        applyDaysTextField.inputAccessoryView = pickBar;
        
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let dateString = formatter.string(from: datePicker.date)
        beginDate = dateString
        beginDateButton.setTitle(dateString, for: .normal)
        
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }
        
    //MARK: - 按鈕動作
    
    @IBAction func didTapSubmit(_ sender: AnyObject) {
    
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .alert)
        
        var components = DateComponents.init()
        components.hour = -2
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.init(secondsFromGMT: 8)
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let dateString = beginDate+" "+beginTime+":00"
        let newDate = NSCalendar(calendarIdentifier: .gregorian)?.date(byAdding: components, to: formatter.date(from: dateString)!, options: .init(rawValue: 0))
        
        var compareResult = ComparisonResult.orderedDescending
        
        if let date = formatter.date(from: systemTime) {
//            let system = formatter.date(from: systemTime)
            compareResult = date.compare(newDate!)
    
        }

        if seletedCountry == nil {
            
            alert.message = "請選擇申請國家"
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alert.addAction(action)
        }
        else if (compareResult != ComparisonResult.orderedAscending){
            
            alert.message = "申請的日期錯誤，\n開始日期要大於目前2小時"
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alert.addAction(action)
        }else{
            
            let effectiveTime = beginDate+" "+beginTime

            alert.message = "確認預約國際漫遊日租服務?"
            let action = UIAlertAction(title: "確定", style: .default, handler:
                { (UIAlertAction) in
                    
                    UbaRecord.sharedInstance.sendUba(withPage: "APP050203", action: "A030201")
                    TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "國際漫遊數據設定日租預約申請", action: "日租優惠申請 申請/\(self.seletedCountry!.countryName)/\(self.applyDay)/\(effectiveTime)", label: nil, value: nil)
                    
                    self.callApplyDailyRoamingAPI(effectiveTime: effectiveTime, applyDays: self.applyDay,countryId: self.seletedCountry!.countryId)
            })
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            
            alert.addAction(action)
            alert.addAction(cancel)
            
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTapQueryRate(_ sender: AnyObject) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP050203", action: "A050201")
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "費率查詢", urlString: RateUrl, viewController: self)
    }

    @IBAction func didTapCountryTextField(_ sender: AnyObject) {
        
        if seletedCountry == nil{
            seletedCountry = countryList.first
            countryButton.setTitle(seletedCountry?.countryName, for: .normal)
        }
        self.countryTextField.becomeFirstResponder()
        didSelectCountry = true
    }
    

    @IBAction func didTapBeginDateTextField(_ sender: AnyObject) {
        if didSelectCountry {
            self.beginDateTextField.becomeFirstResponder()
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇申請國家", viewController: self)
        }
    }
    
    @IBAction func didTapApplyDaysTextField(_ sender: AnyObject) {
        if didSelectCountry {
            self.applyDaysTextField.becomeFirstResponder()
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇申請國家", viewController: self)
        }
    }
    
    @IBAction func didTapBeginTimeTextField(_ sender: AnyObject) {
        if didSelectCountry {
            self.beginTimeTextField.becomeFirstResponder()
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "請選擇申請國家", viewController: self)
        }
    }
    
    @IBAction func didTapChangeView(_ sender: AnyObject) {
        
        switch segementController.selectedSegmentIndex {
            
        case 0:
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP050203", action: "")
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050202", action: "")
//            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "國際漫遊數據設定日租預約申請", label: "日租優惠申請", value: nil)
            TSTAPP2_Utilities.sendGAScreen(withScreenName: "日租優惠申請")
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            scrollView.contentSize = CGSize(width:self.view.bounds.width, height: dailyView.bounds.height+105)
            generalView.isHidden = true
            dailyView.isHidden = false
            break
        case 1:
    
            UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP050202", action: "")
            UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP050203", action: "")
//            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "國際漫遊數據設定日租預約申請", label: "一般計量型申請", value: nil)
            TSTAPP2_Utilities.sendGAScreen(withScreenName: "一般計量型申請")
            self.view.endEditing(true)
            scrollView.contentSize = CGSize(width:self.view.bounds.width, height: generalView.bounds.height+105)
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            //callRoamingAPI()
            callRoamingStatusAPI()

            generalView.isHidden = false
            dailyView.isHidden = true
            break
        default:
            break

        }
        
        
    }

    @objc func didTapDone(button: UIBarButtonItem) {
        
        if seletedCountry == nil {
            seletedCountry = countryList.first
        }
        
        self.applyProductLabel.text = seletedCountry?.productName
        let rate = LoginData.sharedLoginData().custProfile_isVIP == "Y" ? seletedCountry?.vipRates : seletedCountry?.rates
        self.productRateLabel.text = "$\(rate!)/日"
        self.productRateLabel.sizeToFit()
        
        if LoginData.sharedLoginData().custProfile_isVIP == "Y" {
            vipOnlyLabel.isHidden = false
        }
        else {
            vipOnlyLabelWidth.constant = 0
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        let dateString = beginDate+" "+beginTime
  
        var dayComponents = DateComponents.init()
        dayComponents.day = Int(applyDay)
        var hourComponents = DateComponents.init()
        
        let expiredDate = NSCalendar(calendarIdentifier: .gregorian)?.date(byAdding: dayComponents, to:formatter.date(from: dateString)! , options: .init(rawValue: 0))
        self.expiredDateLabel.text = formatter.string(from: expiredDate!)
        
        if Int(seletedCountry!.gmtValue) != nil {
            
            self.beginLocalTimeNoteLabel.text = "("+(seletedCountry?.gmtNote)!+")"
            self.expiredLocalTimeNoteLabel.text =  "("+(seletedCountry?.gmtNote)!+")"
            
            hourComponents.hour = Int(seletedCountry!.gmtValue)! - 8//gmtValue
        
            let locoalDate = NSCalendar(calendarIdentifier: .gregorian)?.date(byAdding: hourComponents, to: formatter.date(from: dateString)!, options: .init(rawValue: 0))
            self.beginLocalTimeLabel.text = formatter.string(from: locoalDate!)

            let expiredLocalDate = NSCalendar(calendarIdentifier: .gregorian)?.date(byAdding: dayComponents, to: locoalDate!, options: .init(rawValue: 0))
            self.expiredLocalTimeLabel.text = formatter.string(from: expiredLocalDate!)
        }
        
        self.view.endEditing(true)
        
    }
    
    //MARK: - PickDelegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 0:
            return countryList.count
        //case 1:
        //    return beginDate.count
        case 2:
            return beginTimeList.count
        default:
            return applyDaysList.count
        }
        //return country.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView.tag {
        case 0:
            return countryList[row].countryName
        //case 1:
        //    return beginDate[row]
        case 2:
            return beginTimeList[row]
        default:
            return applyDaysList[row]
        }
    
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            seletedCountry = countryList[row]
            countryButton.setTitle(countryList[row].countryName, for: .normal)
            break
        //case 1:
        //    beginDateButton.setTitle(beginDate[row], for: .normal)
        //    break
        case 2:
            beginTime = beginTimeList[row]
            beginTimeButton.setTitle(beginTime, for: .normal)
            break
        default:
            applyDay = applyDaysList[row]
            applyDaysButton.setTitle(applyDaysList[row], for: .normal)
            break
        }
        
    }
    
    //MARK: - 一般計量
    
    @IBAction func didTapCountryAndRateQuery(_ sender: AnyObject) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP050202", action: "A050201")
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "國家及費率查詢", urlString: NationRoaming, viewController: self)
    }
    
    @IBAction func didTapSwitch(_ sender: AnyObject) {
        
        let message = !switchButton.isOn ? "提醒您，\n如您已申請國際漫遊行動上網日租型數據服務，系統將於服務開通時間自動為您開啟以下國際漫遊數據服務；\n確認關閉國際漫遊數據服務？":"提醒您，\n如您已申請國際漫遊行動上網日租型數據服務，系統將於服務開通時間自動為您開啟以下國際漫遊數據服務；\n確認開啟國際漫遊數據服務？"
        let alertControll = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let otherButton = UIAlertAction(title: "同意", style: .default) { (UIAlertAction) in
            
            UbaRecord.sharedInstance.sendUba(withPage: "APP050202", action: "A030101")
            let status = !self.switchButton.isOn ? "N":"Y"
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "國際漫遊數據設定日租預約申請", action: "一般計量型申請/\(status)", label: nil, value: nil)
            
            self.callChangeRoamingStatusAPI(status: status)
            
        }
        let cancelButton = UIAlertAction(title: "取消", style: .cancel) { (UIAlertAction) in
            
            self.switchButton.setOn(!self.switchButton.isOn, animated: true)
        }
        alertControll.addAction(cancelButton)
        alertControll.addAction(otherButton)
        self.present(alertControll, animated: true, completion: nil)
    }
    
    //MARK: - API
    func callDailyRoamingAPI() {
        
        TSTAPP2_API.sharedInstance.queryDailyRoaming(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                let dayliRoaming = DailyRoaming.sharedInstance
                dayliRoaming.setDataValue(withDic: dic)
                
                self.countryList = DailyRoaming.sharedInstance.roamingCountryList
                self.systemTime = DailyRoaming.sharedInstance.systemTime
                
                self.setPickerData()
            }
            }) { (Error) in
                
        }
        
    }
    
    func callRoamingAPI() {
    
        TSTAPP2_API.sharedInstance.queryRoaming(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, accountId: LoginData.sharedLoginData().custProfile_accountId, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if (dic["code"] as! String) == "00000" {
                
                if let apiData = dic["data"] as? Dictionary<String, Any> {
                    
                    self.greetingViewHeight.isActive = false
                    self.greeting = apiData.stringValue(ofKey: "greeting")
                    
                    let attr = try! NSAttributedString(data: self.greeting.data(using: .unicode, allowLossyConversion: true)!, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                    self.greetingLabel.attributedText = attr
                    self.greetingLabel.sizeToFit()
                    self.generalView.setNeedsUpdateConstraints()
                }

            }
            
            }) { (Error) in
                
        }
    }
    
    func callRoamingStatusAPI() {
        
        TSTAPP2_API.sharedInstance.queryRoamingStatus(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if (dic["code"] as! String) == "00000" {
                
                if let apiData = dic["data"] as? Dictionary<String, Any> {
                    
                    let status = apiData.stringValue(ofKey: "status")
                    
                    status == "Y" ? self.switchButton.setOn(true, animated: true) : self.switchButton.setOn(false, animated: false)
                    self.switchButton.isHidden = false
                }
                
            }
            else{
                self.switchButton.isHidden = true
            }
            
            }) { (Error) in
                
        }
    }
    
    func callApplyDailyRoamingAPI(effectiveTime :String,applyDays :String, countryId :String) {
        
        let loginData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.applyDailyRoaming(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, accountId: loginData.custProfile_accountId, countryId: countryId, effectiveTime: effectiveTime, applyDays: applyDays, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "errorCode") == "0000" {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "errorMessage"), viewController: self)
            }
            else{
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "errorMessage"), viewController: self)
            }
            
//            self.present(alert, animated: true, completion: nil)
        }) { (Error) in
            
        }
    }
    
    func callCancelDailyRoamingAPI(appliedId:String,countryId:String,statusCode:String) {
        
        TSTAPP2_API.sharedInstance.cancelDailyRoaming(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, appliedId: appliedId, countryId: countryId, statusCode: statusCode, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "errorCode") == "0000" {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "errorMessage"), viewController: self)
            }
            else{
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "errorMessage"), viewController: self)
            }
            
        }) { (Error) in
            
        }
    }
    
    func callChangeRoamingStatusAPI(status:String) {
        
        TSTAPP2_API.sharedInstance.changeRoamingStatus(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, status: status, showProgress: true, completion:  { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            let alert = UIAlertController(title: dic.stringValue(ofKey: "errorMessage"), message: "", preferredStyle: .alert)
            
            if dic.stringValue(ofKey: "errorCode") == "0000" {
                let action = UIAlertAction(title: "確定", style: .default, handler: { (UIAlertAction) in
                    
                    
                })
                
                alert.addAction(action)
            }
            else{
                let action = UIAlertAction(title: "確定", style: .cancel, handler:{ (UIAlertAction) in
                    
                    self.switchButton.setOn(!self.switchButton.isOn, animated: true)
                })
                
                alert.addAction(action)
            }
            
            alert.show(viewController: self)
            
        }) { (Error) in
            
        }

    }
}
