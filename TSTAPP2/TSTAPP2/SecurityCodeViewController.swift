//
//  SecurityCodeViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦-江宗澤 on 2017/6/1.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class SecurityCodeViewController: TSUIViewController,UITextFieldDelegate {

    @IBOutlet weak var securityCodeTextField: UITextField!
    @IBOutlet weak var securityCode2TextField: UITextField!
    @IBOutlet weak var captchaField: UITextField!
    @IBOutlet weak var captchaImageView: UIImageView!
    
    @IBOutlet weak var submitButton: UIButton!
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let captchaImg = Captcha.sharedInstance
        captchaImg.captchaSize = CGSize(width: captchaImageView.frame.width, height: captchaImageView.frame.height)
        self.captchaImageView.image = captchaImg.captchaImage.image
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "我的帳戶", action: "電信帳單代收服務開關管理", label: "設定交易安全碼", value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "設定交易安全碼")
        
        checkVIPStatusAndChangeSkin()
    }
    
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
        UISkinChange.sharedInstance.changeUIColor(theObject: submitButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - textField delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag != 3 {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 6 // Bool
        }
        return true
    }
    //MARK: - IBAction
    @IBAction func didTapGetVerifyCodeButton(_ sender: UIButton) {
        let captchaImg = Captcha.sharedInstance
        self.captchaImageView.image = captchaImg.refresh().image
    }
    
    @IBAction func didTapSubmitButton(_ sender: UIButton) {
        if securityCodeTextField.text != "" && securityCode2TextField.text != "" {
            let regex = try! NSRegularExpression(pattern: "[0-9]", options: .caseInsensitive)
            let range: NSRange = NSMakeRange(0, securityCodeTextField.text!.utf16.count)
            let matchArray: Array = regex.matches(in: securityCodeTextField.text!, options: NSRegularExpression.MatchingOptions.anchored, range: range)
            if matchArray.count < 6 {
                TSTAPP2_Utilities.showAlert(withMessage: "交易安全碼輸入格式錯誤，請再次確認", viewController: self)
            }else if securityCodeTextField.text != securityCode2TextField.text { //先檢查密碼
                TSTAPP2_Utilities.showAlert(withMessage: "交易安全碼輸入前後不一致，請再次確認", viewController: self)
            }else if self.captchaField.text != Captcha.sharedInstance.captchaString { // 再檢查驗證碼
                TSTAPP2_Utilities.showAlert(withMessage: "驗證碼輸入錯誤，請再次確認", viewController: self)
            }else {
                TSTAPP2_API.sharedInstance.updateSwitchStatus(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, switchType: "2", switchStatus: "Y", securityCode: self.securityCodeTextField.text!, showProgress: true, completion: {  (dic:Dictionary<String, Any>, response:URLResponse) in
                    
                    if dic.stringValue(ofKey: "code") == "00000" {
                        if let data = dic["data"] as? Dictionary<String, Any> {
                            if let resultMessage = data["resultMessage"] as? String {
                                TSTAPP2_Utilities.showAlertAndPop(withMessage: resultMessage, viewController: self)
                            }
                        }
                    }else {
                        TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                    }
                    
                }) { (error:Error) in
                    TSTAPP2_Utilities.showAlert(withMessage: "系統忙碌中，請稍後再試。", viewController: self)
                    print(error)
                }
            }
        }else {
            TSTAPP2_Utilities.showAlert(withMessage: "安全碼設定錯誤\n請重新設定", viewController: self)
        }
    }
    
    func didTapRefreshCaptcha() {
        let captchaImg = Captcha.sharedInstance
        self.captchaImageView.image = captchaImg.refresh().image
    }

}
