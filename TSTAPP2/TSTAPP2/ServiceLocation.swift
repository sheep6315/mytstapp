//
//  ServiceLocation.swift
//  TSTAPP2
//
//  Created by rd22 on 2016/10/20.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import CoreLocation

class ServiceLocationList : NSObject {
    
    static let sharedInstance = ServiceLocationList()
    
    private(set) var serviceLocationList : Array<ServiceLocation> = []
    
    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        serviceLocationList = []
        
        if (dic["code"] as! String) == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                let rawData = apiData["serviceLocationList"] as? Array<Dictionary<String, Any>> ?? []
                
                for dictionary in rawData {
                    
                    let location = ServiceLocation()
                    location.setDataValue(withDic: dictionary)
                    serviceLocationList.append(location)
                }
            }
        }
    }
}

class ServiceLocation: NSObject {
    
    private(set) var dealerId : String = "" //店點代碼
    private(set) var defaultFlag	: String = ""
    private(set) var digitalFlag	: String = ""
    private(set) var faxNumber : String = ""
    private(set) var googleSite : String = ""
    private(set) var latlng : String = ""	//經緯度
    private(set) var longitude : String = "" //經度
    private(set) var latitude : String = "" //緯度
    private(set) var locationName : String = "" //店點名稱
    private(set) var mapPhoto : String = ""
    private(set) var modate : String = ""
    private(set) var mouser : String = ""
    private(set) var operateTime : String = "" //營業時間
    private(set) var phoneNumber : String = "" //連絡電話
    private(set) var serviceItem : String = ""
    private(set) var storeAddress : String = "" //店點地址
    private(set) var storePhoto : String = ""
    private(set) var storeType : String = ""
    private(set) var traficBus : String = ""
    private(set) var traficMrt : String = ""
    private(set) var traficPark : String = ""
    private(set) var wapPhoto : String = ""
    private(set) var zipCode : String = "" //郵遞區號

    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {

        dealerId = dic.stringValue(ofKey: "dealerId")
        defaultFlag = dic.stringValue(ofKey: "defaultFlag")
        digitalFlag = dic.stringValue(ofKey: "digitalFlag")
        faxNumber = dic.stringValue(ofKey: "faxNumber")
        googleSite = dic.stringValue(ofKey: "googleSite")
        latlng = dic.stringValue(ofKey: "latlng")
        longitude = dic.stringValue(ofKey: "longitude").trimmingCharacters(in: .whitespacesAndNewlines)
        latitude = dic.stringValue(ofKey: "latitude").trimmingCharacters(in: .whitespacesAndNewlines)
        locationName = dic.stringValue(ofKey: "locationName")
        mapPhoto = dic.stringValue(ofKey: "mapPhoto")
        modate = dic.stringValue(ofKey: "modate")
        mouser = dic.stringValue(ofKey: "mouser")
        operateTime = dic.stringValue(ofKey: "operateTime")
        phoneNumber = dic.stringValue(ofKey: "phoneNumber")
        serviceItem = dic.stringValue(ofKey: "serviceItem")
        storeAddress = dic.stringValue(ofKey: "storeAddress")
        storePhoto = dic.stringValue(ofKey: "storePhoto")
        storeType = dic.stringValue(ofKey: "storeType")
        traficBus = dic.stringValue(ofKey: "traficBus")
        traficMrt = dic.stringValue(ofKey: "traficMrt")
        traficPark = dic.stringValue(ofKey: "traficPark")
        wapPhoto = dic.stringValue(ofKey: "wapPhoto")
        zipCode = dic.stringValue(ofKey: "zipCode")

    }

}
