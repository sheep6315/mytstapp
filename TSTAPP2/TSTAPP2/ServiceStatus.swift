//
//  ServiceStatus.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/8.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ServiceStatus: NSObject {

    static let sharedInstance = ServiceStatus()
    
    private(set) var servicesList : Array<Services> = []
    
    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if (dic["code"] as! String) == "00000" {
        
            servicesList = []
            
            if let list = dic["data"] as? Array<Dictionary<String, Any>> {
   
                for dic in list {
                    
                    let services = Services()
                    services.setDataValue(withDic: dic)
                    servicesList.append(services)
                    
                }
            }
        }
    }
}

class Services: NSObject {
    
    private(set) var feeMonth : String = "" //月租費
    private(set) var servFeeType : String = "" //服務資費名稱(中文)
    private(set) var servName : String = "" //服務名稱
    private(set) var servNo : String = "" //服務項目代碼
    var status : String = "" //是否已申請此服務(0:無; 1:有)
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        feeMonth = dic.stringValue(ofKey: "feeMonth")
        servFeeType = dic.stringValue(ofKey: "servFeeType")
        servName = dic.stringValue(ofKey: "servName")
        servNo = dic.stringValue(ofKey: "servNo")
        status = dic.stringValue(ofKey: "status")
        
    }

}
