//
//  ServicesViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/9.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics


let SERVICE_ORDER = "SERVICE_ORDER"
let SERVICE_NEW_ICON = "SERVICE_NEW_ICON"
let SERVICE_NEW_ICON_DIC = ["14": false, "15": true]

enum EnumEbillType: String {
    case Complete_Y = "Y"
    case Apply_A = "A"
    case Confirming_W = "W"
    case Sms_S = "S"
    case Not_N = "N"
    case NoData = ""
}

class ServicesViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, RearrangeableCollectionViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var ebillSwitch: UISwitch!
    
    @IBOutlet weak var backgroundViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lastRowMiddleView: UIView!
    
    @IBOutlet weak var lastRowLeftView: UIView!
    
    @IBOutlet weak var lastRowRightView: UIView!
    
    
    @IBOutlet weak var basicServiceButton: UIButton!
    @IBOutlet weak var tClubButton: UIButton!
    @IBOutlet weak var roamingButton: UIButton!
    @IBOutlet weak var creditCardTransferButton: UIButton!
    @IBOutlet weak var ticketQueryButton: UIButton!
    @IBOutlet weak var phoneSpecButton: UIButton!
    @IBOutlet weak var memberBonusButton: UIButton!
    @IBOutlet weak var volteButton: UIButton!
    @IBOutlet weak var faqButton: UIButton!
    @IBOutlet weak var eBillButton: UIButton!
    @IBOutlet weak var storeLocationButton: UIButton!
    @IBOutlet weak var determineNetworkButton: UIButton!
    @IBOutlet weak var mailBoxButton: UIButton!
    
    @IBOutlet weak var tutorialView: UIView!
    @IBOutlet weak var webView: UIWebView!
    
    
    
    
    var ebillType: EnumEbillType = EnumEbillType.NoData
    var emailAddress = ""
    var ebillStatusText: String = ""
    
    var originBackgroundViewHeight: CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        self.ebillSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.ebillSwitch.isOn = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(ServicesViewController.backToRootViewController), name: NSNotification.Name(rawValue: SETTING), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ServicesViewController.popBindingViewController), name: NSNotification.Name(rawValue: POP_BINDING_VIEWCONTROLLER), object: nil)
        
        originBackgroundViewHeight = backgroundViewHeight.constant
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        
        
        //Draggable CollectionView
//        automaticallyAdjustsScrollViewInsets = false
        
        collectionViewRearrangeableFlowLayout.draggable = true
        
        collectionViewRearrangeableFlowLayout.axis = .free
        
        setupCellDataArray()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP05", action: "")
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "常用服務")
        
        backgroundViewHeight.constant = originBackgroundViewHeight
        
//        checkVIPStatusAndChangeSkin()
        
        showGif()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        callShowEBillAPI()
        //AutomaticViewChange.sharedInstance.servicesVC = self
        
        collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP05", action: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
//        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
//
//        UISkinChange.sharedInstance.changeUIColor(theObject: basicServiceButton, mode: ButtonColorMode.button_service_3.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: tClubButton, mode: ButtonColorMode.button_service_1.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: roamingButton, mode: ButtonColorMode.button_service_2.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: creditCardTransferButton, mode: ButtonColorMode.button_service_4.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: ticketQueryButton, mode: ButtonColorMode.button_service_5.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: phoneSpecButton, mode: ButtonColorMode.button_service_6.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: memberBonusButton, mode: ButtonColorMode.button_service_8.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: volteButton, mode: ButtonColorMode.button_service_9.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: faqButton, mode: ButtonColorMode.button_service_10.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: eBillButton, mode: ButtonColorMode.button_service_13.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: storeLocationButton, mode: ButtonColorMode.button_service_12.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: determineNetworkButton, mode: ButtonColorMode.button_service_7.rawValue)
//        UISkinChange.sharedInstance.changeUIColor(theObject: mailBoxButton, mode: ButtonColorMode.button_service_11.rawValue)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        return true
    }
    
    @objc func backToRootViewController() {
        print("backToRootViewController")
        let popedVCArray = navigationController?.popToRootViewController(animated: false)
        
        if popedVCArray == nil {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: 0), animated: true)
            
//            AutomaticViewChange.sharedInstance.servicesVC = self
            
            callShowEBillAPI()
        }
        
    }
    
    @objc func popBindingViewController() {
        if navigationController?.viewControllers.count == 2 {
            let VCs = navigationController?.viewControllers
            if (VCs?[1].isKind(of: AI_AccountBindingViewController.self))! {
                let _ = VCs?[1].navigationController?.popViewController(animated: false)
            }
        }
    }
    
    // MARK: - IBAction
    @IBAction func didTapCloseButton(_ sender: Any) {
        UIView.animate(withDuration: 0.7, animations: {
            self.tutorialView.alpha = 0
        }) { (isCompleted: Bool) in
            UserDefaults.standard.set(true, forKey: "DID_SHOW_SERVICE_TUTORIAL")
            self.tutorialView.removeFromSuperview()
        }
    }
    
    
    @IBAction func didTapTClubButton(_ sender: Any) {
        if loginAndNotSuspended(true) {
            
            #if SIT
                let urlString: String = "http://uattsp.tstartel.com/static/temp/vip/"
            #elseif UAT
                let urlString: String = "http://uattsp.tstartel.com/static/temp/vip/"
            #else
                let urlString: String = "http://doc.tstartel.com/tstar_vip/"
            #endif
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "TCLUB會員專區", action: nil, label: nil, value: nil)
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "T-Club 會員專區", urlString: urlString, viewController: self)
            
            Answers.logContentView(withName: "TCLUB會員專區",
                                   contentType: "Function",
                                   contentId: urlString,
                                   customAttributes: [:])
        }
    }
    
    @IBAction func didTapRoamingButton(_ sender: Any) {
        if loginAndNotSuspended() {
            self.performSegue(withIdentifier: "toRoaming", sender: nil)
            
            Answers.logContentView(withName: "國際漫遊日租預約申請",
                                   contentType: "Function",
                                   contentId: "Roaming",
                                   customAttributes: [:])
        }
    }
    
    @IBAction func didTapBasicServiceButton(_ sender: Any) {
        if loginAndNotSuspended() {
            self.performSegue(withIdentifier: "toBasicSetting", sender: nil)
            
            Answers.logContentView(withName: "基本服務設定",
                                   contentType: "Function",
                                   contentId: "BasicSetting",
                                   customAttributes: [:])
        }
    }
    
    @IBAction func didTapCreditCardTransferButton(_ sender: Any) {
        if loginAndNotSuspended(true) {
            TSTAPP2_API.sharedInstance.queryDirectDebit(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, accountId: LoginData.sharedLoginData().custProfile_accountId, custId: LoginData.sharedLoginData().custProfile_custId, companyId: LoginData.sharedLoginData().custProfile_companyId, channel: "APP", isMainMsisdn: LoginData.sharedLoginData().custProfile_isMainMsisdn, showProgress: true, completion:{ (dic :Dictionary<String, Any>, response :URLResponse) in
                
                if dic.stringValue(ofKey: "code") == "00000" {
                    
                    let debit = DirectDebit.sharedInstance
                    debit.setDataValue(withDic: dic)
                    
                    if debit.result == "6" || debit.result == "5" || debit.result == "2" {
                        
                        let storyboard = UIStoryboard(name: "CreditCardTransfer", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditCardTransferViewController") as UIViewController
                        self.navigationController?.show(viewController, sender: nil)
                    }
                    else if debit.result == "1" {
                                            
                        let storyboard = UIStoryboard(name: "CreditCardTransferChange", bundle: nil)
                        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditCardTransferChangeViewController") as UIViewController
                        self.navigationController?.show(viewController, sender: nil)
                    }
                    else {
                        let message = dic.stringValue(ofKey: "message")
                        TSTAPP2_Utilities.showAlert(withMessage: message, viewController: self)
                    }
                    
                    Answers.logContentView(withName: "信用卡轉帳申請",
                                           contentType: "Function",
                                           contentId: "CreditCardTransfer",
                                           customAttributes: ["debit result": debit.result])
                }
                else {
                    let message = dic.stringValue(ofKey: "message")
                    TSTAPP2_Utilities.showAlert(withMessage: message, viewController: self)
                }
                
            }, failure: { (Error) in
                
            })
        }
    }
    
    @IBAction func didTapTicketQueryButton(_ sender: Any) {
        if loginAndNotSuspended(true) {
            self.performSegue(withIdentifier: "toTicketQuery", sender: nil)
            
            Answers.logContentView(withName: "案件查詢",
                                   contentType: "Function",
                                   contentId: "TicketQuery",
                                   customAttributes: [:])
        }
    }
    
    @IBAction func didTapPhoneSpecButton(_ sender: Any) {
        
        var urlString = "https://www.tstartel.com/mCWS/phoneSearch.php"
        
        if TSTAPP2_Utilities.versionStatus() == VersionStatus.notReleased {
            urlString =  "https://www.tstartel.com/mCWS/phoneSearch.php?osType=iOS"
        }
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "手機規格與操作", action: nil, label: nil, value: nil)
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "手機規格與操作", urlString: urlString, appendToken: false, viewController: self)
        
        Answers.logContentView(withName: "手機規格與操作",
                               contentType: "Function",
                               contentId: urlString,
                               customAttributes: [:])
    }
    
    @IBAction func didTapMemberBonusButton(_ sender: Any) {
        if loginAndNotSuspended(true) {
            #if SIT
                let urlString: String = "https://uattsp.tstartel.com/mCWS/memberbenefit.php"
            #elseif UAT
                let urlString: String = "https://uattsp.tstartel.com/mCWS/memberbenefit.php"
            #else
                let urlString: String = "https://www.tstartel.com/mCWS/memberbenefit.php"
            #endif
            
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "異業優惠專區", action:nil, label: nil, value: nil)
            TSTAPP2_Utilities.pushToWebViewControler(withTitle: "異業優惠專區", urlString: urlString, viewController: self)
            
            Answers.logContentView(withName: "異業優惠專區",
                                   contentType: "Function",
                                   contentId: urlString,
                                   customAttributes: [:])
        }
    }
    
    @IBAction func didTapVOLTEButton(_ sender: Any) {
        if loginAndNotSuspended() {
            self.performSegue(withIdentifier: "toVolte", sender: nil)
            
            Answers.logContentView(withName: "VOLTE好聲音",
                                   contentType: "Function",
                                   contentId: "VOLTE",
                                   customAttributes: [:])
        }
    }
    
    @IBAction func didTapFAQButton(_ sender: Any) {
        #if SIT
        let urlString: String = "https://uattsp.tstartel.com/mCWS/faqService.php"
        #elseif UAT
        let urlString: String = "https://uattsp.tstartel.com/mCWS/faqService.php"
        #else
        let urlString: String = "https://www.tstartel.com/mCWS/faqService.php"
        #endif
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常見問題", action:nil,  label: nil, value: nil)
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "常見問題", urlString: urlString, viewController: self)
        
        Answers.logContentView(withName: "常見問題",
                               contentType: "Function",
                               contentId: urlString,
                               customAttributes: [:])
    }
    
    @IBAction func didTapEbillButton(_ sender: Any) {
        if loginAndNotSuspended() {
            UbaRecord.sharedInstance.sendUba(withPage: "APP05", action: "A0511")
            let loginData: LoginData = LoginData.sharedLoginData()
            
            Answers.logContentView(withName: "電子帳單",
                                   contentType: "Function",
                                   contentId: "EBill",
                                   customAttributes: ["isMainMsisdn": loginData.custProfile_isMainMsisdn, "eBillType": self.ebillType.rawValue])
            
            let ebillType = EnumEbillType(rawValue: loginData.billType_memberType)!
            
            if loginData.custProfile_isMainMsisdn == "Y" && ebillType != .NoData {
                if ebillType == .Complete_Y {
                    let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ApplyEbillViewController") as! ApplyEbillViewController
                    vc.email = LoginData.sharedLoginData().billType_email
                    
                    self.navigationController?.show(vc, sender: self)
                }
                else if ebillType == .Apply_A || ebillType == .Confirming_W {
                    let message = loginData.billType_showMessage
                    let action = UIAlertAction(title: "修改", style: UIAlertActionStyle.default, handler: { (action) in
                        let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "ApplyEbillViewController") as! ApplyEbillViewController
                        vc.email = LoginData.sharedLoginData().billType_email
                        
                        self.navigationController?.show(vc, sender: self)
                    })
                    
                    let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel, handler: { (action) in
                        
                    })
                    
                    TSTAPP2_Utilities.showAlert(withMessage: message, action: action, cancel: cancel, viewController: self)
                }
                else {
                    let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "ChangeEmailViewController") as! ChangeEmailViewController
                    
                    self.navigationController?.show(vc, sender: self)
                }
            }
            else {
                TSTAPP2_Utilities.showAlert(withMessage: loginData.billType_showMessage, viewController: self)
            }
            
//            if loginData.custProfile_isMainMsisdn == "N" {
//                if loginData.eBill_showMessage != "" {
//                    TSTAPP2_Utilities.showAlert(withMessage: loginData.eBill_showMessage, viewController: self)
//                }
//                else {
//                    TSTAPP2_Utilities.showAlert(withMessage: "因此帳單為子門號故此功能無法使用！！", viewController: self)
//                }
//                return
//            }
//
//            let showApplyEbill = loginData.eBill_showApplyEbill
//            if showApplyEbill == "Y" {
//                let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
//                let vc = storyBoard.instantiateViewController(withIdentifier: "ChangeEmailViewController") as! ChangeEmailViewController
//                self.navigationController?.show(vc, sender: self)
//            }
//            else {
//
//                switch self.ebillType {
//                case .Sms_S, .Not_N:
//                    let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
//                    let vc = storyBoard.instantiateViewController(withIdentifier: "ChangeEmailViewController") as! ChangeEmailViewController
//                    self.navigationController?.show(vc, sender: self)
//                    break
//                case .Apply_A, .Confirming_W:
//                    if loginData.eBill_showMessage != "" {
//                        TSTAPP2_Utilities.showAlert(withMessage: loginData.eBill_showMessage, viewController: self)
//                    }
//                    else {
//                        TSTAPP2_Utilities.showAlert(withMessage: "電子帳單申請中，若您尚未進行認證，請儘速至信箱認證以開啟服務", viewController: self)
//                    }
//                    break
//                case .Complete_Y:
//
//                    let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
//                    let vc = storyBoard.instantiateViewController(withIdentifier: "ApplyEbillViewController") as! ApplyEbillViewController
//                    vc.email = LoginData.sharedLoginData().eBill_emailAddress
//
//                    self.navigationController?.show(vc, sender: self)
//                    break
//
//                case .NoData:
//                    TSTAPP2_Utilities.showAlert(withMessage: loginData.eBill_showMessage, viewController: self)
//                    break
//                }
//            }
        }
    }
    
    @IBAction func didTapStoreLocationButton(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toStoreLocation", sender: nil)
        
        Answers.logContentView(withName: "服務據點",
                               contentType: "Function",
                               contentId: "StoreLocation",
                               customAttributes: [:])
    }
    
    @IBAction func didTapDetermineNetworkButton(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toDetermineNetwork", sender: nil)
        
        Answers.logContentView(withName: "網內外門號查詢",
                               contentType: "Function",
                               contentId: "DetermineNetwork",
                               customAttributes: [:])
    }
    
    @IBAction func didTapMailBoxButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP05", action: "AA_CS")
        
        self.performSegue(withIdentifier: "toMailBox", sender: nil)
        
        Answers.logContentView(withName: "客服信箱",
                               contentType: "Function",
                               contentId: "MailBox",
                               customAttributes: [:])
    }
    
    @IBAction func didTapOrderQueryButton(_ sender: Any) {
        let urlString = UrlMap.sharedInstance.ORDER_QUERY
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP05", action: "AA_ORDER_QUERY")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "訂單查詢", label: nil, value: nil)
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "訂單查詢", action: "\(urlString)", label: nil, value: nil)
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @IBAction func didTapServiceCoverage(_ sender: Any) {
        UbaRecord.sharedInstance.sendUba(withPage: "APP05", action: "AA_COVERAGE")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "網路涵蓋", label: nil, value: nil)
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "網路涵蓋", action: nil, label: nil, value: nil)
        
        collectionView.reloadData()
        
        let urlString = UrlMap.sharedInstance.SERVICE_COVERAGE
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
//        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "", urlString: urlString, viewController: self)
    }
    
    @IBAction func didTapSpeedometer(_ sender: Any) {
        if let url = URL(string: UrlMap.sharedInstance.SPEED_TEST) {
            UIApplication.shared.openURL(url)
        }
    }
    
//    MARK: -
    func loginAndNotSuspended() -> Bool {
        return loginAndNotSuspended(false)
    }   
    func loginAndNotSuspended(_ checkLoginOnly: Bool) -> Bool {
        var loginOrSuspended = true
        
//        let maintainInfo = UserDefaults.standard.object(forKey: MAINTAIN_INFO) as? [AnyObject] ?? [false as AnyObject, "" as AnyObject, "" as AnyObject]
//        let maintainStatus = maintainInfo[0] as! Bool
        
        TSTAPP2_Utilities.checkIfOnlyFacebookLogin()
        let isMaintain = TSTAPP2_Utilities.isMaintain(showAlert: true, withViewController: self)
        
        if !LoginData.isLogin() || isMaintain {
//            TSTAPP2_Utilities.showAlert(withMessage: "請先登入會員", viewController: self)
            loginOrSuspended = false
            
//            loginOrSuspended = !TSTAPP2_Utilities.isForceAccountBindingAndPushToBindingView(hideBackButton: false, withViewController: self)
            
        }
        else if TSTAPP2_Utilities.isSuspended() && !checkLoginOnly {
//            TSTAPP2_Utilities.showAlert(withMessage: "由於尚未收到您本期的帳款，目前已暫時限制此功能，敬請儘速繳款", viewController: self)
            
            
            let alert = UIAlertController(title: "由於尚未收到您本期的帳款，目前已暫時限制此功能，敬請儘速繳款", message: nil, preferredStyle: .actionSheet)
            
            let barCode = UIAlertAction(title: "超商繳費條碼", style: .default, handler: { (UIAlertAction) in
                let avc = AutomaticViewChange.sharedInstance
                avc.setTabIndex(withAction: AUTO_PAYMENT_BARCODE, actionParam: nil)
            })
            let bank = UIAlertAction(title: "銀行帳戶繳款", style: .default, handler: { (UIAlertAction) in
                let avc = AutomaticViewChange.sharedInstance
                avc.setTabIndex(withAction: AUTO_PAYMENT_BANK, actionParam: nil)
            })
            let credit = UIAlertAction(title: "信用卡繳款", style: .default, handler: { (UIAlertAction) in
                let avc = AutomaticViewChange.sharedInstance
                avc.setTabIndex(withAction: AUTO_PAYMENT_CREDITCARD, actionParam: nil)
            })
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            
            alert.addActions(array: [barCode,bank,credit,cancel])
            alert.show(viewController: self)
            
            
            loginOrSuspended = false
        }
        return loginOrSuspended
    }
    
//    MARK: -
    func showGif() {
        
        
        if !UserDefaults.standard.bool(forKey: "DID_SHOW_SERVICE_TUTORIAL") {
        
//            let url = Bundle.main.url(forResource: "services_dragging", withExtension: "gif")!
//            let data = try! Data(contentsOf: url)
//            
//            if webView != nil {
//                webView.load(data, mimeType: "image/gif", textEncodingName: "UTF-8", baseURL: NSURL(string: "http://")! as URL)
//            }
            
            let storyBoard = UIStoryboard(name: "TabBar", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "TStarFullScreenViewController") as! TStarFullScreenViewController
            
            let url = Bundle.main.url(forResource: "services_dragging", withExtension: "gif")!
            let data = try! Data(contentsOf: url)
            
            self.parent?.present(vc, animated: true, completion: {
                if vc.webView != nil {
                    vc.webView.load(data, mimeType: "image/gif", textEncodingName: "UTF-8", baseURL: NSURL(string: "http://")! as URL)
                }
            })
        
        }
        else {
//            if (tutorialView != nil) {
//                tutorialView.removeFromSuperview()
//            }
        }
    }
    
    // MARK: - API
    func callShowEBillAPI() {
        
        if LoginData.isLogin() {
            let loginData: LoginData = LoginData.sharedLoginData()
            if loginData.custProfile_isMainMsisdn == "Y" {
//                TSTAPP2_API.sharedInstance.showEBill(withMSISDN: loginData.custProfile_msisdn, isMainMsisdn: loginData.custProfile_isMainMsisdn, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//                    if dic.stringValue(ofKey: "code") == "00000" {
//                        if let data = dic["data"] as? Dictionary<String, Any> {
//                            self.ebillType = EnumEbillType(rawValue: (data["memberType"] as? String ?? "N"))!
//                            self.emailAddress = data.stringValue(ofKey: "emailAddress")
//                            if self.ebillType == EnumEbillType.Complete {
//                                self.ebillSwitch.isOn = true
//                                
//                                self.hideEBillButton()
//                            } else {
//                                self.eBillButton.isHidden = false
//                                self.ebillSwitch.isOn = false
//                            }
//                            self.ebillStatusText = data.stringValue(ofKey: "statusText")
//                        }
//                    }
//                    else {
//                        self.hideEBillButton()
//                    }
//                    AutomaticViewChange.sharedInstance.servicesVC = self
//                }) { (Error) in
//                    AutomaticViewChange.sharedInstance.servicesVC = self
//                }
                
                
                self.ebillType = EnumEbillType(rawValue: loginData.billType_memberType)!
                self.emailAddress = loginData.billType_email
                if self.ebillType == EnumEbillType.Complete_Y {
                    self.ebillSwitch.isOn = true
                    
                    self.hideEBillButton()
                } else {
                    self.eBillButton.isHidden = false
                    self.ebillSwitch.isOn = false
                }
//                self.ebillStatusText = loginData.eBill_statusText
            }
            else {
                self.ebillSwitch.isOn = true
                
                hideEBillButton()
//                self.updateViewConstraints()
            }
            
            AutomaticViewChange.sharedInstance.servicesVC = self
        }
    }
    
    func hideEBillButton() {
        self.eBillButton.isHidden = true
        self.backgroundViewHeight.constant = self.backgroundViewHeight.constant - 136
        self.lastRowLeftView.isHidden = true
        self.lastRowMiddleView.isHidden = true
        self.lastRowRightView.isHidden = true
    }
    
    
    //MARK: - Draggable CollectionView
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var collectionViewRearrangeableFlowLayout: RearrangeableCollectionViewFlowLayout!
    
//    var data: [[String]] = [["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r"], ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]]
    
    var collectionCellData: [[[String]]] = [[], []]//[["A", "B", "C", "D", "E", "F"], ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]]
    var didChangeOrder: Bool = false
    
    enum ServiceOrder: Int {
        //section 0
        case basicSetting
        case creditCardTransfer
        case storeLocation
        case phoneSpec
        case roaming
        case determineNetwork
        //section 1
        case speedometer
        case serviceCoverage
        case orderRequest
        case faq
        case mailBox
        case ticketQuery
        case tclub
//        case memberBonus
        case volte
        case ebill

        
        static let tag: [ServiceOrder: String] = [.basicSetting: "0",
                                                .creditCardTransfer: "1",
                                                .storeLocation: "2",
                                                .phoneSpec: "3",
                                                .roaming: "4",
                                                .determineNetwork: "5",
                                                .serviceCoverage: "14",
                                                .speedometer: "15",
                                                .orderRequest: "13",
                                                .faq: "6",
                                                .mailBox: "7",
                                                .ticketQuery: "8",
                                                .tclub: "9",
//                                                .memberBonus: "10",
                                                .volte: "11",
                                                .ebill: "12"]
        
        static let name: [ServiceOrder: String] = [
                                                    .speedometer: "網速測試",
                                                    .basicSetting: "基本服務\n設定",
                                                    .creditCardTransfer: "信用卡\n轉帳申請",
                                                    .storeLocation: "服務據點",
                                                    .phoneSpec: "手機規格\n與操作",
                                                    .roaming: "國際漫遊日租\n預約申請",
                                                    .determineNetwork: "網內外\n門號查詢",
                                                    .serviceCoverage: "訊號地圖",
                                                    .orderRequest: "訂單查詢",
                                                    .faq: "常見問題",
                                                    .mailBox: "客服信箱",
                                                    .ticketQuery: "案件查詢",
                                                    .tclub: "T-CLUB\n會員專區",
//                                                    .memberBonus: "異業\n優惠專區",
                                                    .volte: "VOLTE\n好聲音",
                                                    .ebill: "電子帳單\n申請"]
        
        static let image: [ServiceOrder: String] = [.basicSetting: "button_service_3",
                                                    .creditCardTransfer: "button_service_4",
                                                    .storeLocation: "button_service_12",
                                                    .phoneSpec: "button_service_6",
                                                    .roaming: "button_service_2",
                                                    .determineNetwork: "button_service_7",
                                                    .serviceCoverage: "button_service_17",
                                                    .orderRequest: "button_service_16",
                                                    .faq: "button_service_10",
                                                    .mailBox: "button_service_11",
                                                    .ticketQuery: "button_service_5",
                                                    .tclub: "button_service_1",
//                                                    .memberBonus: "button_service_8",
                                                    .volte: "button_service_9",
                                                    .ebill: "button_service_13",
                                                    .speedometer: "button_service_18"]
        
        var tagString: String {
            return ServiceOrder.tag[self]!
        }
        
        var nameString: String {
            return ServiceOrder.name[self]!
        }
        
        var imageString: String {
            return ServiceOrder.image[self]!
        }
    }
    
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        collectionView.collectionViewLayout.invalidateLayout()
//    }
    
//    MARK: - 
    public func setupCellDataArray() {
        
        if var savedData = UserDefaults.standard.value(forKey: SERVICE_ORDER) as? [[[String]]] {
            print("saved")
            
            let serviceData = generateServiceDataArray()
            
            let newDataCount = serviceData[0].count + serviceData[1].count
            let savedDataCount = savedData[0].count + savedData[1].count
            
            if newDataCount > savedDataCount {
//                let count = serviceData[1].count
//                savedData[1].append(serviceData[1][count - 1])
//                let insertIndex = 6 % Int(serviceData[1][count - 1][2])!
                
//                savedData[1].insert(serviceData[1][count - 1], at: insertIndex)
                savedData[1].insert(serviceData[1][0], at: 0)
            }
            else if newDataCount <= savedDataCount {
                let tempSavedData = savedData
                var sectionCount = 0
                for section in tempSavedData {
//                    var sectionCount = 0
                    var count = 0
                    for functionData in section {
                        
                        let tag = functionData[2]
                        var match = false
                        
                        for newSection in serviceData {
                            for newFunctionData in newSection {
                                if newFunctionData[2] == tag {
                                    match = true
                                }
                            }
                        }
                        
                        if match == false {
                            savedData[sectionCount].remove(at: count)
                        }
                        count = count + 1
//                        sectionCount = sectionCount + 1
                    }
                    sectionCount = sectionCount + 1
                }
            }
            
            collectionCellData = savedData
            saveServiceOrder()
        }
        else {
            collectionCellData = generateServiceDataArray()
            
            saveServiceOrder()
        }
    }
    
    public func generateServiceDataArray() -> [[[String]]] {
        var section = 0
        var dataArray = [""]
        var serviceData: [[[String]]] = [[], []]
        
        for service in TSTAPP2_Utilities.iterateEnum(ServiceOrder.self) {
            
            dataArray = [service.nameString, service.imageString, service.tagString]
            
            serviceData[section].append(dataArray)
            
            if serviceData[0].count == 6 {
                section = 1
            }
        }
        
        return serviceData
    }
    
    @objc func didTapResetServiceOrderButton() {
        
        let alert = UIAlertController(title: "", message: "是否重置服務排序？", preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "確定", style: UIAlertActionStyle.default) { (action: UIAlertAction) in
            self.collectionCellData = self.generateServiceDataArray()
            
            UserDefaults.standard.setValue(self.collectionCellData, forKey: SERVICE_ORDER)
            
            self.collectionView.reloadData()
            
            Answers.logContentView(withName: "常用服務排列重置",
                                   contentType: "Services",
                                   contentId: "ServiceReset",
                                   customAttributes: ["order": self.collectionCellData.description])
        }
        let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel) { (action: UIAlertAction) in
            
        }
        alert.addAction(action)
        alert.addAction(cancel)
        
        alert.show(viewController: self)
    }
    
    func saveServiceOrder() {
        let savedData = UserDefaults.standard.object(forKey: SERVICE_ORDER) as? [[[String]]]
        if collectionCellData[0].description != savedData?[0].description  || collectionCellData[1].description != savedData?[1].description {
            didChangeOrder = true
            UserDefaults.standard.setValue(collectionCellData, forKey: SERVICE_ORDER)
        }
    }
    
    @objc func didTapCellButton(sender: UIButton) {
        
        var serviceNewIconDic = UserDefaults.standard.dictionary(forKey: SERVICE_NEW_ICON)
        
        if serviceNewIconDic?[String(sender.tag)] as? Bool == true {
            serviceNewIconDic?[String(sender.tag)] = false
            UserDefaults.standard.set(serviceNewIconDic, forKey: SERVICE_NEW_ICON)
        }
        
        switch sender.tag {
        case 0:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapBasicServiceButton(sender)
            }
        case 1:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapCreditCardTransferButton(sender)
            }
        case 2:
            didTapStoreLocationButton(sender)
        case 3:
            didTapPhoneSpecButton(sender)
        case 4:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapRoamingButton(sender)
            }
        case 5:
            didTapDetermineNetworkButton(sender)
        case 6:
            didTapFAQButton(sender)
        case 7:
            didTapMailBoxButton(sender)
        case 8:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapTicketQueryButton(sender)
            }
        case 9:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapTClubButton(sender)
            }
        case 10:
            didTapMemberBonusButton(sender)
        case 11:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapVOLTEButton(sender)
            }
        case 12:
            if TSTAPP2_Utilities.isLogin(pushToLoginView: true, withViewController: self) {
                didTapEbillButton(sender)
            }
        case 13:
            didTapOrderQueryButton(sender)
        case 14:
            didTapServiceCoverage(sender)
        case 15:
            didTapSpeedometer(sender)
        default:
            return
        }
    }
    
//    MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return collectionCellData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionCellData[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RearrangeableCollectionViewCell
        
        cell.cellButton.tag = Int(collectionCellData[indexPath.section][indexPath.item][2])!
        
        var imageName = collectionCellData[indexPath.section][indexPath.item][1]
        
        if LoginData.sharedLoginData().custProfile_isVIP == "Y" {
            imageName = imageName + "_gold"
            
            let cellTag = collectionCellData[indexPath.section][indexPath.item][2]
            
            let serviceNewIconDic = UserDefaults.standard.dictionary(forKey: "SERVICE_NEW_ICON")
            if serviceNewIconDic?[cellTag] as? Bool == true {
                cell.newImageView.image = UIImage(named: "icon_new_gold")
            }
            else {
                cell.newImageView.image = nil
            }
        }
        else {
            
            let cellTag = collectionCellData[indexPath.section][indexPath.item][2]
            
            let serviceNewIconDic = UserDefaults.standard.dictionary(forKey: "SERVICE_NEW_ICON")
            if serviceNewIconDic?[cellTag] as? Bool == true {
                cell.newImageView.image = UIImage(named: "icon_new")
            }
            else {
                cell.newImageView.image = nil
            }
//            UserDefaults.standard.set(["13": true], forKey: "SERVICE_NEW_ICON")
//            cell.newImageView.image = UIImage(named: "icon_new")
            if !LoginData.isLogin() {
            
                switch imageName {
                case "button_service_1":
                    imageName = imageName + "_gray"
                case "button_service_2":
                    imageName = imageName + "_gray"
                case "button_service_3":
                    imageName = imageName + "_gray"
                case "button_service_4":
                    imageName = imageName + "_gray"
                case "button_service_5":
                    imageName = imageName + "_gray"
                case "button_service_9":
                    imageName = imageName + "_gray"
                case "button_service_13":
                    imageName = imageName + "_gray"
                default:
                    break;
                }
            }
            else {
                
                if imageName.contains("_gray") {
                    imageName = imageName.replacingOccurrences(of: "_gray", with: "")
                }
                
            }
        }
        cell.cellButton.setImage(UIImage(named: imageName), for: UIControlState.normal)
        cell.cellButton.setImage(UIImage(named: imageName), for: UIControlState.highlighted)
        cell.cellButton.addTarget(self, action: #selector(didTapCellButton(sender:)), for: UIControlEvents.touchUpInside)
        
        cell.titleLabel.text = collectionCellData[indexPath.section][indexPath.item][0]
        
        return cell
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var x: CGFloat = 0.314
        
        if self.view.frame.width <= 320 {
            x = 0.31
        }
        
        return CGSize(width: collectionView.frame.width * x, height: 135)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.frame.width, height: 44)
//    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! RearrangeableCollectionHeaderView
        
        if indexPath.section == 0 {
            headerView.titleLabel.text = "我的服務"
            headerView.resetButton.isHidden = false
            headerView.resetButton.addTarget(self, action: #selector(didTapResetServiceOrderButton), for: UIControlEvents.touchUpInside)
        }
        else if indexPath.section == 1 {
            headerView.titleLabel.text = "其他常用服務"
            headerView.resetButton.isHidden = true
        }
        
        return headerView
    }
    
    
    
//    MARK: - RearrangeableCollectionViewDelegate
    
    func canMoveItem(indexPath: IndexPath) -> Bool {
        return true
    }
    
    func moveDataItem(from source: IndexPath, to destination: IndexPath) {
        let name = collectionCellData[source.section][source.item]
        
        collectionCellData[source.section].remove(at: source.item)
        
        collectionCellData[destination.section].insert(name, at: destination.item)
        
        saveServiceOrder()
    }
    
    func swapDataItem(from source: IndexPath, to destination: IndexPath) {
        
        let sourceName = collectionCellData[source.section][source.item]
        let destName = collectionCellData[destination.section][destination.item]
        
        collectionCellData[source.section].remove(at: source.item)
        collectionCellData[source.section].insert(destName, at: source.item)
        
        collectionCellData[destination.section].remove(at: destination.item)
        collectionCellData[destination.section].insert(sourceName, at: destination.item)
        
        saveServiceOrder()
    }
    
    func didEndDragging() {
        if didChangeOrder {
            didChangeOrder = false
            Answers.logContentView(withName: "常用服務排列",
                                   contentType: "Services",
                                   contentId: "ServiceSwap",
                                   customAttributes: ["ServiceOrder": collectionCellData.description])
        }
    }
}
