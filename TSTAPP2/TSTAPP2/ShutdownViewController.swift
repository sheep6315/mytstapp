//
//  ShutdownViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/12/7.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

let DISMISS_SHUTDOWN = "DISMISS_SHUTDOWN"

class ShutdownViewController: TSUIViewController {
    
    var shutdownMessage = ""
    @IBOutlet weak var shutdownMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: DISMISS_SHUTDOWN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ShutdownViewController.dismissShutdown), name:Notification.Name(rawValue: DISMISS_SHUTDOWN), object: nil);
        
        shutdownMessageLabel.text = shutdownMessage
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: DISMISS_SHUTDOWN), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func dismissShutdown() {
        dismiss(animated: true) {
            
        }
    }

}
