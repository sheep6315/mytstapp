//
//  SmartBannerList.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/18.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class SmartBannerList: NSObject {
    
    static let sharedInstance = SmartBannerList()
    
    private(set) var roleType: String = "" //角色類型
    private(set) var contractdateType: String = "" //合約區間類型
    private(set) var custType: String = "" //用戶類型
    private(set) var onEvent: String = "" //
    private(set) var cmpId: String = "" //
    private(set) var cmpVersion: String = "" //
    
    private(set) var homeSmartBanner : [SmartBanner] = [] //首頁-智能場景
    private(set) var myAccountBanner1 : [SmartBanner] = [] //我的帳戶-中
    private(set) var myAccountBanner2 : [SmartBanner] = [] //我的帳戶-下
    private(set) var contractBanner : [SmartBanner] = [] //專案合約-下
    private(set) var topupBanner : [SmartBanner] = [] //儲值序號
    private(set) var myAccountSmartBanner : [SmartBanner] = [] //我的帳戶-智能提醒
    
    override init () {
        
    }
    
    func clear() {
        roleType = ""
        contractdateType = ""
        custType = ""
        onEvent = ""
        cmpId = ""
        cmpVersion = ""
        homeSmartBanner = []
        myAccountBanner1 = []
        myAccountBanner2 = []
        contractBanner = []
        topupBanner = []
        myAccountSmartBanner = []
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if (dic["code"] as! String) == "00000" {
            
            clear()
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                roleType = apiData.stringValue(ofKey: "roleType")
                contractdateType = apiData.stringValue(ofKey: "contractdateType")
                custType = apiData.stringValue(ofKey: "custType")
                onEvent = apiData.stringValue(ofKey: "onEvent")
                cmpId = apiData.stringValue(ofKey: "cmpId")
                cmpVersion = apiData.stringValue(ofKey: "cmpVersion")
                
                if let homeSmartBanner = apiData["homeSmartBanner"] as? Array<Dictionary<String, Any>>  {
                    
                    for dic in homeSmartBanner {
                        
                        let banner = SmartBanner()
                        banner.setDataValue(withDic: dic)
                        self.homeSmartBanner.append(banner)
                    }
                }
                
                if let myAccountBanner1 = apiData["myAccountBanner1"] as? Array<Dictionary<String, Any>>  {
                    
                    for dic in myAccountBanner1 {
                        
                        let banner = SmartBanner()
                        banner.setDataValue(withDic: dic)
                        self.myAccountBanner1.append(banner)
                    }
                }
                
                if let myAccountBanner2 = apiData["myAccountBanner2"] as? Array<Dictionary<String, Any>>  {
                    
                    for dic in myAccountBanner2 {
                        
                        let banner = SmartBanner()
                        banner.setDataValue(withDic: dic)
                        self.myAccountBanner2.append(banner)
                    }
                }
                
                if let contractBanner = apiData["contractBanner"] as? Array<Dictionary<String, Any>>  {
                    
                    for dic in contractBanner {
                        
                        let banner = SmartBanner()
                        banner.setDataValue(withDic: dic)
                        self.contractBanner.append(banner)
                    }
                }
                
                if let topupBanner = apiData["topupBanner"] as? Array<Dictionary<String, Any>>  {
                    
                    for dic in topupBanner {
                        
                        let banner = SmartBanner()
                        banner.setDataValue(withDic: dic)
                        self.topupBanner.append(banner)
                    }
                }
                
                if let myAccountSmartBanner = apiData["myAccountSmartBanner"] as? Array<Dictionary<String, Any>>  {
                    
                    for dic in myAccountSmartBanner {
                        
                        let banner = SmartBanner()
                        banner.setDataValue(withDic: dic)
                        self.myAccountSmartBanner.append(banner)
                    }
                }
            }
        }
    }
}

class SmartBanner: NSObject {
    
    private(set) var path : String = "" //BANNER圖片網址
    private(set) var linkType : String = "" //連結類型
    /*
     1:小網頁面-內崁Webview
     2:小網頁面-另開Browser
     3:登入頁
     4:客戶服務
     5:我的帳戶
     6:國際漫遊申請
     7:我的帳單
     8:基本服務
     */
    private(set) var url : String = "" //URL
    private(set) var startDate : String = "" //上架時間
    private(set) var endDate : String = "" //下架時間
    private(set) var sort : String = "" //排序
    private(set) var titleText : String = "" //標題
    private(set) var noteText : String = "" //內容
    private(set) var displayType: String = ""//顯示類型
    private(set) var action: String = ""
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        path = dic.stringValue(ofKey: "path")
        linkType = dic.stringValue(ofKey: "linkType")
        url = dic.stringValue(ofKey: "url")
        startDate = dic.stringValue(ofKey: "startDate")
        endDate = dic.stringValue(ofKey: "endDate")
        sort = dic.stringValue(ofKey: "sort")
        titleText = dic.stringValue(ofKey: "titleText")
        noteText = dic.stringValue(ofKey: "noteText")
        displayType = dic.stringValue(ofKey: "displayType")
        action = dic.stringValue(ofKey: "action")
    }

}
