//
//  StoreLocationViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/10.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import GoogleMaps

class StoreLocationViewController: TSUIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var contentMapView: UIView!
    
    var locationManager: CLLocationManager!
    
    var mapView: GMSMapView!
    
    var nearbyContentController: StoreLocationNearbyViewController!
    
    var searchContentController: StoreLocationSearchViewController!
    
    var searchResultMarker: GMSMarker = GMSMarker()
    
    var locationZipCode: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.requestLocationAuthorization()
        let storyBoard = UIStoryboard(name: "StoreLocation", bundle: nil)
        
        self.nearbyContentController = storyBoard.instantiateViewController(withIdentifier: "storeLocationNearby") as! StoreLocationNearbyViewController
        self.nearbyContentController.userLocation = self.locationManager.location
        self.searchContentController = storyBoard.instantiateViewController(withIdentifier: "storeLocationSearch") as! StoreLocationSearchViewController
        
        let camera = GMSCameraPosition.camera(withLatitude: 23.843138,
                                              longitude: 120.992002, zoom: 7)
        self.mapView = GMSMapView.map(withFrame: self.contentMapView.bounds, camera: camera)
        self.mapView.delegate = self
        self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mapView.isMyLocationEnabled = true
        self.mapView.isIndoorEnabled = false
        self.mapView.accessibilityElementsHidden = false
        self.mapView.settings.compassButton = true
        self.mapView.settings.scrollGestures = true
        self.mapView.settings.zoomGestures = true
        self.mapView.settings.myLocationButton = true
        self.mapView.settings.rotateGestures = true
        self.contentMapView.addSubview(self.mapView)
        self.contentMapView.sendSubview(toBack: self.mapView)
        self.nearbyContentController.parentMapView = self.mapView
        self.callServiceLocationAPI()
        self.callZipCodeAPI()
        
        if self.locationManager.location != nil {
            self.mapView.camera = GMSCameraPosition.camera(withTarget: (self.locationManager.location?.coordinate)!, zoom: 11)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0203", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "服務據點", action: nil, label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "服務據點")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0203", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Location
    func requestLocationAuthorization() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    // MARK: - GMSMapViewDelegate
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let phoneNumber = marker.userData as! String? ?? ""
        if phoneNumber != "" {
            let phoneCallAlertController: UIAlertController = UIAlertController(title: "撥打門市電話", message: phoneNumber, preferredStyle: .alert)
            phoneCallAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (_: UIAlertAction) in
                UIApplication.shared.openURL(URL(string: "tel://\(phoneNumber)")!)
            }))
            phoneCallAlertController.addAction(UIAlertAction(title: "取消", style: .default, handler: nil))
            self.present(phoneCallAlertController, animated: true, completion: nil)
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        if self.locationManager.location != nil {
            self.mapView.camera = GMSCameraPosition.camera(withTarget: (self.locationManager.location?.coordinate)!, zoom: 12)
        }
        
        callServiceLocationAPI()
        
        
        return true
    }
    
    // MARK: - IBAction
    @IBAction func didTapNearbyButton(_ sender: Any) {
        
        if self.nearbyContentController.nearbyList.count == 0 {
            let noDataAlertController = UIAlertController(title: nil, message: "查無資料", preferredStyle: .alert)
            noDataAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            self.present(noDataAlertController, animated: true, completion: nil)
            return
        } else {
            let nearbyAlertController = UIAlertController(title: "最近門市", message: nil, preferredStyle: .alert)
            nearbyAlertController.setValue(self.nearbyContentController, forKey: "contentViewController")
            nearbyAlertController.addAction(UIAlertAction(title: "取消", style: .default, handler: nil))
            self.present(nearbyAlertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapSearchButton(_ sender: Any) {
        let searchAlertController = UIAlertController(title: "門市查詢", message: nil, preferredStyle: .alert)
        searchAlertController.setValue(self.searchContentController, forKey: "contentViewController")
        searchAlertController.addAction(UIAlertAction(title: "取消", style: .default, handler: nil))
        searchAlertController.addAction(UIAlertAction(title: "查詢", style: .default, handler: { (_:UIAlertAction) in
            if let cityPickerView = self.searchContentController.cityField.inputView as? UIPickerView {
                if let countyPickerView = self.searchContentController.countyField.inputView as? UIPickerView {
                    self.locationZipCode = self.searchContentController.cityList[cityPickerView.selectedRow(inComponent: 0)].county[countyPickerView.selectedRow(inComponent: 0)].zipCode
                }
            }
            self.didTapSearch()
        }))
        self.present(searchAlertController, animated: true, completion: nil)
    }
    
    func didTapSearch() {
        if self.searchContentController.countyField.text! == "" {
            let noticeAlertController = UIAlertController(title: "錯誤訊息", message: "請先選擇縣市區域", preferredStyle: .alert)
            noticeAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            self.present(noticeAlertController, animated: true, completion: nil)
        } else {
            
            TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "服務據點查詢", action: "\(self.searchContentController.countyField.text!);\(self.searchContentController.addressField.text!)", label: nil, value: nil)
            
            self.nearbyContentController.isSearchResult = true
            self.callGoogleGetGeocodeAPI(withAddress: "\(self.searchContentController.cityField.text!)\(self.searchContentController.countyField.text!)\(self.searchContentController.addressField.text!)")
        }
    }
    
    // MARK: - API
    func callServiceLocationAPI(withZipCode zipCode: String = "") {
        TSTAPP2_API.sharedInstance.getServiceLocationList(withMSISDN: "", zipCode: zipCode, showProgress: true, completion:  { (dic:Dictionary<String, Any>, URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                let serviceLocation = ServiceLocationList.sharedInstance
                serviceLocation.setDataValue(withDic: dic)
                
                let storeIcon = UIImage(named: "google_map_area")
                var nearbyList: [ServiceLocation] = []
                serviceLocation.serviceLocationList.forEach({ (dealer: ServiceLocation) in
                    if dealer.latitude != "" && dealer.longitude != "" {
                        let latitude = Double(dealer.latitude)!
                        let longitude = Double(dealer.longitude)!
                        if zipCode == "" {
                            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                            marker.title = dealer.locationName
                            marker.snippet = "\(dealer.storeAddress)\n\(dealer.operateTime)\n\(dealer.phoneNumber)"
                            marker.appearAnimation = GMSMarkerAnimation.pop
                            marker.userData = dealer.phoneNumber
                            marker.map = self.mapView
                            marker.icon = storeIcon
                            marker.isFlat = true
                        }
                        nearbyList.append(dealer)
                    }
                })
                
                if nearbyList.count == 0 {
                    if zipCode != "" {
                        TSTAPP2_Utilities.showAlert(withMessage: "本區域查無資料", viewController: self)
                    }
                } else {
                    self.nearbyContentController.nearbyList = []
                    if zipCode != "" {
                        nearbyList.sort {
                            store1, store2 in
                            let distance1 = CLLocation(latitude: Double(store1.latitude)!, longitude: Double(store1.longitude)!).distance(from: CLLocation(latitude: self.mapView.camera.target.latitude, longitude: self.mapView.camera.target.longitude))
                            let distance2 = CLLocation(latitude: Double(store2.latitude)!, longitude: Double(store2.longitude)!).distance(from: CLLocation(latitude: self.mapView.camera.target.latitude, longitude: self.mapView.camera.target.longitude))
                            return distance1 < distance2
                        }
                    } else if self.locationManager.location != nil {
                        nearbyList.sort {
                            store1, store2 in
                            let distance1 = self.locationManager.location?.distance(from: CLLocation(latitude: Double(store1.latitude)!, longitude: Double(store1.longitude)!))
                            let distance2 = self.locationManager.location?.distance(from: CLLocation(latitude: Double(store2.latitude)!, longitude: Double(store2.longitude)!))
                            
                            var sortBool = true
                            
                            if distance1 != nil && distance2 != nil {
                                sortBool = round(distance1!) < round(distance2!)
                            }
                            return sortBool
                        }
                    }
                    for (index, storeLocation) in nearbyList.enumerated() {
                        if index > 2 {
                            break
                        }
                        self.nearbyContentController.nearbyList.append(storeLocation)
                    }
                }
                
                self.nearbyContentController.nearbyTableView?.reloadData()
                var cellHeight: CGFloat = 320
                if self.nearbyContentController.nearbyList.count < 4 {
                    cellHeight = CGFloat(self.nearbyContentController.nearbyList.count * 96)
                }
                self.nearbyContentController.preferredContentSize.height = cellHeight
            } else {
                if zipCode != "" {
                    let errorMsgAlertController = UIAlertController(title: nil, message: dic.stringValue(ofKey: "message"), preferredStyle: .alert)
                    errorMsgAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
                    self.present(errorMsgAlertController, animated: true, completion: nil)
                }
            }
            
        }) { (Error) in
            print("error: \(Error)")
        }
    }
    
    func callZipCodeAPI() {
        let zipCode = ZipCode.sharedInstance
        if zipCode.towns.count > 0 {
            self.searchContentController.cityList = zipCode.towns
        } else {
            TSTAPP2_API.sharedInstance.getZipCode(showProgress: true, completion: { (dic:Dictionary<String, Any>, URLResponse) in
                if dic.stringValue(ofKey: "code") == "00000" {
                    zipCode.setDataValue(withDic: dic)
                    self.searchContentController.cityList = zipCode.towns
                    return
                } else {
                    let errorAlertController = UIAlertController(title: nil, message: dic.stringValue(ofKey: "message"), preferredStyle: .alert)
                    errorAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
                    self.present(errorAlertController, animated: true, completion: nil)
                }
            }) { (Error) in
                print("error: \(Error)")
            }
        }
    }
    
    func callGoogleGetGeocodeAPI(withAddress address: String) {
        TSTAPP2_Utilities.showPKHUD()
        GOOGLE_API.sharedInstance.getGeocode(withAddress: address, latlng: "", showProgress: false, completion: { (dic:Dictionary<String, Any>, URLResponse) in
            TSTAPP2_Utilities.hidePKHUD()
            if (dic["status"] as? String ?? "") == "OK" {
                if let results = dic["results"] as? Array<Dictionary<String, Any>> {
                    for result in results {
                        if let geometry = result["geometry"] as? Dictionary<String, Any> {
                            let location = geometry["location"] as? Dictionary<String, Double> ?? [:]
                            let latitude = location["lat"] ?? 0
                            let longitude = location["lng"] ?? 0
                            if latitude != 0 && longitude != 0 {
                                self.searchResultMarker.map = nil
                                self.searchResultMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                                self.searchResultMarker.title = "\(self.searchContentController.cityField.text!)\(self.searchContentController.countyField.text!)"
                                self.searchResultMarker.icon = UIImage(named: "google_map_retailer")
                                self.searchResultMarker.map = self.mapView
                                self.mapView.animate(toLocation: self.searchResultMarker.position)
                                self.mapView.animate(toZoom: 12)
                            }
                        }
                        break
                    }
                    self.callServiceLocationAPI(withZipCode: self.locationZipCode)
                }
            }
            
        }) { (Error) in
            TSTAPP2_Utilities.hidePKHUD()
            print("error: \(Error)")
        }
    }

}

class StoreLocationNearbyViewController: UIViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var nearbyTableView: UITableView!
    
    var parentMapView: GMSMapView!
    
    var isSearchResult: Bool = false
    
    var nearbyList: [ServiceLocation] = []
    
    var userLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.nearbyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "storeInfo", for: indexPath) as! StoreLocationNearbyTableViewCell
        cell.locationNameLabel.text = self.nearbyList[indexPath.row].locationName
        cell.locationAddressLabel.text = self.nearbyList[indexPath.row].storeAddress
        var distance = 0.0
        if self.userLocation != nil && !self.isSearchResult {
            distance = userLocation.distance(from: CLLocation(latitude: Double(self.nearbyList[indexPath.row].latitude)!, longitude: Double(self.nearbyList[indexPath.row].longitude)!)) / 1000
        } else {
            distance = CLLocation(latitude: Double(self.nearbyList[indexPath.row].latitude)!, longitude: Double(self.nearbyList[indexPath.row].longitude)!).distance(from: CLLocation(latitude: parentMapView.camera.target.latitude, longitude: parentMapView.camera.target.longitude)) / 1000
        }
        cell.locationDistanceLabel.text = String(format: "%.1f %@", distance, "KM")
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        parentMapView.animate(toLocation: CLLocationCoordinate2D(latitude: Double(self.nearbyList[indexPath.row].latitude)!, longitude: Double(self.nearbyList[indexPath.row].longitude)!))
        parentMapView.animate(toZoom: 17)
        self.dismiss(animated: true, completion: nil)
    }
}

class StoreLocationNearbyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var locationNameLabel: UILabel!
    
    @IBOutlet weak var locationAddressLabel: UILabel!
    
    @IBOutlet weak var locationDistanceLabel: UILabel!
    
}

class StoreLocationSearchViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var cityField: UITextField!
    
    @IBOutlet weak var countyField: UITextField!
    
    @IBOutlet weak var addressField: UITextField!
    
    var cityList: [City] = []
    
    // MARK: - ENUM
    private enum EnumPickerTag: Int {
        case City
        case County
    }
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let cityFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        cityFieldRightIconImage.contentMode = .scaleAspectFit
        cityFieldRightIconImage.frame.size = CGSize(width: cityFieldRightIconImage.frame.size.width + 16, height: cityFieldRightIconImage.frame.size.height)
        
        var pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.City.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.cityField.rightViewMode = .always
        self.cityField.rightView = cityFieldRightIconImage
        self.cityField.inputView = pickerView
        self.cityField.delegate = self
        
        let countyFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        countyFieldRightIconImage.contentMode = .scaleAspectFit
        countyFieldRightIconImage.frame.size = CGSize(width: countyFieldRightIconImage.frame.size.width + 16, height: countyFieldRightIconImage.frame.size.height)
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerTag.County.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.countyField.rightViewMode = .always
        self.countyField.rightView = countyFieldRightIconImage
        self.countyField.inputView = pickerView
        self.countyField.delegate = self
        
        self.addressField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.cityField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let pickerView = textField.inputView as? UIPickerView {
            if pickerView.numberOfRows(inComponent: 0) > 0 {
                pickerView.delegate?.pickerView!(pickerView, didSelectRow: pickerView.selectedRow(inComponent: 0), inComponent: 0)
            }
        }
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let displayPickerView = EnumPickerTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .City:
            return self.cityList[row].name
        case .County:
            if self.cityField.text == "" {
                return "請先選擇縣市"
            } else {
                let cityPickerView = self.cityField.inputView as? UIPickerView
                let city = self.cityList[(cityPickerView?.selectedRow(inComponent: 0))!]
                return city.county[row].name
            }
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard pickerView.numberOfRows(inComponent: component) > 0 else { return }
        
        let displayPickerView = EnumPickerTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .City:
            if self.cityList.count > 0 && self.cityField.text != self.cityList[row].name {
                self.cityField.text = self.cityList[row].name
                let countyPickerView = self.countyField.inputView as? UIPickerView
                countyPickerView?.reloadComponent(0)
                countyPickerView?.selectRow(0, inComponent: 0, animated: false)
                self.countyField.text = ""
            }
            break
        case .County:
            if self.cityField.text != "" {
                let cityPickerView = self.cityField.inputView as? UIPickerView
                let city = self.cityList[(cityPickerView?.selectedRow(inComponent: 0))!]
                self.countyField.text = city.county[row].name
            }
            break
        }
    }
    
    // MARK: - UIPickerViewDatasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let displayPickerView = EnumPickerTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .City:
            return self.cityList.count
        case .County:
            if self.cityField.text == "" {
                return 1
            } else {
                let cityPickerView = self.cityField.inputView as? UIPickerView
                let city = self.cityList[(cityPickerView?.selectedRow(inComponent: 0))!]
                return city.county.count
            }
        }
    }
    
    // MARK: - IBAction
    func didTapDone() {
        self.view.endEditing(true)
    }
    
    @IBAction func didTapEditCity(_ sender: Any) {
        self.cityField.becomeFirstResponder()
    }
    
    @IBAction func didTapEditCounty(_ sender: Any) {
        self.countyField.becomeFirstResponder()
    }
}
