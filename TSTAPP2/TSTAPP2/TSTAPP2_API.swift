//
//  TSTAPP2_API.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/6.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import CryptoSwift

//let SERVER_HOST = "http://202.144.223.15"
//let SERVER_HOST_PNS = "http://202.144.223.15"
//let SERVER_HOST = "https://vas.tstartel.com"

let API_CACHE_TIME: Double = 1800

#if SIT
let SERVER_HOST = "https://tstappsit.tstartel.com"
let SERVER_HOST_PNS = "https://pnssit.tstartel.com"
let VERSION_CODE = 27
#elseif UAT
//let SERVER_HOST = "https://tstappuat.tstartel.com"
//let SERVER_HOST_PNS = "https://pnsuat.tstartel.com"
//let VERSION_CODE = 27
//-------------------------- Push請先換成上面
let SERVER_HOST = "https://tstappdev.tstartel.com"
let SERVER_HOST_PNS = "https://tstappdev.tstartel.com"
let VERSION_CODE = 27

#elseif EPROD
let SERVER_HOST = "https://tstapp.tstartel.com"
let SERVER_HOST_PNS = "https://pns.tstartel.com"
let VERSION_CODE = 27
#elseif DEV
let SERVER_HOST = "https://tstappdev.tstartel.com"
let SERVER_HOST_PNS = "https://tstappdev.tstartel.com"
let VERSION_CODE = 27
#else
let SERVER_HOST = "https://tstapp.tstartel.com"
let SERVER_HOST_PNS = "https://pns.tstartel.com"
let VERSION_CODE = 27
#endif

//let SERVER_HOST = "https://tstappsit.tstartel.com"
//let SERVER_HOST = "https://tstappuat.tstartel.com"

//let SERVER_HOST = "https://tstapp.tstartel.com"

let API_CACHED_DATA = "API_CACHED_DATA"

class TSTAPP2_API: NSObject {
    
    static let sharedInstance = TSTAPP2_API()
    
    private override init() {
        //This prevents others from using the default '()' initializer for this class.
    }
    
    private func startTask(withURLString urlString: String, data: Data, showProgress: Bool, progressMessage: String?, cacheTime: Double, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let url = URL(string: encodedUrlString!)
        
        if let url = url {
            
            if cacheTime > 0 {
                let cachedDic = returnCachedData(ofName: urlString, inSecond: cacheTime)
                if cachedDic == nil {
                    if showProgress {
                        TSTAPP2_Utilities.showPKHUD(withMessage: progressMessage)
                    }
                    let task = self.createTask(withURL: url, data: data, showProgress: showProgress, completion: completion, failure: failure)
                    task.resume()
                }
                else {
                    if showProgress {
                        TSTAPP2_Utilities.hidePKHUD()
                    }
                    completion(cachedDic!, URLResponse())
                }
            }
            else {
                if showProgress {
                    TSTAPP2_Utilities.showPKHUD(withMessage: progressMessage)
                }
                let task = self.createTask(withURL: url, data: data, showProgress: showProgress, completion: completion, failure: failure)
                task.resume()
            }
            
        }
        else {
            if showProgress {
                TSTAPP2_Utilities.hidePKHUD()
            }
            failure(NSError(domain: "Not valid URL", code: 1, userInfo: nil))
        }
    }
    
    internal func startTask(withURLString urlString: String, data: Data, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        startTask(withURLString: urlString, data: data, showProgress: showProgress, progressMessage: nil, cacheTime: 0, completion: completion, failure: failure)
        
    }
    
    private func startTask(withURLString urlString: String, data: Data, showProgress: Bool, cacheTime: Double, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        startTask(withURLString: urlString, data: data, showProgress: showProgress, progressMessage: nil, cacheTime: cacheTime, completion: completion, failure: failure)
        
    }
    
    private func createTask(withURL url: URL, data: Data, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) -> URLSessionTask {
        
        var request: URLRequest = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 30)
        request.httpBody = data
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session: URLSession = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            
        DispatchQueue.main.async {
            if showProgress {
                TSTAPP2_Utilities.hidePKHUD()
            }
            
            let dic = self.dictionary(fromResponseData: data)
            
            if error == nil {
                print("\nTSTAPP2_API \(url.absoluteString)\n\(dic.description)")
                self.saveDicFromAPI(name: url.absoluteString, dic: dic)
                completion(dic, response!)
            }
            else {
                print("\nTSTAPP2_API \(url.absoluteString)\n\(error!.localizedDescription)")
                failure(error!)
            }
        }
        
        
    })
        
        return task
    }
    
    /**
     Parse JSON Data into Dictionary<String, Any>
     */
    private func dictionary(fromResponseData data: Data?) -> Dictionary<String, Any> {
        var resultDic: Dictionary<String, Any> = [:]
        
        if data != nil {
//            var rawString = String(data: data!, encoding: String.Encoding.utf8)
            do {
                resultDic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! Dictionary<String, Any>
//                print("resultDic \(resultDic.description)")
            } catch let parseError {
                print("parseError \(parseError.localizedDescription)")
            }
            
        }
        
        return resultDic
    }
    
    internal func getEncryptedData(ofDictionary dic: Dictionary<String, Any>) -> Data {
        var jsonData = Data()
        do {
            let dicData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString = String.init(data: dicData, encoding: String.Encoding.utf8)!

            let requestDic = ["moduleName":"app", "data": getAESEncrypt(withString: jsonString)]
            jsonData = try JSONSerialization.data(withJSONObject: requestDic, options: JSONSerialization.WritingOptions(rawValue: 0))
        } catch let error {
            print("encryptedData \(error.localizedDescription)")
        }
        
        return jsonData
    }
    
    private func getEncryptedDataForBSCAPI(ofDictionary dic: Dictionary<String, Any>) -> Data {
        var jsonData = Data()
        do {
            let dicData = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
            let jsonString = String.init(data: dicData, encoding: String.Encoding.utf8)!
            
            let requestDic = ["request": getAESEncrypt(withString: jsonString)]
            jsonData = try JSONSerialization.data(withJSONObject: requestDic, options: JSONSerialization.WritingOptions(rawValue: 0))
        } catch let error {
            print("encryptedData \(error.localizedDescription)")
        }
        
        return jsonData
    }
    
    internal func getAESEncrypt(withString originString: String) -> String {
        var result = ""
        do {
            let aesKeyArray: Array<UInt8> = TSTAPP2_API.sharedInstance.aesEncryptKeyArray()
            let encryptedData = try AES(key: aesKeyArray, blockMode: BlockMode.CBC(iv: aesKeyArray), padding: Padding.pkcs7).encrypt(Array(originString.utf8))//(originString.data(using: String.Encoding.utf8)!)
            
            result = encryptedData.toBase64()!
        } catch let error {
            print("aesEncrypt \(error.localizedDescription)")
        }
        return result
    }
    
    internal func aesEncryptKey() -> String {
        let originKey = "a1a2a3a4a5a6a7a8a9a0aaabacadaeaf"
        var key = ""
        for index in 1...originKey.lengthOfBytes(using: String.Encoding.utf8) {
            if (index % 2) == 1 {
                let char: Character = originKey[originKey.index(originKey.startIndex, offsetBy: index)]
                key.append(char)
            }
        }
        
        return key
    }
    
    internal func aesEncryptKeyArray() -> Array<UInt8> {
        let originKey = "a1a2a3a4a5a6a7a8a9a0aaabacadaeaf"
        var keyArray: Array<UInt8> = []
        var key = ""
        
        for index in 1...originKey.lengthOfBytes(using: String.Encoding.utf8) {
            if (index % 2) == 1 {
                let char: Character = originKey[originKey.index(originKey.startIndex, offsetBy: index)]
                key.append(char)
            }
        }
        keyArray = key.bytes
        
        return keyArray
    }
    
    private func getXmlString(ofDictionary dic: Dictionary<String, String>) -> String {
        var xmlString = ""
        dic.forEach { (raw:(key: String, value: String)) in
            xmlString += "<\(raw.key)>\(raw.value)</\(raw.key)>"
        }
        return xmlString
    }
    
    private func saveDicFromAPI(name: String, dic: Dictionary<String, Any>) {
        
        guard dic.stringValue(ofKey: "code") == "00000" else {
            return
        }
        
//        guard UserDefaults.standard.dictionary(forKey: API_CACHED_DATA) != nil else {
//            UserDefaults.standard.set([:], forKey: API_CACHED_DATA)
//        }
        
        if UserDefaults.standard.dictionary(forKey: API_CACHED_DATA) == nil {
            UserDefaults.standard.set([:], forKey: API_CACHED_DATA)
        }
        
        let timeStamp = Date().timeIntervalSince1970
        let apiResponseData = ["time": timeStamp, "dic": dic] as [String : Any]
        
        var cachedData = UserDefaults.standard.dictionary(forKey: API_CACHED_DATA)! as Dictionary<String, Any>
        cachedData[name] = NSKeyedArchiver.archivedData(withRootObject: apiResponseData).base64EncodedString()
        UserDefaults.standard.set(cachedData, forKey: API_CACHED_DATA)
        UserDefaults.standard.synchronize()
    }
    
    private func returnCachedData(ofName name: String, inSecond second: Double) -> Dictionary<String, Any>? {
        
        guard UserDefaults.standard.dictionary(forKey: API_CACHED_DATA) != nil else {
            return nil
        }
        
        var dic: Dictionary<String, Any>? = nil
        let cachedData = UserDefaults.standard.dictionary(forKey: API_CACHED_DATA)! as Dictionary<String, Any>
        
        if cachedData[name] != nil {
            let timeStamp = Date().timeIntervalSince1970
            
            let cachedAPIResponseDataInBase64String = cachedData[name] as! String
            let cachedAPIResponseData = NSKeyedUnarchiver.unarchiveObject(with: Data(base64Encoded: cachedAPIResponseDataInBase64String)!) as! [String: Any]
            let cachedTime = cachedAPIResponseData["time"] as! TimeInterval
            
            
            let interval = round(timeStamp - cachedTime)
            
            if interval < second {
                dic = cachedAPIResponseData["dic"] as? Dictionary<String, Any>
                print("cached dic \(String(describing: dic?.description))")
            }
        }
        
        return dic
    }
    
//MARK: - API
    
//MARK: - 取得server狀態
    func getFuncStatus(withCompletion completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1"]
        let urlString = String("\(SERVER_HOST)/TAG/rest/tapp/getFuncStatus")
        
        print("TSTAPP2_API getFuncStatus\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: false, completion: completion, failure: failure)
    }
    
//MARK: - 檢查APP版本
    func getAppVersionInfo(withCompletion completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        var urlString = ""
        #if SIT
            urlString = String("https://tstappdev.tstartel.com/AMSWEB/restful/appVersionInfo")
        #elseif DEV
            urlString = String("https://tstappdev.tstartel.com/AMSWEB/restful/appVersionInfo")
        #elseif UAT
            urlString = String("https://tstappdev.tstartel.com/AMSWEB/restful/appVersionInfo")
        #else
            urlString = String("http://202.144.223.62:8080/AMSWEB/restful/appVersionInfo")
        #endif
        
        let dic = ["request": ["osType": "0", "appKey": "com.tstartel.TSTAPP"]]
        
        print("TSTAPP2_API getAppVersionInfo\n\(dic.description)")
        
        let dicData = try! JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0))
        let jsonString = String.init(data: dicData, encoding: String.Encoding.utf8)!
        let data = jsonString.data(using: String.Encoding.utf8)!
        
        self.startTask(withURLString: urlString, data: data, showProgress: false, completion: completion, failure: failure)
    }
    
//MARK: - 登入
    func login(withMSISDN msisdn: String, password: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "psswrd":password.md5(),
                   "password":password.md5()]
        let urlString = String("\(SERVER_HOST)/TAG/rest/mc/login")
        
        print("TSTAPP2_API login\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 第三方登入
    func loginExt(withExtId extId: String, extType: String,extToken: String,email: String? ,name: String? ,birthday: String? ,gender: String?, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        //speed up build time
        var email = email
        var birthday = birthday
        var gender = gender
        var name = name
        
        if email == nil { email = "" }
        if birthday == nil { birthday = "" }
        if gender == nil { gender = "" }
        if name == nil { name = "" }
        
        let dic = ["osType":"1",
                   "extId":extId,
                   "extType":extType,
                   "extToken":extToken,
                   "email":email!,
                   "birthday":birthday!,
                   "gender":gender!,
                   "name":name!]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/loginExt"
        
        print("TSTAPP2_API loginExt\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func bindExt(withMSISDN msisdn: String, extId: String, extType: String, extToken: String, password: String,email: String? ,name: String? ,birthday: String? ,gender: String?, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        //speed up build time
        var email = email
        var birthday = birthday
        var gender = gender
        var name = name
        
        if email == nil { email = "" }
        if birthday == nil { birthday = "" }
        if gender == nil { gender = "" }
        if name == nil { name = "" }
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "psswrd":password.md5(),
                   "extId":extId,
                   "extType":extType,
                   "extToken":extToken,
                   "email":email!,
                   "birthday":birthday!,
                   "gender":gender!,
                   "name":name!]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/bindExt"
        
        print("TSTAPP2_API bindExt\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func unbindExt(withMSISDN msisdn: String, extId: String, contractId: String, extType: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "extId":extId,
                   "extType":extType]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/unbindExt"
        
        print("TSTAPP2_API unbindExt\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 修改電子郵件
    func changeMailAddress(withMSISDN msisdn: String, newEmailAddress: String, contractId: String, accountId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "newEmailAddress":newEmailAddress, "accountId":accountId,
                   "contractId":contractId]
        
        let urlString = SERVER_HOST+"/TAG/rest/ebill/changeMailAddress"
        
        print("TSTAPP2_API changeMailAddress\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 修改帳單地址
    func changeBillAddress(withMSISDN msisdn: String, isMainMsisdn: String, zip: String, city: String, state: String, address: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "isMainMsisdn":isMainMsisdn,
                   "zip":zip,
                   "city":city,
                   "state":state,
                   "address":address]
        let urlString = SERVER_HOST+"/TAG/rest/xws/changeBillAddress"
        
        print("TSTAPP2_API changeBillAddress\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 電子帳單設定
    func showEBill(withMSISDN msisdn: String, isMainMsisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "isMainMsisdn": isMainMsisdn]
        
        let urlString = SERVER_HOST+"/TAG/rest/ebill/show"
        
        print("TSTAPP2_API showEBill\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func applyEBill(withMSISDN msisdn: String, emailAddress: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "emailAddress":emailAddress]
        
        let urlString = SERVER_HOST+"/TAG/rest/ebill/applyEbill"
        
        print("TSTAPP2_API applyEbill\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func esbillMemberApply(withMSISDN msisdn: String, contractId: String, accountId: String, billType: String, emailAddress: String, zip: String, city: String, state: String, address: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "contractId": contractId,
                   "acctId": accountId,
                   "billType": billType,
                   "emailAddress": emailAddress,
                   "zip": zip,
                   "city": city,
                   "state": state,
                   "address": address]
        
        let urlString = SERVER_HOST+"/TAG/rest/ebill/esbillMemberApply"
        
        print("TSTAPP2_API esbillMemberApply\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func getBillTypeInfo(withMsisdn msisdn: String, isMainMsisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "isMainMsisdn": isMainMsisdn]
        
        let urlString = SERVER_HOST+"/TAG/rest/ebill/getBillTypeInfo"
        
        print("TSTAPP2_API getBillTypeInfo\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
        
    }
    
//    MARK: - 密碼變更
    func setPassword(withMSISDN msisdn: String, oldPassword: String, newPassword: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "oldPsswrd":oldPassword,
                   "newPsswrd":newPassword,
                   "oldPassword":oldPassword,
                   "newPassword":newPassword]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/setPwd"
        
        print("TSTAPP2_API setPwd\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func forgetPassword(withMSISDN msisdn: String, custId: String, notificationType: String, notificationEmail: String?, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        var dic = ["osType":"1",
                   "msisdn":msisdn,
                   "custId":custId,
                   "notificationType":notificationType]
        
        if notificationEmail != nil {
            dic["notificationEmail"] = notificationEmail
        }
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/forgetPwd"
        
        print("TSTAPP2_API forgetPwd\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
        
    }
    
//    MARK: - 修改暱稱
    func changeProfile(withMSISDN msisdn: String, contractId: String,nickname: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "nickname":nickname]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/changeProfile"
        
        print("TSTAPP2_API changeProfile\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 更換大頭照
    func setProfilePic(withMSISDN msisdn: String, contractId: String, image: UIImage, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let imageData = UIImagePNGRepresentation(image)
        let picFileBinary = imageData!.base64EncodedString()
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "picFileBinary":picFileBinary]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/setProfilePic"
        
        print("TSTAPP2_API setProfilePic\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 首頁
    func getSmartBanner(withMSISDN msisdn :String?, contractId :String?, companyId :String?, contractStartDate :String?,contractExpireDate :String?, projectName :String?, isRenewal :String?, isBL: String?, isUnlimitedData :String?, birthday :String?, vipDegree :String?, retensionType :String?, custType: String?, customerContribution: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        //speed up build time
        var msisdn = msisdn
        var contractId = contractId
        var companyId = companyId
        var contractStartDate = contractStartDate
        var contractExpireDate = contractExpireDate
        var projectName = projectName
        var isRenewal = isRenewal
        var isBL = isBL
        var isUnlimitedData = isUnlimitedData
        var birthday = birthday
        var vipDegree = vipDegree
        var retensionType = retensionType
        var custType = custType
        
        
        if msisdn == nil { msisdn = "" }
        if contractId == nil { contractId = "" }
        if companyId == nil { companyId = "" }
        if contractStartDate == nil { contractStartDate = "" }
        if contractExpireDate == nil { contractExpireDate = "" }
        if projectName == nil { projectName = "" }
        if isRenewal == nil { isRenewal = "" }
        if isBL == nil { isBL = "" }
        if isUnlimitedData == nil { isUnlimitedData = "" }
        if birthday == nil { birthday = "" }
        if vipDegree == nil { vipDegree = "" }
        if retensionType == nil { retensionType = "" }
        if custType == nil { custType = "" }
        
        var dic = [
            "msisdn": msisdn!,
            "contractId": contractId!,
            "companyId": companyId!,
            "contractStartDate": contractStartDate!,
            "contractExpireDate": contractExpireDate!,
            "projectName": projectName!,
            "isRenewal": isRenewal!,
            "isBL": isBL!,
            "isUnlimitedData": isUnlimitedData!,
            "birthday": birthday!,
            "vipDegree": vipDegree!,
            "retensionType": retensionType!,
            "custType": custType!,
            "customerContribution": customerContribution,
            "osType": "1"
        ]
        
//        var dic = [
//            "msisdn": msisdn ?? "",
//            "contractId": contractId ?? "",
//            "companyId": companyId ?? "",
//            "contractStartDate": contractStartDate ?? "",
//            "contractExpireDate": contractExpireDate ?? "",
//            "projectName": projectName ?? "",
//            "isRenewal": isRenewal ?? "",
//            "isBL": isBL ?? "",
//            "isUnlimitedData": isUnlimitedData ?? "",
//            "birthday": birthday ?? "",
//            "vipDegree": vipDegree ?? "",
//            "retensionType": retensionType ?? "",
//            "custType": custType ?? "",
//            "customerContribution": customerContribution,
//            "osType": "1"
//        ]
        
        if TSTAPP2_Utilities.versionStatus() == VersionStatus.notReleased {
            dic["version"] = "\(VERSION_CODE)"
        }
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/getCMPData"
        
        print("TSTAPP2_API getCMPData\n\(dic.description)")
        
//        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
    
    func recommendProduct(withMSISDN msisdn :String?, contractId :String?, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn ?? "",
            "contractId": contractId ?? "",
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/recommendProduct"
        
        print("TSTAPP2_API recommendProduct\n\(dic.description)")
        
//        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
    
    func getHomepage(withMSISDN msisdn :String?, version :String?, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        var dic = [
            "msisdn": msisdn ?? "",
            "version": version ?? "",
            "osType": "1"
        ]
        
        if TSTAPP2_Utilities.versionStatus() == VersionStatus.notReleased {
            dic["version"] = "\(VERSION_CODE)"
        }
        
        let urlString = SERVER_HOST+"/TAG/rest/cms/getHomepage"
        
        print("TSTAPP2_API getHomepage\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 服務據點
    func getServiceLocationList(withMSISDN msisdn: String,zipCode: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "zipCode":zipCode]
        
        let urlString = SERVER_HOST+"/TAG/rest/cms/getServiceLocationList"
        
        print("TSTAPP2_API getServiceLocationList\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 國際漫遊
    func queryDailyRoaming(withMSISDN msisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {

        let dic = ["osType":"1",
                   "msisdn":msisdn]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/queryDailyRoaming"
        
        print("TSTAPP2_API queryDailyRoaming\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func queryRoamingStatus(withMSISDN msisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn]
        
        let urlString = SERVER_HOST+"/TAG/rest/crm/queryRoamingStatus"
        
        print("TSTAPP2_API queryRoamingStatus\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func applyDailyRoaming(withMSISDN msisdn: String, contractId: String, accountId: String, countryId: String, effectiveTime: String, applyDays: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "contractId": contractId,
                   "accountId": accountId,
                   "countryId": countryId,
                   "effectiveTime": effectiveTime,
                   "applyDays": applyDays]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/applyDailyRoaming"
        
        print("TSTAPP2_API applyDailyRoaming\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func getDailyRoamingCancelMessage(withMSISDN msisdn: String, appliedId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "appliedId":appliedId]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/getDailyRoamingCancelMessage"
        
        print("TSTAPP2_API getDailyRoamingCancelMessage\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func cancelDailyRoaming(withMSISDN msisdn: String, appliedId: String, countryId: String, statusCode: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "appliedId":appliedId,
                   "countryId":countryId,
                   "statusCode":statusCode]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/cancelDailyRoaming"
        
        print("TSTAPP2_API cancelDailyRoaming\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func queryRoaming(withMSISDN msisdn: String, contractId: String, accountId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "accountId":accountId]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/queryRoaming"
        
        print("TSTAPP2_API queryRoaming\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func changeRoamingStatus(withMSISDN msisdn: String, contractId: String, status: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "contractId": contractId,
                   "status": status]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/changeRoamingStatus"
        
        print("TSTAPP2_API changeRoamingStatus\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 網內外門號查詢
    func onNetOffNet(withMSISDN msisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn]
        
        let urlString = SERVER_HOST+"/TAG/rest/crm/onNetOffNet"
        
        print("TSTAPP2_API onNetOffNet\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - VOLTE
    func getVolteDevice(withMSISDN msisdn: String, showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        let dic = [
            "osType": "1",
            "msisdn": msisdn,
            "deviceModel": TSTAPP2_Utilities.hardwareString()
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tapp/getVolteDevice"
        
        print("TSTAPP2_API getVolteDevice\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress,completion: completion, failure: failure)
    }
    
    func doVolteProcess(withMsisdn msisdn: String, contractId: String, op: String, showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "contractId": contractId,
                   "op": op.uppercased()]
        
        let urlString = SERVER_HOST+"/TAG/rest/crm/doVolteProcess"
        
        print("TSTAPP2_API doVolteProcess\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
        
    }
    
//    MARK: - 基本服務設定
    func changeServiceStatus(withMSISDN msisdn: String, contractId: String,serviceStatus :String, showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType": "1",
                   "msisdn": msisdn,
                   "contractId": contractId,
                   "serviceStatus": serviceStatus]
     
        let urlString = SERVER_HOST+"/TAG/rest/xws/changeServiceStatus"
        
        print("TSTAPP2_API changeServiceStatus\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    
    func queryServiceStatus(withMSISDN msisdn: String, custId: String, companyId: String, contractId: String, showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "custId":custId,
                   "serviceType":"1",
                   "companyId": companyId,
                   "contractId": contractId]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/queryServiceStatus"
        
        print("TSTAPP2_API queryServiceStatus\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 未出帳資訊
    func unbillData(withMSISDN msisdn: String, contractId: String,isUnlimitedData: String,companyId: String,isBL: String, gsmToLteFlag: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "isUnlimitedData":isUnlimitedData,
                   "companyId":companyId,
                   "isBL":isBL,
                   "gsmToLteFlag":gsmToLteFlag]
        
        let urlString = SERVER_HOST+"/TAG/rest/billing/unbillData"
        
        print("TSTAPP2_API unbillData\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 帳單資訊
    func queryBill(withMSISDN msisdn: String, contractId: String,custId: String,accountId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "custId":custId,
                   "accountId":accountId]
        
        let urlString = SERVER_HOST+"/TAG/rest/billing/queryBillAndDetail"
        
        print("TSTAPP2_API queryBillAndDetail\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - ZIP Code
    func getZipCode(showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1"]
        
        let urlString = SERVER_HOST+"/TAG/rest/cms/getZipCode"
        
        print("TSTAPP2_API getZipCode\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 合約內容
    func queryProjectAgreement(withMSISDN msisdn: String, projectCode: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "projectCode":projectCode]
        
        let urlString = SERVER_HOST+"/TAG/rest/xws/queryProjectAgreement"
        
        print("TSTAPP2_API queryProjectAgreement\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }

//    MARK: - 信用卡轉帳申請
    func queryDirectDebit(withMSISDN msisdn: String, contractId: String, accountId: String, custId: String, companyId: String, channel: String, isMainMsisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "accountId":accountId,
                   "custId":custId,
                   "companyId":companyId,
                   "channel":channel,
                   "isMainMsisdn":isMainMsisdn]
        
        let urlString = SERVER_HOST + "/TAG/rest/directDebit/query"
        
        print("TSTAPP2_API queryDirectDebit\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func submitDirectDebit(withMSISDN msisdn: String, contractId: String, accountId: String, custId: String,companyId: String, channel: String, action: String, custName: String, cardNumber: String, cvv2: String, expiredDate: String,progressMessage: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "accountId":accountId,
                   "custId":custId,
                   "companyId":companyId,
                   "channel":channel,
                   "action":action,
                   "custName":custName,
                   "cardNumber":cardNumber,
                   "cvv2":cvv2,
                   "expiredDate":expiredDate]
        
        let urlString = SERVER_HOST+"/TAG/rest/directDebit/submit"
        
        print("TSTAPP2_API submitDirectDebit\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, progressMessage: progressMessage, cacheTime: 0, completion: completion, failure: failure)
    }
    
//    MARK: - 客服信箱
    //取客服信箱分類
    func getCmsCategory(withMSISDN msisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        let dic = [
            "msisdn": msisdn,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/cms/getCategory"
        
        print("TSTAPP2_API getCmsCategory\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //客服信箱回覆已讀
    func replyIssueRead(withMSISDN msisdn: String, issueId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "issueId": issueId,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/crm/replyIssueRead"
        
        print("TSTAPP2_API replyIssueRead\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //客服信箱再發問
    func addIssueAgain(withMSISDN msisdn: String, issueId: String, issueNote: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "issueId": issueId,
            "issueNote": issueNote,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/crm/addIssueAgain"
        
        print("TSTAPP2_API addIssueAgain\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func addContactUs(withType type: String, name: String,mobile: String, dayPhonePrefix: String, dayPhoneNumber: String, dayPhoneExtension: String, nightPhonePrefix: String, nightPhoneNumber: String, email: String,description: String, issueIdentify1: String, issueIdentify2: String, issueIdentify3: String,signalType: String, signalStrength: String, addressType1City: String, addressType1Section: String, addressType1Addr: String, addressType2Addr: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "osType": "1",
            "customerType": (LoginData.isLogin() == true) ? "既有客戶" : "潛在客戶",
            "msisdn": mobile,
            "contractId": LoginData.sharedLoginData().custProfile_contractId,
            "type": type,
            "custName": name,
            "daytimeContactArea": dayPhonePrefix,
            "daytimeContactPhone": dayPhoneNumber,
            "daytimeContactExt": dayPhoneExtension,
            "nightContactArea": nightPhonePrefix,
            "nightContactPhone": nightPhoneNumber,
            "email": email,
            "issueDescription": description,
            "issueIdentify1": issueIdentify1,
            "issueIdentify2": issueIdentify2,
            "issueIdentify3": issueIdentify3,
            "signalType": signalType,
            "signalStrength": signalStrength,
            "addressType1City": addressType1City,
            "addressType1Section": addressType1Section,
            "addressType1Addr": addressType1Addr,
            "addressType2Addr": addressType2Addr
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/cms/addContactUs"
        
        print("TSTAPP2_API addContactUs\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 問與答
    func queryIssue(withMSISDN msisdn: String, contractId: String, startDate: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "osType": "1",
            "msisdn": msisdn,
            "contractId": contractId,
            "startDate": startDate
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/crm/queryIssue"
        
        print("TSTAPP2_API queryIssue\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 案件查詢
    func getQueryTicketByDate(withMSISDN msisdn:String, contractId:String, year:String, month:String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "year": year,
            "month": month,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/queryTicket"
        
        print("TSTAPP2_API queryTicket\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func getQueryTicketByCaseNo(withMSISDN msisdn: String, contractId: String, ticketId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "ticketId": ticketId,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/queryTicket"
        
        print("TSTAPP2_API queryTicket\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 用戶基本資料
    func getCustProfile(withMSISDN msisdn :String,contractId: String, uid: String, token: String,  showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "contractId": contractId,
            "uid": uid,
            "token": token,
            "msisdn": msisdn,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/mc/getCustProfile"
        
        print("TSTAPP2_API getCustProfile\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func getCustomerInfoBrief(withMSISDN msisdn :String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/getCustomerInfoBrief"
        
        print("TSTAPP2_API getCustomerInfoBrief\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }

//    MARK: - 家族優惠
    func getGroupBuy(withMSISDN msisdn :String,contractId :String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/cgc"
        
        print("TSTAPP2_API getGroupBuy\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }

//    MARK: - 推播
    func registerDevice(withMSISDN msisdn: String, contractId: String, deviceId: String, token: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        let dic = [
            "appVersion": appVersion,
            "deviceModel": TSTAPP2_Utilities.hardwareString(),
            "osVersion": UIDevice.current.systemVersion,
            "msisdn": msisdn,
            "contractId": contractId,
            "deviceId": deviceId,
            "token": token,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tapp/registerDevice"
        
        print("TSTAPP2_API registerDevice\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func removeDevice(withMSISDN msisdn: String, contractId: String, deviceId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "deviceId": deviceId,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tapp/removeDevice"
        
        print("TSTAPP2_API removeDevice\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func changeDeviceAllowSend(withMSISDN msisdn: String, contractId: String, deviceId: String, allowSend: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "deviceId": deviceId,
            "allowSend": allowSend,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tapp/changeDeviceAllowSend"
        
        print("TSTAPP2_API changeDeviceAllowSend\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
//    MARK: - 推播訊息
    
    //推撥訊息 新
    func queryMessage(withMSISDN msisdn: String, contractId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "appId": "TSTAPP",
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST_PNS + "/PNS/rest/msg/queryMessage"
        
        print("TSTAPP2_API queryMessage\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //刪除推撥訊息 新
    func deleteMessage(withMSISDN msisdn: String, messageSeq: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "messageSeq": messageSeq,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST_PNS + "/PNS/rest/msg/deleteMessage"
        
        print("TSTAPP2_API deleteMessage\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //更新推播訊息讀取狀態為已讀
    func updateReadStatus(withMSISDN msisdn: String, messageSeq: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "messageSeq": messageSeq,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST_PNS + "/PNS/rest/msg/updateReadStatus"
        
        print("TSTAPP2_API updateReadStatus\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //MARK: - DIY資費
    func verifyDIY(withMSISDN msisdn: String, projectCode: String, isBL: Bool, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "isBL": isBL ? "Y" : "N",
            "projectCode": projectCode,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/tsp/verifyDIY"
        
        print("TSTAPP2_API verifyDIY\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //MARK: - 銀行資料
    func getBankInfo(withMSISDN msisdn: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/cms/getBankInfo"
        
        print("TSTAPP2_API getBankInfo\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    // MARK: - 查詢可變更專案
    func findRatePlan(withMSISDN msisdn: String, contractId: String, planName: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "planName": planName,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST + "/TAG/rest/crm/findRatePlan"
        
        print("TSTAPP2_API findRatePlan\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
        
    }
    
    //MARK: - 銀行繳款
    func doBank(withMSISDN msisdn: String, contractId: String, custId: String, accountId: String, isMainMsisdn: String, bankId: String, bankAccount: String, payAmount: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        
        let dic = [
            "osType": "1",
            "msisdn": msisdn,
            "contractId": contractId,
            "custId": custId,
            "accountId": accountId,
            "isMainMsisdn": isMainMsisdn,
            "bankId": bankId,
            "bankAccount": bankAccount,
            "payAmount": payAmount]
        
        let urlString = SERVER_HOST+"/TAG/rest/payment/doBank"
        
        print("TSTAPP2_API doBank\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    // MARK: - 變更專案
    func changeRatePlan(withMSISDN msisdn: String, contractId: String, fromSystem: String, newRatePlan: String, oriProjName: String, chgProjName: String, handset: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "osType": "1",
            "msisdn": msisdn,
            "contractId": contractId,
            "fromSystem": fromSystem,
            "newRatePlan": newRatePlan,
            "oriProjName": oriProjName,
            "chgProjName": chgProjName,
            "handset": handset
        ]
        
        let urlString = SERVER_HOST + "/TAG/rest/crm/changeRatePlan"
        
        print("TSTAPP2_API changeRatePlan\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
        
    }
    
    //MARK: - 信用卡繳款
    func doCreditCard(withMSISDN msisdn: String, contractId: String, accountId: String, custId: String, isMainMsisdn: String, purchAmt: String, cardNumber: String, cvv2: String, expiredDate: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = [
            "msisdn": msisdn,
            "contractId": contractId,
            "accountId": accountId,
            "custId": custId,
            "isMainMsisdn": isMainMsisdn,
            "purchAmt": purchAmt,
            "cardNumber": cardNumber,
            "cvv2": cvv2,
            "expiredDate": expiredDate,
            "osType": "1"
        ]
        
        let urlString = SERVER_HOST+"/TAG/rest/payment/doCreditCard"
        
        print("TSTAPP2_API doCreditCard\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    func getBL(withMSISDN msisdn: String, contractId: String, companyId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn": msisdn,
                   "contractId": contractId,
                   "companyId": companyId]
        
        
        let urlString = SERVER_HOST + "/TAG/rest/cms/getBL"
        
        print("TSTAPP2_API getBL\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, completion: completion, failure: failure)
    }
    
    // MARK: - 取得帳單barcode
    func getBarcodeInfo(withBillDate billDate: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":LoginData.sharedLoginData().custProfile_msisdn,
                   "accountId":LoginData.sharedLoginData().custProfile_accountId,
                   "billDate":billDate]
        
        let urlString = SERVER_HOST+"/TAG/rest/billing/getBarcodeInfo"
        
        print("TSTAPP2_API getBarcodeInfo\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //MARK: - 用戶額度資訊查詢
    func queryUserQuota(withMSISDN msisdn: String, contractId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId]
        
        let urlString = SERVER_HOST+"/TAG/rest/cbmp/queryUserQuota"
        
        print("TSTAPP2_API queryUserQuota\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    //MARK: - 用戶可用額度更新
    /**
        回傳：newQuota：調整後的額度。若更新失敗則會回傳目前的額度
             effectiveTime： 新額度生效時間，格式:yyyy/MM/dd HH:mm:ss
     */
    func updateQuota(withMSISDN msisdn: String, contractId: String,newQuota: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "newQuota":newQuota]
        
        let urlString = SERVER_HOST+"/TAG/rest/cbmp/updateQuota"
        
        print("TSTAPP2_API updateQuota\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, completion: completion, failure: failure)
    }
    
    
    //MARK: - 預付卡查詢
    func prepaidFamily(withMSISDN msisdn: String, contractId: String,custId: String, showProgress: Bool, completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "custId":custId]
        
        let urlString = SERVER_HOST+"/TAG/rest/pfs/prepaidFamily"
        
        print("TSTAPP2_API prepaidFamily\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
    //REMARK: - ----我的帳戶----
    //MARK:電信代收/小額付費開關檢查
    func checkLock(withMSISDN msisdn: String,contractId: String,showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId]
        
        let urlString = SERVER_HOST+"/TAG/rest/cbmp/checkLock"
        
        print("TSTAPP2_API relationQuery\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
    
    //-MARK:電信代收/小額付費服務狀態更新(開/關/安全碼)
    func updateSwitchStatus(withMSISDN msisdn: String,contractId: String,switchType:String,switchStatus:String,securityCode:String,showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        var dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId,
                   "switchType":switchType,
                   "switchStatus":switchStatus]
        if securityCode != "" {
            dic.updateValue(securityCode, forKey: "securityCode")
        }
        let urlString = SERVER_HOST+"/TAG/rest/cbmp/updateSwitchStatus"
        
        print("TSTAPP2_API relationQuery\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic), showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
    
//    MARK: - 加值服務
    func getAppliedAddValueSvc(withMSISDN msisdn: String, contractId: String, showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        
        let dic = ["osType":"1",
                   "msisdn":msisdn,
                   "contractId":contractId]
        
        let urlString = SERVER_HOST+"/TAG/rest/addvalue/getAppliedSvc"
        
        print("TSTAPP2_API getAppliedAddValueSvc\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
    
//    MARK: - 限時活動
    func getCampaignInfo(showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        let dic = ["osType": "1"]
        
        let urlString = SERVER_HOST+"/TAG/rest/tapp/getCampaignInfo"
        
        print("TSTAPP2_API getCampaignInfo\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, cacheTime: 0, completion: completion, failure: failure)
    }
    
//    MARK: - 取得URL
    func getUrlMap(showProgress: Bool,completion: @escaping (Dictionary<String, Any>, URLResponse) -> Void, failure: @escaping (Error) -> Void) {
        let dic = ["osType": "1"]
        
        let urlString = SERVER_HOST+"/TAG/rest/tapp/getUrlMap"
        
        print("TSTAPP2_API getUrlMap\n\(dic.description)")
        
        self.startTask(withURLString: urlString, data: getEncryptedData(ofDictionary: dic),  showProgress: showProgress, cacheTime: API_CACHE_TIME, completion: completion, failure: failure)
    }
}
