//
//  TSTAPP2_Utilities.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/11.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Crashlytics
import CryptoSwift
import Firebase
import UserNotifications
import SystemConfiguration

extension NSLayoutConstraint {
    /**
     Change multiplier constraint
     
     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
     */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem as Any,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension UIAlertController {
    
    public func addActions(array: Array<UIAlertAction>) {
        
        for action in array {
            self.addAction(action)
        }
    }
    
    public func show(viewController: UIViewController) {
        
        viewController.present(self, animated: true, completion: nil)
    }
}

extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any{
    
    public func valueIsString(ofKey key: String) -> Bool {
        let dic = (self as Any) as? Dictionary<String, Any>
        return dic?[key] as? String != nil
    }
    
    public func stringValue(ofKey key: String) -> String {
        let dic = (self as Any) as? Dictionary<String, Any>
        var theString = ""
        if let value = dic?[key] as? String {
            theString = value
        }
        else if let value = dic?[key] as? NSNumber {
            theString = value.stringValue
        }
        return theString
    }
}

extension UITableView {
    
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

extension CALayer {
    var borderColorFromUIColor: UIColor {
        get {
            return UIColor(cgColor: self.borderColor!)
        } set {
            self.borderColor = newValue.cgColor
        }
    }
}

extension UIImageView {
    
    public func imageFromURL(urlString: String) {
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }
        
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview()
                self.image = image
            })
            
        }).resume()
    }
}

extension String {

    var toFloatValue: Float! {
        let numberFormatter = NumberFormatter()
        if let number = numberFormatter.number(from: self) {
            return number.floatValue
        }
        return 0.0
    }
}

enum encodeForSecurityType {
    
    case Email
    case Phone
    case Address
    case Name
    case CustomerId
    case CardNumber
}


var originContentInset: UIEdgeInsets?
var originScrollIndicatorInsets: UIEdgeInsets?

class TSTAPP2_Utilities: NSObject {
    
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("user@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
    }
    
    class func showAlertAndLogoutThenShowLoginView(withMessage message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "重新登入", style: .cancel, handler: {(action: UIAlertAction) in
            
            TSTAPP2_Utilities.logout(completion: {
                AutomaticViewChange.sharedInstance.setTabIndex(withAction: AUTO_LOGIN, actionParam: nil)
            }, failure: {
                
            })
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }

    class func showAlertAndLogout(withMessage message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "重新登入", style: .cancel, handler: {(action: UIAlertAction) in
            //let _ = viewController.navigationController?.popViewController(animated: true)
            
            TSTAPP2_Utilities.logout(completion: {
                viewController.performSegue(withIdentifier: "toMain", sender: nil)
            }, failure: {
                
            })
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func showAlertAndPop(withMessage message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: {(action: UIAlertAction) in
            let _ = viewController.navigationController?.popViewController(animated: true)
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func showAlertAndPop(toLevel level:Int, message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: {(action: UIAlertAction) in
            if let viewCount = viewController.navigationController?.viewControllers.count {
                let vc = viewController.navigationController?.viewControllers[viewCount - level]
                viewController.navigationController?.popToViewController(vc!, animated: true)
            }
            let _ = viewController.navigationController?.popViewController(animated: true)
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func showAlertAndChangeToLanding(withTitle title: String, message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: {(action: UIAlertAction) in
            AutomaticViewChange.sharedInstance.setTabIndex(withAction: AUTO_LANDING, actionParam: nil)
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func showAlertAndPop(withTitle title: String, message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: {(action: UIAlertAction) in
            let _ = viewController.navigationController?.popViewController(animated: true)
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func showAlertAndPop(toLevel level: Int, title: String, message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: {(action: UIAlertAction) in
            if let viewCount = viewController.navigationController?.viewControllers.count {
                let vc = viewController.navigationController?.viewControllers[viewCount - level]
                viewController.navigationController?.popToViewController(vc!, animated: true)
            }
            let _ = viewController.navigationController?.popViewController(animated: true)
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    
    class func showAlert(withTitle title: String, message: String, viewController: UIViewController) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func showAlert(withMessage message: String, viewController: UIViewController) {
        
//        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
//        let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
//        controller.addAction(action)
//        
//        controller.show(viewController: viewController)
        
        TSTAPP2_Utilities.showAlert(withTitle: "", message: message, viewController: viewController)
    }
    
    class func showAlert(withTitle title:String, message: String, action: UIAlertAction, viewController: UIViewController) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(action)
        controller.show(viewController: viewController)
    }
    
    class func showAlert(withMessage message: String, action: UIAlertAction, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        controller.addAction(action)
        controller.show(viewController: viewController)
    }
    
    class func showAlert(withTitle title:String, message: String, action: UIAlertAction, cancel: UIAlertAction, viewController: UIViewController) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(action)
        controller.addAction(cancel)
        controller.show(viewController: viewController)
    }
    
    class func showAlert(withMessage message: String, action: UIAlertAction, cancel: UIAlertAction, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        controller.addAction(action)
        controller.addAction(cancel)
        controller.show(viewController: viewController)
    }
    
    class func showAlertAndSegue(withMessage message:String,segueIdentifier:String,sender:AnyObject?, viewController: UIViewController) {
        let controller = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .cancel, handler: {(action: UIAlertAction) in
            viewController.performSegue(withIdentifier: segueIdentifier, sender: sender)
        })
        controller.addAction(action)
        
        controller.show(viewController: viewController)
    }
    
    class func createGAIBuilderWithCategory(withCategory category: String?, action: String?, label: String?, value: NSNumber?) {
        let builder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value).build() as NSDictionary
        GAI.sharedInstance().defaultTracker.send(builder as! [AnyHashable: Any])
        
//        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//            AnalyticsParameterItemID: "\(String(describing: category))" as NSObject,
//            AnalyticsParameterItemName: "\(String(describing: action))" as NSObject,
//            AnalyticsParameterContentType: "\(String(describing: label))" as NSObject
//            ])
    }
    
    class func sendGAScreen(withScreenName screenName: String) {
        Analytics.setScreenName(screenName, screenClass: nil)
        
    }
    
    class func hardwareString() -> String {
        var name: [Int32] = [CTL_HW, HW_MACHINE]
        var size: Int = 2
        
        var tempName: [Int32] = name
        sysctl(&tempName, 2, nil, &size, &name, 0)
        var hw_machine = [CChar](repeating: 0, count: Int(size))
        sysctl(&tempName, 2, &hw_machine, &size, &name, 0)
        
        let hardware: String = String(cString: hw_machine)
        return hardware
    }
    
    class func setBackBarButtonItemTitle(_ VC: UIViewController, title: String) {
        if let topItem = VC.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: title, style:UIBarButtonItemStyle.plain, target:nil, action:nil)
        }
    }

    class func resize(image: UIImage, maxWidth: CGFloat, maxHeight: CGFloat) -> UIImage {
        
        var resizedImage = UIImage()
        
        let width = image.size.width
        let height = image.size.height
        
        if width <= maxWidth && height <= maxHeight {
            resizedImage = image
        }
        else {
            var rect = CGRect(x: 0, y: 0, width: width, height: height)
            let ratio = width / height
            if ratio > 1 {
                rect.size.width = maxWidth
                rect.size.height = maxWidth / ratio
            }
            else {
                rect.size.width = maxHeight * ratio
                rect.size.height = maxHeight
            }
            
            UIGraphicsBeginImageContext(rect.size)
            image.draw(in: rect)
            resizedImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return resizedImage
    }
    
    class func showPKHUD(withMessage message: String?) {
        PKHUD.sharedHUD.dimsBackground = false
        PKHUD.sharedHUD.contentView = PKHUDProgressView(title: nil, subtitle: message)
        PKHUD.sharedHUD.show()
    }
    
    class func showPKHUD() {
        PKHUD.sharedHUD.dimsBackground = false
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    class func hidePKHUD() {
        PKHUD.sharedHUD.hide()
    }
    
    class func hidePKHUD(afterDelay delay: TimeInterval, completion: PKHUD.TimerAction?) {
        PKHUD.sharedHUD.hide(afterDelay: delay, completion: completion)
    }
    
    class func roundedImage(_ imageView: UIImageView, radius: CGFloat) {
        let imageLayer = imageView.layer
        if radius == 0 {
            imageLayer.cornerRadius = imageView.frame.width/2
        }
        else {
            imageLayer.cornerRadius = radius
        }
        imageLayer.borderWidth = 2
        imageLayer.borderColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1).cgColor
        imageLayer.masksToBounds = true
    }
    
    class func setScrollViewInsetWhenKBShow(_ view: AnyObject, notification: Notification?) {
        if let info = (notification as NSNotification?)?.userInfo {
            let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let bottom: CGFloat = keyboardFrame.size.height
            if let tableView = view as? UITableView {
                let y = tableView.contentInset.top
                let indicatorY = tableView.scrollIndicatorInsets.top
                tableView.contentInset = UIEdgeInsetsMake(y, 0, bottom + 20, 0)
                tableView.scrollIndicatorInsets = UIEdgeInsetsMake(indicatorY, 0, bottom, 0)
            }
            else if let scrollView = view as? UIScrollView {
                if originContentInset == nil && originScrollIndicatorInsets == nil {
                    originContentInset = scrollView.contentInset
                    originScrollIndicatorInsets = scrollView.scrollIndicatorInsets
                }
                let y = scrollView.contentInset.top
                let indicatorY = scrollView.scrollIndicatorInsets.top
                scrollView.contentInset = UIEdgeInsetsMake(y, 0, bottom + 20, 0)
                scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(indicatorY, 0, bottom, 0)
            }
        }
    }
    
    class func setScrollViewInsetWhenKBHide(_ view: AnyObject, hasTabBar: Bool, notification: Notification?) {
        if let info = (notification as NSNotification?)?.userInfo {
            let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            if let tableView = view as? UITableView {
                let y = tableView.contentInset.top
                let bottomVariable: CGFloat = hasTabBar ? 29 : -20
                let bottom = tableView.contentInset.bottom - keyboardFrame.size.height + bottomVariable //- 20//+ 30
                tableView.contentInset = UIEdgeInsetsMake(y, 0, bottom, 0)
                tableView.scrollIndicatorInsets = UIEdgeInsetsMake(y, 0, bottom, 0)
            }
            else if let scrollView = view as? UIScrollView {
//                let y = scrollView.contentInset.top
//                let bottomVariable: CGFloat = hasTabBar ? 29 : -20
//                let bottom = scrollView.contentInset.bottom - keyboardFrame.size.height + bottomVariable //- 20//+ 30
//                scrollView.contentInset = UIEdgeInsetsMake(y, 0, bottom, 0)
//                scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(y, 0, bottom, 0)
                if originContentInset != nil && originScrollIndicatorInsets != nil {
                    scrollView.contentInset = originContentInset!
                    scrollView.scrollIndicatorInsets = originScrollIndicatorInsets!
                    originContentInset = nil
                    originScrollIndicatorInsets = nil
                }
            }
        }
    }
    
    class func addKeyboardShowHideObserver(_ anyViewController: AnyObject) {
        NotificationCenter.default.removeObserver(anyViewController, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(anyViewController, selector: Selector(("keyboardWillShow:")), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.removeObserver(anyViewController, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(anyViewController, selector: Selector(("keyboardWillHide:")), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    class func removeKeyboardShowHideObserver(_ anyViewController: AnyObject) {
        NotificationCenter.default.removeObserver(anyViewController, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(anyViewController, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    class func nowDateFromGMT(dateFormatter: String?) -> String {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "zh_TW")
        //formatter.timeZone = TimeZone(secondsFromGMT: 8)
        formatter.dateFormat = dateFormatter ?? "yyyy/MM/dd HH:mm:ss"
        
        return formatter.string(from: date)
    }
    
    class func encodeForSecurity(sourceString :String, type :encodeForSecurityType) -> String {
        
        let length = sourceString.count
        var str = sourceString as NSString
        
        if sourceString == "" {
            return ""
        }
        
        if type == .Phone {
            
            for i in 4..<length where i % 2 == 0 {
                
                str = str.replacingCharacters(in: NSMakeRange(i, 1), with: "*") as NSString
            }
        }
        
        if type == .CustomerId {
            
            for i in 5..<length {
                
                str = str.replacingCharacters(in: NSMakeRange(i, 1), with: "*") as NSString
            }
        }
        
        if type == .Name {
            
            var isEnglish = true
 
            if length <= 1 {
                
                str = sourceString as NSString
            }
            else {
                
                for i in 0..<length {
                    
                    let string = str.substring(with: NSMakeRange(i, 1)) as NSString
                    
                    if string.utf8String != nil {
                        isEnglish = strlen(string.utf8String) > 1 ? false:true
                    }
                }
                
                if isEnglish {
                    
                    str = str.replacingCharacters(in: NSMakeRange(1, length-2), with: "***") as NSString
                }
                else {
                    
                    if length == 2 {
                        
                        str = str.replacingCharacters(in: NSMakeRange(1, 1), with: "＊") as NSString
                    }
                    else {
                        
                        str = str.replacingCharacters(in: NSMakeRange(1, length-2), with: "＊") as NSString
                    }
                }
            }
        }
        
        if type == .Email {
            
            let range = str.range(of: "@")
            if range.length == 1 {
                if range.location > 4 {
                    str = str.replacingCharacters(in: NSMakeRange(range.location - 4, 4), with: "****") as NSString
                }
                else {
                    str = str.replacingCharacters(in: NSMakeRange(0, range.location), with: "****") as NSString
                }
            }
        }
        
        if type == .Address {
            
            if length < 6 {
                
                str = str.replacingCharacters(in: NSMakeRange(3, length-3), with: "******") as NSString
            }
            else {
                
                str = str.replacingCharacters(in: NSMakeRange(6, length-6), with: "******") as NSString
            }
        }
        
        if type == .CardNumber {
            
            let first = str.substring(with: NSMakeRange(0, 4))
            let forth = str.substring(with: NSMakeRange(12, 4))
            
            str = "\(first)-****-****-\(forth)" as NSString
        }
        
        return str as String
    }
    
    class func getAESDecrypt(withString originString: String) -> String {
        var result = ""
        do {
            
            let aesKeyArray: Array<UInt8> = TSTAPP2_API.sharedInstance.aesEncryptKeyArray()
            
            let encryptedArray = Data(base64Encoded: originString)?.bytes
            
            let decryptedData = try AES(key: aesKeyArray, blockMode: BlockMode.CBC(iv: aesKeyArray), padding: Padding.pkcs7).decrypt(encryptedArray!)
            result = String(bytes: decryptedData, encoding: .utf8)!
        } catch let error {
            print("aesDecrypt \(error.localizedDescription)")
        }
        return result
    }
    
    class func logout(completion: @escaping () -> Void, failure: @escaping () -> Void) {
        
        if LoginData.isLogin() {
        
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            let loginData = LoginData.sharedLoginData()
            TSTAPP2_API.sharedInstance.removeDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                
            }) { (error: Error) in
                
            }
            LoginData.sharedLoginData().deleteData()
            FBSDKLoginManager().logOut()
            FBSDKAccessToken.setCurrent(nil)
            UserDefaults.standard.removeObject(forKey: FACEBOOK_LOGIN_DATA)
            UserDefaults.standard.removeObject(forKey: API_CACHED_DATA)
            UserDefaults.standard.removeObject(forKey: CARD_NUMBER)
            UserDefaults.standard.removeObject(forKey: ACCOUNT_PASSWORD)
            UserDefaults.standard.removeObject(forKey: PAUSE_TIME)
            UserDefaults.standard.removeObject(forKey: MESSAGE_COUNT)
            UserDefaults.standard.set(true, forKey: KEEP_LOGIN)
            UserDefaults.standard.removeObject(forKey: SERVICE_ORDER)
            UserDefaults.standard.set(true, forKey: SHOW_MYACCONUT_TOP_MESSAGE)
            
            let pauseTime = 1
            UserDefaults.standard.set(pauseTime, forKey: PAUSE_TIME)
//            UserDefaults.standard.set(SERVICE_NEW_ICON_DIC, forKey: SERVICE_NEW_ICON)
            
            UserDefaults.standard.synchronize()
            
            URLCache.shared.removeAllCachedResponses()
            URLCache.shared.diskCapacity = 0
            URLCache.shared.memoryCapacity = 0
            
            if let cookies = HTTPCookieStorage.shared.cookies {
                for cookie in cookies {
                    HTTPCookieStorage.shared.deleteCookie(cookie)
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
            
            completion()
        }
        else {
            LoginData.sharedLoginData().deleteData()
            FBSDKLoginManager().logOut()
            FBSDKAccessToken.setCurrent(nil)
            UserDefaults.standard.removeObject(forKey: FACEBOOK_LOGIN_DATA)
            UserDefaults.standard.removeObject(forKey: API_CACHED_DATA)
            UserDefaults.standard.removeObject(forKey: CARD_NUMBER)
            UserDefaults.standard.removeObject(forKey: ACCOUNT_PASSWORD)
            UserDefaults.standard.removeObject(forKey: PAUSE_TIME)
            UserDefaults.standard.removeObject(forKey: MESSAGE_COUNT)
            UserDefaults.standard.set(true, forKey: KEEP_LOGIN)
            UserDefaults.standard.removeObject(forKey: SERVICE_ORDER)
            UserDefaults.standard.set(true, forKey: SHOW_MYACCONUT_TOP_MESSAGE)
            
            let pauseTime = 1
            UserDefaults.standard.set(pauseTime, forKey: PAUSE_TIME)
            
            UserDefaults.standard.synchronize()
            
            URLCache.shared.removeAllCachedResponses()
            URLCache.shared.diskCapacity = 0
            URLCache.shared.memoryCapacity = 0
            
            if let cookies = HTTPCookieStorage.shared.cookies {
                for cookie in cookies {
                    HTTPCookieStorage.shared.deleteCookie(cookie)
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
            completion()
        }
    }
    
    class func logoutFacebook() {
        UserDefaults.standard.removeObject(forKey: FACEBOOK_LOGIN_DATA)
//        UserDefaults.standard.removeObject(forKey: SERVICE_ORDER)
//        UserDefaults.standard.removeObject(forKey: API_CACHED_DATA)
//        UserDefaults.standard.removeObject(forKey: CARD_NUMBER)
//        UserDefaults.standard.removeObject(forKey: ACCOUNT_PASSWORD)
//        UserDefaults.standard.removeObject(forKey: PAUSE_TIME)
//        UserDefaults.standard.removeObject(forKey: MESSAGE_COUNT)
        FBSDKLoginManager().logOut()
        FBSDKAccessToken.setCurrent(nil)
    }
    
    class func is4GNetwork(withCompanyId companyId: String) -> Bool {
        return (companyId == "025" || companyId == "026")
    }
    
    //門號是否停用
    class func isSuspended() -> Bool {
        
        let bool :Bool = LoginData.sharedLoginData().custProfile_userState == "0" ? false : true
        return bool
    }
    
    class func isFunctionShutdown() -> (status: Bool, title: String, message: String) {
        var status = false
        var title = ""
        var message = ""
        if let data = UserDefaults.standard.object(forKey: FUNCTION_STATUS) as? Dictionary<String, Any> {
            UserDefaults.standard.set(data, forKey: FUNCTION_STATUS)
            UserDefaults.standard.synchronize()
            if let shutdown = data["SYS_SHUTDOWN"] as? Dictionary<String, Any> {
                status = shutdown.stringValue(ofKey: "activeStatus") == "Y" ? true : false
                title = shutdown.stringValue(ofKey: "title")
                message = shutdown.stringValue(ofKey: "message")
            }
        }
        return (status, title, message)
    }
    
    class func isFunctionMaintain() -> (status: Bool, title: String, message: String) {
        var status = false
        var title = ""
        var message = ""
        if let data = UserDefaults.standard.object(forKey: FUNCTION_STATUS) as? Dictionary<String, Any> {
            UserDefaults.standard.set(data, forKey: FUNCTION_STATUS)
            UserDefaults.standard.synchronize()
            if let maintain = data["SYS_MAINTAIN"] as? Dictionary<String, Any> {
                status = maintain.stringValue(ofKey: "activeStatus") == "Y" ? true : false
                title = maintain.stringValue(ofKey: "title")
                message = maintain.stringValue(ofKey: "message")
            }
        }
        return (status, title, message)

    }
    
    class func isMEGAMaintain() -> (status: Bool, title: String, message: String) {
        var status = false
        var title = ""
        var message = ""
        if let data = UserDefaults.standard.object(forKey: FUNCTION_STATUS) as? Dictionary<String, Any> {
            UserDefaults.standard.set(data, forKey: FUNCTION_STATUS)
            UserDefaults.standard.synchronize()
            if let maintain = data["MEGA_MAINTAIN"] as? Dictionary<String, Any> {
                status = maintain.stringValue(ofKey: "activeStatus") == "Y" ? true : false
                title = maintain.stringValue(ofKey: "title")
                message = maintain.stringValue(ofKey: "message")
            }
        }
        return (status, title, message)
        
    }
    
    class func isCTCBMaintain() -> (status: Bool, title: String, message: String) {
        var status = false
        var title = ""
        var message = ""
        if let data = UserDefaults.standard.object(forKey: FUNCTION_STATUS) as? Dictionary<String, Any> {
            UserDefaults.standard.set(data, forKey: FUNCTION_STATUS)
            UserDefaults.standard.synchronize()
            if let maintain = data["CTCB_MAINTAIN"] as? Dictionary<String, Any> {
                status = maintain.stringValue(ofKey: "activeStatus") == "Y" ? true : false
                title = maintain.stringValue(ofKey: "title")
                message = maintain.stringValue(ofKey: "message")
            }
        }
        return (status, title, message)
        
    }
    
    class func versionStatus() -> VersionStatus {
        return VersionStatus(rawValue: UserDefaults.standard.integer(forKey: VERSION_STATUS))!
    }
    
    class func showHtmlMessage(withTitle title: String, message: String, viewController: UIViewController, completion: @escaping () -> Void) {
        
        viewController.view.endEditing(true)
        let htmlInfoController = TstarHtmlInfoViewController(withTitle: title, htmlString: message)
        htmlInfoController.view.alpha = 0
        viewController.addChildViewController(htmlInfoController)
        viewController.view.addSubview(htmlInfoController.view)
        htmlInfoController.view.translatesAutoresizingMaskIntoConstraints = false
        viewController.view.addConstraints([
            NSLayoutConstraint(item: htmlInfoController.view, attribute: .top, relatedBy: .equal, toItem: viewController.topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: htmlInfoController.view, attribute: .bottom, relatedBy: .equal, toItem: viewController.bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: htmlInfoController.view, attribute: .leading, relatedBy: .equal, toItem: viewController.view, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: htmlInfoController.view, attribute: .trailing, relatedBy: .equal, toItem: viewController.view, attribute: .trailing, multiplier: 1, constant: 0)
            ])
        htmlInfoController.didMove(toParentViewController: viewController)
        UIView.animate(withDuration: 0.5) {
            htmlInfoController.view.alpha = 1
            htmlInfoController.completion = completion
        }
    }
    
    class func showHtmlMessage(withTitle title: String, message: String, viewController: UIViewController) {
        TSTAPP2_Utilities.showHtmlMessage(withTitle: title, message: message, viewController: viewController, completion: {  })
    }
    
    class func isMaintain(showAlert: Bool, withViewController viewController: UIViewController?) -> Bool {
        var isMaintain = false
        
        let maintainInfo = UserDefaults.standard.object(forKey: MAINTAIN_INFO) as? [AnyObject] ?? [false as AnyObject, "" as AnyObject, "" as AnyObject]
        
        if maintainInfo[0] as! Bool && showAlert && viewController != nil {
            TSTAPP2_Utilities.showAlert(withTitle: maintainInfo[1] as! String, message: maintainInfo[2] as! String, viewController: viewController!)
            isMaintain = true
        }
        
        return isMaintain
    }
    
    class func checkIfOnlyFacebookLogin() {
        if LoginData.isFacebookLogin() && !LoginData.isLogin() {
            TSTAPP2_Utilities.logout(completion: { 
                
            }, failure: { 
                
            })
        }
    }
    
    class func isLogin(pushToLoginView: Bool, withViewController viewController: UIViewController) -> Bool {
        var isLogin  = false
        
        if LoginData.isLogin() {
            isLogin = true
        }
        else if !LoginData.isLogin() && pushToLoginView {
            
            let actionDefault = UIAlertAction(title: "登入", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
                TSTAPP2_Utilities.pushToLoginViewController(withViewController: viewController)
            })
            let actionCancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction) in
            })
            
            let alert = UIAlertController(title: "", message: "馬上登入使用完整功能", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(actionCancel)
            alert.addAction(actionDefault)
            
            alert.show(viewController: viewController)
            
        }
        
        
        return isLogin
    }
    
    class func pushToLoginViewController(withViewController viewController: UIViewController) {
        let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
        let accountLoginVC: AI_AccountLoginViewController = storyBoard.instantiateViewController(withIdentifier: "AI_AccountLoginViewController") as! AI_AccountLoginViewController
        accountLoginVC.hideBackButton = false
        
        viewController.navigationController?.pushViewController(accountLoginVC, animated: true)
    }
    
    class func isForceAccountBindingAndPushToBindingView(hideBackButton:Bool, withViewController viewController: UIViewController) -> Bool {
        
        var disableBackToRoot = false
        
        let maintainInfo = UserDefaults.standard.object(forKey: MAINTAIN_INFO) as? [AnyObject] ?? [false as AnyObject, "" as AnyObject, "" as AnyObject]
        
        if maintainInfo[0] as! Bool {
            TSTAPP2_Utilities.showAlertAndChangeToLanding(withTitle: maintainInfo[1] as! String, message: maintainInfo[2] as! String, viewController: viewController)
            disableBackToRoot = true
        }
        
        if !LoginData.isLogin() && (LoginData.isFacebookLogin() || LoginData.sharedLoginData().custProfile_extId != "") {
            let storyBoard = UIStoryboard(name: "AccountInformation", bundle: nil)
            let accountBindingVC: AI_AccountBindingViewController = storyBoard.instantiateViewController(withIdentifier: "AI_AccountBindingViewController") as! AI_AccountBindingViewController
            accountBindingVC.hideBackButton = hideBackButton
            viewController.navigationController?.pushViewController(accountBindingVC, animated: true)
            disableBackToRoot = true
        }
        return disableBackToRoot
    }
    
    class func isAppScheme(scheme: String?) -> Bool {
        var isAppScheme = false
        
        if scheme == URL_SCHEME.tstarcs.rawValue || scheme == URL_SCHEME.tstapp.rawValue || scheme == URL_SCHEME.tstappE.rawValue {
            isAppScheme = true
        }
        
        return isAppScheme
    }
    
    class func changeFunctionPage(withAction action: String, actionParam: String) {
        var urlString = "tstapp://?action=\(action)"
        
        if actionParam != "" {
            urlString = "\(urlString)&actionParam=\(actionParam)"
        }
        
        let url = URL(string: urlString)!
        
        TSTAPP2_Utilities.changeFunctionPage(url: url)
    }
    
    class func changeFunctionPage(url: URL) {
        if TSTAPP2_Utilities.isAppScheme(scheme: url.scheme) {
            if let query = url.query?.components(separatedBy: "&") {
                var paramDic: Dictionary<String, Any> = [:]
                for component in query {
                    let elements = component.components(separatedBy: "=")
                    if elements.count == 2 {
                        let key = elements[0].removingPercentEncoding
                        let value = elements[1].removingPercentEncoding
                        if key != nil && value != nil {
                            paramDic[key!] = value!
                        }
                    }
                }
                
                if let action = paramDic["action"] as? String {
                    AutomaticViewChange.sharedInstance.setTabIndex(withAction: action, actionParam: paramDic["actionParam"] as? String)
                }
            }
        }
    }
    
    class func pushToWebViewControler(withTitle title: String, urlString: String, viewController: UIViewController) {
        let webVC = UIStoryboard.init(name: "TStarWebView", bundle: nil).instantiateViewController(withIdentifier: "TstarWebViewController") as! TstarWebViewController
        webVC.title = title
        webVC.webUrl = urlString
        viewController.navigationController?.pushViewController(webVC, animated: true)
    }
    
    class func pushToWebViewControler(withTitle title: String, urlString: String, appendAppToken: Bool, viewController: UIViewController) {
        let webVC = UIStoryboard.init(name: "TStarWebView", bundle: nil).instantiateViewController(withIdentifier: "TstarWebViewController") as! TstarWebViewController
        webVC.title = title
        webVC.webUrl = urlString
        webVC.appendToken = false
        webVC.appendAppToken = appendAppToken
        viewController.navigationController?.pushViewController(webVC, animated: true)
    }
    
    class func pushToWebViewControler(withTitle title: String, urlString: String, appendToken: Bool, viewController: UIViewController) {
        let webVC = UIStoryboard.init(name: "TStarWebView", bundle: nil).instantiateViewController(withIdentifier: "TstarWebViewController") as! TstarWebViewController
        webVC.title = title
        webVC.webUrl = urlString
        webVC.appendToken = appendToken
        webVC.appendAppToken = false
        viewController.navigationController?.pushViewController(webVC, animated: true)
    }
    
    class func pushToWebViewControler(withTitle title: String, urlString: String, appendToken: Bool, openBrowser: Bool, viewController: UIViewController) {
        let webVC = UIStoryboard.init(name: "TStarWebView", bundle: nil).instantiateViewController(withIdentifier: "TstarWebViewController") as! TstarWebViewController
        webVC.title = title
        webVC.webUrl = urlString
        webVC.appendToken = appendToken
        webVC.openBrowser = openBrowser
        viewController.navigationController?.pushViewController(webVC, animated: true)
    }
    
    class func registerDevice() {
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.registerDevice(withMSISDN: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, deviceId: deviceID, token: "", showProgress: false, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
//            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//            UIApplication.shared.registerForRemoteNotifications()
            
            if #available(iOS 10.0, *) {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.current().delegate = UIApplication.shared.delegate as! AppDelegate
                
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(
                    options: authOptions,
                    completionHandler: {_, _ in })
            }
            else {
                let settings: UIUserNotificationSettings =
                    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
            
            UIApplication.shared.registerForRemoteNotifications()
            
            let token = Messaging.messaging().fcmToken
            print("FCM token: \(token ?? "")")
            
        }, failure: { (error: Error) in
            
        })
    }
    
    //MARK: - enum iterator
    class func iterateEnum<T: Hashable>(_: T.Type) -> AnyIterator<T> {
        var i = 0
        return AnyIterator {
            let next = withUnsafeBytes(of: &i) { $0.load(as: T.self) }
            if next.hashValue != i { return nil }
            i += 1
            return next
        }
    }

}


// MARK: - Validators
class Validators: NSObject {
    
    private(set) var verifyRules: [Dictionary<String, Any>] = []
    
    private(set) var errors: [String] = []
    
    private(set) var data: Dictionary<String, String> = [:]
    
    private enum EnumVerifyResult: Error {
        case HasError
        case StopOnError
    }
    
    init(withRules rules: [Dictionary<String, Any>]) {
        super.init()
        self.verifyRules = rules
    }
    
    func verify() throws -> Bool {
        var result = true
        self.data = [:]
        for setting in verifyRules {
            let rules = setting["rules"] as? Array<String> ?? []
            let rawValue = setting["value"] as? String ?? ""
            let label = setting["label"] as? String ?? ""
            for rule in rules {
                do {
                    switch rule {
                    case "required":
                        try requiredValidator(withValue: rawValue, label: label)
                        break
                    case "mobile":
                        try mobileValidator(withValue: rawValue, label: label)
                        break
                    case "email":
                        try emailValidator(withValue: rawValue, label: label)
                        break
                    case "number":
                        try numberValidator(withValue: rawValue, label: label)
                        break
                    default:
                        break
                    }
                } catch {
                    result = false
                    break
                }
            }
            if result != true {
                break
            } else {
                let attrbute = setting["attribute"] as? String ?? ""
                self.data[attrbute] = rawValue
            }
        }
        guard result == true else {
            throw EnumVerifyResult.StopOnError
        }
        return result
    }
    
    private func requiredValidator(withValue value: String, label: String) throws {
        let verifyString = value.trimmingCharacters(in: .whitespacesAndNewlines)
        guard verifyString != "" else {
            self.errors.append("\(label) 不可為空白")
            throw EnumVerifyResult.HasError
        }
    }
    
    private func mobileValidator(withValue value: String, label: String) throws {
        let regex = "(^$|^09[0-9]{2}[0-9]{6}$)"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        
        guard  predicate.evaluate(with: value) else {
            self.errors.append("\(label) 格式錯誤")
            throw EnumVerifyResult.HasError
        }
    }
    
    private func numberValidator(withValue value: String, label: String) throws {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        
        guard  predicate.evaluate(with: value) else {
            self.errors.append("\(label) 格式錯誤")
            throw EnumVerifyResult.HasError
        }
    }
    
    private func emailValidator(withValue value: String, label: String) throws {
        let regex = "(^$|^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$)"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        
        guard  predicate.evaluate(with: value) else {
            self.errors.append("\(label) 格式錯誤")
            throw EnumVerifyResult.HasError
        }
    }

}


// MARK: - Captcha
class Captcha: NSObject {
    
    static let sharedInstance = Captcha()
    
    var captchaLength: Int = 4
    
    var originCharacter: [String] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    
    var captchaSize: CGSize = CGSize.zero
    
    var fontStyle: UIFont = UIFont(name: "Helvetica Bold", size: 32)!
    
    lazy var captchaImage: UIImageView = {
        [unowned self] in
        
        if self.captchaString == "" {
            self.captchaString = self.generateCaptchaString()
        }
        
        return self.generateCaptchaImage()
        }()
    
    private(set) var captchaString: String = ""
    
    private override init() {
        super.init()
    }
    
    func refresh() -> UIImageView {
        self.captchaString = self.generateCaptchaString()
        self.captchaImage = self.generateCaptchaImage()
        return self.captchaImage
    }
    
    private func generateCaptchaString() -> String {
        
        var randomString = ""
        
        (0..<self.captchaLength).forEach {_ in
            let randomIdx = Int(arc4random_uniform(UInt32(self.originCharacter.count)))
            randomString += self.originCharacter[randomIdx]
        }
        
        return randomString
    }
    
    private func generateCaptchaImage() -> UIImageView {
        
        let imageView = UIImageView()
        
        UIGraphicsBeginImageContextWithOptions(self.captchaSize, false, UIScreen.main.scale)
        var image = UIImage()
        
        // background
        UIColor(red: CGFloat(arc4random_uniform(128) + 128)/255, green: CGFloat(arc4random_uniform(128) + 128)/255, blue: CGFloat(arc4random_uniform(128) + 128)/255, alpha: 1).setFill()
        UIRectFill(CGRect(origin: CGPoint.zero, size: self.captchaSize))
        
        // draw image
        image.draw(in: CGRect(origin: CGPoint.zero, size: self.captchaSize))
        
        // draw text
        
        for index in 0..<4 {
        
            let textFontAttributes = [
                NSAttributedStringKey.font: self.fontStyle,
                NSAttributedStringKey.foregroundColor: UIColor(red: CGFloat(arc4random_uniform(128))/255, green: CGFloat(arc4random_uniform(128))/255, blue: CGFloat(arc4random_uniform(128))/255, alpha: 1)
                ] as [NSAttributedStringKey: Any]
            let rect = CGRect(origin: CGPoint(x: 15 + (20 * index), y: 5), size: self.captchaSize)
            let text: NSString = self.captchaString as NSString
            text.substring(with: NSRange(location: index, length: 1)).draw(in: rect, withAttributes: textFontAttributes)
        }
        
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        imageView.image = image
        
        return imageView
    }
}

