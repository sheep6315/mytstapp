//
//  TStarFullScreenViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2017/8/17.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class TStarFullScreenViewController: TSUIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var closeButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func didTapCloseButton(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "DID_SHOW_SERVICE_TUTORIAL")
        self.dismiss(animated: true) { 
            
        }
    }
}
