//
//  TabBarViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/13.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit
import Crashlytics

let LANDING = "首頁"
let MY_ACCOUNT = "帳戶"
let MY_BILL = "帳單"
let BONUS = "優惠"
let SETTING = "服務"

let RELOAD_LANDING_PAGE = "RELOAD_LANDING_PAGE"

let POP_BINDING_VIEWCONTROLLER = "POP_BINDING_VIEWCONTROLLER"

class TabBarViewController: UITabBarController {
    
    var currentTab = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
//        var vcArray = navigationController?.viewControllers
        
//        vcArray!.removeAll()
//        vcArray!.append(self)
        
//        navigationController?.viewControllers = vcArray!
        
        
        AutomaticViewChange.sharedInstance.tabBarVC = self
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TabBarViewController.checkVIPStatusAndChangeSkin), name: NSNotification.Name(rawValue: DID_UPDATE_LOGIN_DATA), object: nil)
        
        UISkinChange.sharedInstance.changeUIColor(theObject: self.tabBar)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        currentTab = tabBar.selectedItem!.title!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("currentTab \(currentTab)")
        print("\(item.title!)")
        
        
        if currentTab == item.title! {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: currentTab), object: nil)
        }
        else if item.title! == LANDING {
            let dic = ["reloadAPI": true]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LANDING), object: dic)
            
            Answers.logContentView(withName: "\(item.title!)",
                contentType: "TabBar",
                contentId: "Tab01",
                customAttributes: [:])
        }
        else if item.title! == SETTING {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: POP_BINDING_VIEWCONTROLLER), object: nil)
            
            Answers.logContentView(withName: "\(item.title!)",
                contentType: "TabBar",
                contentId: "Tab04",
                customAttributes: [:])
        }
        else if item.title! == MY_ACCOUNT {
            
            Answers.logContentView(withName: "\(item.title!)",
                contentType: "TabBar",
                contentId: "Tab02",
                customAttributes: [:])
        }
        else if item.title! == MY_BILL {
            
            Answers.logContentView(withName: "\(item.title!)",
                contentType: "TabBar",
                contentId: "Tab03",
                customAttributes: [:])
        }
        else if item.title! == BONUS {
            
            UbaRecord.sharedInstance.sendUba(withPage: "AP_PROMO_ZONE", action: "AA_PROMO_ZONE")
            
            Answers.logContentView(withName: "\(item.title!)",
                contentType: "TabBar",
                contentId: "Tab05",
                customAttributes: [:])
        }
        
        
        currentTab = item.title!
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: self.tabBar)
//        UISkinChange.sharedInstance.changeUIColor(theObject: self.navigationController!)
    }

}
