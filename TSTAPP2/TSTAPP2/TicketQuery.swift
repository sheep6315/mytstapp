//
//  TicketQuery.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/17.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class TicketQuery: NSObject {

    static let sharedInstance = TicketQuery()
    
    // 案件清單
    private(set) var casesList: [TicketQueryCase] = []
    
    func setDataValue(withDic dic: Dictionary<String, Any>) {
        casesList = []
        let resultCode = dic.stringValue(ofKey: "code")
        if resultCode == "00000" {
            if let rawData = dic["data"] as? [Dictionary<String, Any>] {
                rawData.forEach({ (ticket: [String : Any]) in
                    casesList.append(TicketQueryCase(withDic: ticket))
                })
            }
        }
    }
}

class TicketQueryCase: NSObject {
    
    // 案件編號
    private(set) var caseNo: String = ""
    
    // 反應日期
    private(set) var replyDate: String = ""
    
    // 案件類別
    private(set) var caseType: String = ""
    
    // 案件狀態
    private(set) var caseStatus: String = ""
    
    // 結案日期
    private(set) var closedDate: String = ""
    
    // 處理部門
    private(set) var processDepartment: String = ""
    
    // 處理單位
    private(set) var processUnit: String = ""
    
    // 處理結果
    private(set) var processResult: String = ""
    
    init(withDic dic: Dictionary<String, Any>) {
        
        self.caseNo = dic["ticketId"] as? String ?? ""
        self.caseType = dic["questionType"] as? String ?? ""
        self.processDepartment = dic["ownerUnit"] as? String ?? ""
        self.processUnit = dic["ticketOwner"] as? String ?? ""
        self.processResult = dic["ticketResult"] as? String ?? ""
        
        self.replyDate = dic["openDateText"] as? String ?? ""
        self.caseStatus = dic["tStatus"] as? String ?? ""
        self.closedDate = dic["closeDateText"] as? String ?? ""
        
    }
}
