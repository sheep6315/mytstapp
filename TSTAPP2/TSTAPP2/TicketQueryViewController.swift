//
//  TicketQueryViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/16.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit


let caseCellHeaderHeight: CGFloat = 44
let caseCellContentHeight: CGFloat = 287

class TicketQueryCaseCell: UITableViewCell {
    
    @IBOutlet weak var caseNoLabel: UILabel!
    
    @IBOutlet weak var replyDateLabel: UILabel!
    
    @IBOutlet weak var caseTypeLabel: UILabel!
    
    @IBOutlet weak var caseStatusLabel: UILabel!
    
    @IBOutlet weak var closedDateLabel: UILabel!
    
    @IBOutlet weak var processDepartmentLabel: UILabel!
    
    @IBOutlet weak var processUnitLabel: UILabel!
    
    @IBOutlet weak var processResultLabel: UILabel!
    
    var isExpand = false
}

class TicketQueryViewController: TSUIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    lazy var replyRange: [Dictionary<String, [String]>] = {
        var range: [Dictionary<String, [String]>] = []
        let calendar = NSCalendar.current
        let component = calendar.dateComponents([.year, .month], from: Date())
        
        var monthList: [String] = []
        for month: Int in 1...component.month! {
            monthList.insert(String(month), at: 0)
        }
        range.append([
            String(component.year!): monthList
            ])
        
        if component.month! < 12 {
            monthList = []
            let prevYear = component.year! - 1
            for prevMonth: Int in component.month!..<12 {
                monthList.insert(String(prevMonth+1), at: 0)
            }
            range.append([
                String(prevYear): monthList
                ])
        }
        
        return range
    }()

    lazy var keyboardToolBar: UIToolbar = {
        [unowned self] in
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.didTapDone))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: false)
        return toolBar
        }()
    
    var tickerQueryList: [TicketQueryCase] = []
    
    @IBOutlet weak var caseNoView: UIView!
    
    @IBOutlet weak var replyRangeView: UIView!
    
    @IBOutlet weak var casesListTableView: UITableView!
    
    var expandCaseCellIndex: IndexPath!
    
    // MARK: - Fields
    @IBOutlet weak var queryTypeField: UITextField!
    
    @IBOutlet weak var replyYearField: UITextField!
    
    @IBOutlet weak var replyMonthField: UITextField!
    
    @IBOutlet weak var caseNoField: UITextField!
    
    @IBOutlet weak var noticeTextView: UITextView!
    
    @IBOutlet weak var noCaseLabel: UILabel!
    
    @IBOutlet weak var noCaseIconImageView: UIImageView!
    
    
    // MARK: - ENUM
    enum EnumPickerViewTag: Int {
        case QueryType
        case ReplyYear
        case ReplyMonth
    }
    
    enum EnumQueryType: String {
        case Date = "依照時間查詢"
        case SerialNo = "依照案件編號"
        
        static let maps = [Date, SerialNo]
    }
    
    // MARK: - Constraints
    @IBOutlet weak var caseListTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var caseListTableViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var noCaseFoundViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var noCaseFoundViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchButton: UIButton!
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if !LoginData.isLogin() {
            let alertController = UIAlertController(title: "尚未登入", message: "請先登入", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (_:UIAlertAction) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
//        self.setNoticeText()
        self.setInputFields()
        
        self.casesListTableView.rowHeight = UITableViewAutomaticDimension
        self.casesListTableView.estimatedRowHeight = caseCellHeaderHeight + caseCellContentHeight
        self.caseListTableViewHeightConstraint.constant = 0
        self.caseListTableViewTopConstraint.constant = 0
        self.noCaseLabel.isHidden = true
        self.noCaseIconImageView.isHidden = true
        self.noCaseFoundViewHeightConstraint.constant = 0
        self.noCaseFoundViewTopConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        self.view.setNeedsLayout()
        
        if let queryTypePickerView = self.queryTypeField.inputView as? UIPickerView {
            queryTypePickerView.delegate?.pickerView!(queryTypePickerView, didSelectRow: 0, inComponent: 0)
        }
        
        self.casesListTableView.rowHeight = UITableViewAutomaticDimension
        self.casesListTableView.estimatedRowHeight = caseCellHeaderHeight
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0508", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "案件查詢", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "案件查詢")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0508", action: "")
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: searchButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    private func setInputFields() {
        var pickerView = UIPickerView()
        
        let queryTypeFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        queryTypeFieldRightIconImage.contentMode = .scaleAspectFit
        queryTypeFieldRightIconImage.frame.size = CGSize(width: queryTypeFieldRightIconImage.frame.size.width + 16, height: queryTypeFieldRightIconImage.frame.size.height)
        pickerView.tag = EnumPickerViewTag.QueryType.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.queryTypeField.rightView = queryTypeFieldRightIconImage
        self.queryTypeField.rightViewMode = .always
        self.queryTypeField.inputView = pickerView
        
        let replyYearFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        replyYearFieldRightIconImage.contentMode = .scaleAspectFit
        replyYearFieldRightIconImage.frame.size = CGSize(width: replyYearFieldRightIconImage.frame.size.width + 16, height: replyYearFieldRightIconImage.frame.size.height)
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerViewTag.ReplyYear.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.replyYearField.rightView = replyYearFieldRightIconImage
        self.replyYearField.rightViewMode = .always
        self.replyYearField.inputView = pickerView
        
        let replyMonthFieldRightIconImage: UIImageView = UIImageView(image: UIImage(named: "icon_triangle_down"))
        replyMonthFieldRightIconImage.contentMode = .scaleAspectFit
        replyMonthFieldRightIconImage.frame.size = CGSize(width: replyMonthFieldRightIconImage.frame.size.width + 16, height: replyMonthFieldRightIconImage.frame.size.height)
        pickerView = UIPickerView()
        pickerView.tag = EnumPickerViewTag.ReplyMonth.rawValue
        pickerView.delegate = self
        pickerView.dataSource = self
        self.replyMonthField.rightView = replyMonthFieldRightIconImage
        self.replyMonthField.rightViewMode = .always
        self.replyMonthField.inputView = pickerView
    }
    
    private func setNoticeText() {
        let redString: String = "2016年1月份起"
        let range = (self.noticeTextView.text as NSString).range(of: redString)
        let noticeString = NSMutableAttributedString.init(string: self.noticeTextView.text)
        noticeString.setAttributes([ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)], range: NSMakeRange(0, self.noticeTextView.text.count))
        noticeString.addAttributes([NSAttributedStringKey.foregroundColor: UIColor.red], range: range)
        self.noticeTextView.attributedText = noticeString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.inputAccessoryView = self.keyboardToolBar
    }
    
    // MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let displayPickerView = EnumPickerViewTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .QueryType:
            return EnumQueryType.maps.count
        case .ReplyYear:
            return self.replyRange.count
        case .ReplyMonth:
            if self.replyYearField.text != "" {
                if let replyYearPickerView = self.replyYearField.inputView as? UIPickerView {
                    let (_, months) = self.replyRange[replyYearPickerView.selectedRow(inComponent: 0)].first!
                    return months.count
                }
            }
            return 1
        }
    }
    
    // MARK: - UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let displayPickerView = EnumPickerViewTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .QueryType:
            return EnumQueryType.maps[row].rawValue
        case .ReplyYear:
            let (year, _) = self.replyRange[row].first!
            return String(year)
        case .ReplyMonth:
            if self.replyYearField.text != "" {
                if let replyYearPickerView = self.replyYearField.inputView as? UIPickerView {
                    let (_, months) = self.replyRange[replyYearPickerView.selectedRow(inComponent: 0)].first!
                    return months[row]
                }
            }
            return "請先選擇年度"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let displayPickerView = EnumPickerViewTag(rawValue: pickerView.tag)!
        switch displayPickerView {
        case .QueryType:
            self.queryTypeField.text = EnumQueryType.maps[row].rawValue
            switch EnumQueryType.maps[row] {
            case .Date:
                self.caseNoView.isHidden = true
                self.replyRangeView.isHidden = false
                break
            case .SerialNo:
                self.caseNoView.isHidden = false
                self.replyRangeView.isHidden = true
                break
            }
            if self.replyYearField.text == "", let replyYearPickerView = self.replyYearField.inputView as? UIPickerView {
                replyYearPickerView.selectRow(0, inComponent: 0, animated: false)
                replyYearPickerView.delegate?.pickerView!(replyYearPickerView, didSelectRow: 0, inComponent: 0)
            }
            break
        case .ReplyYear:
            let (year, _) = self.replyRange[row].first!
            if self.replyYearField.text != year {
                self.replyYearField.text = year
                self.replyMonthField.text = ""
                if let replyMonthPickerView = self.replyMonthField.inputView as? UIPickerView {
                    replyMonthPickerView.selectRow(0, inComponent: 0, animated: false)
                    replyMonthPickerView.delegate?.pickerView!(replyMonthPickerView, didSelectRow: 0, inComponent: 0)
                }
            }
            break
        case .ReplyMonth:
            if self.replyYearField.text != "" {
                if let replyYearPickerView = self.replyYearField.inputView as? UIPickerView {
                    let (_, months) = self.replyRange[replyYearPickerView.selectedRow(inComponent: 0)].first!
                    self.replyMonthField.text = months[row]
                }
            }
            break
        }
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tickerQueryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCaseCell", for: indexPath) as! TicketQueryCaseCell
        
        let data = self.tickerQueryList[indexPath.row]
        cell.caseNoLabel.text = data.caseNo
        cell.replyDateLabel.text = data.replyDate
        cell.caseTypeLabel.text = data.caseType
        cell.caseStatusLabel.text = data.caseStatus
        cell.closedDateLabel.text = data.closedDate
        cell.processDepartmentLabel.text = data.processDepartment
        cell.processUnitLabel.text = data.processUnit
        cell.processResultLabel.text = data.processResult
        cell.isExpand = false
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var cellHeight: CGFloat = caseCellHeaderHeight
        
        if let caseCell = tableView.cellForRow(at: indexPath) as? TicketQueryCaseCell {
            if caseCell.isExpand {
                cellHeight += caseCellContentHeight
                cellHeight += caseCell.processResultLabel.frame.height
            }
        }
        
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.expandCaseCellIndex != nil && self.expandCaseCellIndex != indexPath {
            if let expandCell = self.casesListTableView.cellForRow(at: self.expandCaseCellIndex) as? TicketQueryCaseCell {
                expandCell.isExpand = false
            }
        }
        
        let caseCell = tableView.cellForRow(at: indexPath) as! TicketQueryCaseCell
        var contentHeight: CGFloat = caseCellContentHeight + caseCell.processResultLabel.frame.height
//        contentHeight += caseCell.processResultLabel.frame.height
        if caseCell.isExpand {
            contentHeight = 0
            self.expandCaseCellIndex = nil
        } else {
            self.expandCaseCellIndex = indexPath
        }
        caseCell.isExpand = !caseCell.isExpand
        
        let tableHeight = CGFloat(self.tickerQueryList.count) * caseCellHeaderHeight + contentHeight

        self.caseListTableViewHeightConstraint.constant = tableHeight
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: { (isComplete: Bool) in
            
        })
    }

    // MARK: - IBAction
    @IBAction func didTapSearchButton(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP0508", action: "A0508")
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "案件查詢", action: nil, label: nil, value: nil)
        
        self.view.endEditing(true)
        
        let selectedQueryType = EnumQueryType(rawValue: self.queryTypeField.text!)!
        switch selectedQueryType {
        case .Date:
            self.callGetQueryTicketByDateAPI()
            break
        case .SerialNo:
            self.callGetQueryTicketByCaseNoAPI()
            break
        }
    }
    
    @IBAction func didTapPickQueryTypeField(_ sender: Any) {
        self.queryTypeField.becomeFirstResponder()
    }
    
    @IBAction func didTapPickReplyYearField(_ sender: Any) {
        self.replyYearField.becomeFirstResponder()
    }
    
    @IBAction func didTapPickReplyMonthField(_ sender: Any) {
        self.replyMonthField.becomeFirstResponder()
    }
    
    @objc func didTapDone() {
        self.view.endEditing(true)
    }
    
    // MARK: - API
    func callGetQueryTicketByDateAPI() {
        let userData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.getQueryTicketByDate(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, year: self.replyYearField.text!, month: self.replyMonthField.text!, showProgress: true, completion:  { (dic:Dictionary<String, Any>, URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                self.displayCasesList(withDic: dic)
            }
            
        }) { (Error) in
            print("error: \(Error)")
        }
    }
    
    func callGetQueryTicketByCaseNoAPI() {
        
        let caseNo = self.caseNoField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if caseNo == "" {
            let noticeAlertController = UIAlertController(title: nil, message: "請輸入案件編號", preferredStyle: .alert)
            noticeAlertController.addAction(UIAlertAction(title: "確定", style: .default, handler: nil))
            self.present(noticeAlertController, animated: true, completion: nil)
            return
        }
        
        let userData = LoginData.sharedLoginData()
        
        TSTAPP2_API.sharedInstance.getQueryTicketByCaseNo(withMSISDN: userData.custProfile_msisdn, contractId: userData.custProfile_contractId, ticketId: caseNo!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                self.displayCasesList(withDic: dic)
            }
        }) { (error: Error) in
            print("error: \(error)")
        }
    }
    
    // MARK: -
    func displayCasesList(withDic dic:Dictionary<String, Any>) {
        let ticketQuery = TicketQuery.sharedInstance
        ticketQuery.setDataValue(withDic: dic)
        self.tickerQueryList = ticketQuery.casesList
        
        if self.tickerQueryList.count == 0 {
            self.noCaseFoundViewTopConstraint.constant = 1
            self.noCaseLabel.isHidden = false
            self.noCaseIconImageView.isHidden = false
            self.noCaseFoundViewHeightConstraint.constant = 50
            self.caseListTableViewHeightConstraint.constant = 0
            self.caseListTableViewTopConstraint.constant = 0
        } else {
            self.noCaseFoundViewTopConstraint.constant = 0
            self.noCaseLabel.isHidden = true
            self.noCaseIconImageView.isHidden = true
            self.noCaseFoundViewHeightConstraint.constant = 0
            self.caseListTableViewTopConstraint.constant = 1
            self.caseListTableViewHeightConstraint.constant = CGFloat(self.tickerQueryList.count) * caseCellHeaderHeight
        }
        self.view.setNeedsUpdateConstraints()
        self.view.setNeedsLayout()
        self.casesListTableView.reloadData()
    }
}
