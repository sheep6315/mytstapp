//
//  TstarHtmlInfoViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/12/9.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class TstarHtmlInfoViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var contentWebView: UIWebView!
    
    @IBOutlet weak var button: UIButton!
    
    var alertTitle: String!
    
    var contentString: String!
    
    var completion: () -> Void = { }
    
    init(withTitle title: String, htmlString: String) {
        super.init(nibName: "TstarHtmlInfoViewController", bundle: nil)
        self.alertTitle = title
        self.contentString = htmlString
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.titleLabel.text = self.alertTitle
        self.contentWebView.loadHTMLString(self.contentString, baseURL: nil)
        contentWebView.dataDetectorTypes = UIDataDetectorTypes.link
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        checkVIPStatusAndChangeSkin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: button, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - IBAction
    @IBAction func didTapButton() {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 0
        }, completion: { (isComplete: Bool) in
            self.willMove(toParentViewController: nil)
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
            self.completion()
        })
    }
}
