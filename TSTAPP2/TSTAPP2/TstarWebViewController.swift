//
//  WebViewController.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/23.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class TstarWebViewController: TSUIViewController, UIWebViewDelegate {
    
    var webUrl: String! = ""
    
    @IBOutlet weak var progressBar: UIProgressView!
    
    @IBOutlet weak var contentWebView: UIWebView!
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    
    @IBOutlet weak var stopButton: UIBarButtonItem!
    
    @IBOutlet weak var reloadButton: UIBarButtonItem!
    
    @IBOutlet weak var progressViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var errorMessage: UILabel!
    
    var appendAppToken: Bool = true
    var appendToken: Bool = true
    var openBrowser: Bool = false
    
    var isSuccess:Bool = false
    
//    init(withTitle pageTitle: String, url: String) {
//        super.init(nibName: "TstarWebViewController", bundle: nil)
//        self.title = pageTitle
//        self.webUrl = url
//    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.edgesForExtendedLayout = [
//            UIRectEdge.bottom
//        ]
        self.automaticallyAdjustsScrollViewInsets = true
//        self.tabBarController?.tabBar.isTranslucent = true
//        self.tabBarController?.tabBar.isHidden = true
//        self.extendedLayoutIncludesOpaqueBars = true
//        self.edgesForExtendedLayout = UIRectEdge.bottom
//        self.hidesBottomBarWhenPushed = true
//        self.tabBarController?.tabBar.layer.zPosition = -1
//        self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height + 49)
        
//        self.navigationController?.navigationBar.title = title
        
//        self.title = title
        
        let logo = UIImage(named: "icon_logo_small")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        self.navigationItem.titleView?.sizeToFit()
        
        self.progressBar.setProgress(0, animated: false)
        self.progressViewHeightConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
        self.contentWebView.delegate = self
//        let agent = self.contentWebView.stringByEvaluatingJavaScript(from: "navigator.userAgent")!
//        if !agent.contains("_tstar") {
//            UserDefaults.standard.register(defaults: ["UserAgent": "\(agent)_tstar"])
//        }
        
//        self.contentWebView.scrollView.contentInset = UIEdgeInsets(top: self.contentWebView.scrollView.contentInset.top, left: self.contentWebView.scrollView.contentInset.left, bottom: 44, right: self.contentWebView.scrollView.contentInset.right)
//        self.contentWebView.scrollView.scrollIndicatorInsets = UIEdgeInsets(top: self.contentWebView.scrollView.contentInset.top, left: self.contentWebView.scrollView.contentInset.left, bottom: 44, right: self.contentWebView.scrollView.contentInset.right)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        preprocessURL()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isTranslucent = false
        self.tabBarController?.tabBar.isHidden = false
        contentWebView.stopLoading()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func toggleToolButton() {
        self.backButton.isEnabled = self.contentWebView.canGoBack
        self.forwardButton.isEnabled = self.contentWebView.canGoForward
    }
    
    // MARK: - UIWebViewDelegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        var shouldLoad = true
        
        let url = request.url
        let scheme = url?.scheme
        let host = url!.host
        
        if TSTAPP2_Utilities.isAppScheme(scheme: scheme) {
            
            self.tabBarController?.tabBar.isTranslucent = false
            self.tabBarController?.tabBar.isHidden = false
            
            TSTAPP2_Utilities.changeFunctionPage(url: url!)
            shouldLoad = false
        }
        else if host == "shop.tstartel.com" || host == "uatshop.tstartel.com" {
            let _ = self.navigationController?.popViewController(animated: false)
            
            let urlString = url!.absoluteString
            var urlWithToken: URL
            
            if !urlString.contains("appToken") {
                if urlString.contains("?") {
                    urlWithToken = URL(string: "\(urlString)&appToken=\(LoginData.sharedLoginData().token)")!
                }
                else {
                    urlWithToken = URL(string: "\(urlString)?appToken=\(LoginData.sharedLoginData().token)")!
                }
            }
            else {
                urlWithToken = URL(string: "\(urlString)")!
            }

            UIApplication.shared.openURL(urlWithToken)
//            UIApplication.shared.openURL(url!)
//            self.tabBarController?.tabBar.isTranslucent = false
//            self.tabBarController?.tabBar.isHidden = false
            shouldLoad = false
        }
        else if url!.absoluteString.contains("rest/sso/return&nu=mCWS%7CprepaidFamily%7C0.0.0.0&token=") {
            let _ = self.navigationController?.popViewController(animated: false)
            UIApplication.shared.openURL(url!)
            shouldLoad = false
        }
        else if openBrowser {
            let _ = self.navigationController?.popViewController(animated: false)
            UIApplication.shared.openURL(url!)
            shouldLoad = false
        }
        
        print("TstarWebView \(url!.description)")
        
        return shouldLoad
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.progressBar.setProgress(0.6, animated: true)
        self.stopButton.isEnabled = true
        self.reloadButton.isEnabled = false
        self.progressViewHeightConstraint.constant = 2
        self.view.setNeedsUpdateConstraints()
        self.view.layoutIfNeeded()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.progressBar.setProgress(1, animated: true)
        self.stopButton.isEnabled = false
        self.reloadButton.isEnabled = true
        self.errorMessage.isHidden = true
        isSuccess = true
        self.progressViewHeightConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        }, completion:  { (isComplete: Bool) in
            self.progressBar.setProgress(0, animated: false)
        })
        self.toggleToolButton()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("error: ",error)
        self.progressBar.setProgress(1, animated: true)
        self.stopButton.isEnabled = false
        self.reloadButton.isEnabled = true
        self.errorMessage.isHidden = isSuccess
        self.progressViewHeightConstraint.constant = 0
        self.view.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        }, completion:  { (isComplete: Bool) in
            self.progressBar.setProgress(0, animated: false)
        })
        self.toggleToolButton()
    }
    
    // MARK: - IBAction
    @IBAction func didTapBack(_ sender: Any) {
        self.contentWebView.goBack()
        self.toggleToolButton()
    }
    
    @IBAction func didTapForward(_ sender: Any) {
        self.contentWebView.goForward()
        self.toggleToolButton()
    }
    
    @IBAction func didTapStop(_ sender: Any) {
        self.contentWebView.stopLoading()
        self.toggleToolButton()
    }
    
    @IBAction func didTapReload(_ sender: Any) {
        self.contentWebView.reload()
        self.toggleToolButton()
    }
    
    @IBAction func didTapAction(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "使用Safari開啟", style: .default, handler: { (_: UIAlertAction) in
//            if let url = URL(string: self.webUrl) {
//                UIApplication.shared.openURL(url)
//            }
            UIApplication.shared.openURL((self.contentWebView.request?.url)!)
        }))
        alertController.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
//        UIApplication.shared.keyWindow?.rootViewController?.present(actionAlertController, animated: true, completion: nil)
        
        alertController.show(viewController: self)
    }
    
//    MARK: - Preprocess URL
    func preprocessURL() {
        DispatchQueue.main.async {
            
        
            self.webUrl = self.webUrl.replacingOccurrences(of: " ", with: "")
            
            if let url = URL(string: self.webUrl) {
                let scheme = url.scheme
                let host = url.host
                if TSTAPP2_Utilities.isAppScheme(scheme: scheme) {
                    self.navigationController?.popViewController(animated: false)
                    TSTAPP2_Utilities.changeFunctionPage(url: url)
                    self.tabBarController?.tabBar.isTranslucent = false
                    self.tabBarController?.tabBar.isHidden = false
                    
                    //                print("TstarWebView \(url.description)")
                }
                    //            else if host == "shop.tstartel.com" || host == "uatshop.tstartel.com" {
                    //                UIApplication.shared.openURL(url)
                    //                let _ = self.navigationController?.popViewController(animated: false)
                    //                self.tabBarController?.tabBar.isTranslucent = false
                    //                self.tabBarController?.tabBar.isHidden = false
                    //
                    //                print("TstarWebView \(url.description)")
                    //            }
                else if let host = host {
                    if (host.contains("tstartel.com")) && LoginData.isLogin() {
                        var urlWithToken = URL(string: "")
                        if self.appendAppToken {
                            if self.webUrl.contains("?") {
                                urlWithToken = URL(string: "\(self.webUrl!)&appToken=\(LoginData.sharedLoginData().token)")!
                            }
                            else {
                                urlWithToken = URL(string: "\(self.webUrl!)?appToken=\(LoginData.sharedLoginData().token)")!
                            }
                        }
                        else if self.appendToken {
                            if self.webUrl.contains("?") {
                                urlWithToken = URL(string: "\(self.webUrl!)&token=\(LoginData.sharedLoginData().token)")!
                            }
                            else {
                                urlWithToken = URL(string: "\(self.webUrl!)?token=\(LoginData.sharedLoginData().token)")!
                            }
                        }
                        else {
                            urlWithToken = URL(string: "\(self.webUrl!)")!
                        }
                        
                        //                self.tabBarController?.tabBar.isTranslucent = true
                        //                self.tabBarController?.tabBar.isHidden = true
                        let request = URLRequest(url: urlWithToken!)
                        self.contentWebView.loadRequest(request)
                        
                        //                print("TstarWebView \(urlWithToken?.description)")
                    }
                    else {
                        let request = URLRequest(url: url)
                        self.contentWebView.loadRequest(request)
                    }
                }
                else {
                    //                self.tabBarController?.tabBar.isTranslucent = true
                    //                self.tabBarController?.tabBar.isHidden = true
                    let request = URLRequest(url: url)
                    self.contentWebView.loadRequest(request)
                    
                    //                print("TstarWebView \(url.description)")
                }
            }
        }

    }
    
}
