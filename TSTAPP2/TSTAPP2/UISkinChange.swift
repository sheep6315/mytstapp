//
//  UISkinChange.swift
//  TSTAPP2
//
//  Created by Tony on 2017/2/15.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

let goldColor: UIColor = UIColor(red: 204/255, green: 164/255, blue: 89/255, alpha: 1)
let darkGoldColor: UIColor = UIColor(red: 152/255, green: 115/255, blue: 43/255, alpha: 1)
let buttonGoldColor: UIColor = UIColor(red: 202/255, green: 164/255, blue: 89/255, alpha: 1)
let labelGoldColor: UIColor = UIColor(red: 167/255, green: 98/255, blue: 42/255, alpha: 1)

let purpleColor: UIColor = UIColor(red: 133/255, green: 39/255, blue: 107/255, alpha: 1)
let darkPurpleColor: UIColor = UIColor(red: 87/255, green: 14/255, blue: 68/255, alpha: 1)
let lightPurpleColor: UIColor = UIColor(red: 183/255, green: 78/255, blue: 115/255, alpha: 1)

enum ButtonColorMode: Int {
    case backgroundImage
    case textColor
    case button_share
    case button_photo
    case button_purple
    case button_service_1
    case button_service_2
    case button_service_3
    case button_service_4
    case button_service_5
    case button_service_6
    case button_service_7
    case button_service_8
    case button_service_9
    case button_service_10
    case button_service_11
    case button_service_12
    case button_service_13
    case button_service_14
    case button_service_15
    case button_account_Info
}

enum ImageColorMode: Int {
    case icon_head
}

enum LabelColorMode: Int {
    case purple
    case lightPurple
}


class UISkinChange: NSObject {
    
    static let sharedInstance = UISkinChange()
    
    private override init() {
        
    }
    
    func isVIP() -> Bool {
        
        var vip = false
        
//        if LoginData.sharedLoginData().custProfile_isVIP == "Y" {
//            vip = true
//        }
        
        return vip
    }
    
    func changeUIColor(theObject: AnyObject) {
        self.changeUIColor(theObject: theObject, mode: 0)
    }
    
    func changeUIColor(theObject: AnyObject, mode: Int) {
        
        let changeToGold = self.isVIP()
        
        if theObject.isKind(of: UIButton.self) {
            
            let theButton = theObject as! UIButton
            
            if changeToGold {
                switch mode {
                case ButtonColorMode.backgroundImage.rawValue:
                    theButton.setBackgroundImage(UIImage(named: "button_normal_gold"), for: UIControlState.normal)
                case ButtonColorMode.textColor.rawValue:
                    theButton.setTitleColor(buttonGoldColor, for: .normal)
                case ButtonColorMode.button_share.rawValue:
                    theButton.setImage(UIImage(named: "button_share_gold"), for: .normal)
                case ButtonColorMode.button_photo.rawValue:
                    theButton.setImage(UIImage(named: "button_photo_gold"), for: .normal)
                case ButtonColorMode.button_purple.rawValue:
                    theButton.setTitleColor(buttonGoldColor, for: .normal)
                    theButton.setBackgroundImage(UIImage(named: "button_purple_gold"), for: .normal)
                case ButtonColorMode.button_service_1.rawValue:
                    theButton.setImage(UIImage(named: "button_service_1_gold"), for: .normal)
                case ButtonColorMode.button_service_2.rawValue:
                    theButton.setImage(UIImage(named: "button_service_2_gold"), for: .normal)
                case ButtonColorMode.button_service_3.rawValue:
                    theButton.setImage(UIImage(named: "button_service_3_gold"), for: .normal)
                case ButtonColorMode.button_service_4.rawValue:
                    theButton.setImage(UIImage(named: "button_service_4_gold"), for: .normal)
                case ButtonColorMode.button_service_5.rawValue:
                    theButton.setImage(UIImage(named: "button_service_5_gold"), for: .normal)
                case ButtonColorMode.button_service_6.rawValue:
                    theButton.setImage(UIImage(named: "button_service_6_gold"), for: .normal)
                case ButtonColorMode.button_service_7.rawValue:
                    theButton.setImage(UIImage(named: "button_service_7_gold"), for: .normal)
                case ButtonColorMode.button_service_8.rawValue:
                    theButton.setImage(UIImage(named: "button_service_8_gold"), for: .normal)
                case ButtonColorMode.button_service_9.rawValue:
                    theButton.setImage(UIImage(named: "button_service_9_gold"), for: .normal)
                case ButtonColorMode.button_service_10.rawValue:
                    theButton.setImage(UIImage(named: "button_service_10_gold"), for: .normal)
                case ButtonColorMode.button_service_11.rawValue:
                    theButton.setImage(UIImage(named: "button_service_11_gold"), for: .normal)
                case ButtonColorMode.button_service_12.rawValue:
                    theButton.setImage(UIImage(named: "button_service_12_gold"), for: .normal)
                case ButtonColorMode.button_service_13.rawValue:
                    theButton.setImage(UIImage(named: "button_service_13_gold"), for: .normal)
                case ButtonColorMode.button_service_14.rawValue:
                    theButton.setImage(UIImage(named: "button_service_14_gold"), for: .normal)
                case ButtonColorMode.button_service_15.rawValue:
                    theButton.setImage(UIImage(named: "button_service_15_gold"), for: .normal)
                default:
                    theButton.setBackgroundImage(UIImage(named: "button_normal_gold"), for: UIControlState.normal)
                }
            }
            else {
                switch mode {
                case ButtonColorMode.backgroundImage.rawValue:
                    theButton.setBackgroundImage(UIImage(named: "button_normal"), for: UIControlState.normal)
                case ButtonColorMode.textColor.rawValue:
                    theButton.setTitleColor(purpleColor, for: .normal)
                case ButtonColorMode.button_share.rawValue:
                    theButton.setImage(UIImage(named: "button_share"), for: .normal)
                case ButtonColorMode.button_photo.rawValue:
                    theButton.setImage(UIImage(named: "button_photo"), for: .normal)
                case ButtonColorMode.button_purple.rawValue:
                    theButton.setTitleColor(lightPurpleColor, for: .normal)
                    theButton.setBackgroundImage(UIImage(named: "button_purple"), for: .normal)
                case ButtonColorMode.button_service_1.rawValue:
                    theButton.setImage(UIImage(named: "button_service_1"), for: .normal)
                case ButtonColorMode.button_service_2.rawValue:
                    theButton.setImage(UIImage(named: "button_service_2"), for: .normal)
                case ButtonColorMode.button_service_3.rawValue:
                    theButton.setImage(UIImage(named: "button_service_3"), for: .normal)
                case ButtonColorMode.button_service_4.rawValue:
                    theButton.setImage(UIImage(named: "button_service_4"), for: .normal)
                case ButtonColorMode.button_service_5.rawValue:
                    theButton.setImage(UIImage(named: "button_service_5"), for: .normal)
                case ButtonColorMode.button_service_6.rawValue:
                    theButton.setImage(UIImage(named: "button_service_6"), for: .normal)
                case ButtonColorMode.button_service_7.rawValue:
                    theButton.setImage(UIImage(named: "button_service_7"), for: .normal)
                case ButtonColorMode.button_service_8.rawValue:
                    theButton.setImage(UIImage(named: "button_service_8"), for: .normal)
                case ButtonColorMode.button_service_9.rawValue:
                    theButton.setImage(UIImage(named: "button_service_9"), for: .normal)
                case ButtonColorMode.button_service_10.rawValue:
                    theButton.setImage(UIImage(named: "button_service_10"), for: .normal)
                case ButtonColorMode.button_service_11.rawValue:
                    theButton.setImage(UIImage(named: "button_service_11"), for: .normal)
                case ButtonColorMode.button_service_12.rawValue:
                    theButton.setImage(UIImage(named: "button_service_12"), for: .normal)
                case ButtonColorMode.button_service_13.rawValue:
                    theButton.setImage(UIImage(named: "button_service_13"), for: .normal)
                case ButtonColorMode.button_service_14.rawValue:
                    theButton.setImage(UIImage(named: "button_service_14"), for: .normal)
                case ButtonColorMode.button_service_15.rawValue:
                    theButton.setImage(UIImage(named: "button_service_15"), for: .normal)
                default:
                    theButton.setBackgroundImage(UIImage(named: "button_normal"), for: UIControlState.normal)
                }
            }
        }
        else if theObject.isKind(of: UILabel.self) {
            if changeToGold {
                (theObject as! UILabel).textColor = labelGoldColor
            }
            else {
                (theObject as! UILabel).textColor = lightPurpleColor
            }
        }
        else if theObject.isKind(of: UIImageView.self) {
            if changeToGold {
                switch mode {
                case ImageColorMode.icon_head.rawValue:
                    (theObject as! UIImageView).image = UIImage(named: "icon_head_gold")
                default:
                    break
                }
            }
            else {
                switch mode {
                case ImageColorMode.icon_head.rawValue:
                (theObject as! UIImageView).image = UIImage(named: "icon_head")
                default:
                    break
                }
            }
        }
        else if theObject.isKind(of: UISwitch.self) {
            if changeToGold {
                (theObject as! UISwitch).onTintColor = goldColor
            }
            else {
                (theObject as! UISwitch).onTintColor = purpleColor
            }
        }
        else if theObject.isKind(of: UITabBar.self) {
            
            if changeToGold {
                (theObject as! UITabBar).tintColor = darkGoldColor
                (theObject as! UITabBar).barTintColor = goldColor
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: darkGoldColor], for: .selected)
                (theObject as! UITabBar).items?[0].selectedImage = UIImage(named: "tabbar_home_selected_gold")
                (theObject as! UITabBar).items?[1].selectedImage = UIImage(named: "tabbar_my_account_selected_gold")
                (theObject as! UITabBar).items?[2].selectedImage = UIImage(named: "tabbar_my_bill_selected_gold")
                (theObject as! UITabBar).items?[3].selectedImage = UIImage(named: "tabbar_gift_selected_gold")
                (theObject as! UITabBar).items?[4].selectedImage = UIImage(named: "tabbar_services_selected_gold")
            }
            else {
                let tabBarColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
                let tabBarTintColor = UIColor(red: 135/255, green: 40/255, blue: 110/255, alpha: 1)
                (theObject as! UITabBar).tintColor = tabBarTintColor
                (theObject as! UITabBar).barTintColor = tabBarColor //tabBar背景底色
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)], for: UIControlState.normal) //選擇之前的文字顏色
                UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: tabBarTintColor], for: .selected) //選擇後的文字顏色

                (theObject as! UITabBar).items?[0].selectedImage = UIImage(named: "tabbar_home_selected")
                (theObject as! UITabBar).items?[1].selectedImage = UIImage(named: "tabbar_my_account_selected")
                (theObject as! UITabBar).items?[2].selectedImage = UIImage(named: "tabbar_my_bill_selected")
                (theObject as! UITabBar).items?[3].selectedImage = UIImage(named: "tabbar_gift_selected")
                (theObject as! UITabBar).items?[4].selectedImage = UIImage(named: "tabbar_services_selected")
            }
        }
        else if theObject.isKind(of: UISegmentedControl.self) {
            if changeToGold {
                (theObject as! UISegmentedControl).tintColor = buttonGoldColor
            }
            else {
                (theObject as! UISegmentedControl).tintColor = purpleColor
            }
        }
        else if theObject.isKind(of: UINavigationController.self) {
            if changeToGold {
                (theObject as! UINavigationController).navigationBar.barTintColor = goldColor
            }
            else {
                (theObject as! UINavigationController).navigationBar.barTintColor = purpleColor
            }
        }
        else if theObject.isKind(of: UIBarButtonItem.self) {
            if changeToGold {
                switch mode {
                case ButtonColorMode.button_account_Info.rawValue:
                    (theObject as! UIBarButtonItem).image = UIImage(named: "button_account_gold")
                default:
                    break
                }
            }
            else {
                switch mode {
                case ButtonColorMode.button_account_Info.rawValue:
                    (theObject as! UIBarButtonItem).image = UIImage(named: "button_account")
                default:
                    break
                }
            }
        }
    }
    
//    func change(imageName: String) -> String {
//        var newImageName = ""
//        if imageName.contains("_gold") {
//            newImageName = imageName.replacingOccurrences(of: "_gold", with: "")
//        }
//        else {
//            newImageName = imageName.appending("_gold")
//        }
//        
//        return newImageName
//    }
}
