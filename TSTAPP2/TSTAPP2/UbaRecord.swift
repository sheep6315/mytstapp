//
//  UbaRecord.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/11.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class UbaRecord: NSObject {
    
    
    static let sharedInstance = UbaRecord()
    
    var loginData = LoginData.sharedLoginData()
    
    let osType : String = "1"
    let device : String = TSTAPP2_Utilities.hardwareString()
    let channel : String = "3"
    
    var user : String = ""
    var companyId : String = ""
    var iden : String = ""
    var token : String = ""
    

    private override init () {
        
    }
    
    func sendUba(withPage page: String, action: String) {
        
        if LoginData.isLogin() {
            let loginData = LoginData.sharedLoginData()
            user = loginData.custProfile_msisdn
            companyId = loginData.custProfile_companyId
            iden = loginData.roleType
            token = loginData.token
        }
        
        var idfv = ""
        if UIDevice.current.identifierForVendor != nil {
            idfv = UIDevice.current.identifierForVendor!.uuidString
        }
        
        let dic = ["osType":osType,
                   "user":user,
                   "companyId":companyId,
                   "iden":iden,
                   "deviceId":idfv,
                   "token":token,
                   "device":device,
                   "pageCode":page,
                   "actionCode":action]
        
        let ubaURL = SERVER_HOST + "/TAG/rest/tsp/insUbaAction"
        print("TSTAPP2_API sendUba\n\(dic.description)")
        
        TSTAPP2_API.sharedInstance.startTask(withURLString: ubaURL, data: TSTAPP2_API.sharedInstance.getEncryptedData(ofDictionary: dic), showProgress: false, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            }, failure: { (error:Error) in
                
        })
    }
    
    func sendUba(withPage page: String, action: String, codeId: String, codeVersion: String, codeDisplayType: String, codeSort: String) {
        
        if LoginData.isLogin() {
            let loginData = LoginData.sharedLoginData()
            user = loginData.custProfile_msisdn
            companyId = loginData.custProfile_companyId
            iden = loginData.roleType
            token = loginData.token
        }
        
        var idfv = ""
        if UIDevice.current.identifierForVendor != nil {
            idfv = UIDevice.current.identifierForVendor!.uuidString
        }
        
        let dic = ["osType":osType,
                   "user":user,
                   "companyId":companyId,
                   "iden":iden,
                   "deviceId":idfv,
                   "token":token,
                   "device":device,
                   "pageCode":page,
                   "actionCode":action,
                   "codeId": codeId,
                   "codeVersion": codeVersion,
                   "codeDisplayType": codeDisplayType,
                   "codeSort": codeSort]
        
        let ubaURL = SERVER_HOST + "/TAG/rest/tsp/insUbaAction"
        print("TSTAPP2_API sendUba\n\(dic.description)")
        
        TSTAPP2_API.sharedInstance.startTask(withURLString: ubaURL, data: TSTAPP2_API.sharedInstance.getEncryptedData(ofDictionary: dic), showProgress: false, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
        }, failure: { (error:Error) in
            
        })
    }
    
    func sendUbaPageStart(withPage page: String, action: String) {
        
        if LoginData.isLogin() {
            let loginData = LoginData.sharedLoginData()
            user = loginData.custProfile_msisdn
            companyId = loginData.custProfile_companyId
            iden = loginData.roleType
            token = loginData.token
        }
        
        var idfv = ""
        if UIDevice.current.identifierForVendor != nil {
            idfv = UIDevice.current.identifierForVendor!.uuidString
        }
        
        let dic = ["osType":osType,
                   "user":user,
                   "companyId":companyId,
                   "iden":iden,
                   "deviceId": idfv,
                   "token":token,
                   "device":device,
                   "pageCode":page,
                   "actionCode":action]
        
        let ubaURL = SERVER_HOST + "/TAG/rest/tsp/insUbaPageStart"
        print("TSTAPP2_API sendUbaPageStart\n\(dic.description)")
        
        TSTAPP2_API.sharedInstance.startTask(withURLString: ubaURL, data: TSTAPP2_API.sharedInstance.getEncryptedData(ofDictionary: dic), showProgress: false, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
        }, failure: { (error:Error) in
            
        })
    }

    func sendUbaPageEnd(withPage page: String, action: String) {
        
        if LoginData.isLogin() {
            let loginData = LoginData.sharedLoginData()
            user = loginData.custProfile_msisdn
            companyId = loginData.custProfile_companyId
            iden = loginData.roleType
            token = loginData.token
        }
        else {
            let extInfoDic = UserDefaults.standard.dictionary(forKey: FACEBOOK_LOGIN_DATA)
            
            if extInfoDic != nil {   
                let id = extInfoDic!.stringValue(ofKey: "id")
                user = id
            }
            else {
                user = "guest"
            }
        }
        
        var idfv = ""
        if UIDevice.current.identifierForVendor != nil {
            idfv = UIDevice.current.identifierForVendor!.uuidString
        }
        
        let dic = ["osType":osType,
                   "user":user,
                   "companyId":companyId,
                   "iden":iden,
                   "deviceId": idfv,
                   "token":token,
                   "device":device,
                   "pageCode":page,
                   "actionCode":action]
        
        let ubaURL = SERVER_HOST + "/TAG/rest/tsp/insUbaPageEnd"
        print("TSTAPP2_API sendUbaPageEnd\n\(dic.description)")
        
        TSTAPP2_API.sharedInstance.startTask(withURLString: ubaURL, data: TSTAPP2_API.sharedInstance.getEncryptedData(ofDictionary: dic), showProgress: false, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
        }, failure: { (error:Error) in
            
        })
    }
}
