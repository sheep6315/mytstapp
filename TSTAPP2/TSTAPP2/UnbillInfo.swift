//
//  UnbillInfo.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/9.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class UnbillInfo: NSObject {
    
    static let sharedInstance = UnbillInfo()
    
    private(set) var onNetVoice : Int = 0 //網內語音
    private(set) var offNetVoice : Int = 0 //網外語音
    private(set) var pstn : Int = 0 //市話
    private(set) var onNetSMS : Int = 0 //網內簡訊
    private(set) var offNetSMS : Int = 0 //網外簡訊
    private(set) var dataUsageMB : Int = 0 //數據使用量(MB)
    private(set) var dataUsageGB : Double = 0 //數據使用量(GB)
    private(set) var onNetVoiceFree : String = "" //網內語音是否吃到飽(Y/N)
    private(set) var onNetVoiceFreeMin : Int = 0 //網內語音贈送分鐘數
    private(set) var offNetVoiceFreeMin : Int = 0 //網外語音贈送分鐘數
    private(set) var pstnFreeMin : Int = 0 //市話贈送分鐘數
    private(set) var onNetSMSFreeItem : Int = 0 //網內簡訊贈送則數
    private(set) var offNetSMSFreeItem : Int = 0 //網外簡訊贈送則數
    private(set) var dataFree : String = "" //數據吃到飽(Y/N)
    private(set) var dataFreeMB : Int = 0 //數據贈送MB量
    private(set) var uppUsedDataMB : Int = 0 //UPP已使用數據量數
    private(set) var uppTotalDataMB : Int = 0 //UPP全部數據量
    private(set) var onNetVoiceDesc : String = "" //網內語音說明
    private(set) var offNetVoiceDesc : String = "" //網外語音說明
    private(set) var pstnDesc : String = "" //市話說明
    private(set) var onNetSMSDesc : String = "" //網內簡訊說明
    private(set) var offNetSMSDesc : String = "" //網外簡訊說明明
    private(set) var dataUsage : String = "" //數據使用量/剩餘數據量
    private(set) var dataText : String = "" //數據文字
    private(set) var dataDesc : String = "" //數據說明
    private(set) var unlimitedData : String = "" //是否吃到飽(Y/N)
    private(set) var showBuyButton : Bool = false //顯示加價購按鈕(Y:顯示/N:不顯示)
    private(set) var warningWording : String = "" //注意事項
    
    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if (dic["code"] as! String) == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                onNetVoice = apiData["onNetVoice"] as? Int ?? 0
                offNetVoice = apiData["offNetVoice"] as? Int ?? 0
                pstn = apiData["offNetVoice"] as? Int ?? 0
                onNetSMS = apiData["onNetSMS"] as? Int ?? 0
                offNetSMS = apiData["offNetSMS"] as? Int ?? 0
                dataUsageMB = apiData["dataUsageMB"] as? Int ?? 0
                dataUsageGB = apiData["dataUsageMB"] as? Double ?? 0
                onNetVoiceFree = apiData.stringValue(ofKey: "onNetVoiceFree")
                onNetVoiceFreeMin = apiData["onNetVoiceFreeMin"] as? Int ?? 0
                offNetVoiceFreeMin = apiData["offNetVoiceFreeMin"] as? Int ?? 0
                pstnFreeMin = apiData["pstnFreeMin"] as? Int ?? 0
                onNetSMSFreeItem = apiData["onNetSMSFreeItem"] as? Int ?? 0
                offNetSMSFreeItem = apiData["offNetSMSFreeItem"] as? Int ?? 0
                dataFree = apiData.stringValue(ofKey: "dataFree")
                dataFreeMB = apiData["dataFreeMB"] as? Int ?? 0
                uppUsedDataMB = apiData["uppUsedDataMB"] as? Int ?? 0
                uppTotalDataMB = apiData["uppTotalDataMB"] as? Int ?? 0
                onNetVoiceDesc  = apiData.stringValue(ofKey: "onNetVoiceDesc")
                offNetVoiceDesc = apiData.stringValue(ofKey: "offNetVoiceDesc")
                pstnDesc = apiData.stringValue(ofKey: "pstnDesc")
                onNetSMSDesc = apiData.stringValue(ofKey: "onNetSMSDesc")
                offNetSMSDesc = apiData.stringValue(ofKey: "offNetSMSDesc")
                dataUsage = apiData.stringValue(ofKey: "dataUsage")
                dataText = apiData.stringValue(ofKey: "dataText")
                dataDesc = apiData.stringValue(ofKey: "dataDesc")
                unlimitedData = apiData.stringValue(ofKey: "unlimitedData")
                showBuyButton = apiData.stringValue(ofKey: "showBuyButton") == "Y" ? true : false
                warningWording = apiData.stringValue(ofKey: "warningWording")
            }
        }
    }
}
