//
//  UpdatePasswordViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/14.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: TSUIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rePasswordTextField: UITextField!
    
    @IBOutlet weak var captchaField: UITextField!
    
    @IBOutlet weak var captchaImageView: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var sendButton: UIButton!
    
    var hideBackButton: Bool = false
    var msisdn = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let captchaImg = Captcha.sharedInstance
        captchaImg.captchaSize = CGSize(width: 120, height: 44)
        self.captchaImageView.image = captchaImg.captchaImage.image
        let tapCaptchaGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.didTapRefreshCaptcha))
        self.captchaImageView.addGestureRecognizer(tapCaptchaGestureRecognizer)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard(_:)))
        self.scrollView.addGestureRecognizer(tap)
        
        oldPasswordTextField.delegate = self
        passwordTextField.delegate = self
        rePasswordTextField.delegate = self
        captchaField.delegate = self
        
        TSTAPP2_Utilities.addKeyboardShowHideObserver(self)
        
        if hideBackButton {
            self.navigationItem.setHidesBackButton(true, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP020102", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "會員中心", action: "修改密碼", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "修改密碼")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        TSTAPP2_Utilities.removeKeyboardShowHideObserver(self)
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP020102", action: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {
        UISkinChange.sharedInstance.changeUIColor(theObject: sendButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    @objc func dismissKeyBoard(_ recognizer:UITapGestureRecognizer) -> Void {
        
        self.scrollView.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBShow(scrollView, notification: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        TSTAPP2_Utilities.setScrollViewInsetWhenKBHide(scrollView, hasTabBar: true, notification: notification)
    }
    
    //MARK: - textFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
//    MARK: - IBAction
    @IBAction func didTapSendButton(_ sender: Any) {
    
        self.view.endEditing(true)
        if self.captchaField.text == "" {
            TSTAPP2_Utilities.showAlert(withMessage: "請輸入驗證碼", viewController: self)
            return
        } else if oldPasswordTextField.text != "" && passwordTextField.text != "" && rePasswordTextField.text != "" {
            
            if self.captchaField.text != Captcha.sharedInstance.captchaString {
                TSTAPP2_Utilities.showAlert(withMessage: "驗證碼輸入錯誤", viewController: self)
                return
            }
            
            let regex = try! NSRegularExpression(pattern: ".*[^0-9a-zA-Z].*", options: .caseInsensitive)
            
            let pwText = passwordTextField.text!
            var validPW = true
            
            let range: NSRange = NSMakeRange(0, pwText.utf16.count)
            let matchArray: Array = regex.matches(in: pwText, options: NSRegularExpression.MatchingOptions.anchored, range: range)
            if matchArray.count > 0 {
                validPW = false
            }
            
            if passwordTextField.text!.count < 6 || passwordTextField.text!.count > 12 || !validPW {
                TSTAPP2_Utilities.showAlert(withMessage: "密碼格式錯誤", viewController: self)
            }
            else if passwordTextField.text != rePasswordTextField.text {
                TSTAPP2_Utilities.showAlert(withMessage: "新密碼輸入不一致", viewController: self)
            }
            else if passwordTextField.text == rePasswordTextField.text {
                
                UbaRecord.sharedInstance.sendUba(withPage: "APP020102", action: "A020102")
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "修改密碼(會員中心)", action: nil, label: nil, value: nil)
                
                var currentMsisdn = ""
                
                if msisdn == "" {
                    currentMsisdn = LoginData.sharedLoginData().custProfile_msisdn
                }
                else {
                    currentMsisdn = msisdn
                }
                
                TSTAPP2_API.sharedInstance.setPassword(withMSISDN: currentMsisdn, oldPassword: oldPasswordTextField.text!, newPassword: passwordTextField.text!, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
                    if dic.stringValue(ofKey: "code") == "00000" {
//                        UserDefaults.standard.set(self.passwordTextField.text!, forKey: ACCOUNT_PASSWORD)
//                        UserDefaults.standard.synchronize()
//                        TSTAPP2_Utilities.showAlertAndLogout(withMessage: "密碼已修改完成，下次請使用新密碼登入。", viewController: self)
                        TSTAPP2_Utilities.showAlertAndLogoutThenShowLoginView(withMessage: "密碼已修改完成，下次請使用新密碼登入。", viewController: self)
                    }
                    else {
                        TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
                    }
                }, failure: { (error: Error) in
                    
                })
            }
            
        }
        else {
            TSTAPP2_Utilities.showAlert(withMessage: "密碼不能為空白", viewController: self)
        }
        
    }
    
    @objc func didTapRefreshCaptcha() {
        let captchaImg = Captcha.sharedInstance
        self.captchaImageView.image = captchaImg.refresh().image
    }
    

}
