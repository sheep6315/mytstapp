//
//  UrlMap.swift
//  TSTAPP2
//
//  Created by Tony on 2017/9/20.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class UrlMap: NSObject {
    
    static let sharedInstance = UrlMap()
    
    private(set) var RT: String = ""
    
    private(set) var PROMO_ZONE: String = ""
    
    private(set) var ORDER_QUERY: String = ""
    
    private(set) var BL: String = ""
    
    private(set) var SERVICE_COVERAGE: String = ""
    
    private(set) var SPEED_TEST: String = ""
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if dic.stringValue(ofKey: "code") == "00000" {
            
            let apiData = dic["data"] as! Dictionary<String, Any>
            
            RT = apiData.stringValue(ofKey: "RT")
            
            PROMO_ZONE = apiData.stringValue(ofKey: "PROMO_ZONE")
            
            ORDER_QUERY = apiData.stringValue(ofKey: "ORDER_QUERY")
            
            BL = apiData.stringValue(ofKey: "BL")
            
            SERVICE_COVERAGE = apiData.stringValue(ofKey: "SERVICE_COVERAGE")
            
            SPEED_TEST = apiData.stringValue(ofKey: "SPEED_TEST")
            
        }
        
    }

}
