//
//  UserQuota.swift
//  TSTAPP2
//
//  Created by apple on 2017/2/15.
//  Copyright © 2017年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class UserQuota: NSObject {
    
    static let sharedInstance = UserQuota()
    
    /**
        Y:User can't use service
        N:User can use service
        N/A:Unknow
     */
    private(set) var lock: String = ""
    
    ///可用額度
    private(set) var quota: String = ""
    
    ///目前餘額
    private(set) var remaining: String = ""
    
    /**
     若用戶可使用DCB (lock值為N)，則值為 N/A；反之若無法使用DCB服務 (lock值為Y)
     */
    private(set) var lockReason: String = ""
    
    ///信用額度
    private(set) var creditQuota: String = ""
    
    ///可調整額度清單
    private(set) var availableQuotaList: [Int] = []
    
    ///最近一次重置額度，若無記錄則為0。
    private(set) var resetQuota: String = ""
    
    ///最近一次額度重置日期，若無記錄則為null，格式：yyyyMMdd
    private(set) var resetDate: String = ""
    
    ///本帳週是否已應客戶要求，暫時重置額度(Y:已重置過/N:未重置)
    private(set) var resetQuotaByRequest: String = ""

    private override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                lock = apiData.stringValue(ofKey: "lock")
                quota = apiData.stringValue(ofKey: "quota")
                remaining = apiData.stringValue(ofKey: "remaining")
                lockReason = apiData.stringValue(ofKey: "lockReason")
                creditQuota = apiData.stringValue(ofKey: "creditQuota")
                resetQuota = apiData.stringValue(ofKey: "resetQuota")
                resetDate = apiData.stringValue(ofKey: "resetDate")
                resetQuotaByRequest = apiData.stringValue(ofKey: "resetQuotaByRequest")
                availableQuotaList = apiData["availableQuotaList"] as? [Int] ?? []
            }
        }
    }
}
