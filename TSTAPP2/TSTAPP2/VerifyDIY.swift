//
//  VerifyDIY.swift
//  TSTAPP2
//
//  Created by apple on 2016/11/28.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import Foundation

class VerifyDIY: NSObject {
    
    static let sharedInstance = VerifyDIY()
    
    private(set) var statusCode : String = "" //官網API回傳代碼
    private(set) var statusDesc : String = "" //官網API回傳說明
    private(set) var isDIY : Bool = false //是否為DIY資費 Y:DIY資費 N:非DIY資費
    private(set) var promoCode : String = "" //專案代碼
    private(set) var packageNameNew : String = "" //專案名稱
    private(set) var monthlyFee : String = "" //目前月租費
    private(set) var trafficCapacity : String = "" //網外通話基本用量
    private(set) var netCapacity : String = "" //上網量
    private(set) var externalTrafficFee : String = "" //網外/市話通話費率
    private(set) var netType : String = "" //網路速度
    private(set) var internalTrafficFee : String = "" //網內通話費率
    private(set) var buttonText: String = "" // 按鈕文字
    private(set) var buttonLinkUrl: String = "" // 按鈕連結
    private(set) var showChangeButton: String = "" //顯示資費異動按鈕(Y:顯示, N:不顯示)
    
    override init () {
        
    }
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        
        if dic.stringValue(ofKey: "code") == "00000" {
            
            if let apiData = dic["data"] as? Dictionary<String, Any> {
                
                statusCode = apiData.stringValue(ofKey: "statusCode")
                statusDesc = apiData.stringValue(ofKey: "statusDesc")
                isDIY = (apiData.stringValue(ofKey: "isDIY") == "Y")
                promoCode = apiData.stringValue(ofKey: "promoCode")
                packageNameNew = apiData.stringValue(ofKey: "packageNameNew")
                monthlyFee = apiData.stringValue(ofKey: "monthlyFee")
                trafficCapacity = apiData.stringValue(ofKey: "trafficCapacity")
                netCapacity = apiData.stringValue(ofKey: "netCapacity")
                externalTrafficFee = apiData.stringValue(ofKey: "externalTrafficFee")
                netType = apiData.stringValue(ofKey: "netType")
                internalTrafficFee = apiData.stringValue(ofKey: "internalTrafficFee")
                buttonText = apiData.stringValue(ofKey: "buttonText")
                buttonLinkUrl = apiData.stringValue(ofKey: "buttonLinkUrl")
                showChangeButton = apiData.stringValue(ofKey: "showChangeButton")
            }
        }
    }
}
