//
//  ViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/10/4.
//  Copyright © 2016年 Tony Hsu. All rights reserved.
//

import UIKit
import FBSDKCoreKit

class ViewController: TSUIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationController?.setNavigationBarHidden(true, animated: false)
        
//        if !UserDefaults.standard.bool(forKey: KEEP_LOGIN) {
//            TSTAPP2_Utilities.logout(completion: { 
//                UserDefaults.standard.set(true, forKey: KEEP_LOGIN)
//                UserDefaults.standard.synchronize()
//                
//                self.determineWhichSegueToPerform()
//            }, failure: { 
//                self.determineWhichSegueToPerform()
//            })
//        }
//        else {
//            if LoginData.sharedLoginData().custProfile_pwschange != "1" {
//                TSTAPP2_Utilities.logout(completion: {
//                    self.determineWhichSegueToPerform()
//                }, failure: {
//                    
//                })
//            }
//            else {
//                determineWhichSegueToPerform()
//            }
//        }
        
        self.performSegue(withIdentifier: "toTabBar", sender: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func determineWhichSegueToPerform() {
        if FBSDKAccessToken.current() == nil {
            UserDefaults.standard.removeObject(forKey: FACEBOOK_LOGIN_DATA)
        }
        
        if LoginData.isFacebookLogin() || LoginData.isLogin() {
            self.performSegue(withIdentifier: "toTabBar", sender: nil)
        }
        else {
            self.performSegue(withIdentifier: "toFacebookLogin", sender: nil)
        }
    }

}

