//
//  VolteTableViewController.swift
//  TSTAPP2
//
//  Created by apple on 2016/12/1.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

enum VoLteOperation :String{
    
    case VoLteOperationApply = "Apply", //申請VoLTE
    VoLteOperationCancel = "Cancel", //退租VoLTE
    VoLteOperationSendOta = "SendOTA", //重新傳送OTA
    VoLteOperationPreChk = "PreChk", //申請VoLTE預查
    VoLteOperationQryState = "QryState" //查詢VoLTE及OTA狀態
}

class VolteTableViewController: UITableViewController {
    
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var msisdnLabel: UILabel!
    
    @IBOutlet weak var projectLabel: UILabel!
    
    @IBOutlet weak var projectImage: UIImageView!
    
    @IBOutlet weak var phoneModelLabel: UILabel!
    
    @IBOutlet weak var phoneModelImage: UIImageView!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBOutlet weak var versionImage: UIImageView!

    @IBOutlet weak var applyStatus: UILabel!
    
    @IBOutlet weak var checkBox: UIButton!
    
    @IBOutlet weak var unqualifiedLabel: UILabel!
    
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var noticeLabelTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var phoneListButton: UIButton!
    
    @IBOutlet weak var applyVolteButton: UIButton!
    
    @IBOutlet weak var cancelVolteButton: UIButton!
    
    @IBOutlet weak var knowVolteButton: UIButton!
    
    @IBOutlet weak var setVolteButton: UIButton!
    
    let type4G :String = "025"
    
    //voLte申請回應狀態
    let stateUnApply: String = "033"//"0" //未申請
    let stateApplying: String = "025"//"1" //已申請開通中
    let stateAppliedNotEffective: String = "027"//"2" //已申請未生效
    let stateAppliedEffective: String = "026"//"8" //已申請且生效
    
    //voLte按鈕 url 連結
    let KnowMoreUrl = "http://www.tstartel.com/static/discount/20150731_volte/index.htm#tab01" //了解VoLTE服務,了解更多
    let SetVoLteUrl = "http://www.tstartel.com/static/discount/20150731_volte/index.htm#tab02" //如何設定VoLTE
    let VoLtePhoneListUrl = "http://www.tstartel.com/static/discount/20150731_volte/index.htm#tab02" //VoLTE相容手機列表
    
    var stateCode :String = ""//記錄volte狀態
    var filterName :String = ""
    var checkPhoneModelQualified :Bool = false //手機型號資格
    var checkProjectQualified :Bool = false //資費專案資格
    var checkSoftwareVersionQualified :Bool = false //軟體版號資格
    var isGetContractAPIDone :Bool = false {
        
        didSet{
            apiResponded()
        }
    }
    var isGetFilterListAPIDone :Bool = false {
        
        didSet{
            apiResponded()
        }
    }
    var isDoVoLteAPIDone :Bool = false {
        
        didSet{
            apiResponded()
        }
    }
    var isQualified :Bool = false //是否符合資格
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        msisdnLabel.text = LoginData.sharedLoginData().custProfile_msisdn
        
        unqualifiedLabel.isHidden = true
        applyVolteButton.isHidden = true
        cancelVolteButton.isHidden = true
        phoneListButton.isHidden = true
        footerView.isHidden = true
        
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        checkQualified()
        callGetContractAPI()
        callGetVolteDeviceAPI()
        callDoVolteProcess(operation: .VoLteOperationQryState, showProgress: false)
        
        //volte邏輯 -> callAPI -> 進volteProcess設定顯示內容 -> 所有API Done -> checkLoadAPI reloadTableView
        //有申裝 顯示 第1 section , 沒申裝 顯示 第2 Section , 無資格都不顯示
        
//        tableView.contentInset = UIEdgeInsets(top: 64, left: tableView.contentInset.left, bottom: 49, right: tableView.contentInset.right)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageStart(withPage: "APP0506", action: "")
//        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "常用服務", action: "VOLTE好聲音", label: nil, value: nil)
        TSTAPP2_Utilities.sendGAScreen(withScreenName: "VOLTE好聲音")
        
        checkVIPStatusAndChangeSkin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UbaRecord.sharedInstance.sendUbaPageEnd(withPage: "APP0506", action: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func checkVIPStatusAndChangeSkin() {

        UISkinChange.sharedInstance.changeUIColor(theObject: applyVolteButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: cancelVolteButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: phoneListButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: setVolteButton, mode: ButtonColorMode.backgroundImage.rawValue)
        UISkinChange.sharedInstance.changeUIColor(theObject: knowVolteButton, mode: ButtonColorMode.backgroundImage.rawValue)
    }
    
    //MARK: - Custom Function
    func checkQualified() {
        
        let version = Double("\(ProcessInfo.processInfo.operatingSystemVersion.majorVersion).\(ProcessInfo.processInfo.operatingSystemVersion.minorVersion)")!
        
        versionLabel.text = "iOS \(version)"
    
        checkSoftwareVersionQualified = version >= 9.2 ? true:false
        let image = version >= 9.2 ? "icon_v" : "icon_x"
        versionImage.image = UIImage(named: image)
        
        let companyID = LoginData.sharedLoginData().custProfile_companyId
        
        checkProjectQualified = companyID == type4G ? true : false
        let prjectImage = companyID == type4G ? "icon_v" : "icon_x"
        projectImage.image = UIImage(named: prjectImage)
    }
    
    func apiResponded() {
        if isDoVoLteAPIDone && isGetFilterListAPIDone && isGetContractAPIDone {
            
            if checkSoftwareVersionQualified && checkPhoneModelQualified && checkProjectQualified {
                
                isQualified = true
                
                if stateCode == stateAppliedEffective {
                    noticeLabelTopConstraint.constant = 0
                    self.view.needsUpdateConstraints()
                    self.view.layoutIfNeeded()
                }
            }
            else {
                
                if stateCode == stateAppliedEffective {
                    isQualified = true
                    noticeLabelTopConstraint.constant = 0
                    self.view.needsUpdateConstraints()
                    self.view.layoutIfNeeded()
                }
                else {
                    applyVolteButton.isHidden = true
                    cancelVolteButton.isHidden = true
                    unqualifiedLabel.isHidden = false
                    isQualified = false
                }
            }
            
            self.tableView.reloadData()
            
            TSTAPP2_Utilities.hidePKHUD()
            self.footerView.isHidden = false
        }
    }
    
    func volteProcess(operation:VoLteOperation, dic:Dictionary<String,Any>) {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        var action = UIAlertAction()
        
        switch operation {
        case VoLteOperation.VoLteOperationApply:
            
            action = UIAlertAction(title: "確認", style: .cancel, handler: { (UIAlertAction) in
                
                let _ = self.navigationController?.popToRootViewController(animated: true)
            })
            
            alert.message = "已為您送出，稍後將以簡訊通知申裝結果"
            alert.addAction(action)
            alert.show(viewController: self)
            break
        case VoLteOperation.VoLteOperationCancel:
            
            alert.message = "退租成功！\n待收到簡訊通知後停止本服務。"
            action = UIAlertAction(title: "確認", style: .cancel, handler: { (UIAlertAction) in
                
                let _ = self.navigationController?.popToRootViewController(animated: true)
            })
            alert.addAction(action)
            alert.show(viewController: self)
            break
        case VoLteOperation.VoLteOperationPreChk:
            break
        case VoLteOperation.VoLteOperationSendOta:
            
            action = UIAlertAction(title: "確認", style: .cancel, handler: { (UIAlertAction) in
                
                let _ = self.navigationController?.popToRootViewController(animated: true)
            })
            alert.message = "已送出執行申裝作業，稍後將以簡訊通知申裝結果"
            alert.addAction(action)
            alert.show(viewController: self)
            break
        case VoLteOperation.VoLteOperationQryState:
            
            if dic.stringValue(ofKey: "StatusMsg") != "" {
                
                let statusMsg = dic.stringValue(ofKey: "StatusMsg")
                let stateArray = statusMsg.components(separatedBy: ".")
                
                if stateArray.count <= 1 {
                    alert.message = dic.stringValue(ofKey: "StatusMsg")
                    action = UIAlertAction(title: "確定", style: .cancel, handler: { (UIAlertAction) in
                        
                        let _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                }
                else{
                    stateCode = stateArray.first!
                    
                    if stateCode == stateUnApply || stateCode == stateAppliedNotEffective {
                        
                        applyVolteButton.isHidden = false
                        cancelVolteButton.isHidden = true
                    }
                    else if stateCode == stateApplying {
                        
                        applyStatus.text = "申請中"
                        applyVolteButton.isHidden = true
                        cancelVolteButton.isHidden = false
                    }
                    else if stateCode == stateAppliedEffective {
                        
                        applyStatus.text = "已申裝"
                        applyVolteButton.isHidden = true
                        cancelVolteButton.isHidden = false
                        
                    }
                    else {

                        applyStatus.text = "系統錯誤，請重新進入"
                    }
                
                }
                break
            }
            
            stateCode = dic["errCode"] as! String
            
            if stateCode == stateUnApply {
                
                applyVolteButton.isHidden = false
                cancelVolteButton.isHidden = true
            }
            else if stateCode == stateAppliedNotEffective {
                
                applyStatus.text = "服務異動申請中"
                applyVolteButton.isHidden = true
                cancelVolteButton.isHidden = true
            }
            else if stateCode == stateApplying {
                
                applyStatus.text = "申請中"
                applyVolteButton.isHidden = true
                cancelVolteButton.isHidden = false
            }
            else if stateCode == stateAppliedEffective {
                
                applyStatus.text = "已申裝"
                applyVolteButton.isHidden = true
                cancelVolteButton.isHidden = false
                
            }
            else {
                
                applyStatus.text = "系統錯誤，請重新進入"
            }
            break
            
//            alert.addAction(action)
//            alert.show(viewController: self)
        }
    }
    
    //MARK: - 按鈕
    @IBAction func didTapContract(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toContract", sender: nil)
    }
    
    @IBAction func didTapCheckBox(_ sender: Any) {
        
        let isOn = checkBox.isSelected
        
        if isOn {
            checkBox.isSelected = false
        }
        else {
            checkBox.isSelected = true
        }
    }
    
    @IBAction func didTapApplyVolte(_ sender: Any) {
        
        if checkBox.isSelected {
            let alert  = UIAlertController(title: "服務申裝確認", message: "確定要申裝VOLTE好聲音服務？\n按下確認後將收取月租費$30。\n(申裝步驟完成前，手機請勿關機)", preferredStyle: .alert)
            let submit = UIAlertAction(title: "確認", style: .default, handler: { (UIAlertAction) in
                
                UbaRecord.sharedInstance.sendUba(withPage: "APP0506", action: "A0506")
                TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "VOLTE好聲音", action:"申請", label: nil, value: nil)
                
                if self.stateCode == self.stateUnApply {
                    
                    self.callDoVolteProcess(operation: .VoLteOperationApply)
                }
                else if self.stateCode == self.stateAppliedNotEffective {
                    
                    self.callDoVolteProcess(operation: .VoLteOperationSendOta)
                }
                self.applyVolteButton.isUserInteractionEnabled = false
            })
            let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            
            alert.addActions(array: [submit,cancel])
            alert.show(viewController: self)
        }
        else {
            
            TSTAPP2_Utilities.showAlert(withMessage: "請勾選本人已詳閱並同意線上契約內容", viewController: self)
        }
    }
    
    @IBAction func didTapCancelVolte(_ sender: Any) {
        
        UbaRecord.sharedInstance.sendUba(withPage: "APP0506", action: "A0507")
        
        TSTAPP2_Utilities.createGAIBuilderWithCategory(withCategory: "VOLTE好聲音", action:"取消", label: nil, value: nil)
        
        let alert  = UIAlertController(title: "服務退租確認", message: "確定要退租VOLTE好聲音服務？", preferredStyle: .alert)
        let submit = UIAlertAction(title: "確認", style: .default, handler: { (UIAlertAction) in
            
            if self.stateCode == self.stateAppliedEffective {
                
                self.callDoVolteProcess(operation: .VoLteOperationCancel)
            }
            self.cancelVolteButton.isUserInteractionEnabled = false
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        alert.addActions(array: [submit,cancel])
        alert.show(viewController: self)
    }

    @IBAction func didTapKnowVolte(_ sender: Any) {
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "了解更多", urlString: KnowMoreUrl, viewController: self)
    }
    
    @IBAction func didTapPhoneList(_ sender: Any) {
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "手機支援", urlString: VoLtePhoneListUrl, viewController: self)
    }
    
    @IBAction func didTapSetVolte(_ sender: Any) {
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "如何設定", urlString: SetVoLteUrl, viewController: self)
    }
    
    //MARK: - API
    func callGetContractAPI() {
        
        TSTAPP2_Utilities.showPKHUD()
        
        self.projectLabel.text = LoginData.sharedLoginData().custProfile_projectName
        self.isGetContractAPIDone = true
        
    }
    
    func callGetVolteDeviceAPI() {
        
        TSTAPP2_Utilities.showPKHUD()
        
        TSTAPP2_API.sharedInstance.getVolteDevice(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, showProgress: false, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
            
            if dic.stringValue(ofKey: "code") == "00000" {
                
                if let apiData = dic["data"] as? Array<Any> {
                    if let data = apiData.first as? Dictionary<String, Any> {
                        self.checkPhoneModelQualified = data.stringValue(ofKey: "text") == "" ? true : false
                        self.filterName = data.stringValue(ofKey: "name")
                        self.unqualifiedLabel.text = data.stringValue(ofKey: "text")
                    }
                }
                
            } else{
                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            
            let image = self.checkPhoneModelQualified ? "icon_v" : "icon_x"
            self.phoneModelImage.image = UIImage(named: image)
            self.phoneModelLabel.text = self.filterName == "" ? "此型號不支援" : self.filterName
            
            self.isGetFilterListAPIDone = true
        }) { (Error) in
            
            self.isGetFilterListAPIDone = true
        }
    }
    
    func callDoVolteProcess(operation: VoLteOperation, showProgress: Bool) {
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.doVolteProcess(withMsisdn: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, op: operation.rawValue, showProgress: showProgress, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                self.volteProcess(operation: operation, dic: dic["data"] as! Dictionary)
            }
            else {
                TSTAPP2_Utilities.showAlertAndPop(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
            }
            self.isDoVoLteAPIDone = true
        }) { (error: Error) in
            self.isDoVoLteAPIDone = true
        }
    }
    
    func callDoVolteProcess(operation: VoLteOperation) {
        callDoVolteProcess(operation: operation, showProgress: true)
        
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if isQualified {
            
            if cancelVolteButton.isHidden && section == 1 {
                
                //沒申裝
                return 0.1
            }
            else if applyVolteButton.isHidden && section == 2 {
                
                //有申裝
                return 0.1
            }
        }
        else {
            
            if section == 1 || section == 2 {
                
                return 0.1
            }
        }
        
        return super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if isQualified {
            
            if cancelVolteButton.isHidden && section == 1 {
                
                //沒申裝
                return 0.1
            }
            else if applyVolteButton.isHidden && section == 2 {
                
                //有申裝
                return 0.1
            }
        }
        else {
            
            if section == 1 || section == 2 {
                
                return 0.1
            }
        }
        
        return super.tableView(tableView, heightForHeaderInSection: section)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //有申裝 顯示 第1 section , 沒申裝 顯示 第2 Section , 無資格都不顯示
        //027狀態：顯示section1
        if isQualified {
            
            if stateCode != stateAppliedNotEffective && cancelVolteButton.isHidden && section == 1 {
                
                //沒申裝
                return 0
            }
            else if applyVolteButton.isHidden && section == 2 {
                
                //有申裝
                return 0
            }
        }
        else {
            
            if section == 1 || section == 2 {
                
                return 0
            }
        }
        
        return super.tableView(tableView, numberOfRowsInSection: section)
    }
}
