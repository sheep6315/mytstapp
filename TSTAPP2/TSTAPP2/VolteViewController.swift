//
//  VolteViewController.swift
//  TSTAPP2
//
//  Created by Tony on 2016/11/1.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit



class VolteViewController: TSUIViewController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var projectNameQualified: UILabel!
    @IBOutlet weak var phoneVersion: UILabel!
    @IBOutlet weak var phoneVersionQualified: UILabel!
    @IBOutlet weak var softwareVersion: UILabel!
    @IBOutlet weak var softwareVersionQualified: UILabel!
    @IBOutlet weak var applySymbol: UILabel!
    @IBOutlet weak var applyNotice: UILabel!
    
    //QualifiedView
    @IBOutlet weak var qualifiedView: UIView!
    @IBOutlet weak var applyVolteServiceButton: UIButton!
    @IBOutlet weak var cancelVolteServiceButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    //UnqualifiedView
    @IBOutlet weak var unQualifiedView: UIView!
    
    let type4G :String = "025"
    
    //voLte申請回應狀態
    let stateUnApply :String = "033"//"0" //未申請
    let stateApplying :String = "025"//"1" //已申請開通中
    let stateAppliedNotEffective :String = "027"//"2" //已申請未生效
    let stateAppliedEffective :String = "026"//"8" //已申請且生效
    
    //voLte按鈕 url 連結
    let KnowMoreUrl = "http://www.tstartel.com/static/discount/20150731_volte/index.htm#tab01" //了解VoLTE服務,了解更多
    let SetVoLteUrl = "http://www.tstartel.com/static/discount/20150731_volte/index.htm#tab02" //如何設定VoLTE
    let VoLtePhoneListUrl = "http://www.tstartel.com/static/discount/20150731_volte/index.htm#tab02" //VoLTE相容手機列表
    
    var stateCode :String = ""//記錄volte狀態
    var filterName :String = ""
    var checkPhoneModelQualified :Bool = false //手機型號資格
    var checkProjectNameQualified :Bool = false //資費專案資格
    var checkSoftwareVersionQualified :Bool = false //軟體版號資格
    var isGetContractAPIDone :Bool = false
    var isGetFilterListAPIDone :Bool = false
    var isDoVoLteAPIDone :Bool = false
    
    //MARK: - ViewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()

        checkDeviceVer()
        setUserInformation()
        callAPI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func checkDeviceVer() {
        
        let version = Double("\(ProcessInfo.processInfo.operatingSystemVersion.majorVersion).\(ProcessInfo.processInfo.operatingSystemVersion.minorVersion)")!

        softwareVersionQualified.text = version >= 9.2 ? "":"(資格不符)"
        checkSoftwareVersionQualified = version >= 9.2 ? true : false
        softwareVersion.text = "iOS \(version)"
    }
    
    func checkLoadAPI() {
        if isDoVoLteAPIDone && isGetFilterListAPIDone && isGetContractAPIDone {
            
            if checkSoftwareVersionQualified && checkPhoneModelQualified && checkProjectNameQualified {
                
                showQualifiedView()
            }
            else {
                
                showUnqualifiedView()
            }
            
            applyNotice.isHidden = false
            applySymbol.isHidden = false
        }
    }
    
    func setUserInformation() {
        
        userName.text = LoginData.sharedLoginData().custProfile_custName
        
        let companyID = LoginData.sharedLoginData().custProfile_companyId
        projectNameQualified.text = companyID == type4G ? "" : "(資格不符)"
        checkProjectNameQualified = companyID == type4G ? true : false
    }
    
    func showQualifiedView() {
        qualifiedView.isHidden = false
        unQualifiedView.isHidden = true
    }
    
    func showUnqualifiedView() {
        qualifiedView.isHidden = true
        unQualifiedView.isHidden = false
        
        let color = UIColor.red
        applySymbol.textColor = color
        
        if stateCode == stateAppliedNotEffective || stateCode == stateAppliedEffective {
            
            applyNotice.text = "很抱歉，您的手機不支援VoLTE功能，無法使用VOLTE好聲音服務"
        }
        else {
            applyNotice.text = "很抱歉，您的資格不符，無法啟用VOLTE好聲音服務"
        }
        
        applyNotice.textColor = color
    }
    
    //MARK: - API
    
    func callAPI() {
        
//        TSTAPP2_API.sharedInstance.getContract(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, showProgress: true, completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
//            
//            if let contractRes = dic["GetContractRes"] as? Dictionary<String, Any> {
//            
//                if contractRes.stringValue(ofKey: "ResultCode") == "00000" {
//                    
//                    if contractRes.stringValue(ofKey: "Status") == "0" {
//                        
//                        self.projectName.text = contractRes.stringValue(ofKey: "ProjectRate")
//                    }
//                    else{
//                        TSTAPP2_Utilities.showAlert(withMessage: contractRes.stringValue(ofKey: "StatusMsg"), viewController: self)
//                    }
//                }
//                else{
//                    TSTAPP2_Utilities.showAlert(withMessage: contractRes.stringValue(ofKey: "ResultText"), viewController: self)
//                }
//            }
//            self.isGetContractAPIDone = true
//            self.checkLoadAPI()
//            }) { (Error) in
//                
//                self.isGetContractAPIDone = true
//                self.checkLoadAPI()
//        }
        
//        TSTAPP2_API.sharedInstance.getFilterList(showProgress: true,completion: { (dic:Dictionary<String, Any>, response:URLResponse) in
//            
//            if dic.stringValue(ofKey: "status") == "00000"{
//                
//                self.checkPhoneModelQualified = dic.stringValue(ofKey: "filterResult") == "1" ? true : false
//                self.phoneVersionQualified.text = self.checkPhoneModelQualified ? "":"(資格不符)"
//            }
//            else{
//                TSTAPP2_Utilities.showAlert(withMessage: dic.stringValue(ofKey: "message"), viewController: self)
//            }
//            
//            self.filterName = dic.stringValue(ofKey: "filterName")
//            self.phoneVersion.text = self.filterName == "" ? "此型號不支援" : self.filterName
//            
//            self.isGetFilterListAPIDone = true
//            self.checkLoadAPI()
//            }) { (Error) in
//                
//                self.isGetFilterListAPIDone = true
//                self.checkLoadAPI()
//        }
        
        callDoVolteProcess(operation: .VoLteOperationQryState)
      
    }
    
    func callDoVolteProcess(operation: VoLteOperation) {
        let loginData = LoginData.sharedLoginData()
        TSTAPP2_API.sharedInstance.doVolteProcess(withMsisdn: loginData.custProfile_msisdn, contractId: loginData.custProfile_contractId, op: operation.rawValue, showProgress: true, completion: { (dic: Dictionary<String, Any>, response: URLResponse) in
            if dic.stringValue(ofKey: "code") == "00000" {
                self.volteProcess(operation: operation, dic: dic["data"] as! Dictionary)
            }
        }) { (error: Error) in
            
        }
    }
    
    func callDoVolteAPI(operation:VoLteOperation) {
        
//        TSTAPP2_API.sharedInstance.doVoLTE(withMSISDN: LoginData.sharedLoginData().custProfile_msisdn, contractId: LoginData.sharedLoginData().custProfile_contractId, operation: operation, showProgress: true,completion: { (dic:Dictionary<String, Any>, respose:URLResponse) in
//            
//            if let doVoLTERes = dic["DoVoLTERes"] as? Dictionary<String,Any> {
//            
//                if doVoLTERes.stringValue(ofKey: "ResultCode") == "00000" && doVoLTERes.stringValue(ofKey: "Status") == "0"{
//                    
//                    self.volteProcess(operation: operation, dic: doVoLTERes)
//                }
//                else {
//                    TSTAPP2_Utilities.showAlert(withMessage: doVoLTERes.stringValue(ofKey: "StatusMsg"), viewController: self)
//                }
//            }
//            
//            self.isDoVoLteAPIDone = true
//            self.checkLoadAPI()
//                
//            }) { (Error) in
//                
//                self.isDoVoLteAPIDone = true
//                self.checkLoadAPI()
//        }
        
    }
    
    func volteProcess(operation:VoLteOperation, dic:Dictionary<String,Any>) {
    
        let alert = UIAlertController(title: "訊息", message: "", preferredStyle: .alert)
        var action = UIAlertAction()
        
        switch operation {
        case VoLteOperation.VoLteOperationApply:
            
            action = UIAlertAction(title: "確認", style: .cancel, handler: nil)
            alert.message = "已為您送出，稍後將以簡訊通知申裝結果"
            
            break
        case VoLteOperation.VoLteOperationCancel:
            
            alert.message = "已停用"
            action = UIAlertAction(title: "確認", style: .cancel, handler: nil)
            
            break
        case VoLteOperation.VoLteOperationPreChk:
            break
        case VoLteOperation.VoLteOperationSendOta:
            
            action = UIAlertAction(title: "確認", style: .cancel, handler: { (UIAlertAction) in
                
                let _ = self.navigationController?.popToRootViewController(animated: true)
            })
            alert.message = "已送出執行申裝作業，稍後將以簡訊通知申裝結果"
            
            break
        case VoLteOperation.VoLteOperationQryState:
            
            if dic.stringValue(ofKey: "StatusMsg") != "" {
                
                let statusMsg = dic.stringValue(ofKey: "StatusMsg") 
                let stateArray = statusMsg.components(separatedBy: ".")
                
                if stateArray.count <= 1 {
                    alert.message = dic.stringValue(ofKey: "StatusMsg")
                    action = UIAlertAction(title: "確定", style: .cancel, handler: { (UIAlertAction) in
                        
                        let _ = self.navigationController?.popToRootViewController(animated: true)
                    })
                }
                else{
                    stateCode = stateArray.first!
                    
                    //let stateMsg = stateArray[1]
                    var useColor = UIColor()
                    
                    if stateCode == stateUnApply {
                        
                        useColor = UIColor(red: CGFloat(34.0/255.0), green: CGFloat(171.0/255.0), blue: CGFloat(55.0/255.0), alpha: 1)
                        applyNotice.text = "啟用資格符合，請於閱讀注意事項後，按下「我要啟用」按鈕"
                    }
                    else if stateCode == stateApplying {
                        // hiddenApplyVoLteServiceButton
                        useColor = UIColor(red: CGFloat(252.0/255.0), green: CGFloat(189.0/255.0), blue: CGFloat(0.0/255.0), alpha: 1)
                        applyNotice.text = "申請中"
                    }
                    else if stateCode == stateAppliedNotEffective {
                        
                        useColor = UIColor.red
                        applyNotice.text = "啟用未成功，請重新點擊下方「我要啟用」按鈕"
                    }
                    else if stateCode == stateAppliedEffective {
                        
                        useColor = UIColor(red: CGFloat(110.0/255.0), green: CGFloat(47.0/255.0), blue: CGFloat(158.0/255.0), alpha: 1)
                        applyNotice.text = "已啟用"
                        applyVolteServiceButton.isHidden = true
                        cancelVolteServiceButton.isHidden = false
                    }
                    else {
                        
                        useColor = UIColor.red
                        applyNotice.text = "系統錯誤，請重新進入"
                    }
                    
                    applyNotice.textColor = useColor
                    applySymbol.textColor = useColor

                }
                break
            }
            
            alert.addAction(action)
            alert.show(viewController: self)
        }
    }
    
    //MARK: - Button 事件
    
    @IBAction func didTapApplyVolteService(_ sender: AnyObject) {
        
        let alert  = UIAlertController(title: "VOLTE好聲音服務申裝確認", message: "確定要申裝VOLTE好聲音服務？(申裝步驟完成前，手機請勿關機)", preferredStyle: .alert)
        let submit = UIAlertAction(title: "確認", style: .default, handler: { (UIAlertAction) in
            
            if self.stateCode == self.stateUnApply {
                
                self.callDoVolteProcess(operation: .VoLteOperationApply)
            }
            else if self.stateCode == self.stateAppliedNotEffective {
                
                self.callDoVolteProcess(operation: .VoLteOperationSendOta)
            }
            self.applyVolteServiceButton.isUserInteractionEnabled = false
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        alert.addActions(array: [submit,cancel])
        alert.show(viewController: self)
        
    }

    
    @IBAction func didTapCancelVolteService(_ sender: AnyObject) {
        
        let alert  = UIAlertController(title: "VOLTE好聲音服務停用確認", message: "確定要停用VOLTE好聲音服務？", preferredStyle: .alert)
        let submit = UIAlertAction(title: "確認", style: .default, handler: { (UIAlertAction) in
            
            if self.stateCode == self.stateUnApply {
                
                self.callDoVolteProcess(operation: .VoLteOperationApply)
            }
            else if self.stateCode == self.stateAppliedNotEffective {
                
                self.callDoVolteProcess(operation: .VoLteOperationSendOta)
            }
            self.cancelVolteServiceButton.isUserInteractionEnabled = false
        })
        let cancel = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        alert.addActions(array: [submit,cancel])
        alert.show(viewController: self)
        
    }
    
    
    @IBAction func didTapKnowVolte(_ sender: AnyObject) {
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "了解更多", urlString: KnowMoreUrl, viewController: self)
    }

    @IBAction func didTapSetVolte(_ sender: AnyObject) {
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "如何設定", urlString: SetVoLteUrl, viewController: self)
    }
    
    @IBAction func didTapPhoneList(_ sender: AnyObject) {
        
        TSTAPP2_Utilities.pushToWebViewControler(withTitle: "手機支援", urlString: VoLtePhoneListUrl, viewController: self)
    }
    
    func setWebView() {
        
    }
    
}
