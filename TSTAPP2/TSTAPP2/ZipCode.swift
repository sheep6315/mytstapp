//
//  ZipCode.swift
//  TSTAPP2
//
//  Created by 詮通電腦－劉文景 on 2016/11/7.
//  Copyright © 2016年 Taiwan Star Telecom. All rights reserved.
//

import UIKit

class ZipCode: NSObject {
    
    static let sharedInstance = ZipCode()
    
    // 縣市清單
    private(set) var towns: [City] = []
    
    public func setDataValue(withDic dic: Dictionary<String, Any>) {
        if (dic["code"] as! String) == "00000" {
            
            let apiData = dic["data"] as! Dictionary<String, Any>
            
            let rawData = apiData["zipCodeList"] as! Array<Dictionary<String, String>>

            var countyList: Dictionary<String, [County]> = [:]
            var cityList: [String] = []
            rawData.forEach { (data: [String : String]) in
                let cityName = data["city"] ?? ""
                if cityName != "" {
                    let county = County(withDic: data)
                    
                    let zipCode = county.zipCode
                    
                    if zipCode == "290" || zipCode == "817" || zipCode == "819" {
                        return
                    }
                    
                    if countyList[cityName]?.count == nil {
                        countyList[cityName] = [county]
                    } else {
                        countyList[cityName]?.append(county)
                    }
                    if !cityList.contains(cityName) {
                        cityList.append(cityName)
                    }
                }
            }
            
            for city in cityList {
                self.towns.append(City(withDic: ["name": city, "county": countyList[city]!]))
            }
        }
    }
}

class City: NSObject {
    
    // 縣市名稱
    private(set) var name: String = ""
    
    // 鄉鎮清單
    private(set) var county: [County] = []
    
    init(withDic dic: Dictionary<String, Any>) {
        self.name = dic["name"] as? String ?? ""
        self.county = dic["county"] as? [County] ?? []
    }
    
}

class County: NSObject {
    
    // 鄉鎮名稱
    private(set) var name: String = ""
    
    // 郵遞區號
    private(set) var zipCode: String = ""
    
    init(withDic dic: Dictionary<String, String>) {
        self.name = dic["district"] ?? ""
        self.zipCode = dic["zipCode"] ?? ""
    }
}
